﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;

namespace Managers
{
    public static class Imaging
    {
        public static void SaveImage(string sourcePath, string imageName)
        {
            FileStream fs = new FileStream(sourcePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            Image imgSource = Image.FromStream(fs);

            imgSource.ExifRotate();

            int sourceW = imgSource.Width;
            int sourceH = imgSource.Height;

            List<Resulation> resulations = new List<Resulation>();

            resulations.Add(new Resulation { Name = "4K", Width = 3840, Height = 2160 });
            resulations.Add(new Resulation { Name = "3K", Width = 2880, Height = 1620 });
            resulations.Add(new Resulation { Name = "2K", Width = 2048, Height = 1080 });
            resulations.Add(new Resulation { Name = "FullHD", Width = 1920, Height = 1080 });
            resulations.Add(new Resulation { Name = "HD", Width = 1280, Height = 720 });
            resulations.Add(new Resulation { Name = "Thumb", Width = 200, Height = 200 });

            int i = 0;

            if (sourceW <= resulations[resulations.Count - 1].Width && sourceH <= resulations[resulations.Count - 1].Height)
            {
                saveImageToServer(imgSource, Globals.Folders.Images, imageName + "_" + resulations[resulations.Count - 1].Name);
            }
            else if(sourceW <= resulations[resulations.Count - 1].Width || sourceH <= resulations[resulations.Count - 1].Height)
            {
                resizeImg(imgSource, resulations[resulations.Count - 1].Width, resulations[resulations.Count - 1].Height, Globals.Folders.Images, imageName + "_" + resulations[resulations.Count - 1].Name);
            }
            else
            {
                while (i != (resulations.Count - 1))
                {
                    i = ZacniOdDalsi(i, sourceW, sourceH, resulations, imgSource, imageName);
                }
            }

            fs.Close();
            imgSource.Dispose();
        }

        private static int ZacniOdDalsi(int i, int sourceW, int sourceH, List<Resulation> resulations, Image imgSource, string imageName)
        {
            if (i == resulations.Count) return resulations.Count - 1;

            if (sourceW >= resulations[i].Width || sourceH >= resulations[i].Height)
            {
                resizeImg(imgSource, resulations[i].Width, resulations[i].Height, Globals.Folders.Images, imageName + "_Full");
                i++;

                if (i >= resulations.Count) i = resulations.Count - 1;

                for (int x = i; x < resulations.Count; x++)
                {
                    resizeImg(imgSource, resulations[x].Width, resulations[x].Height, Globals.Folders.Images, imageName + "_" + resulations[x].Name);
                    i = x;
                }
            }
            else
            {
                i++;
                return ZacniOdDalsi(i, sourceW, sourceH, resulations, imgSource, imageName);
            }

            return i;
        }

        public struct Resulation
        {
            public string Name { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
        }

        private static void saveImageToServer(Image bmPhoto, string folder, string imageName)
        {
            string path = folder + "\\" + imageName + ".jpg";

            bmPhoto.Save(path, ImageFormat.Jpeg);
        }

        private static void resizeImg(Image imgZdroj, int cilW, int cilH, string folder, string imageName)
        {
            int newW = 0;
            int newH = 0;

            int zdrojW = imgZdroj.Width;
            int zdrojH = imgZdroj.Height;

            if (zdrojW < cilW && zdrojH < cilH)
            {
                newW = zdrojW;
                newH = zdrojH;
            }
            else
            {
                newW = cilW;
                newH = (newW * zdrojH) / zdrojW;

                if (newH > cilH)
                {
                    newH = cilH;
                    newW = (newH * zdrojW) / zdrojH;
                }
            }
            
            //prevzorkovani

            Bitmap bmPhoto = new Bitmap(newW, newH, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgZdroj.HorizontalResolution, imgZdroj.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //grPhoto.FillRectangle(Brushes.Transparent, 0, 0, cilW, cilH);

            grPhoto.DrawImage(imgZdroj, 0, 0, newW, newH);
            /*
                new Rectangle(0, 0, newW, newH),
                new Rectangle(0, 0, zdrojW, zdrojH),
                GraphicsUnit.Pixel);
            */
            grPhoto.Dispose();

            saveImageToServer(bmPhoto, folder, imageName);
        }

        public static Image GetFromFile(string filePatch)
        {
            if (filePatch == null)
            {
                return null;
            }
            else
            {
                return Image.FromFile(filePatch, true);
            }
        }


        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        public static Image GetFromUrl(string url)
        {
            Image result = null;

            try
            {
                WebRequest requestPic = WebRequest.Create(url);
                WebResponse responsePic = requestPic.GetResponse();
                Image webImage = Image.FromStream(responsePic.GetResponseStream());

                return webImage;
            }
            catch
            {
            }

            return result;
        }

        public static Bitmap GetGrayScale(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int i = 0; i < original.Width; i++)
            {
                for (int j = 0; j < original.Height; j++)
                {
                    Color originalColor = original.GetPixel(i, j);

                    int grayScale = (int)((originalColor.R * .3) + (originalColor.G * .59) + (originalColor.B * .11));

                    Color newColor = Color.FromArgb(grayScale, grayScale, grayScale);

                    newBitmap.SetPixel(i, j, newColor);
                }
            }

            return newBitmap;
        }

        private const int exifOrientationID = 0x112; //274

        public static void ExifRotate(this Image img)
        {
            if (!img.PropertyIdList.Contains(exifOrientationID))
                return;

            var prop = img.GetPropertyItem(exifOrientationID);
            int val = BitConverter.ToUInt16(prop.Value, 0);
            var rot = RotateFlipType.RotateNoneFlipNone;

            if (val == 3 || val == 4)
                rot = RotateFlipType.Rotate180FlipNone;
            else if (val == 5 || val == 6)
                rot = RotateFlipType.Rotate90FlipNone;
            else if (val == 7 || val == 8)
                rot = RotateFlipType.Rotate270FlipNone;

            if (val == 2 || val == 4 || val == 5 || val == 7)
                rot |= RotateFlipType.RotateNoneFlipX;

            if (rot != RotateFlipType.RotateNoneFlipNone)
                img.RotateFlip(rot);
        }
    }
}
