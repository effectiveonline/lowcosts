﻿using Data;
using EffectiveTools.Models;
using Managers.Models;

namespace Managers
{
    public static class DbContext
    {
        public static void Inicialize()
        {
            Folders = new DbSet<FolderModel>();
            Images = new DbSet<ImageModel>();
            Files = new DbSet<FileModel>();

            Pages = new DbSet<ContentModel>();
        }

        public static DbSet<FolderModel> Folders { get; set; }
        public static DbSet<ImageModel> Images { get; set; }
        public static DbSet<FileModel> Files { get; set; }


        public static DbSet<ContentModel> Pages { get; set; }
    }
}
