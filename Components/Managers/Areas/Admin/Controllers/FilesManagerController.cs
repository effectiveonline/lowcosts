﻿using Managers.Models;
using Managers.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Managers.Controllers
{
  [Authorize]
  [Area("Admin")]
  public class FilesManagerController : Controller
  {
    [Route("/admin/managers/files/{folderId}")]
    [HttpGet]
    public async Task<IActionResult> Index(int folderId)
    {
      FolderModel curentFolder = await DbContext.Folders.SelectSingleByIdAsync(folderId);

      FilesViewModel model = new FilesViewModel
      {
        Folders = new List<FolderViewModel>()
      };

      List<FolderModel> folders = DbContext.Folders.GetList("Parent = " + curentFolder.Parent);

      foreach (FolderModel folder in folders)
      {
        model.Folders.Add(new FolderViewModel
        {
          Id = folder.Id,
          Name = folder.Name,
          ItemsCount = DbContext.Files.SelectCount("Folder = " + folder.Id),
          Expanded = folder.Id == folderId
        });
      }

      model.Files = DbContext.Files.GetList("Folder = " + folderId);

      return PartialView(model);
    }

    [Route("/admin/managers/files/list/{folderId}")]
    [HttpGet]
    public async Task<IActionResult> List(int folderId)
    {
      return PartialView(await DbContext.Files.GetListAsync("Folder = " + folderId));
    }
  }
}