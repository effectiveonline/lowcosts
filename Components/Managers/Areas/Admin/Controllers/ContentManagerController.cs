﻿using EffectiveTools.Models;
using Managers.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Managers.Areas.Admin.Controllers
{
  [Authorize]
  [Area("Admin")]
  public class ContentManagerController : Controller
  {
    [Route("/admin/managers/content")]
    [HttpGet]
    public ActionResult Index(int folderId)
    {
      List<ContentGroupViewModel> model = new List<ContentGroupViewModel>();

      List<ContentModel> pages = DbContext.Pages.GetList("Parent = 0");

      foreach (var modul in Globals.InstaledModules)
      {
        List<ContentItemViewModel> items = new List<ContentItemViewModel>();
        List<ContentModel> _pages = pages.Where(p => p.Modul == modul.Name).OrderBy(p => p.Id).ToList();

        ContentGroupViewModel group = new ContentGroupViewModel
        {
          Name = modul.Title,
          Items = new List<ContentItemViewModel>()
        };

        foreach (ContentModel _page in _pages)
        {
          group.Items.Add(new ContentItemViewModel
          {
            Id = _page.Id,
            Name = _page.Name,
            Link = _page.Url,
            SubPages = pages.Where(p => p.Parent == _page.Id).Count()
          });
        }

        model.Add(group);
      }

      return PartialView(model);
    }

    [Route("/admin/managers/content/{id}")]
    [HttpGet]
    public async Task<IActionResult> SubPages(int id)
    {
      List<ContentItemViewModel> items = new List<ContentItemViewModel>();

      List<ContentModel> pages = await DbContext.Pages.GetListAsync("Parent = " + id);
      pages = pages.OrderBy(p => p.Id).ToList();

      foreach (ContentModel page in pages)
      {
        items.Add(new ContentItemViewModel
        {
          Id = page.Id,
          Name = page.Name,
          Link = page.Url,
          SubPages = DbContext.Pages.SelectCount("Parent = " + page.Id)
        });
      }

      return PartialView(items);

    }
  }
}
