﻿using Managers.Models;
using Managers.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Managers.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class ImagesManagerController : Controller
    {
        [Route("/admin/managers/images/{folderId}")]
        [HttpGet]
        public async Task<IActionResult> Index(int folderId)
        {
            Globals.AdminNavigation.Icon = "pe-7s-photo-gallery";
            Globals.AdminNavigation.Title = "Vložení obrázku";
            Globals.AdminNavigation.SubTitle = "Správce obrázků";

            FolderModel curentFolder = await DbContext.Folders.SelectSingleByIdAsync(folderId);

            ImagesViewModel model = new ImagesViewModel
            {
                Folders = new List<FolderViewModel>(),
                ActiveFolder = folderId
            };

            /*
            List<FolderModel> folders = DbContext.Folders.GetList("Parent = " + curentFolder.Parent);

            foreach (FolderModel folder in folders)
            {
                model.Folders.Add(new FolderViewModel
                {
                    Id = folder.Id,
                    Name = folder.Name,
                    ItemsCount = DbContext.Images.SelectCount("Folder = " + folder.Id),
                    Expanded = folder.Id == folderId
                });
            }
            */

            model.Images = DbContext.Images.GetList("Folder = " + folderId);

            return PartialView(model);
        }

        [Route("/admin/managers/images/list/{folderId}")]
        [HttpGet]
        public async Task<IActionResult> List(int folderId)
        {
            return PartialView(await DbContext.Images.GetListAsync("Folder = " + folderId));
        }


        [Route("/admin/managers/images/upload/{folderId}")]
        [HttpPost]
        public async Task<IActionResult> Index(ImagesViewModel model, int folderId)
        {
            string filePath = Path.Combine(Globals.Folders.Upload, model.Image.FileName);

            FileStream fs = new FileStream(filePath, FileMode.Create);
            model.Image.CopyTo(fs);
            fs.Close();

            FileInfo info = new FileInfo(filePath);

            string imageTitle = info.Name.Replace(info.Extension, "");

            ImageModel newImage = new ImageModel
            {
                Extension = ".jpg",
                FileName = Globals.TextToUrl(imageTitle).Replace("/",""),
                Folder = folderId,
                Title = imageTitle
            };

            DbContext.Images.Add(newImage);

            newImage.FileName += "_" + newImage.Id.ToString();

            DbContext.Images.Update(newImage);

            Imaging.SaveImage(filePath, newImage.FileName);

            System.IO.File.Delete(filePath);

            return PartialView("List", await DbContext.Images.GetListAsync("Folder = " + folderId));
        }

        [Route("/admin/managers/images/delete/{imageId}")]
        [HttpPost]
        public ActionResult Delete(int imageId)
        {
            

            return Ok(true);
        }
    }
}