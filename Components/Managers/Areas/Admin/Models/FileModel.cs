﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Managers.Models
{
    [Table("DataFiles")]
    public class FileModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
}
