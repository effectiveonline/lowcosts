﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Managers.Models
{
    [Table("DataImages")]
    public class ImageModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string Title { get; set; }
    }
}
