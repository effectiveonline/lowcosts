﻿using Managers.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Managers.ViewModels
{
    public class FilesViewModel
    {
        public List<FolderViewModel> Folders { get; set; }
        public List<FileModel> Files { get; set; }
    }
}
