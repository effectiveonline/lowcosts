﻿namespace Managers.ViewModels
{
    public class ContentItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public int SubPages { get; set; }
    }
}