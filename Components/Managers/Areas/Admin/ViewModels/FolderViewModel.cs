﻿namespace Managers.ViewModels
{
    public class FolderViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Expanded { get; set; }
        public int ItemsCount { get; set; }
    }
}
