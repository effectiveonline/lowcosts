﻿using System.Collections.Generic;

namespace Managers.ViewModels
{
    public class ContentGroupViewModel
    {
        public string Name { get; set; }
        public List<ContentItemViewModel> Items { get; set; }
    }
}
