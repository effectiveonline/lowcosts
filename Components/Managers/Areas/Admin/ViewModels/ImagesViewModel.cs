﻿using Managers.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Managers.ViewModels
{
    public class ImagesViewModel
    {
        public List<FolderViewModel> Folders { get; set; }
        public List<ImageModel> Images { get; set; }

        public IFormFile Image { get; set; }

        public int ActiveFolder { get; set; }
    }
}
