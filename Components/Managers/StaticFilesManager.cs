﻿using Managers.Models;
using System.Collections.Generic;
using System.IO;

namespace Managers
{
    public static class StaticFilesManager
    {
        public static void DeleteFolder(int folderId)
        {
            deleteImages(folderId);
            //deleteFiles(folderId);
            //deleteVideos(folderId);

            DbContext.Folders.Delete(folderId);
            DbContext.Folders.Delete("Parent = " + folderId);
        }

        public static int CreateFolder(string name, string type)
        {
            FolderModel newFolder = new FolderModel
            {
                Name = name,
                Type = type,
            };

            DbContext.Folders.Add(newFolder);

            return newFolder.Id;
        }

        private static void deleteImages(int id)
        {
            string[] resulations = new string[] { "Full", "4K", "3K", "2K", "FullHD", "HD", "Thumb" };

            List<ImageModel> images = DbContext.Images.GetList("Folder = " + id);

            foreach (ImageModel img in images)
            {
                foreach (string resulation in resulations)
                {
                    string imgPath = Globals.Folders.Images + "\\" + img.FileName + "_" + resulation + img.Extension;

                    if (File.Exists(imgPath))
                    {
                        File.Delete(imgPath);
                    }
                }

                DbContext.Images.Delete(img.Id);
            }
        }
    }
}
