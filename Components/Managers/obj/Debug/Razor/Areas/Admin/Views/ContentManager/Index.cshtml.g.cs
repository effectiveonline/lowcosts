#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7c5ff3bdd5a486f18e6901cd24f3dc66d575441d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_ContentManager_Index), @"mvc.1.0.view", @"/Areas/Admin/Views/ContentManager/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\_ViewImports.cshtml"
using Managers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\_ViewImports.cshtml"
using Managers.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\_ViewImports.cshtml"
using Managers.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7c5ff3bdd5a486f18e6901cd24f3dc66d575441d", @"/Areas/Admin/Views/ContentManager/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73cbd0b72faf06470f99a3942ae443670de8dad8", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_ContentManager_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<ContentGroupViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"accordion\" id=\"accordion\">\r\n\r\n");
#nullable restore
#line 5 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
     for (int i = 0; i < Model.Count; i++)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"card\">\r\n            <div class=\"card-header\"");
            BeginWriteAttribute("id", " id=\"", 195, "\"", 210, 2);
            WriteAttributeValue("", 200, "heading_", 200, 8, true);
#nullable restore
#line 8 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
WriteAttributeValue("", 208, i, 208, 2, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                <h5 class=\"mb-0\">\r\n                    <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapse_");
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                                                                        Write(i);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"\r\n                        aria-expanded=\"true\" aria-controls=\"collapseOne\">\r\n                        ");
#nullable restore
#line 12 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                   Write(Model[i].Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </button>\r\n                </h5>\r\n            </div>\r\n\r\n            <div");
            BeginWriteAttribute("id", " id=\"", 570, "\"", 586, 2);
            WriteAttributeValue("", 575, "collapse_", 575, 9, true);
#nullable restore
#line 17 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
WriteAttributeValue("", 584, i, 584, 2, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"collapse\"");
            BeginWriteAttribute("aria-labelledby", " aria-labelledby=\"", 604, "\"", 632, 2);
            WriteAttributeValue("", 622, "heading_", 622, 8, true);
#nullable restore
#line 17 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
WriteAttributeValue("", 630, i, 630, 2, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" data-parent=\"#accordion\">\r\n                <div class=\"card-body\">\r\n\r\n                    <ul class=\"nav nav-tabs d-block dd-container\">\r\n");
#nullable restore
#line 21 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                         foreach (ContentItemViewModel item in Model[i].Items)
                        {
                            if (item.SubPages == 0)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <div class=\"dd-item\">\r\n                                    <li>\r\n                                        <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-editPage\" data-url=\"");
#nullable restore
#line 27 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                                                                        Write(item.Link);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"\r\n                                            data-id=\"");
#nullable restore
#line 28 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n                                            ");
#nullable restore
#line 29 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                       Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                        </a>\r\n                                    </li>\r\n                                </div>\r\n");
#nullable restore
#line 33 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li data-expanded=\"collapse\">\r\n                                    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-editPage\" data-url=\"");
#nullable restore
#line 37 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                                                                    Write(item.Link);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"\r\n                                        data-subitems=\"");
#nullable restore
#line 38 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                  Write(item.SubPages);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" data-id=\"");
#nullable restore
#line 38 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                                                           Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n                                        ");
#nullable restore
#line 39 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                                   Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                        <span class=""sub-nav-icon rot-90""> <i class=""stroke-arrow""></i> </span>
                                    </a>
                                    <ul id=""subitems"" class=""subitems-list"">
                                    </ul>
                                </li>
");
#nullable restore
#line 45 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
                            }
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </ul>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 52 "D:\Visual Studio\Effective Online\EffectiveTools\Components\Managers\Areas\Admin\Views\ContentManager\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<ContentGroupViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
