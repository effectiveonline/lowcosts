﻿using Interface;
using System;

namespace Managers
{
    public class Component : IComponent
    {
        IComponentHost myHost = null;

        public Component()
        {
            DbContext.Inicialize();
        }

        public string Name
        {
            get { return "managers"; }
        }

        public IComponentHost Host
        {
            get { return myHost; }
            set { myHost = value; }
        }

        public void Initialize() { }

        public void Dispose() { }
    }
}
