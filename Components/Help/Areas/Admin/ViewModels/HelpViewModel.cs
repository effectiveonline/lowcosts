﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Areas.Admin.ViewModels
{
    public class HelpViewModel
    {
        public string View { get; set; }
        public List<HelpMenuItem> Menu { get; set; }
    }
}
