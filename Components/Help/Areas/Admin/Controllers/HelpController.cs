﻿using Areas.Admin.ViewModels;
using Data;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class HelpController : Controller
    {
        [Route("/admin/help")]
        public IActionResult Help()
        {
            Globals.AdminNavigation.Icon = "pe-7s-help1";
            Globals.AdminNavigation.Title = "Nápověda";
            Globals.AdminNavigation.SubTitle = "Rejstřík";

            HelpViewModel model = new HelpViewModel
            {
                View = "Index",
                Menu = new List<HelpMenuItem>()
            };

            foreach(var adminModul in Globals.InstaledModules)
            {
                model.Menu.Add(adminModul.Help);
            }

            model.Menu = model.Menu.OrderBy(i => i.Order).ToList();

            return View(model);
        }

        [Route("/admin/help/{modul}/{view}")]
        public IActionResult Browser(string modul, string view)
        {
            Globals.AdminNavigation.Icon = "pe-7s-help1";
            Globals.AdminNavigation.SubTitle = "Nápověda";

            return PartialView(modul + "/" + view);
        }

        [Route("/admin/help-modal/{modul}/{view}")]
        public IActionResult Modal(string modul, string view)
        {
            Globals.AdminNavigation.Icon = "pe-7s-help1";
            Globals.AdminNavigation.SubTitle = "Nápověda";
            
            HelpViewModel model = new HelpViewModel
            {
                View = modul + "/" + view,
            };

            foreach (var adminModul in Globals.InstaledModules)
            {
                if (adminModul.Help.Items != null)
                {
                    if (adminModul.Help.View.ToLower() == view.ToLower())
                    {
                        Globals.AdminNavigation.Title = adminModul.Help.NavigatinTitle;
                    }
                    else if (adminModul.Help.Items != null)
                    {
                        getNavigation(adminModul.Help, view);
                    }
                }
            }

            return PartialView(model);
        }

        private void getNavigation(HelpMenuItem item, string view)
        {
            if (item.Items != null)
            {
                foreach (var subitem in item.Items)
                {
                    if (subitem.View.ToLower() == view.ToLower())
                    {
                        Globals.AdminNavigation.Title = subitem.NavigatinTitle;
                    }
                    else if(subitem.Items != null)
                    {
                        getNavigation(subitem, view);
                    }
                }
            }
        }
    }
}
