﻿using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;

namespace Sections
{
    public static class DbContext
    {
        public static void Inicialize()
        {
            Persons = new DbSet<PersonModel>();

            Carousels = new DbSet<CarouselModel>();
            CarouselsData = new DbSet<CarouselDataModel>();

            Paralaxs = new DbSet<ParalaxModel>();
            
            ContactForms = new DbSet<FormsModel>();

            WebMails = new DbSet<WebMailModel>();
            WebNewsRecipients = new DbSet<WebNewModel>();

            Texts = new DbSet<TextModel>();

            Folders = new DbSet<FolderModel>();
            Images = new DbSet<ImagesModel>();
            ImagesItems = new DbSet<ImagesItemModel>();

            Image = new DbSet<ImageModel>();

            NewsList = new DbSet<EventsModel>();

            ContentSections = new DbSet<EffectiveTools.Models.ContentSectionModel>();
            Pages = new DbSet<ContentModel>();
        }

        public static DbSet<PersonModel> Persons { get; set; }

        public static DbSet<CarouselModel> Carousels { get; set; }
        public static DbSet<CarouselDataModel> CarouselsData { get; set; }

        public static DbSet<ParalaxModel> Paralaxs { get; set; }

        public static DbSet<FormsModel> ContactForms { get; set; }
        public static DbSet<WebMailModel> WebMails { get; set; }
        public static DbSet<WebNewModel> WebNewsRecipients { get; set; }

        public static DbSet<TextModel> Texts { get; set; }

        public static DbSet<FolderModel> Folders { get; set; }
        public static DbSet<ImagesModel> Images { get; set; }
        public static DbSet<ImagesItemModel> ImagesItems { get; set; }

        public static DbSet<ImageModel> Image { get; set; }

        public static DbSet<EventsModel> NewsList { get; set; }

        public static DbSet<ContentModel> Pages { get; set; }
        public static DbSet<ContentSectionModel> ContentSections { get; set; }

        
    }
}