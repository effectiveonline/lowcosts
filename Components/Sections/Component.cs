﻿using Areas.Sections.ViewModels;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using System;
using System.IO;

namespace Sections
{
    public class Component : IComponent
    {
        IComponentHost myHost = null;

        public Component()
        {
            DbContext.Inicialize();

            Globals.AvalibleSections.Add(new CarouselViewModel());
            Globals.AvalibleSections.Add(new ParalaxViewModel());
            Globals.AvalibleSections.Add(new FormsViewModel());
            Globals.AvalibleSections.Add(new TextViewModel());
            Globals.AvalibleSections.Add(new EventsViewModel());
            Globals.AvalibleSections.Add(new ImagesViewModel());
        }

        public string Name
        {
            get { return "sections"; }
        }

        public IComponentHost Host
        {
            get { return myHost; }
            set { myHost = value; }
        }

        public void Initialize() { }

        public void Dispose() { }


        public static async System.Threading.Tasks.Task<string> RenderToStringAsync(ViewResult viewResult, IServiceProvider serviceProvider)
        {
            if (viewResult == null) throw new ArgumentNullException(nameof(viewResult));

            var httpContext = new DefaultHttpContext
            {
                RequestServices = serviceProvider
            };

            var actionContext = new ActionContext(httpContext, new Microsoft.AspNetCore.Routing.RouteData(), new ActionDescriptor());

            using (var stream = new MemoryStream())
            {
                httpContext.Response.Body = stream; // inject a convenient memory stream
                await viewResult.ExecuteResultAsync(actionContext); // execute view result on that stream

                httpContext.Response.Body.Position = 0;
                return new StreamReader(httpContext.Response.Body).ReadToEnd(); // collect the content of the stream
            }
        }
    }
}