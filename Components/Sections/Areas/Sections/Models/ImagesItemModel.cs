﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Areas.Sections.Models
{
    [Table("SectionImagesItems")]
    public class ImagesItemModel
    {
        public int Id { get; set; }
        public int Section { get; set; }
        public int Image { get; set; }
        public int Order { get; set; }
    }
}
