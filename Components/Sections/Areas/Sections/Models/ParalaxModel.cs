﻿using Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionParalax")]

    public class ParalaxModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public int IdImg { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
    }
}
