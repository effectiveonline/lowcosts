﻿using Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionImages")]
    public class ImagesModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string ShowAs { get; set; }
    }

}
