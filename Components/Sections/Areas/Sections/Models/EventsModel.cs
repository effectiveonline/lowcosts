﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionEvents")]
    public class EventsModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string Title { get; set; }
        public string Items { get; set; }
    }
}
