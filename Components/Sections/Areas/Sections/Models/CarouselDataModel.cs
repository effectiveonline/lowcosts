﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionCarouselData")]
    public class CarouselDataModel
    {
        public int Id { get; set; }
        public int IdCarousel { get; set; }
        public string Name { get; set; } = string.Empty;
        public int IdImg { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Text { get; set; } = string.Empty;
        public string Link { get; set; } = string.Empty;
        public int Order { get; set; }
    }
}
