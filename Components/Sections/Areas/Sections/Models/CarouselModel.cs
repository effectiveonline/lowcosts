﻿using Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionCarousel")]
    public class CarouselModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public bool Loop { get; set; }
        public int LoopTime { get; set; }
    }
}
