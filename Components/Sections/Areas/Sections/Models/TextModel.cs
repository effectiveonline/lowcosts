﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionText")]
    public class TextModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string Text { get; set; }
    }
}
