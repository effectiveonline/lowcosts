﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("DataFolders")]
    public class FolderModel
    {
        public int Id { get; set; }
        public int Parent { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
