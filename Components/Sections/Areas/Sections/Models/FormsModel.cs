﻿using Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace Areas.Sections.Models
{
    [Table("SectionForms")]
    public class FormsModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string ShowAs { get; set; }
        public int Folder { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Recipient { get; set; }
        public bool ShowGDPR { get; set; }
        public bool CopyToSender { get; set; }
    }

}
