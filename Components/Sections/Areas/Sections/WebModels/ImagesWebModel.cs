﻿using Areas.Sections.Models;
using System.Collections.Generic;

namespace Areas.Sections.WebModels
{
    public class ImagesWebModel
    {
        public string ShowAs { get; set; }
        public List<ImageModel> Images { get; set; }
    }
}
