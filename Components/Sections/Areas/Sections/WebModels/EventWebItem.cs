﻿using System;

namespace Areas.Sections.WebModels
{
    public class EventWebItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public DateTime Created { get; set; }
    }
}
