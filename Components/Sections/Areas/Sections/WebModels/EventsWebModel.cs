﻿using System.Collections.Generic;

namespace Areas.Sections.WebModels
{
    public class EventsWebModel
    {
        public string Title { get; set; }

        public List<EventWebItem> Items { get; set; }
    }
}
