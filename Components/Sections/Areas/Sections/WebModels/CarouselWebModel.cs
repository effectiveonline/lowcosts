﻿using Areas.Sections.ViewModels;
using System.Collections.Generic;

namespace Areas.Sections.WebModels
{
    public class CarouselWebModel
    {
        public bool Loop { get; set; }
        public int LoopTime { get; set; }
        public List<CarouselItemViewModel> Items { get; set; }
    }
}