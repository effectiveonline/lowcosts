﻿namespace Areas.Sections.WebModels
{
    public class FormsWebModel
    {
        public int Id { get; set; }
        public string ShowAs { get; set; }
        public string FormType { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Recipient { get; set; }
        public bool ShowGDPR { get; set; }
        public bool CopyToSender { get; set; }
        public string SenderName { get; set; }
        public string SenderStreet { get; set; }
        public string SenderCity { get; set; }
        public string SenderZipCode { get; set; }
        public string SenderEmail { get; set; }
        public string SenderPhone { get; set; }
        public string Message { get; set; }
        public string Note { get; set; }
        public string FormBody { get; set; }
        public bool GDPR { get; set; }
    }
}
