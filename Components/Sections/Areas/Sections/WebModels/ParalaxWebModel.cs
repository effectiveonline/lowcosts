﻿namespace Areas.Sections.WebModels
{
    public class ParalaxWebModel
    {
        public int IdImg { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
    }
}
