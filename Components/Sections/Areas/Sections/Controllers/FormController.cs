﻿using Data;
using Sections;
using EffectiveTools.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class FormController : Controller
    {
        IServiceProvider _serviceProvider;

        public FormController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        [Route("/admin/sections/contactform/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(FormsViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            return Ok(viewModel.Create(page.Folder));
        }

        [Route("/admin/sections/contactform/edit/{formId}")]
        [HttpPost]
        public async Task<IActionResult> Edit(FormsViewModel viewModel, int formId)
        {
            var model = DbContext.ContactForms.SelectSingleById(formId);

            if (model != null)
            {

                model.ShowGDPR = viewModel.ShowGDPR;
                model.Recipient = viewModel.Recipients.Aggregate((a, b) => a + ";" + b);
                model.Text = viewModel.Text;
                model.Title = viewModel.Title;
                model.ShowAs = viewModel.ShowAs;
                model.Type = viewModel.FormType;

                await DbContext.ContactForms.UpdateAsync(model);
            }

            return Ok(true);
        }

        [Route("/contactform/send/{id}")]
        [HttpPost]
        public async Task<IActionResult> Send(FormsViewModel formData, int id)
        {
            var form = await DbContext.ContactForms.SelectSingleByIdAsync(id);

            ContentSectionModel contentSection = DbContext.ContentSections.FirstOrDefault("IdSekce = " + id + " AND Type = 'ContactForm'");

            // Zpracování kontaktního formuláře
            if (string.IsNullOrEmpty(formData.ShowAs))
            {
                WebMailModel mailMessage = new WebMailModel
                {
                    //ContentId = section.IdPage,
                    Name = formData.SenderName,
                    Email = formData.SenderEmail,
                    Phone = formData.SenderPhone,
                    Message = formData.Message,
                    Created = DateTime.Now,
                };
                
                await DbContext.WebMails.AddAsync(mailMessage);

                var emailView = View("Areas/Web/Views/Emails/Message.cshtml", mailMessage);
                var emailBody = await Component.RenderToStringAsync(emailView, _serviceProvider);

                MailSender mail = new MailSender
                {
                    Recipient = Globals.WebConfig.SmtpEmail,
                    Subject = "Dotaz ze stránek " + Globals.WebConfig.Domain,
                    Message = emailBody
                };

                if (!String.IsNullOrEmpty(form.Recipient))
                {
                    mail.Recipient = form.Recipient;
                }

                if (await mail.SendAsync())
                {
                    if (formData.CopyToSender)
                    {
                        mail.Recipient = formData.SenderEmail;
                        mail.Subject = "Kopie Vaší zprávy ze stránek " + Globals.WebConfig.Domain;
                        mail.Message = emailBody;
                        await mail.SendAsync();
                    }

                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }

            // Zpracování Zavolejte mi zpět
            if (formData.ShowAs == "CallMyBack")
            {
                var emailView = View("Areas/Web/Views/Emails/CallBack.cshtml", formData.SenderPhone);
                var emailBody = await Component.RenderToStringAsync(emailView, _serviceProvider);

                MailSender mail = new MailSender
                {
                    Recipient = Globals.WebConfig.SmtpEmail,
                    Subject = "Žádost o zavolání z " + Globals.WebConfig.Domain,
                    Message = emailBody
                };

                await mail.SendAsync();
            }

            // Zpracování zasílání novinek
            if (formData.ShowAs == "RegNews")
            {
                WebNewModel newRecipient = new WebNewModel
                {
                    Email = formData.SenderEmail,
                    Token = Globals.Md5(formData.SenderEmail),
                    Registred = DateTime.Now
                };

                await DbContext.WebNewsRecipients.AddAsync(newRecipient);

                var emailView = View("Areas/Web/Views/Emails/RegNews.cshtml", newRecipient);
                var emailBody = await Component.RenderToStringAsync(emailView, _serviceProvider);

                MailSender mail = new MailSender
                {
                    Recipient = formData.SenderEmail,
                    Subject = "Registrace novinek na "+ Globals.WebConfig.Domain,
                    Message = emailBody
                };

                await mail.SendAsync();
            }

            return Ok(true);
        }

            
    }
}
