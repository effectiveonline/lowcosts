﻿using Sections;
using EffectiveTools.Models;
using Microsoft.AspNetCore.Mvc;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class ImagestController : Controller
    {
        [Route("admin/sections/images/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(ImagesViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            return Ok(viewModel.Create(page.Folder));
        }

        [Route("admin/sections/images/edit/{idSekce}")]
        [HttpPost]
        public async Task<IActionResult> Edit(ImagesViewModel viewModel, int idSekce)
        {
            ImagesModel model = await DbContext.Images.SelectSingleByIdAsync(idSekce);

            if (model != null)
            {

                model.ShowAs = viewModel.ShowAs;

                await DbContext.Images.UpdateAsync(model);

                if (viewModel.Images != null)
                {
                    foreach (ImagesItemViewModel image in viewModel.Images)
                    {
                        ImagesItemModel item = DbContext.ImagesItems.FirstOrDefault("Section = " + idSekce + "AND Image = " + image.Image);

                        if (item == null)
                        {
                            //pridat
                            ImagesItemModel newImage = new ImagesItemModel
                            {
                                Section = idSekce,
                                Image = image.Image,
                                Order = image.Order,
                            };

                            await DbContext.ImagesItems.AddAsync(newImage);
                        }
                        else if (image.Order == -1)
                        {
                            await DbContext.ImagesItems.DeleteAsync(item.Id);
                        }
                        else
                        {
                            //uloyit zmeny
                            item.Order = image.Order;
                            await DbContext.ImagesItems.UpdateAsync(item);
                        }
                    }
                }
            }

            return Ok(true);
        }
    }
}
