﻿using Sections;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EffectiveTools.Models;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class EventsController : Controller
    {
        [Route("/admin/sections/events/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(EventsViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            viewModel.Items = new List<EventItemViewModel>();
            viewModel.Items.Add(new EventItemViewModel
            {
                Id = idPage
            });

            return Ok(viewModel.Create(page.Folder));
        }

        #region Editace Výpisu novinek

        [Route("/admin/sections/events/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> Edit(EventsViewModel viewModel, int id)
        {
            try
            {
                var model = DbContext.NewsList.SelectSingleById(id);

                if (model != null)
                {

                    model.Title = viewModel.Title;

                    List<string> items = new List<string>();

                    if (viewModel.Items != null)
                    {
                        foreach (EventItemViewModel item in viewModel.Items)
                        {
                            if (item.Selected) items.Add(item.Id.ToString());
                        }
                    }

                    if (items.Count != 0)
                    {
                        model.Items = items.Aggregate((a, b) => a + "," + b);
                    }
                    else
                    {
                        model.Items = string.Empty;
                    }

                    await DbContext.NewsList.UpdateAsync(model);
                }

                return Ok(true);
            }
            catch
            {
                return Ok(false);
            }
        }

        #endregion
    }
}
