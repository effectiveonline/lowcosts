﻿using Sections;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using EffectiveTools.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class CarouselController : Controller
    {
        #region Vytvoření Carouselu

        [Route("/admin/sections/carousel/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(CarouselViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            return Ok(viewModel.Create(page.Folder));
        }

        #endregion

        #region Editace Carouselu

        [Route("/admin/sections/carousel/edit/{carouselId}")]
        [HttpPost]
        public async Task<IActionResult> Edit(CarouselViewModel viewModel, int carouselId)
        {
            try
            {
                var model = DbContext.Carousels.SelectSingleById(carouselId);
                if (model != null)
                {
                    model.Loop = viewModel.Loop;
                    model.LoopTime = viewModel.LoopTime;

                    // aktualizace položek carouselu
                    if (viewModel.Items != null)
                    {
                        for (int i = 0; i < viewModel.Items.Count; i++)
                        {
                            CarouselItemViewModel item = viewModel.Items[i];

                            if (item.Id == 0)
                            {
                                CarouselDataModel newItem = new CarouselDataModel
                                {
                                    IdCarousel = item.Carousel,
                                    IdImg = item.IdImg,
                                    Name = item.Name,
                                    Order = item.Order,
                                    Text = item.Text,
                                    Title = item.Title,
                                    Link = item.Link
                                };

                                DbContext.CarouselsData.Add(newItem);

                                // vytvoření položky
                            }
                            else if (item.Order == -1)
                            {
                                //odstranění položky
                                DbContext.CarouselsData.Delete(item.Id);
                                viewModel.Items.RemoveAt(i);
                            }
                            else
                            {
                                CarouselDataModel itemData = DbContext.CarouselsData.SelectSingleById(item.Id);

                                itemData.Name = item.Name;
                                itemData.Text = item.Text;
                                itemData.Title = item.Title;
                                itemData.IdImg = item.IdImg;
                                itemData.Order = item.Order;
                                itemData.Link = item.Link;

                                DbContext.CarouselsData.Update(itemData);
                            }
                        }
                    }

                    await DbContext.Carousels.UpdateAsync(model);
                }

                return Ok(true);
            }
            catch
            {
                return Ok(false);
            }
        }

        #endregion
        /*
        #region Vytvoření položky Carouselu

        [Route("/admin/sections/carousel/additem/{id}")]
        [HttpGet]
        public ActionResult CreateItem(int id)
        {
            Globals.AdminNavigation.Title = "Vytvoření položky Carouselu";
            Globals.AdminNavigation.Icon = "pe-7s-loop";

            CarouselDataModel model = new CarouselDataModel
            {
                IdCarousel = id
            };

            return PartialView(model);
        }

        [Route("/admin/sections/carousel/additem")]
        [HttpPost]
        public ActionResult CreateItem(CarouselDataModel model)
        {
            if (ModelState.IsValid)
            {
                model.Order = DbContext.CarouselsData.SelectCount("IdCarousel = " + model.IdCarousel) + 1;

                DbContext.CarouselsData.Add(model);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Editace položek Carouselu

        [Route("/admin/sections/carousel/edititems/{idCarousel}")]
        [HttpGet]
        public ActionResult EditItem(int idCarousel)
        {
            Globals.AdminNavigation.Title = "Editace položky Carouselu";
            Globals.AdminNavigation.Icon = "pe-7s-loop";

            List<CarouselDataModel> model = DbContext.CarouselsData.GetList("IdCarousel = " + idCarousel);

            return PartialView(model);
        }

        [Route("/admin/sections/carousel/edititems")]
        [HttpPost]
        public ActionResult EditItem(List<CarouselDataModel> items)
        {
            foreach(CarouselDataModel item in items)
            {
                DbContext.CarouselsData.Update(item);
            }

            return Ok(true);
        }

        #endregion
        */
    }
}
