﻿using Sections;
using EffectiveTools.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using System.Threading.Tasks;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class ParalaxController : Controller
    {
        [Route("admin/sections/paralax/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(ParalaxViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            return Ok(viewModel.Create(page.Folder));
        }

        [Route("admin/sections/paralax/edit/{paralaxId}")]
        [HttpPost]
        public async Task<IActionResult> Edit(ParalaxViewModel viewModel, int paralaxId)
        {
            var model = await DbContext.Paralaxs.SelectSingleByIdAsync(paralaxId);

            if (model != null)
            {
                model.Content = viewModel.Content;
                model.IdImg = viewModel.IdImg;
                model.Link = viewModel.Link;
                model.Title = viewModel.Title;

                await DbContext.Paralaxs.UpdateAsync(model);
            }

            return Ok(true);
        }
    }
}
