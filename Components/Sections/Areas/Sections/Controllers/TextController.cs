﻿using Sections;
using Microsoft.AspNetCore.Mvc;
using Areas.Sections.Models;
using Areas.Sections.ViewModels;
using System.Threading.Tasks;
using EffectiveTools.Models;

namespace Areas.Sections.Controllers
{
    [Area("Sections")]
    public class TextController : Controller
    {
        [Route("admin/sections/text/create/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Create(TextViewModel viewModel, int idPage)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(idPage);

            return Ok(viewModel.Create(page.Folder));
        }

        [Route("admin/sections/text/edit/{textId}")]
        [HttpPost]
        public async Task<IActionResult> Edit(TextViewModel viewModel, int textId)
        {
            var model = await DbContext.Texts.SelectSingleByIdAsync(textId);

            if (model != null)
            {
                model.Text = viewModel.Text;

                await DbContext.Texts.UpdateAsync(model);
            }

            return Ok(true);
        }
    }
}