﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Areas.Sections.ViewModels
{
    public class ImagesItemViewModel
    {
        public int Image { get; set; }
        public int Section { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public int Order { get; set; }
    }
}
