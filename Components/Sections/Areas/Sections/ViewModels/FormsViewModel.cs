﻿using Sections;
using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Areas.Sections.WebModels;
using System.Linq;

namespace Areas.Sections.ViewModels
{
    public class FormsViewModel : Section
    {
        public int IdContactForm { get; set; }

        [Display(Name = "Nadpis")]
        public string Title { get; set; }

        [Display(Name = "Textový obsah")]
        public string Text { get; set; }

        [Display(Name = "Příjemci")]
        public List<string> Recipients { get; set; }
        public List<string> RecipientsList { get; set; }

        [Display(Name = "Vyžadovat souhlas s GDPR.")]
        public bool ShowGDPR { get; set; }

        [Display(Name = "Přeji si zaslat kopii mé zprávy.")]
        public bool CopyToSender { get; set; }

        [Display(Name = "Jméno a příjméní")]
        public string SenderName { get; set; }

        [Display(Name = "Ulice a číslo popisné")]
        public string SenderStreet { get; set; }

        [Display(Name = "Město")]
        public string SenderCity { get; set; }

        [Display(Name = "PSČ")]
        public string SenderZipCode { get; set; }

        [Display(Name = "E-mail")]
        public string SenderEmail { get; set; }

        [Display(Name = "Telefon")]
        public string SenderPhone { get; set; }

        [Display(Name = "Zpráva")]
        public string Message { get; set; }

        [Display(Name = "Poznámka")]
        public string Note { get; set; }

        [Display(Name = "Souhlasím se zpracováním osobních údajů")]
        public bool GDPR { get; set; }

        [Display(Name = "Formulář")]

        public string FormType { get; set; }
        public List<SelectBoxItem> FormTypes { get; set; }

        public FormsViewModel()
        {
            Type = "Forms";
            Name = "Formulář";
            Description = "Vložení formuláře do stránky";
        }

        public override int Create(int folder)
        {
            if(Recipients == null)
            {
                Recipients = new List<string>();
                Recipients.Add(Globals.Company.Email);
            }

            FormsModel model = new FormsModel
            {
                Folder = folder,
                CopyToSender = CopyToSender,
                Recipient = Recipients.Aggregate((a, b) => a + ";" + b),
                Text = Text,
                Title = Title,
                ShowAs = "Pages",
                Type = "Message",
                ShowGDPR = ShowGDPR
            };

            DbContext.ContactForms.Add(model);

            return model.Id;
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new FormsViewModel { ShowAs = "Pages", FormType = "Message" };

            FormsModel sectionData = DbContext.ContactForms.SelectSingleById(contentSection.IdSekce);

            return new FormsViewModel
            {
                IdContentSection = contentSection.Id,
                FolderId = sectionData.Folder,

                IdContactForm = sectionData.Id,
                Text = sectionData.Text,
                Title = sectionData.Title,
                Recipients = sectionData.Recipient.Split(";").ToList(),
                ShowGDPR = sectionData.ShowGDPR,
                CopyToSender = sectionData.CopyToSender,
                RecipientsList = getRecipientsList(),
                ShowAs = sectionData.ShowAs,
                FormType = sectionData.Type,
                FormTypes = Globals.InstaledModules.FirstOrDefault(m => m.Name == sectionData.ShowAs).FormTypes
            };
        }

        public override Section Get(int idSekce)
        {
            FormsModel sectionData = DbContext.ContactForms.SelectSingleById(idSekce);
            ContentSectionModel contentSection = DbContext.ContentSections.FirstOrDefault("Type = 'Forms' AND IdSekce = " + idSekce);

            return new FormsViewModel
            {
                //IdContentSection = contentSection.Id,
                //FolderId = contentSection.Folder,

                IdContactForm = sectionData.Id,
                Text = sectionData.Text,
                Title = sectionData.Title,
                Recipients = sectionData.Recipient.Split(";").ToList(),
                ShowGDPR = sectionData.ShowGDPR,
                CopyToSender = sectionData.CopyToSender,
                ShowAs = sectionData.ShowAs,
                FormType = sectionData.Type,
                FormTypes = Globals.InstaledModules.FirstOrDefault(m => m.Name == sectionData.ShowAs).FormTypes
            };
        }

        public override Section Get()
        {
            return new FormsViewModel();
        }

        public override Object GetWebModel(int idSekce)
        {
            FormsModel sectionData = DbContext.ContactForms.SelectSingleById(idSekce);

            return new FormsWebModel
            {
                Id = sectionData.Id,
                Text = sectionData.Text,
                Title = sectionData.Title,
                Recipient = sectionData.Recipient,
                ShowGDPR = sectionData.ShowGDPR,
                CopyToSender = sectionData.CopyToSender,
                FormType = sectionData.Type,
                ShowAs = sectionData.ShowAs,
                FormBody = getFormBody(sectionData.ShowAs, sectionData.Type)
            };
        }

        public override bool Delete(int id)
        {
            return DbContext.ContactForms.Delete(id);
        }

        private List<string> getRecipientsList()
        {
            List<string> result = new List<string>();

            result.Add(Globals.Company.Email);

            List<PersonModel> persons = DbContext.Persons.GetList();

            foreach (string recipient in persons.Select(p => p.Email))
            {
                if (!result.Contains(recipient)) result.Add(recipient);
            }

            return result;
        }

        private string getFormBody(string modul, string id)
        {

            return Globals.InstaledModules.FirstOrDefault(m => m.Name == modul).GetFormBody(id);
        }
    }
}
