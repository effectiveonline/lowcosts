﻿using Sections;
using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using Areas.Sections.WebModels;

namespace Areas.Sections.ViewModels
{
    public class ImagesViewModel : Section
    {
        public int IdImages { get; set; }

        public List<ImagesItemViewModel> Images { get; set; }

        public ImagesViewModel()
        {
            Type = "Images";
            Name = "Obrázky";
            Description = "Vložení obrázků do stránky";
        }

        public override int Create(int folder)
        {
            ImagesModel model = new ImagesModel
            {
                Folder = folder,
                ShowAs = "Tiles"
            };

            DbContext.Images.Add(model);

            return model.Id;
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new ImagesViewModel();

            var sectionData = DbContext.Images.SelectSingleById(contentSection.IdSekce);

            return new ImagesViewModel
            {
                FolderId = sectionData.Folder,

                IdImages = sectionData.Id,
                ShowAs = sectionData.ShowAs,
                Images = getImages(contentSection.IdSekce),
            };
        }

        public override Section Get(int idSekce)
        {
            var sectionData = DbContext.Images.SelectSingleById(idSekce);

            return new ImagesViewModel
            {
                IdImages = sectionData.Id,
                ShowAs = sectionData.ShowAs,
                FolderId = sectionData.Folder,
                Images = getImages(idSekce)
                
            };
        }

        public override Section Get()
        {
            return new ImagesViewModel();
        }

        public override Object GetWebModel(int idSekce)
        {
            //bude chtit optimalizovat sqldotaz

            var sectionData = DbContext.Images.SelectSingleById(idSekce);
            List<ImagesItemModel> images = DbContext.ImagesItems.GetList("Section = " + idSekce).OrderBy(i => i.Order).ToList();

            ImagesWebModel result = new ImagesWebModel
            {
                ShowAs = sectionData.ShowAs,
                Images = new List<ImageModel>()
            };

            foreach (ImagesItemModel img in images)
            {
                result.Images.Add(DbContext.Image.SelectSingleById(img.Image));
            }

            return result;
        }

        public override bool Delete(int id)
        {
            return DbContext.Images.Delete(id);
        }

        private List<ImagesItemViewModel> getImages(int sekceId)
        {
            List<ImagesItemViewModel> result = new List<ImagesItemViewModel>();

            List<ImagesItemModel> items = DbContext.ImagesItems.GetList("Section = " + sekceId);

            foreach(ImagesItemModel item in items)
            {
                ImageModel imgData = DbContext.Image.SelectSingleById(item.Image);

                result.Add(new ImagesItemViewModel
                { 
                    Image = imgData.Id,
                    Section = sekceId,
                    FileName = imgData.FileName,
                    Extension = imgData.Extension,
                    Order = item.Order,
                    Title = imgData.Title,
                });
            }

            return result;
        }
    }
}
