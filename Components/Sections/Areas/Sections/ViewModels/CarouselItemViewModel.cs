﻿using Areas.Sections.Models;
using System.ComponentModel.DataAnnotations;

namespace Areas.Sections.ViewModels
{
    public class CarouselItemViewModel
    {
        public int Id { get; set; }
        public int Carousel { get; set; }

        [Required(ErrorMessage = "Zadejte název položky.")]
        [Display(Name = "Název položky")]
        public string Name { get; set; } = string.Empty;

        public int IdImg { get; set; }
        [Display(Name = "Nadpis")]
        public string Title { get; set; } = string.Empty;
        [Display(Name = "Vlastní text")]
        public string Text { get; set; } = string.Empty;
        [Display(Name = "Odkaz")]
        public string Link { get; set; } = string.Empty;
        public int Order { get; set; }
        public ImageModel Image { get; set; }
    }
}
