﻿using Sections;
using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;
using System;
using System.ComponentModel.DataAnnotations;
using Areas.Sections.WebModels;

namespace Areas.Sections.ViewModels
{
    public class ParalaxViewModel : Section
    {
        public int IdParalax { get; set; }
        public int IdImg { get; set; }

        [Display(Name = "Nadpis")]
        public string Title { get; set; }

        [Display(Name = "Textový obsah")]
        public string Content { get; set; }

        [Display(Name = "Odkaz")]
        public string Link { get; set; }

        public ParalaxViewModel()
        {
            Type = "Paralax";
            Name = "Paralax";
            Description = "Paralax popis";
        }

        public override int Create(int folder)
        {
            ParalaxModel model = new ParalaxModel
            {
                Folder = folder,
                Content = Content,
                Link = Link,
                Title = Title
            };

            DbContext.Paralaxs.Add(model);

            return model.Id;
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new ParalaxViewModel();

            var sectionData = DbContext.Paralaxs.SelectSingleById(contentSection.IdSekce);

            return new ParalaxViewModel
            {
                IdContentSection = contentSection.Id,
                FolderId = sectionData.Folder,

                IdParalax = sectionData.Id,
                Content = sectionData.Content,
                Title = sectionData.Title,
                IdImg = sectionData.IdImg,
                Link = sectionData.Link
            };
        }

        public override Section Get(int idSekce)
        {
            var sectionData = DbContext.Paralaxs.SelectSingleById(idSekce);

            return new ParalaxViewModel
            {
                //IdContentSection = contentSection.Id,
                //FolderId = contentSection.Folder,

                IdParalax = sectionData.Id,
                Content = sectionData.Content,
                Title = sectionData.Title,
                IdImg = sectionData.IdImg,
                Link = sectionData.Link
            };
        }
        public override Section Get()
        {
            return new ParalaxViewModel();
        }

        public override Object GetWebModel(int idSekce)
        {
            var sectionData = DbContext.Paralaxs.SelectSingleById(idSekce);

            return new ParalaxWebModel
            {
                Content = sectionData.Content,
                Title = sectionData.Title,
                IdImg = sectionData.IdImg,
                Link = sectionData.Link
            };
        }

        
        public override bool Delete(int id)
        {
            return DbContext.Paralaxs.Delete(id);
        }
    }
}
