﻿using Sections;
using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Areas.Sections.ViewModels
{
    public class TextViewModel : Section
    {
        public int IdText { get; set; }

        [Display(Name = "Odkaz")]
        public string Text { get; set; }

        public TextViewModel()
        {
            Type = "Text";
            Name = "Vlastní text";
            Description = "Vložení vlastního textu";
        }

        public override int Create(int folder)
        {
            TextModel model = new TextModel
            {
                Folder = folder,
                Text = Text
            };

            DbContext.Texts.Add(model);

            return model.Id;
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new TextViewModel();

            var sectionData = DbContext.Texts.SelectSingleById(contentSection.IdSekce);

            return new TextViewModel
            {
                IdContentSection = contentSection.Id,
                FolderId = sectionData.Folder,

                IdText = sectionData.Id,
                Text = sectionData.Text,
            };
        }

        public override Section Get(int idSekce)
        {
            var sectionData = DbContext.Texts.SelectSingleById(idSekce);

            return new TextViewModel
            {
                //FolderId = idFolder,

                IdText = sectionData.Id,
                Text = sectionData.Text,
            };
        }

        public override Section Get()
        {
            return new TextViewModel();
        }

        public override Object GetWebModel(int idSekce)
        {
            var sectionData = DbContext.Texts.SelectSingleById(idSekce);

            return sectionData.Text;
        }

        public override bool Delete(int id)
        {
            return DbContext.Texts.Delete(id);
        }
    }
}
