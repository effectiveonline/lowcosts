﻿using Sections;
using Areas.Sections.Models;
using Data;
using EffectiveTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Areas.Sections.WebModels;
using System.Linq;

namespace Areas.Sections.ViewModels
{
    public class CarouselViewModel : Section
    {
        public int IdCarousel { get; set; }

        [Display(Name = "Automatická smyčka")]
        public bool Loop { get; set; }

        [Display(Name = "Doba zobrazení položky")]
        public int LoopTime { get; set; }

        public List<CarouselItemViewModel> Items { get; set; }

        public CarouselViewModel()
        {
            Type = "Carousel";
            Name = "Carousel";
            Description = "Carousel popis";
        }

        public override int Create(int folder)
        {
            // vytvoření sekce
            CarouselModel model = new CarouselModel
            {
                Folder = folder,
                Loop = Loop,
                LoopTime = LoopTime,
            };

            DbContext.Carousels.Add(model);

            //vytvoření položek sekce
            if (Items != null)
            {
                foreach (CarouselItemViewModel item in Items)
                {
                    CarouselDataModel newItem = new CarouselDataModel
                    {
                        Name = item.Name,
                        IdCarousel = model.Id,
                        Link = item.Link,
                        Order = item.Order,
                        Text = item.Text,
                        Title = item.Title
                    };

                    DbContext.CarouselsData.Add(newItem);
                }
            }

            return model.Id;
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new CarouselViewModel();

            var carouesl = DbContext.Carousels.SelectSingleById(contentSection.IdSekce);

            return new CarouselViewModel
            {
                IdContentSection = contentSection.Id,
                FolderId = carouesl.Folder,

                IdCarousel = carouesl.Id,
                Loop = carouesl.Loop,
                LoopTime = carouesl.LoopTime,
                Items = getItems(carouesl.Id)
            };
        }

        public override Section Get(int idSekce)
        {
            var sectionData = DbContext.Carousels.SelectSingleById(idSekce);

            return new CarouselViewModel
            {
                //IdContentSection = contentSection.Id,
                //FolderId = contentSection.Folder,

                IdCarousel = sectionData.Id,
                Loop = sectionData.Loop,
                LoopTime = sectionData.LoopTime,
                Items = getItems(sectionData.Id)
            };
        }

        public override Section Get()
        {
            return new CarouselViewModel
            {
                IdContentSection = 0,
                FolderId = 0,
                Name = Name,
                Items = new List<CarouselItemViewModel>()
            };
        }

        public override Object GetWebModel(int idSekce)
        {
            var sectionData = DbContext.Carousels.SelectSingleById(idSekce);

            return new CarouselWebModel
            {
                Loop = sectionData.Loop,
                LoopTime = sectionData.LoopTime,
                Items = getItems(sectionData.Id)
            };
        }

        public override bool Delete(int id)
        {
            List<CarouselDataModel> items = DbContext.CarouselsData.GetList("IdCarousel = " + id);

            foreach(CarouselDataModel item in items)
            {
                DbContext.CarouselsData.Delete(item.Id);
            }

            return DbContext.Carousels.Delete(id);
        }

        private List<CarouselItemViewModel> getItems(int carouselId)
        {
            List<CarouselItemViewModel> result = new List<CarouselItemViewModel>(); 

            var items = DbContext.CarouselsData.GetList("IdCarousel = " + carouselId);

            foreach (CarouselDataModel item in items)
            {
                result.Add(new CarouselItemViewModel
                {
                    Id = item.Id,
                    Carousel = item.IdCarousel,
                    IdImg = item.IdImg,
                    Link = item.Link,
                    Name = item.Name,
                    Order = item.Order,
                    Text = item.Text,
                    Title = item.Title,
                    Image = DbContext.Image.SelectSingleById(item.IdImg)
                });
            }

            return result.OrderBy(i => i.Order).ToList();
        }
    }
}
