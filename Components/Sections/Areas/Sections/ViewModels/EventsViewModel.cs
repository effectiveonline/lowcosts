﻿using Areas.Sections.Models;
using Areas.Sections.WebModels;
using Data;
using EffectiveTools.Models;
using Sections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Areas.Sections.ViewModels
{
    public class EventsViewModel : Section
    {
        public int IdNewsList { get; set; }

        [Display(Name = "Nadpis")]
        public string Title { get; set; }

        public List<EventItemViewModel> Items { get; set; }

        public List<ContentModel> Events { get; set; }

        public EventsViewModel()
        {
            Type = "Events";
            Name = "Výpis událostí";
            Description = "Tato sekce Vám umožní vložit do stránky výpis událostí z vytvořenách skupin.";
        }

        public override int Create(int folder)
        {
            EventsModel model = new EventsModel
            {
                Folder = folder,
                Title = Title,
            };

            if(Items != null)
            {
                model.Items = Items.Select(i => i.Id.ToString()).ToList().Aggregate((a, b) => a + "," + b);
            }

            DbContext.NewsList.Add(model);

            return model.Id;
        }

        public override void AddItem(ContentSectionModel contentSection)
        {
            var newsList = DbContext.NewsList.SelectSingleById(contentSection.IdSekce);

            newsList.Items += contentSection.IdPage.ToString();

            DbContext.NewsList.Update(newsList);
        }

        public override Section Get(ContentSectionModel contentSection)
        {
            if (contentSection.IdSekce == 0) return new EventsViewModel
            {
                Items = new List<EventItemViewModel>()
            };

            var sectionData = DbContext.NewsList.SelectSingleById(contentSection.IdSekce);

            EventsViewModel result = new EventsViewModel
            {
                IdContentSection = contentSection.Id,
                FolderId = sectionData.Folder,

                IdNewsList = sectionData.Id,
                Title = sectionData.Title,
                Items = new List<EventItemViewModel>()
            };

            if (contentSection.Locked && contentSection.Required)
            {
                result.Events = DbContext.Pages.GetList("Parent = " + contentSection.IdPage);
            }
            else
            {
                var items = DbContext.Pages.GetList("Modul = 'Events' AND Parent = 0");

                if (sectionData.Items == null) sectionData.Items = "";

                var selelctedItems = sectionData.Items.Split(",");

                foreach (ContentModel item in items)
                {
                    result.Items.Add(new EventItemViewModel
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Selected = selelctedItems.Contains(item.Id.ToString())
                    });
                }
            }

            return result;
        }

        public override Section Get(int idSekce)
        {
            var sectionData = DbContext.NewsList.SelectSingleById(idSekce);

            EventsViewModel result = new EventsViewModel
            {
                //IdContentSection = contentSection.Id,
                //FolderId = contentSection.Folder,

                IdNewsList = sectionData.Id,
                Title = sectionData.Title,
                Items = new List<EventItemViewModel>()
            };

            var items = DbContext.Pages.GetList("Modul = 'Events' AND Parent = 0");

            if (sectionData.Items == null) sectionData.Items = string.Empty;

            var selelctedItems = sectionData.Items.Split(",");

            foreach (ContentModel item in items)
            {
                result.Items.Add(new EventItemViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Selected = selelctedItems.Contains(item.Id.ToString())
                });
            }

            return result;
        }

        public override Section Get()
        {
            return new EventsViewModel
            {
                IdContentSection = 0,
                FolderId = 0,
                Name = Name,
            };
        }

        public override Object GetWebModel(int idSekce)
        {
            var sectionData = DbContext.NewsList.SelectSingleById(idSekce);

            EventsWebModel model = new EventsWebModel
            {
                Title = sectionData.Title,
                Items = new List<EventWebItem>()
            };

            if (sectionData.Items == null) sectionData.Items = "";

            var items = sectionData.Items.Split(",");

            foreach(string id in items)
            {
                if (string.IsNullOrEmpty(id)) continue;

                var pages = DbContext.Pages.GetList("Parent = " + id);

                foreach(ContentModel page in pages)
                {
                    model.Items.Add(new EventWebItem 
                    {
                        Title = page.Name,
                        Description = page.Description,
                        Link = page.Url,
                        Created = page.Created
                    });
                }
            }

            model.Items = model.Items.OrderByDescending(i => i.Created).ToList();

            return model;
        }

        public override bool Delete(int id)
        {
            return DbContext.NewsList.Delete(id);
        }
    }
}
