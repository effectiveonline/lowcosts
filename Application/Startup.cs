using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EffectiveTools
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;

            Globals.Inicialize(env.WebRootPath);

            if (!Globals.RunInstall)
            {
                DbContext.Inicialize();
                Globals.AdminConfig = DbContext.AdminConfig.FirstOrDefault();
                Globals.WebConfig = DbContext.WebConfig.FirstOrDefault();
                Globals.SectionShowAsItems = DbContext.SectionShowAsItems.GetList();
                Globals.Company = DbContext.SettingCompany.FirstOrDefault();

                Globals.Securityes.Add(new Security.Application());
                Globals.Securityes.Add(new Security.Pages());
                Globals.Securityes.Add(new Security.Events());
                Globals.Securityes.Add(new Security.PhotoGallery());

                Interface.Services.Templates.FindTemplates(Globals.Folders.Templates);
                Interface.Services.Components.FindComponents(Globals.Folders.Components);
                Interface.Services.Modules.FindPlugins(Globals.Folders.Modules);

                foreach (Interface.Types.AvailablePlugin plugin in Interface.Services.Modules.AvailablePlugins)
                {
                    Globals.AdminMenu.Add(plugin.Instance.AdminMenuItem);

                    Globals.InstaledModules.Add(plugin.Instance);
                    
                }

                Globals.AdminMenu = Globals.AdminMenu.OrderBy(itm => itm.Order).ToList();

                if (!DbContext.Pages.HaveData) DbContext.CreateHomepage(); 
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(
                CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(
                CookieAuthenticationDefaults.AuthenticationScheme,
                options =>
                {
                    options.LoginPath = "/admin/login";
                    options.LogoutPath = "/admin/logout";
                    options.AccessDeniedPath = "/admin/denied";
                });

            services.AddMvc(options =>
            {
          /*
          var policy = new AuthorizationPolicyBuilder()
                           .RequireAuthenticatedUser()
                           .Build();
          options.Filters.Add(new AuthorizeFilter(policy));
          */
                options.EnableEndpointRouting = false;
            })
                .AddXmlSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .ConfigureApplicationPartManager(ConfigureApplicationParts);
        }

        private void ConfigureApplicationParts(ApplicationPartManager apm)
        {
            var modulesFiles = Directory.GetFiles(Globals.Folders.Modules, "*.dll");
            var componentsFiles = Directory.GetFiles(Globals.Folders.Components, "*.dll");
            var templatesFiles = Directory.GetFiles(Globals.Folders.Templates, "*.dll");

            string[] assemblyFiles = modulesFiles.Concat(componentsFiles).Concat(templatesFiles).ToArray();

            foreach (var assemblyFile in assemblyFiles)
            {
                try
                {
                    var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(assemblyFile);

                    if (assemblyFile.EndsWith(this.GetType().Namespace + ".Views.dll") || assemblyFile.EndsWith(this.GetType().Namespace + ".dll"))
                        continue;
                    else if (assemblyFile.EndsWith(".Views.dll"))
                        apm.ApplicationParts.Add(new CompiledRazorAssemblyPart(assembly));
                    else
                        apm.ApplicationParts.Add(new AssemblyPart(assembly));
                }
                catch (Exception e) { }
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseStatusCodePagesWithReExecute("/");
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/");
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );

                routes.MapRoute(
                  name: "default",
                  template: "{controller=Web}/{action=Index}/{id?}"
                );
            });

            /*
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                  name: "areas",
                  pattern: "{area:exists}/{controller=Web}/{action=Index}/{id?}"
                );
            });
            */
        }
    }
}
