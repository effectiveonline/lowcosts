﻿using System;
using System.Security.Claims;
using EffectiveTools.Models;

namespace EffectiveTools
{
    public static class AdminLogger
    {
        public static async void LogEvent(ClaimsPrincipal user, string modul, AdminLoggerAction action, string description)
        {
            if (Globals.AdminConfig.DisableAdminLoging) return;

            var log = new UserLogModel
            {
                UserId = Convert.ToInt32(user.FindFirstValue(ClaimTypes.NameIdentifier)),
                Modul = modul,
                Action = (int)action,
                Description = description,
                Date = DateTime.Now
            };

            await DbContext.LogItems.AddAsync(log);
        }
    }
}
