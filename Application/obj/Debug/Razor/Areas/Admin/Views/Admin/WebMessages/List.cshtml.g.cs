#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "39a10c6ed0529fb5b7d1096659d58dc68a78036e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Admin_WebMessages_List), @"mvc.1.0.view", @"/Areas/Admin/Views/Admin/WebMessages/List.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"39a10c6ed0529fb5b7d1096659d58dc68a78036e", @"/Areas/Admin/Views/Admin/WebMessages/List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Admin_WebMessages_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Data.WebMailModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
 foreach (Data.WebMailModel message in Model)
{
    string color = "";

    if (!message.Readed) color = "c-accent";


#line default
#line hidden
#nullable disable
            WriteLiteral("    <div");
            BeginWriteAttribute("class", " class=\"", 166, "\"", 188, 2);
            WriteAttributeValue("", 174, "message", 174, 7, true);
#nullable restore
#line 9 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
WriteAttributeValue(" ", 181, color, 182, 6, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" data-url=\"/admin/webmessage/");
#nullable restore
#line 9 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
                                                       Write(message.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n        <div class=\"sender\">\r\n            <strong>");
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
               Write(message.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong><br />\r\n            <span class=\"email\">");
#nullable restore
#line 12 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
                           Write(message.Email);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n        </div>\r\n        <div class=\"created-date\">\r\n            ");
#nullable restore
#line 15 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
       Write(message.Created);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n        <a class=\"item-delete\"><i class=\"fa fa-times\"></i></a>\r\n    </div>\r\n");
#nullable restore
#line 19 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Admin\WebMessages\List.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Data.WebMailModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
