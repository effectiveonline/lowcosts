#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fbef42e07a25012e0e565515ff326e2931786b8b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Templates_ContentSections), @"mvc.1.0.view", @"/Areas/Admin/Views/Templates/ContentSections.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fbef42e07a25012e0e565515ff326e2931786b8b", @"/Areas/Admin/Views/Templates/ContentSections.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Templates_ContentSections : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<TemplateSectionModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n<div class=\"dd-list\">\r\n");
#nullable restore
#line 5 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
     for (int i = 0; i < Model.Count; i++)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div");
            BeginWriteAttribute("id", " id=\"", 121, "\"", 152, 2);
            WriteAttributeValue("", 126, "pageSection_", 126, 12, true);
#nullable restore
#line 7 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 138, Model[i].Type, 138, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" data-id=\"");
#nullable restore
#line 7 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
                                             Write(Model[i].Type);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" class=\"panel panel-filled\">\r\n        <div class=\"panel-heading\">\r\n            <div class=\"panel-tools\">\r\n                <a class=\"panel-toggle\" draggable=\"false\"><i class=\"fa fa-chevron-down\"></i></a>\r\n");
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
                 if (!Model[i].Locked || Model[i].Required)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <a class=\"panel-close\" draggable=\"false\"><i class=\"fa fa-times\"></i></a>\r\n");
#nullable restore
#line 14 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("    \r\n            </div>\r\n            ");
#nullable restore
#line 17 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
       Write(Model[i].Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n        <div class=\"panel-body\" style=\"display:none\">\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 730, "\"", 761, 3);
            WriteAttributeValue("", 737, "ContentSections[", 737, 16, true);
#nullable restore
#line 20 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 753, i, 753, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 755, "].Name", 755, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 762, "\"", 784, 1);
#nullable restore
#line 20 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 770, Model[i].Name, 770, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"form-control\" />\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 841, "\"", 870, 3);
            WriteAttributeValue("", 848, "ContentSections[", 848, 16, true);
#nullable restore
#line 21 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 864, i, 864, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 866, "].Id", 866, 4, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 871, "\"", 896, 1);
#nullable restore
#line 21 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 879, Model[i].IdSekce, 879, 17, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 932, "\"", 963, 3);
            WriteAttributeValue("", 939, "ContentSections[", 939, 16, true);
#nullable restore
#line 22 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 955, i, 955, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 957, "].Type", 957, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 964, "\"", 986, 1);
#nullable restore
#line 22 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 972, Model[i].Type, 972, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 1022, "\"", 1054, 3);
            WriteAttributeValue("", 1029, "ContentSections[", 1029, 16, true);
#nullable restore
#line 23 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1045, i, 1045, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1047, "].Order", 1047, 7, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 1055, "\"", 1078, 1);
#nullable restore
#line 23 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1063, Model[i].Order, 1063, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"order\" />\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 1128, "\"", 1159, 3);
            WriteAttributeValue("", 1135, "ContentSections[", 1135, 16, true);
#nullable restore
#line 24 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1151, i, 1151, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1153, "].Type", 1153, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 1160, "\"", 1184, 1);
#nullable restore
#line 24 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1168, Model[i].Locked, 1168, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n            <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 1220, "\"", 1251, 3);
            WriteAttributeValue("", 1227, "ContentSections[", 1227, 16, true);
#nullable restore
#line 25 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1243, i, 1243, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1245, "].Type", 1245, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 1252, "\"", 1278, 1);
#nullable restore
#line 25 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
WriteAttributeValue("", 1260, Model[i].Required, 1260, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n        </div>\r\n    </div>\r\n");
#nullable restore
#line 28 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Templates\ContentSections.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<TemplateSectionModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
