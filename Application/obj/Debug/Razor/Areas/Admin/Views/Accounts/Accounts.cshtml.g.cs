#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "32e9f878cc5df19b7bbcece40f61f4cc0e506715"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Accounts_Accounts), @"mvc.1.0.view", @"/Areas/Admin/Views/Accounts/Accounts.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"32e9f878cc5df19b7bbcece40f61f4cc0e506715", @"/Areas/Admin/Views/Accounts/Accounts.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Accounts_Accounts : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<BasicItemViewModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "ItemsList", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div data-script=\"userAccount\"></div>\r\n\r\n<div class=\"btn-group edit-btns\" data-url=\"/admin/accounts\">\r\n    <button class=\"btn btn-light btn-squared\" type=\"button\" data-action=\"create\" data-alowed=\"");
#nullable restore
#line 5 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
                                                                                          Write(User.IsInRole("app_accounts"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\" data-modalId=\"accountsCreate\">\r\n        <span class=\"pe-7s-plus mr-1\"></span>Vytvořit\r\n    </button>\r\n    <button class=\"btn btn-light btn-squared\" type=\"button\" data-action=\"delete\" data-alowed=\"");
#nullable restore
#line 8 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
                                                                                          Write(User.IsInRole("app_accounts"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n        <span class=\"pe-7s-trash mr-1\"></span>Odstranit\r\n    </button>\r\n    <button class=\"btn btn-light btn-squared\" type=\"button\" data-action=\"save\" data-alowed=\"");
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
                                                                                        Write(User.IsInRole("app_accounts"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n        <span class=\"pe-7s-diskette mr-1\"></span>Uložit\r\n    </button>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-3 col-lg-2 items-list\">\r\n        <div class=\"panel mt-0\">\r\n            <div class=\"panel-body pl-0\">\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "32e9f878cc5df19b7bbcece40f61f4cc0e5067155662", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#nullable restore
#line 19 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => Model);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("for", __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            </div>
        </div>
    </div>
    <div class=""col-9 col-lg-10 item-content pr-1 pt-1"">
        <div class=""container-fluid pl-0"">
            <div class=""row"">
                <div class=""col-12 p-0"">
                    <div class=""basicContent"">
");
#nullable restore
#line 28 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
                         if (Model.Count == 0)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <div class=\"text-center text-danger\">Nemáte vytvořeny žádné uživatele.</div>\r\n");
#nullable restore
#line 31 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Accounts\Accounts.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<BasicItemViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
