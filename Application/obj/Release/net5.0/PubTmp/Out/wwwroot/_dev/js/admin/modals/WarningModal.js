﻿export function WarrningModal() {

  const selector = "#warrningModal";
  const container = document.querySelector(selector);
  const confirmBtn = container.querySelector("#confirm");
  const dismissBtn = container.querySelector('button[data-dismiss="modal"]');

  const _show = (msg) => {

    const message = `
    <span class="pe-7s-close-circle text-danger"></span> 
    ${msg}
    `
    container.querySelector('.message').innerHTML = message;

    $(selector).modal("show");

    $('#confirm').unbind();
    return new Promise((resolve, reject) => {
      confirmBtn.addEventListener('click', _ => { $(selector).modal("hide"); resolve(true); });
      dismissBtn.addEventListener('click', _ => resolve(false));
    });
  }

  return {
    show: _show
  }

}