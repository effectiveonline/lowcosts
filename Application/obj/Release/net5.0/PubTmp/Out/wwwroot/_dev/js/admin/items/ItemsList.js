import * as utils from "../_partials/utils.js";
import { SubItemsList } from "./SubItemsList.js";

export function ItemsList(content, container = document) {
  let activeItem = 0;
  let _content = content;
  let itemsList = null;
  let firstInit = true;
  let subitems = null;

  const basicUrl = _content.getBasicUrl();

  const _initItemsList = async () => {
    itemsList = container.querySelectorAll(".items-list a[data-url]");

    for (let i = 0; i < itemsList.length; i++) {
      if (itemsList[i].getAttribute("data-subitems")) {
        itemsList[i].querySelector(".sub-nav-icon").addEventListener(
          "click",
          (e) => {
            e.stopPropagation();
            subitems = new SubItemsList(
              e.target.parentNode.parentNode,
              basicUrl,
              _content
            );
          },
          { once: true }
        );
      }

      if(itemsList[i].classList.contains('active')) activeItem = i;

      itemsList[i].addEventListener('click', e => _onClick(e));

    }

    if (itemsList.length < 1 && !firstInit) {
      const c = container.querySelector(".basicContent");
      c.innerHTML = "";
      c.append(await utils.getPartialHtml(basicUrl, ".text-danger"));
      _content.setEmpty(true);
      return;
    }

    if (itemsList.length < 1) {
      _content.setEmpty(true);
      return;
    } else if (itemsList.length > 0 && _content.isEmpty()) {
      _content.setEmpty(false);
    }

    if (
      itemsList[activeItem] &&
      !itemsList[activeItem].classList.contains("active")
    ) {
      itemsList[activeItem].classList.add("active");
      itemsList[activeItem].classList.add("show");
    }
  };

  const _setactiveItem = (id) => {
    activeItem = id;

    if (itemsList.length < 1) return;

    if (itemsList[activeItem] === undefined) activeItem = 0;

    if (firstInit) {
      firstInit = false;
    }

    itemsList[activeItem].click();
  };

  const _getactiveItem = () => {
    return activeItem;
  };

  const _onClick = (e) => {
    const active = container.querySelector(".subitems-list .nav-link.active");
    if (active) {
      container
        .querySelector(".subitem-active")
        .classList.remove("subitem-active");
      active.classList.remove("active");
    }

    e.preventDefault();
    if (
      e.target.classList.contains("sub-nav-icon") ||
      e.target.classList.contains("stroke-arrow")
    )
      return;

    if (_content.isUnsave()) return;

    const url = e.target.getAttribute("data-url");
    _content.load(url);

    activeItem = Array.prototype.indexOf.call(itemsList, e.target);

    let id = url.split("/");
    id = id[id.length - 1];
    _content.setActive(id);
  };

  const _reload = async (item) => {
    const res = await utils.getHtml(`${basicUrl}/itemslist`);
    container.querySelector(".items-list .panel .panel-body").innerHTML = res;

    if (res) {
      _initItemsList();

      if (item == -1) _setactiveItem(activeItem);
      else if (item == -2) _setactiveItem(itemsList.length - 1);
      else _setactiveItem(item);
    }
  };

  document.body.addEventListener("itemsChange", (e) => {
   // _initItemsList();

    if (!e.detail) return;

    if ((e.detail.reload = true)) {
      _reload(e.detail.activeItem);
      return;
    }

    _setactiveItem(e.detail.activeItem);
  });

  // utils.addGlobalEventListener("click", ".items-list .nav-link", _onClick, {
  //   capture: true,
  // });

  return {
    init: _initItemsList,
    setItem: _setactiveItem,
    getItem: _getactiveItem,
  };
}
