import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { Edit2 } from "../../_partials/Edit.js";

export function Templates() {
  const container = document.querySelector(".basicContent");
  const btnSave = document.querySelector('button[data-action="save"]');
  const btnDelete = document.querySelector('button[data-action="delete"]');
  const basicUrl = document
    .querySelector(".btn-group")
    .getAttribute("data-url");
  const msgTitle = document.querySelector(".view-header .header-title h3")
    .innerText;

  const content = Edit2(
    {
      container: container,
      btnSave: btnSave,
      basicUrl: basicUrl,
      msgTitle: msgTitle,
      btnDelete: btnDelete,
    },
    {
      listClass: "individual-layout",
      itemToAddClass: "avalible-sections",
      form: "templateEdit",
      inputName: "Sections[@]",
      onChange: () => content.setUnsave(true),
    }
  );

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  if (btnSave) {
    btnSave.addEventListener("click", (_) => content.submit());

    container.addEventListener("submitFinish", (e) =>
      content.submitFinished(e, ".basicContent")
    );
  }

  if (btnDelete) btnDelete.addEventListener("click", (_) => content.delete());

  utils.addGlobalEventListener(
    "click",
    ".nav-link",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    ".luna-nav a",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    "button[data-modalid='createModal']",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );
}