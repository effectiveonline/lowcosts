import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { Draggable } from "../_partials/dragable.js";

export function CreateTemplate(url, id) {
  const dragOpt = {
    listClass: "individual-layout",
    itemToAddClass: "avalible-sections",
    form: null,
    inputName: "Sections[@]",
  };

  const modal = Modal(url, id);

  let container = null;
  let sectionsList, btnSave;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');

    btnSave.addEventListener("click", (_) => {
      sectionsList.sortOrder();
      modal.submit();
    });

    //_individualLayoutHandle();

    sectionsList = Draggable(container, dragOpt);
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad();
    else {
      document.body.dispatchEvent(
        new CustomEvent("itemsChange", {
          bubbles: true,
          detail: { reload: true, activeItem: -2 },
        })
      );
    }
  };

  //checkbox individuální rozložení
  // const _individualLayoutHandle = () => {
  //   const sections = container.querySelector(".sections");
  //   const layoutSelect = container.querySelector("#Template");

  //   if (layoutSelect) {
  //     layoutSelect.addEventListener("change", async () => {
  //       sections.innerHTML = await utils.getHtml(
  //         `/admin/templates/getsections/${layoutSelect.value}`
  //       );

  //       utils.initScripts(".sections");

  //       sectionsList = Draggable(container, dragOpt);
  //     });
  //   }

  //   const inLayout = container.querySelector("input#CustomSectionsNpage");
  //   if (inLayout) {
  //     inLayout.checked
  //       ? $("#createSectionsList").show(200)
  //       : $("#createSectionsList").hide(200);
  //     inLayout.addEventListener("change", () => {
  //       inLayout.checked
  //         ? $("#createSectionsList").show(200)
  //         : $("#createSectionsList").hide(200);
  //     });
  //   }
  // };


  return {
    init: _init,
  };
}
