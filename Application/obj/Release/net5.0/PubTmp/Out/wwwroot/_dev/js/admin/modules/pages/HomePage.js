import * as utils from "../../_partials/utils.js";
import { FormsHandle } from "../../_partials/_form.js";
import { Draggable } from "../../_partials/dragable.js";
import { WarrningModal } from "../../modals/WarningModal.js";
import { CustomEvents } from "../../_partials/customEvents.js";

export function HomePage() {
  const container = document.querySelector("#mainContainer");
  let btnSave = document.querySelector('button[data-action="save"]');
  const formsHandle = FormsHandle();
  const events = CustomEvents();
  const body = document.body;
  let timer;
  let unsave = false;

  const dragOpt = {
    listClass: "individual-layout",
    itemToAddClass: "avalible-sections",
    form: null,
    inputName: "ContentSections[@]",
  };

  let draggable = Draggable(container, dragOpt);

  const _unSave = () => {
    console.log("unsave");
    btnSave.classList.remove("btn-light");
    btnSave.classList.add("btn-accent");
    unsave = true;
  };

  const _removeUnsave = () => {
    if (btnSave.classList.contains("btn-accent")) {
      btnSave.classList.remove("btn-accent");
      btnSave.classList.add("btn-light");
    }
    unsave = false;
  };

  const _unsaveWarning = async (e) => {
    if (!unsave || e.target.hasAttribute("aria-expanded")) return;

    e.stopImmediatePropagation();
    e.preventDefault();

    const warning = WarrningModal();
    const confirm = await warning.show("Máte neuložené změny. Pokračovat?");

    if (!confirm) return;

    _removeUnsave();

    e.target.click();
    return;
  };

  const _onSubmit = () => {
    if (btnSave.getAttribute("data-alowed") === "False") {
      const modal = Modal("/admin/denied-modal", "denied");
      modal.init();
      return;
    }

    draggable.sortOrder();

    timer = setTimeout(() => {
      body.dispatchEvent(events.showLoader);
    }, 100);

    formsHandle.onSubmit(container);
  };

  const _onSubmitFinish = (e) => {
    const res = e.detail.result.filter((r) => r.response !== "true");
    let error = null;

    if (res.length > 0) {
      res.forEach((r) => {
        if (r.response !== "false") {
          const form = document.querySelector(`#${r.id}`);
          if (form) form.innerHTML = r.response;
          utils.initScripts(`#${form.id}`);
        } else {
          error = "Nepodařilo se uložit změny";
        }
      });

      utils.initUnsaveWarning(container, _unSave);
      draggable = Draggable(container, dragOpt);
      btnSave = document.querySelector('button[data-action="save"]');

      const errorMsg = container.querySelector(
        ".text-danger.field-validation-error"
      );
      if (errorMsg) error = errorMsg.innerText;

      if (error) {
        toastr["error"](error, "Titulní stránka");
      } else {
        toastr["success"]("Změny byly uloženy", "Titulní stránka");
      }
    } else toastr["error"]("Nepodařilo se uložit změny", "Titulní stránka");

    _removeUnsave();
    clearTimeout(timer);
    body.dispatchEvent(events.hideLoader);
  };

  utils.addGlobalEventListener("click", 'button[data-action="save"]', (e) => {
    draggable.sortOrder();
    _onSubmit();
  });
  container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));
  utils.addGlobalEventListener("click", ".luna-nav a", _unsaveWarning, {
    capture: true,
  });

  utils.initUnsaveWarning(container, _unSave);

  utils.addGlobalEventListener("click", ".nav-link", removeActiveTab, {
    capture: true,
  });
}

function removeActiveTab(e) {
  document.querySelector(".nav-link.active").classList.remove("active");
  if (e.target.classList.contains("sub-link"))
    document.querySelector("a.sub-tab").classList.add("active");
  else document.querySelector("a.sub-tab").classList.remove("active");
}
