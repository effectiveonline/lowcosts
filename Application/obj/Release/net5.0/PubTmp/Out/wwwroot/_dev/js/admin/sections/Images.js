import { ContentManager } from "../content-managers/ContentManager.js";
import { ImagesManager } from "../modals/ImagesManager.js";


export async function Images(pageId, parentContainer) {
  //variables
  const addBtn = parentContainer.querySelector(
    'button[data-action="addImage"]'
  );
  const imgsContainer = parentContainer.querySelector(".images-container");
  const imgsManager = ImagesManager(
    addBtn.getAttribute("data-url"),
    "imagesModal"
  );

  let sidebarContent, contentManager;
  let imgs = imgsContainer.querySelectorAll("img");

  //methods
  const _onAddClick = async () => {

    const modalContent = await imgsManager.init();

    _initContentManager(modalContent);

    modalContent
      .querySelector(".basicContent")
      .addEventListener("contentLoad", (e) =>
        _initContentManager(modalContent)
      );

    modalContent
      .querySelector('button[data-action="save"]')
      .addEventListener("click", (e) => _addImages());
  };

  const _initContentManager = (modalContent) => {
    contentManager = new ContentManager(modalContent);

    contentManager.setCopyOnClick("img", null, false, null);
  };

  const _addImages = () => {
    imgsManager.closeModal();

    const images = contentManager.getSelectedItems();

    images.forEach((img) => {
      const el = document.createElement("div");
      el.classList = "panel panel-filled mb-0";
      el.innerHTML = `
      <div class="panel-heading">
        <div class="panel-tools">
          <a class="item-delete"><i class="fa fa-times"></i></a>
        </div>
        ${img.getAttribute('alt')}
      </div>
      <div class="panel-body mb-0">
        <input type="hidden" name="Images[${imgs.length}].Image" value="${ img.id }" />
        <input type="hidden" class="order" name="Images[${imgs.length}].Order" value="${imgs.length + 1}" />
      </div>
      `;

      img.classList.remove("selected");
      
      el.querySelector('.panel-body').appendChild(img);

      imgsContainer.appendChild(el);
      imgs = imgsContainer.querySelectorAll("img");
    });
  };

  //listeners
  addBtn.addEventListener("click", (e) => _onAddClick());
}
