import { ItemsList } from "./ItemsList.js";
import { getHtml } from "../_partials/utils.js";

export class SubItemsList {
  constructor(parent, basicUrl, content) {
    this.parent = parent;
    this.content = content;
    this.activeItem = null;
    this.items = null;

    this.id = parent.getAttribute("data-url");
    if (this.id) {
      this.id = this.id.split("/");
      this.id = this.id[this.id.length - 1];
    }

    this.url = basicUrl;

    this.list = parent.parentNode.querySelector(".subitems-list");

    this.init();
  }

  async init() {
    this.list.innerHTML = await getHtml(`${this.url}/subitems/${this.id}`);

    this.list.classList.add("expanded");
    this.list.parentNode.setAttribute("data-expanded", "expanded");

    const expandBtn = this.parent.querySelector(".sub-nav-icon");
    if (expandBtn) {
      expandBtn.addEventListener("click", (e) => {
        e.stopPropagation();
        this.list.classList.toggle("expanded");
        const isExpand = this.list.classList.contains("expanded")
          ? "expanded"
          : "collapse";
        this.list.parentNode.setAttribute("data-expanded", isExpand);
      });
    }

    this.items = this.list.querySelectorAll(".nav-link");

    this.items.forEach((item) => {
      if (item.getAttribute("data-subitems")) {
        item.querySelector(".sub-nav-icon").addEventListener(
          "click",
          (e) => {
            e.stopPropagation();
            const subitem = new SubItemsList(
              e.target.parentNode.parentNode,
              this.url
            );
          },
          { once: true }
        );
      }

      item.addEventListener("click", (e) => this.onClick(e));
    });
  }

  onClick(e) {
    this.list.parentNode
      .querySelector(".nav-link")
      .classList.add("subitem-active");

    const url = e.target.getAttribute("data-url");
    this.content.load(url);

     this.activeItem = Array.prototype.indexOf.call(itemsList, e.target);

    let id = url.split("/");
    id = id[id.length - 1];
    this.content.setActive(id);
  }

  getParent() { return this.parent }

  getActiveItem() { return this.items[this.activeItem] }
}
