import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { Draggable } from "../_partials/dragable.js";
import { FormsHandle } from "../_partials/_form.js";
import { ReloadEvents } from "../modules/news/News.js";

export function CreateEvent(url, id) {
  const dragOpt = {
    listClass: "individual-layout",
    itemToAddClass: "avalible-sections",
    form: null,
    inputName: "ContentSections[@]",
  };

  const modal = Modal(url, id);

  let container = null,
    sectionsList,
    btnSave,
    formsHandle = FormsHandle();

  const _init = async () => {
    container = await modal.init();
    if (container) {
      btnSave = container.querySelector('button[data-action="save"]');
      btnSave.addEventListener("click", _onSubmit);
      _afterLoad();
    }
  };

  const _afterLoad = () => {
    //_individualLayoutHandle();
    utils.hardUrlHandle(container);
    sectionsList = Draggable(container, dragOpt);
  };

  const _onSubmit = async () => {
    const propertiesForm = container.querySelector("#createProperties");
    const req = await formsHandle.submitForm(propertiesForm);

    const pageId = validateForm(propertiesForm, req.response);

    if (!pageId) return;

    // formuláře pro sekce obsahu
    const sections = container.querySelectorAll(".section-tab");

    for (let i = 0; i < sections.length; i++) {
      const section = sections[i];
      const form = section.querySelector("form");

      form.setAttribute("action", `${form.getAttribute("action")}/${pageId}`);

      const res = await formsHandle.submitForm(form);

      const sectionId = res.response;

      _setId(section, sectionId);
    }

    sectionsList.sortOrder();

    // formulář pro update content sections
    const sectionsForm = document.querySelector("#createSections");

    sectionsForm.setAttribute(
      "action",
      `${sectionsForm.getAttribute("action")}/${pageId}`
    );

    formsHandle.submitForm(sectionsForm);

    modal.closeModal();
    toastr["success"](
      "Vytvořeno",
      container.querySelector(".view-header .header-title h3").innerText
    );

    // přenačtení po submitu podle typu
    const itemToActive = id !== "createEvent" ? -2 : -1;
    
    document.body.dispatchEvent(
      new CustomEvent("itemsChange", {
        bubbles: true,
        detail: { reload: true, activeItem: itemToActive },
      })
    );
  };

  const validateForm = (form, res) => {
    if (parseInt(res)) return res;

    const msgTitle = container.querySelector(".view-header .header-title h3")
      .innerText;

    let errMsg = "";

    form.innerHTML = res;

    utils.initScripts(`#${form.id}`);
    //_individualLayoutHandle();
    utils.hardUrlHandle(container);

    const error =
      container.querySelector(`.text-danger.field-validation-error`) ||
      container.querySelector(".field-validation-error");

    if (error) errMsg = error.innerText;

    toastr["error"](errMsg, msgTitle);

    return false;
  };

  //checkbox individuální rozložení
  const _individualLayoutHandle = () => {
    const sections = container.querySelector(".sections");
    const layoutSelect = container.querySelector("#Template");

    if (layoutSelect) {
      layoutSelect.addEventListener("change", async () => {
        sections.innerHTML = await utils.getHtml(
          `/admin/templates/getsections/${layoutSelect.value}`
        );

        utils.initScripts(".sections");

        sectionsList = Draggable(container, dragOpt);
      });
    }

    const inLayout = container.querySelector("input#CustomSectionsNpage");
    if (inLayout) {
      inLayout.checked
        ? $("#createSectionsList").show(200)
        : $("#createSectionsList").hide(200);
      inLayout.addEventListener("change", () => {
        inLayout.checked
          ? $("#createSectionsList").show(200)
          : $("#createSectionsList").hide(200);
      });
    }
  };
  
  //přenesení id vytvořené sekce do rozložení
  const _setId = (section, sectionId) => {
    let tabId = section.id.split("-");
    tabId = tabId[tabId.length - 1];

    container.querySelector(
      `input[name="ContentSections[${tabId}].IdSekce"]`
    ).value = sectionId;
  };

  return {
    init: _init,
  };
}
