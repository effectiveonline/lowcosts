import { Modal } from "./Modal.js";
import { Edit } from "../_partials/Edit.js";
import { ItemsList } from "../items/ItemsList.js";
import { FormsHandle } from "../_partials/_form.js";

export function ImagesManager(url, id) {
  const modal = Modal(url, id);
  const formsHandle = FormsHandle();

  let container = null;
  let btnSave, content, uploadForm ,uploadUrl;


  //methods
  const _init = async () => {
    container = await modal.init();
    if (container) {
      

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {

    content = Edit(
      {
        container: container.querySelector('.basicContent'),
        btnSave: btnSave,
        basicUrl: url,
        msgTitle: 'Image Manager',
        useUnsave: false
        //subPageBtn: subPageBtn,
        //btnDelete: btnDelete,
      }
    );

    const itemsList = ItemsList(content, container);

    itemsList.init();
    //itemsList.setItem(1);

    uploadForm = container.querySelector('form#uploadImage');
    uploadUrl = uploadForm.getAttribute('action');

    container.querySelector('input#Image')
      .addEventListener('change', _onUpload);
  }


  const _onUpload = async (e) => {
    

    uploadForm.setAttribute('action', `${uploadUrl}/${content.getActive()}`)

    const res = await formsHandle.submitForm(uploadForm);

    container.querySelector('.basicContent').innerHTML = res.response;

    content.getContainer().dispatchEvent(
      new CustomEvent("contentLoad", {
        bubbles: true,
      })
    );

  };

  //listeners

  return {
    ...modal,
    init: _init,
  };
}
