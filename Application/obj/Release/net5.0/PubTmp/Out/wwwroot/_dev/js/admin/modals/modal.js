import * as utils from "../_partials/utils.js";
import { CustomEvents } from "../_partials/customEvents.js";
import { FormsHandle } from "../_partials/_form.js";

import { BasicModal } from "./BasicModal.js";
import { CreateModal as cModal } from "./createModal.js";
import { CreateAccountModal as caModal } from "./CreateAccountModal.js";
import { CreateEvent } from "./CreateEvent.js";
import { EditEvent } from "./EditEvent.js";
import { CreateTemplate } from "./CreateTemplate.js";

export function Modal(url, modalId) {
  const modalSelector = `.modalBx #${modalId}`;
  const events = CustomEvents();
  const formsHandle = FormsHandle();
  const body = document.body;

  let modalContainer;
  let timer = null;

  /*
  ----  Methods ----
  */
  const _init = async () => {
    modalContainer = _createModal();

    const modalContent = modalContainer.querySelector(".modal-content");
    modalContent.innerHTML = await utils.getHtml(url);

    utils.initScripts(modalSelector);

    $(modalSelector).modal("show");

    // smazání modalu po zavření
    const btnClose = modalContainer.querySelector(
      `${modalSelector} button.btn.btn-default[data-dismiss="modal"]`
    );

    btnClose.addEventListener(
      "click",
      (e) => {
        e.stopImmediatePropagation();
        _closeModal();
      },
      { capture: true }
    );

    modalContainer.addEventListener(
      "keydown",
      (e) => {
        if (e.key === "Escape") {
          e.stopImmediatePropagation();
          _closeModal();
        }
      },
      { capture: true }
    );

    return modalContainer;
  };

  const _createModal = () => {
    modalContainer = document.createElement("div");
    modalContainer.classList.add("modalBx");
    modalContainer.innerHTML = `
      <div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
         <div class="modal-dialog modal-lg"> 
              <div class="modal-content"></div>
          </div>
      </div>
    `;

    document.body.append(modalContainer);
    return modalContainer;
  };

  const _onSubmit = (forms = null) => {
    timer = setTimeout(() => {
      body.dispatchEvent(events.showLoader);
    }, 0);

    formsHandle.onSubmit(modalContainer, forms);
  };

  const _onSubmitFinish = (e, responseTarget = null, closeModal = true) => {
    let res = e.detail.result;
    let invalid = res.filter((r) => r.response !== "true");
    const msgTitle = document.querySelector(".view-header .header-title h3")
      .innerText;
    let errMsg = "";

    if (invalid.length > 0) {
      invalid.forEach((f, index) => {
        const targetSelector = responseTarget ? responseTarget : `#${f.id}`;
        const target = document.querySelector(targetSelector);
        target.innerHTML = f.response;
        utils.initScripts(targetSelector);
      });

      clearTimeout(timer);
      body.dispatchEvent(events.hideLoader);

      const error =
        modalContainer.querySelector(`.text-danger.field-validation-error`) ||
        modalContainer.querySelector(".field-validation-error");
      if (error) errMsg = error.innerText;

      toastr["error"](errMsg, msgTitle);

      return false;
    }

    clearTimeout(timer);
    body.dispatchEvent(events.hideLoader);

    if(closeModal) _closeModal();

    toastr["success"]("Změny byly uloženy", msgTitle);

    return true;
  };

  const _closeModal = () => {
    $(modalSelector).modal("hide");
    console.log("modal delete");
    body.removeChild(document.querySelector(".modalBx"));
  };

  return {
    init: _init,
    closeModal: _closeModal,
    creatModal: _createModal,
    submit: _onSubmit,
    submitFinish: _onSubmitFinish,
    selector: modalSelector,
  };
}

const modalTypes = [
  { id: "", modal: BasicModal },
  { id: "createModal", modal: cModal },
  { id: "createEvent", modal: CreateEvent },
  { id: "createEventGroup", modal: CreateEvent },
  { id: "eventEdit", modal: EditEvent },
  { id: "accountsCreate", modal: caModal },
  { id: "createGallery", modal: cModal },
  { id: "createTemplate", modal: CreateTemplate },
];

export function GetModal(id) {
  let match = modalTypes.find((type) => type.id === id);

  if (!match) match = modalTypes[0];

  return match;
}
