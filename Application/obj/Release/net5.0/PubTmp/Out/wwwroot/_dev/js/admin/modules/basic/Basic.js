import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { Edit } from "../../_partials/Edit.js";

export function Basic() {
  const container = document.querySelector(".basicContent");
  const btnSave = document.querySelector('button[data-action="save"]');
  const btnDelete = document.querySelector('button[data-action="delete"]');
  const basicUrl = document
    .querySelector(".btn-group")
    .getAttribute("data-url");
  const msgTitle = document.querySelector(".view-header .header-title h3")
    .innerText;

  const content = Edit({
    container: container,
    btnSave: btnSave,
    basicUrl: basicUrl,
    msgTitle: msgTitle,
  });

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  const _onDelete = async () => {
    const allowDelete =
      btnDelete.getAttribute("data-alowed") === "False" ? false : true;

    const res = await content.delete(
      allowDelete,
      `${basicUrl}/delete/${content.getActive()}`
    );

    if (res) {
      if (res === "true") {
        content.reload();
        toastr["success"]("smazáno", msgTitle);
      } else toastr["error"](res, msgTitle);
    }
  };

  if (btnSave) {
    btnSave.addEventListener("click", (e) => content.submit());
    container.addEventListener("submitFinish", (e) =>
      content.submitFinished(e)
    );
  }

  if (btnDelete) btnDelete.addEventListener("click", (_) => _onDelete());

  utils.addGlobalEventListener(
    "click",
    ".nav-link",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    ".luna-nav a",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );
}
