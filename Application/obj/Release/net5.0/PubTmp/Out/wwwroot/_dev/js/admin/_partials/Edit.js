import * as utils from "./utils.js";
import { CustomEvents } from "./customEvents.js";
import { FormsHandle } from "./_form.js";
import { WarrningModal } from "../modals/WarningModal.js";
import { BasicModal as Modal } from "../modals/BasicModal.js";
import { Draggable } from "../_partials/dragable.js";
import { InitSectionsScript } from "../sections/Sections.js";

export function Edit(settings) {
  const container = settings.container;
  const btnSave = settings.btnSave;
  const basicUrl = settings.basicUrl;
  const msgTitle = settings.msgTitle;
  const events = CustomEvents();
  const body = document.body;
  const formsHandle = FormsHandle();
  const useUnsave = 
    settings.useUnsave != undefined || settings.useUnsave != null
      ? settings.useUnsave
      : true;

  let activeId = 0;
  let timer = null;
  let firstLoad = true;
  let isUnsave = false;
  let tabs, activeTab;
  let isEmpty = true;

  const _init = () => {
    utils.initScripts(`.basicContent`, container);

    if (useUnsave) {
      utils.initUnsaveWarning(container, _unSave);
      _removeUnsave();
    }

    /* zapamatování aktivního tabu po přenačtení */
    tabs = container.querySelectorAll('a[data-toggle="tab"]');

    if (tabs.length > 0) {
      if (firstLoad) {
        activeTab = {
          itemId: activeId,
          tab: tabs[0].getAttribute("href"),
        };
        firstLoad = false;
      }

      activeTab =
        activeTab.itemId === activeId
          ? activeTab
          : (activeTab = {
              itemId: activeId,
              tab: tabs[0].getAttribute("href"),
            });

      const t = container.querySelector(`a.nav-link[href="${activeTab.tab}"]`);
      if (t) t.click();

      tabs.forEach((tab) =>
        tab.addEventListener("click", (_) => {
          activeTab.itemId = activeId;
          activeTab.tab = tab.getAttribute("href");
        })
      );
    }

    //init pro scripty jednotlivých sekcí
    InitSectionsScript(container, activeId);
  };

  const _loadContent = async (url) => {
    if (!firstLoad)
      timer = setTimeout(() => {
        body.dispatchEvent(events.showLoader);
      }, 100);

    const res = await utils.getHtml(url);
    container.innerHTML = res;

    clearTimeout(timer);
    body.dispatchEvent(events.hideLoader);

    _init();

    container.dispatchEvent(
      new CustomEvent("contentLoad", {
        bubbles: true,
      })
    );

    return true;
  };

  const _unSave = () => {
    btnSave.classList.remove("btn-light");
    btnSave.classList.add("btn-accent");
    isUnsave = true;
  };

  const _removeUnsave = () => {
    if (btnSave.classList.contains("btn-accent")) {
      btnSave.classList.remove("btn-accent");
      btnSave.classList.add("btn-light");
    }
    isUnsave = false;
  };

  const _onSubmit = (formsArr) => {
    if (btnSave.getAttribute("data-alowed") === "False") {
      const modal = Modal("/admin/denied-modal", "denied");
      modal.init();
      return;
    }

    timer = setTimeout(() => {
      body.dispatchEvent(events.showLoader);
    }, 100);

    formsHandle.onSubmit(container, formsArr);
  };

  const _onSubmitFinish = (e, responseTarget = null) => {
    let res = e.detail.result;
    let invalid = res.filter((r) => r.response !== "true");
    let errMsg = "";

    if (invalid.length > 0) {
      invalid.forEach((f, index) => {
        const targetSelector = responseTarget ? responseTarget : `#${f.id}`;
        const target = document.querySelector(targetSelector);
        target.innerHTML = f.response;
        utils.initScripts(targetSelector);
      });

      const error =
        container.querySelector(`.text-danger.field-validation-error`) ||
        container.querySelector(".field-validation-error");
      if (error) errMsg = error.innerText;

      clearTimeout(timer);
      body.dispatchEvent(events.hideLoader);

      toastr["error"](errMsg, msgTitle);

      return false;
    }

    _reload();

    const errors = document.querySelectorAll(".text-danger");
    for (let i = 0; i < errors.length; i++) {
      errors[i].parentNode.removeChild(errors[i]);
    }
    console.log("success submit");

    clearTimeout(timer);
    body.dispatchEvent(events.hideLoader);

    toastr["success"]("Změny byly uloženy", msgTitle);

    _removeUnsave();

    return true;
  };

  const _getActiveId = () => activeId;
  const _setActiveId = (id) => (activeId = id);

  const _onDelete = async (allowDelete, url) => {
    if (isEmpty) return;

    if (!allowDelete) {
      const modal = Modal("/admin/denied-modal", "denied");
      modal.init();
      return false;
    }

    const warning = WarrningModal();
    const confirm = await warning.show(
      "Opravdu chcete trvale odstranit tuto položku?"
    );

    if (!confirm) return;

    const response = await utils.getHtml(url);

    return response;
  };

  const _reload = async (setItem = -1) => {
    document.body.dispatchEvent(
      new CustomEvent("itemsChange", {
        bubbles: true,
        detail: { reload: true, activeItem: setItem },
      })
    );
  };

  const _getIsUnsave = () => {
    return isUnsave;
  };
  const _setIsUnsave = (param) => {
    if (param && useUnsave) _unSave();
    else isUnsave = false;
  };

  const _getIsEmpty = () => {
    return isEmpty;
  };
  const _setIsEmpty = (param) => {
    isEmpty = param;
  };

  const _getBasicUrl = () => {
    return basicUrl;
  } 
  
  const _getContainer = () => {
    return container;
  } 



  body.addEventListener("unsave", (e) => _setIsUnsave(true));

  return {
    init: _init,
    load: _loadContent,
    getActive: _getActiveId,
    setActive: _setActiveId,
    isUnsave: _getIsUnsave,
    setUnsave: _setIsUnsave,
    submit: _onSubmit,
    submitFinished: _onSubmitFinish,
    delete: _onDelete,
    reload: _reload,
    isEmpty: _getIsEmpty,
    setEmpty: _setIsEmpty,
    getBasicUrl: _getBasicUrl,
    getContainer: _getContainer
  };
}

export function Edit2(settings, dragSettings) {
  const basic = Edit({
    container: settings.container,
    btnSave: settings.btnSave,
    basicUrl: settings.basicUrl,
    msgTitle: settings.msgTitle,
    useUnsave: settings.useUnsave
      
  });

  const subPageBtn = settings.subPageBtn;
  const btnDelete = settings.btnDelete;

  let drag, basicSubpageUrl;

  const _init = () => {
    if (subPageBtn && !basic.isEmpty()) {
      if (!basicSubpageUrl)
        basicSubpageUrl = subPageBtn.getAttribute("data-url");
      let uri = `${basicSubpageUrl}/${basic.getActive()}`;
      subPageBtn.setAttribute("data-url", uri);
    }

    drag = Draggable(settings.container, dragSettings);

    utils.hardUrlHandle(settings.container);
  };

  const _loadContent = async (url) => {
    const res = await basic.load(url);
    if (res) _init();
  };

  const _onSubmit = (formsArr = null) => {
    drag.sortOrder();
    basic.submit(formsArr);
  };

  const _onSubmitFinish = (e, responseTarget = null) => {
    const success = basic.submitFinished(e, responseTarget);
    if (!success) drag = Draggable(settings.container, dragSettings);
  };

  const _onDelete = async () => {
    const allowDelete =
      btnDelete.getAttribute("data-alowed") === "False" ? false : true;

    const res = await basic.delete(
      allowDelete,
      `${settings.basicUrl}/delete/${basic.getActive()}`
    );

    if (res) {
      if (res === "true") {
        basic.reload();
        toastr["success"]("smazáno", settings.msgTitle);
      } else toastr["error"](res, settings.msgTitle);
    }
  };

  const _setIsEmpty = (param) => {
    if(subPageBtn)
      param ? (subPageBtn.disabled = true) : (subPageBtn.disabled = false);

    basic.setEmpty(param);
  };

  return {
    ...basic,
    init: _init,
    load: _loadContent,
    submit: _onSubmit,
    delete: _onDelete,
    deleteItem: basic.delete,
    submitFinished: _onSubmitFinish,
    setEmpty: _setIsEmpty,
  };
}
