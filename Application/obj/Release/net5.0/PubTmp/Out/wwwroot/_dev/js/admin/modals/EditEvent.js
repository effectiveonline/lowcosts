import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { Draggable } from "../_partials/dragable.js";
import { FormsHandle } from "../_partials/_form.js";
import { ReloadEvents } from "../modules/news/News.js";

export function EditEvent(url, id) {
  const dragOpt = {
    listClass: "individual-layout",
    itemToAddClass: "avalible-sections",
    form: null,
    inputName: "ContentSections[@]",
  };

  const modal = Modal(url, id);

  let container = null,
    sectionsList,
    btnSave,
    btnSaveAndClose,
    clsMdlAfterSubmit,
    formsHandle = FormsHandle(),
    tabs,
    activeTab;

  /**
   * Methods
   */

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();

    }
  };

  const _afterLoad = () => {
    console.log(activeTab)

     /* zapamatování aktivního tabu po přenačtení */
     tabs = container.querySelectorAll('a[data-toggle="tab"]');

     if (tabs.length > 0) {

       tabs.forEach((tab) =>
         tab.addEventListener("click", (_) => {
           activeTab = tab.getAttribute("href");
           
         })
       );
     }

    if(activeTab) {
      const t = container.querySelector(`a.nav-link[href="${activeTab}"]`);
      
      if (t) t.click();
    }

    btnSave = container.querySelector('button[data-action="save"]');
    btnSaveAndClose = container.querySelector(
      'button[data-action="saveandclose"]'
    );

    btnSave.addEventListener("click", (e) => _onSubmit(false));
    btnSaveAndClose.addEventListener("click", (e) => _onSubmit(true));

    //_individualLayoutHandle();
    utils.hardUrlHandle(container);
    sectionsList = Draggable(container, dragOpt);
  };

  const _onSubmit = async (closeModal) => {
    clsMdlAfterSubmit = closeModal;

    const propertiesForm = container.querySelector("#editEventProperties");
    const req = await formsHandle.submitForm(propertiesForm);

    if (req.response !== "true") {
      propertiesForm.innerHTML = req.response;
      utils.initScripts(`#${propertiesForm.id}`);
      utils.hardUrlHandle(container);

      const msgTitle = container.querySelector(".view-header .header-title h3")
        .innerText;

      let errMsg = "";

      const error =
        container.querySelector(`.text-danger.field-validation-error`) ||
        container.querySelector(".field-validation-error");

      if (error) errMsg = error.innerText;

      toastr["error"](errMsg, msgTitle);

      return;
    }

    sectionsList.sortOrder();

    const remainingForms = container.querySelectorAll(
      `form:not(#${propertiesForm.id})`
    );

    modal.submit(remainingForms);
  };

  const _onSubmitFinish = async (e) => {
    if (clsMdlAfterSubmit) {
      modal.submitFinish(e);
    } else {
      modal.submitFinish(e, null, false);
      container.querySelector(".modal-content").innerHTML = await utils.getHtml(
        url
      );
      utils.initScripts(`#${id}`);

      container.querySelector('button.btn.btn-default[data-dismiss="modal"]').addEventListener(
        "click",
        (e) => {
          e.stopImmediatePropagation();
          modal.closeModal();
        },
        { capture: true }
      );

      _afterLoad();
    }

   ReloadEvents();
  };

  //checkbox individuální rozložení
  const _individualLayoutHandle = () => {
    const sections = container.querySelector(".sections");
    const layoutSelect = container.querySelector("#Template");

    if (layoutSelect) {
      layoutSelect.addEventListener("change", async () => {
        sections.innerHTML = await utils.getHtml(
          `/admin/templates/getsections/${layoutSelect.value}`
        );

        utils.initScripts(".sections");

        sectionsList = Draggable(container, dragOpt);
      });
    }

    const inLayout = container.querySelector("input#CustomSectionsNpage");
    if (inLayout) {
      inLayout.checked
        ? $("#createSectionsList").show(200)
        : $("#createSectionsList").hide(200);
      inLayout.addEventListener("change", () => {
        inLayout.checked
          ? $("#createSectionsList").show(200)
          : $("#createSectionsList").hide(200);
      });
    }
  };

  return {
    init: _init,
  };
}
