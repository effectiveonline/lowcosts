import "../../../../../lib/nestable/nestable.js";
import { Sidebar } from "../../_partials/sidebar.js";
import { Basic } from "../basic/Basic.js";
import { ContentManager } from "../../content-managers/ContentManager.js";
import { sortList, onItemRemove } from "../../_partials/dragable.js";
import { Edit } from "../../_partials/Edit.js";
import { ItemsList } from "../../items/ItemsList.js";
import { SubItemsList } from "../../items/SubItemsList.js";

export function Menu() {
  const sidebar = new Sidebar();
  const addBtn = document.querySelector("button[data-action='create']");
  const saveBtn = document.querySelector("button[data-action='save']");
  let contentManager = null;
  const container = document.querySelector(".basicContent");
  let emptyList = true;
  const basicUrl = document
    .querySelector(".btn-group")
    .getAttribute("data-url");
  const msgTitle = document.querySelector(".view-header .header-title h3")
    .innerText;

  const edit = Edit({
    container: container,
    btnSave: saveBtn,
    basicUrl: basicUrl,
    msgTitle: msgTitle,
  });

  let list;

  const itemsList = ItemsList(edit);
  itemsList.init();
  itemsList.setItem(0);

  const afterLoad = () => {
    emptyList = container.querySelector(".dd-item") ? false : true;
    list = container.querySelector(".dd-container");

    Sortable.create(list, {
      group: {
        name: "menuItems",
        pull: "clone",
        put: true,
      },
      sort: true,
      animation: 150,
      handle: ".dd-item",
      ghostClass: "dd-placeholder",
      forceFallback: true,
      fallbackClass: "dragged-cursor",
      onAdd: function (evt) {
        itemAdded(evt);
      },
    });

    onItemRemove(list.querySelectorAll(".item-delete"));
  };

  const itemAdded = (evt) => {
    console.log(emptyList);
    if (emptyList) {
      container.querySelector(".text-danger").remove();
      emptyList = false;
    }

    const item = evt.item.querySelector("a.nav-link");
    const name = item.innerText;
    const id = item.getAttribute("data-id");
    const pageUrl = item.getAttribute("data-url");
    const parent = 0;
    const order = evt.newIndex + 1;

    const closeBtn = document.createElement("a");
    closeBtn.classList = "item-delete";
    closeBtn.innerHTML = '<i class="fa fa-times"></i>';

    closeBtn.addEventListener("click", (_) =>
      closeBtn.closest(".dd-item").remove()
    );

    evt.item.innerHTML = `
       <input type="text" name="[@].Page" value = "${id}">
       <input type="text" name="[@].Parent" value = "${parent}">
       <input type="text" name="[@].Order" value = "${order}" class="order">
       <div class="dd-handle"><span>${name}</span></div>
       `;

    evt.item.appendChild(closeBtn);
    evt.item.classList.add("new");
  };

  container.addEventListener("contentLoad", () => afterLoad());
  container.addEventListener("submitFinish", (e) => {
    edit.submitFinished(e);
    afterLoad();
  });

  if (addBtn) {
    addBtn.addEventListener("click", (_) => sidebar.openSidebar());
    addBtn.addEventListener(
      "click",
      async (_) => {
        const res = await sidebar.loadContent("/admin/managers/content");
        contentManager = new ContentManager(res);
        contentManager.setDrag("menuItems");
      },
      { once: true }
    );
  }

  if (saveBtn) {
    saveBtn.addEventListener("click", (e) => {
      sortList(list, ".dd-item");

      const form = container.querySelector("#editMenuForm");
      let url = form.getAttribute("action");
      url = `${url}/${edit.getActive()}`;
      form.setAttribute("action", url);

      edit.submit();
    });
  }
}
