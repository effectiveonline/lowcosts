export function FormsHandle() {
  let submitResult = [];
  let wrapper = null;
  let forms = null;
  let counter = 0;

  const _handleSumbit = (container, formsArr = null) => {
    submitResult = [];
    forms = formsArr ? formsArr : container.querySelectorAll("form");
    wrapper = container;

    if (forms.length === 0) return _onFinish();

    for (let i = 0; i < forms.length; i++) {
      const form = forms[i];
      const data = new FormData(form);
      const url = form.getAttribute("action");
      const id = form.id;

      _submitForm(url, data, id);
    }
  };

  const _submitForm = async (url, data, id) => {
    try {
      const request = await fetch(url, {
        method: "POST",
        body: data,
      });

      const response = await request.text();
      submitResult.push({ id: id, response: response });

      if (counter === forms.length - 1) return _onFinish();

      counter++;
    } catch (e) {
      console.log(e);
      counter++;
      toastr["error"]("Nepodařilo se odeslat formulář", "");
    }
  };

  const _submitSingleForm = async (form) => {
    const data = new FormData(form);
    const url = form.getAttribute("action");
    const id = form.id;

    try {
      const request = await fetch(url, {
        method: "POST",
        body: data,
      });

      const response = await request.text();
      const result = { id: id, response: response };

      return result;
    } catch (e) {
      console.log(e);
      toastr["error"]("Nepodařilo se odeslat formulář", "");
    }
  };

  const _onFinish = () => {
    counter = 0;

    wrapper.dispatchEvent(
      new CustomEvent("submitFinish", {
        bubbles: false,
        detail: { result: submitResult },
      })
    );

    console.log(submitResult);
    submitResult = [];

    return;
  };

  return {
    onSubmit: _handleSumbit,
    submitForm: _submitSingleForm,
  };
}
