import { WarrningModal } from "../modals/WarningModal.js";

const defOptions = {
  listClass: "dd-content",
  itemToAddClass: "dd-list",
  form: null,
  inputName: "Sections[@]",
  onChange: null,
};

export function Draggable(container, options = defOptions) {
  const list = container.querySelector(`.${options.listClass}`);
  const avalibleList = container.querySelector(`.${options.itemToAddClass}`);

  let removeBtns;

  const _initList = () => {
    removeBtns = list.querySelectorAll(".item-delete");
    _onRemove();

    return Sortable.create(list, {
      group: {
        name: "shared",
        pull: "clone",
        put: true,
      },
      sort: true,
      animation: 150,
      handle: ".panel-heading",
      ghostClass: "dd-placeholder-none",
      forceFallback: true,
      fallbackClass: "dragged-cursor",
      onAdd: function (evt) {
        options.onChange ? options.onChange() : null;
        _itemAdded(evt);
      },
      onUpdate: function (evt) {
        options.onChange ? options.onChange() : null;
      },
    });
  };

  const _initAvalibleList = () => {
    return Sortable.create(avalibleList, {
      group: {
        name: "shared",
        pull: "clone",
        put: false,
      },
      sort: false,
      animation: 150,
      handle: ".panel-heading",
      ghostClass: "dd-placeholder-none",
      forceFallback: true,
      fallbackClass: "dragged-cursor",
    });
  };

  const _itemAdded = (evt) => {
    evt.item.classList.add("new");

    const closeBtn = document.createElement("a");
    closeBtn.classList = "item-delete";
    closeBtn.innerHTML = '<i class="fa fa-times"></i>';
    evt.item.querySelector(".panel-tools").appendChild(closeBtn);

    closeBtn.addEventListener("click", (_) =>
      closeBtn.closest(".panel").remove()
    );

    const type = evt.item.getAttribute("data-id");

    evt.item.querySelector(".panel-body").innerHTML = `
    <div class="form-group">
      <label>Název</label>
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Name" value="${evt.item.innerText}" class="form-control" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Type" value="${type}" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Order" value="${evt.newIndex + 1}" class="order" />
    </div>
       `;
  };

  const _onRemove = () => {
    onItemRemove(removeBtns, ".panel", options.onChange);
  };

  const _sort = () => sortList(list, ".panel");

  if (list) _initList();
  if (avalibleList) _initAvalibleList();

  return {
    sortOrder: _sort,
  };
}

export function onItemRemove(
  removeBtns,
  itemSelector = ".dd-item",
  callback = null
) {
  removeBtns.forEach((rmBtn) => {
    rmBtn.addEventListener("click", async (e) => {
      e.preventDefault();

      const warning = WarrningModal();
      const confirm = await warning.show(
        "Opravdu chcete odstranit? Dojde ke smazání veškerého obsahu."
      );

      if (!confirm) return;

      rmBtn.closest(itemSelector).classList.add("removed");

      if (callback) callback();
    });
  });
}

export function sortList(list, itemSelector) {
  if (!list) return;

  let order = 1;
  const sections = list.querySelectorAll(itemSelector);
  let itemsCount = list.querySelectorAll(`${itemSelector}:not(.new)`).length;

  sections.forEach((item) => {
    const orderInput = item.querySelector("input.order");
    const isNew = item.classList.contains("new") ? true : false;

    if (isNew) {
      const inputs = item.querySelectorAll("input");

      inputs.forEach((input) => {
        let name = input.getAttribute("name").replace("@", `${itemsCount}`);
        input.setAttribute("name", name);
      });

      item.classList.remove("new");
      itemsCount++;
    }

    if (item.classList.contains("removed")) orderInput.value = -1;
    else {
      orderInput.value = order;
      order++;
    }
  });
}
