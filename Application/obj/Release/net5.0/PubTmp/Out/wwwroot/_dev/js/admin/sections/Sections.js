import { Images } from "./Images.js";

const modalTypes = [{ id: "images", fce: Images }];

export function GetScript(id) {
  return modalTypes.find((type) => type.id === id);
}

export function InitSectionsScript(container, pageId) {
  const dataScripts = container.querySelectorAll("div[data-script]");

  dataScripts.forEach((ds) => {
    const id = ds.getAttribute("data-script");

    const s = GetScript(id);

    if (s) s.fce(pageId, ds.closest("div[id]"));
  });
}
