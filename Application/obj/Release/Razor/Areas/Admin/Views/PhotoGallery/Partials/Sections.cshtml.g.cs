#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "82a0e2b19996dcc65050f7656c2f886cefc8f1d5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_PhotoGallery_Partials_Sections), @"mvc.1.0.view", @"/Areas/Admin/Views/PhotoGallery/Partials/Sections.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"82a0e2b19996dcc65050f7656c2f886cefc8f1d5", @"/Areas/Admin/Views/PhotoGallery/Partials/Sections.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_PhotoGallery_Partials_Sections : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<PageViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("select form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("width: 100%"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("form", new global::Microsoft.AspNetCore.Html.HtmlString("formEdit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
 if (Model.ParentId == 0)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"row\">\r\n\r\n    <div class=\"col col-sm-6 col-md-3\">\r\n        <div class=\"form-group\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("label", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82a0e2b19996dcc65050f7656c2f886cefc8f1d55072", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper);
#nullable restore
#line 9 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Template);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82a0e2b19996dcc65050f7656c2f886cefc8f1d56557", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Template);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList(Model.Templates,"Id","Name"));

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n\r\n\r\n    <div class=\"col col-md-3 col-sm-6\">\r\n        <div class=\"form-group\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("label", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82a0e2b19996dcc65050f7656c2f886cefc8f1d58868", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper);
#nullable restore
#line 17 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.LayoutId);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82a0e2b19996dcc65050f7656c2f886cefc8f1d510354", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
#nullable restore
#line 18 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.LayoutId);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 18 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList(Model.Layouts,"Id","Name"));

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n</div>\r\n");
#nullable restore
#line 22 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col col-md-9 individual-layout\">\r\n");
#nullable restore
#line 26 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
         for (int i = 0; i < Model.ContentSections.Count; i++)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"panel panel-filled\">\r\n                <div class=\"panel-heading\">\r\n                    <div class=\"panel-tools\">\r\n                        <a class=\"panel-toggle\"><i class=\"fa fa-chevron-down\"></i></a>\r\n");
#nullable restore
#line 32 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                         if (!Model.ContentSections[i].Required)
                        {


#line default
#line hidden
#nullable disable
            WriteLiteral("                            <a class=\"item-delete\"><i class=\"fa fa-times\"></i></a>\r\n");
#nullable restore
#line 36 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </div>\r\n                    ");
#nullable restore
#line 38 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
               Write(Model.ContentSections[i].Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"panel-body\" style=\"display:none;\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col col-md-6 form-group mb-3\">\r\n");
#nullable restore
#line 43 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                             if (Model.ContentSections[i].Required)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 1707, "\"", 1738, 3);
            WriteAttributeValue("", 1714, "ContentSections[", 1714, 16, true);
#nullable restore
#line 45 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 1730, i, 1730, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1732, "].Name", 1732, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 1739, "\"", 1777, 1);
#nullable restore
#line 45 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 1747, Model.ContentSections[i].Name, 1747, 30, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n");
#nullable restore
#line 46 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <label>Název sekce</label>\r\n                                <input");
            BeginWriteAttribute("name", " name=\"", 1977, "\"", 2008, 3);
            WriteAttributeValue("", 1984, "ContentSections[", 1984, 16, true);
#nullable restore
#line 50 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 2000, i, 2000, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2002, "].Name", 2002, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 2009, "\"", 2047, 1);
#nullable restore
#line 50 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 2017, Model.ContentSections[i].Name, 2017, 30, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"form-control\" />\r\n                                <small class=\"text-muted\">Měl by být v rozsahu 8-20 znaků.</small><br />\r\n");
#nullable restore
#line 52 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                        </div>\r\n\r\n                        <div class=\"col col-md-6 form-group mb-3\">\r\n                            <label>Zobrazit jako</label>\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82a0e2b19996dcc65050f7656c2f886cefc8f1d517915", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#nullable restore
#line 57 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.ContentSections[i].ShowAs);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 57 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList(Globals.SectionShowAsItems.Where(s => s.Section == Model.ContentSections[i].Type).ToList(),"Value","Text"));

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                            <small class=\"text-muted\">Přepne vzhled zobrazení obrázků na stránce.</small><br />\r\n                        </div>\r\n                    </div>\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 2866, "\"", 2895, 3);
            WriteAttributeValue("", 2873, "ContentSections[", 2873, 16, true);
#nullable restore
#line 61 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 2889, i, 2889, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2891, "].Id", 2891, 4, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 2896, "\"", 2932, 1);
#nullable restore
#line 61 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 2904, Model.ContentSections[i].Id, 2904, 28, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 2976, "\"", 3007, 3);
            WriteAttributeValue("", 2983, "ContentSections[", 2983, 16, true);
#nullable restore
#line 62 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 2999, i, 2999, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3001, "].Type", 3001, 6, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3008, "\"", 3046, 1);
#nullable restore
#line 62 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3016, Model.ContentSections[i].Type, 3016, 30, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 3090, "\"", 3122, 3);
            WriteAttributeValue("", 3097, "ContentSections[", 3097, 16, true);
#nullable restore
#line 63 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3113, i, 3113, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3115, "].Order", 3115, 7, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3123, "\"", 3162, 1);
#nullable restore
#line 63 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3131, Model.ContentSections[i].Order, 3131, 31, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"order\" />\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 3220, "\"", 3254, 3);
            WriteAttributeValue("", 3227, "ContentSections[", 3227, 16, true);
#nullable restore
#line 64 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3243, i, 3243, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3245, "].IdSekce", 3245, 9, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3255, "\"", 3296, 1);
#nullable restore
#line 64 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3263, Model.ContentSections[i].IdSekce, 3263, 33, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 3340, "\"", 3373, 3);
            WriteAttributeValue("", 3347, "ContentSections[", 3347, 16, true);
#nullable restore
#line 65 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3363, i, 3363, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3365, "].Locked", 3365, 8, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3374, "\"", 3425, 1);
#nullable restore
#line 65 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3382, Model.ContentSections[i].Locked.ToString(), 3382, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    <input type=\"text\"");
            BeginWriteAttribute("name", " name=\"", 3469, "\"", 3504, 3);
            WriteAttributeValue("", 3476, "ContentSections[", 3476, 16, true);
#nullable restore
#line 66 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3492, i, 3492, 2, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3494, "].Required", 3494, 10, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3505, "\"", 3558, 1);
#nullable restore
#line 66 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3513, Model.ContentSections[i].Required.ToString(), 3513, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                </div>\r\n            </div>\r\n");
#nullable restore
#line 69 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n    <div class=\"form-group col-md-3 avalible-sections-wrap\">\r\n        <div class=\"panel mb-0\">\r\n            <div class=\"panel-body bg-none avalible-sections mb-0 p-0\">\r\n");
#nullable restore
#line 74 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                 foreach (Data.Section sectionItem in Globals.AvalibleSections)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div");
            BeginWriteAttribute("id", " id=\"", 3924, "\"", 3958, 2);
            WriteAttributeValue("", 3929, "pageSection_", 3929, 12, true);
#nullable restore
#line 76 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
WriteAttributeValue("", 3941, sectionItem.Type, 3941, 17, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" data-id=\"");
#nullable restore
#line 76 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                                                                Write(sectionItem.Type);

#line default
#line hidden
#nullable disable
            WriteLiteral(@""" class=""panel panel-filled"">
                        <div class=""panel-heading"">
                            <div class=""panel-tools"">
                                <a class=""panel-toggle""><i class=""fa fa-chevron-down""></i></a>
                            </div>
                            ");
#nullable restore
#line 81 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                       Write(sectionItem.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                        <div class=\"panel-body\" style=\"display:none;\">\r\n                            ");
#nullable restore
#line 84 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                       Write(sectionItem.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n");
#nullable restore
#line 87 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\PhotoGallery\Partials\Sections.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n        </div>\r\n\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<PageViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
