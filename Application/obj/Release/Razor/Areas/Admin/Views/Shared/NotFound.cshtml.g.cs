#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\NotFound.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "db74a552878b06e3183816bb932b06efcb79e1da"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Shared_NotFound), @"mvc.1.0.view", @"/Areas/Admin/Views/Shared/NotFound.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"db74a552878b06e3183816bb932b06efcb79e1da", @"/Areas/Admin/Views/Shared/NotFound.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Shared_NotFound : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<!-- Main content-->
<section class=""content"">
    <div class=""container-center md animated slideInDown"">

        <div class=""view-header"">
            <div class=""header-icon"">
                <i class=""pe page-header-icon pe-7s-close-circle""></i>
            </div>
            <div class=""header-title"">
                <h3>Položka nenalezena</h3>
                <small>
                    Data odstraněna
                </small>
            </div>
        </div>

        <div class=""panel panel-filled"">
            <div class=""panel-body"">
                Je nám líto, Vámi požadovaná položka nebyla nalezena.
            </div>
        </div>
        <div>
            <a href=""/admin"" class=""btn btn-accent"">Zpět do administrace</a>
            <a href=""/admin"" class=""btn btn-default"">Odeslat žádost</a>
        </div>

    </div>
</section>
<!-- End main content-->");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
