#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "bb237fe23092c56c2f2e92f27b63aaf445e9e5de"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Shared_ItemsList), @"mvc.1.0.view", @"/Areas/Admin/Views/Shared/ItemsList.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bb237fe23092c56c2f2e92f27b63aaf445e9e5de", @"/Areas/Admin/Views/Shared/ItemsList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Shared_ItemsList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<BasicItemViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<ul class=\"nav nav-tabs d-block\">\r\n");
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
     foreach (BasicItemViewModel item in Model)
    {
        if (@item.Pages == 0)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <li>\r\n                <a class=\"nav-link\"\r\n                   data-toggle=\"tab\" href=\"#tab-editPage\"\r\n                   data-url=\"");
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
                        Write(item.Url);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n                    ");
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
               Write(item.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </a>\r\n            </li>\r\n");
#nullable restore
#line 14 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <li data-expanded=\"collapse\">\r\n                <a class=\"nav-link\"\r\n                   data-toggle=\"tab\" href=\"#tab-editPage\"\r\n                   data-url=\"");
#nullable restore
#line 20 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
                        Write(item.Url);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"\r\n                   data-subitems=\"");
#nullable restore
#line 21 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
                             Write(item.Pages);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" \r\n                >\r\n                    ");
#nullable restore
#line 23 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
               Write(item.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                     <span class=\"sub-nav-icon rot-90\"> <i class=\"stroke-arrow\"></i> </span>\r\n                </a>\r\n                <ul id=\"subitems\" class=\"subitems-list\">\r\n                    \r\n                </ul>\r\n            </li>\r\n");
#nullable restore
#line 30 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\ItemsList.cshtml"
        }
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</ul>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<BasicItemViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
