#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5a3a2b905e6bd4c63cc9b04300de7309da8a29e8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Shared_AdminMenuItem), @"mvc.1.0.view", @"/Areas/Admin/Views/Shared/AdminMenuItem.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\_ViewImports.cshtml"
using EffectiveTools.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5a3a2b905e6bd4c63cc9b04300de7309da8a29e8", @"/Areas/Admin/Views/Shared/AdminMenuItem.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2e02f60f70611d760ebd0797c08c63f259f9cf32", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_Shared_AdminMenuItem : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Data.AdminMenuItem>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 4 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
  
    if (Model.Items == null)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <li>\r\n            <a");
            BeginWriteAttribute("href", " href=\"", 100, "\"", 118, 1);
#nullable restore
#line 8 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
WriteAttributeValue("", 107, Model.Link, 107, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 8 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                             Write(Model.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n        </li>\r\n");
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <li class=\"nav-category\">\r\n            ");
#nullable restore
#line 14 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
       Write(Model.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </li>\r\n");
#nullable restore
#line 16 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"

        foreach (Data.AdminMenuItem item in Model.Items)
        {
            if (item.Items == null)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <li>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 418, "\"", 435, 1);
#nullable restore
#line 22 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
WriteAttributeValue("", 425, item.Link, 425, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 22 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                                    Write(item.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                </li>\r\n");
#nullable restore
#line 24 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
            }
            else
            {
                if (item.Items != null)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <li>\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 636, "\"", 654, 2);
            WriteAttributeValue("", 643, "#", 643, 1, true);
#nullable restore
#line 30 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
WriteAttributeValue("", 644, item.Link, 644, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" data-toggle=\"collapse\" aria-expanded=\"false\">\r\n                            ");
#nullable restore
#line 31 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                       Write(item.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("<span class=\"sub-nav-icon\"> <i class=\"stroke-arrow\"></i> </span>\r\n                        </a>\r\n                        <ul");
            BeginWriteAttribute("id", " id=\"", 864, "\"", 879, 1);
#nullable restore
#line 33 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
WriteAttributeValue("", 869, item.Link, 869, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"nav nav-second collapse\">\r\n\r\n");
#nullable restore
#line 35 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                             foreach (Data.AdminMenuItem subitem in item.Items)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li><a");
            BeginWriteAttribute("href", " href=\"", 1067, "\"", 1087, 1);
#nullable restore
#line 37 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
WriteAttributeValue("", 1074, subitem.Link, 1074, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 37 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                                                       Write(subitem.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></li>\r\n");
#nullable restore
#line 38 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                        </ul>\r\n                    </li>\r\n");
#nullable restore
#line 41 "D:\Visual Studio\Effective Online\EffectiveTools\Application\Areas\Admin\Views\Shared\AdminMenuItem.cshtml"
                }
            }
        }
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Data.AdminMenuItem> Html { get; private set; }
    }
}
#pragma warning restore 1591
