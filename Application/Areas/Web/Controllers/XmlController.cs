﻿using EffectiveTools.Models;
using EffectiveTools.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EffectiveTools.Areas.Web.Controllers
{
    [Area("Web")]
    public class XmlController : Controller
    {
        [Route("/sitemap.xml")]
        public IActionResult Sitemap()
        {
            List<SitemapModel> model = new List<SitemapModel>();

            var pages = DbContext.Pages.GetList();

            foreach(ContentModel page in pages)
            {
                model.Add(new SitemapModel
                {
                    Url = page.Url,
                    LastChange = page.Modified,
                    Priority = page.Priority/10.0
                });
            }

            Response.ContentType = "text/xml";

            return PartialView(model);
        }
    }
}
