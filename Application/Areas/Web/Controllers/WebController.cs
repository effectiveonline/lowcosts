﻿using Data;
using Data.WebViewModels;
using EffectiveTools.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.Controllers
{
    //[AllowAnonymous]
    [Area("Web")]
    public class WebController : Controller
    {
        #region Zpracovaní požadavku

        [Route("/{*queryvalues}")]
        public async Task<IActionResult> Index()
        {
            if (Globals.RunInstall) return Redirect("/admin/installation");

            string url = HttpContext.Request.Path.ToString().ToLower();

            var page = await DbContext.Pages.FirstOrDefaultAsync("Url = '" + url + "'");

            if (page != null)
            {
                var model = new WebPageViewModel
                {
                    Name = page.Name,
                    Title = page.Title,
                    Keywords = page.Keywords,
                    Description = page.Description,
                    MenuItems = await getMenu(page.Layout),
                    ShowTitle = page.ShowName,
                    Sections = await DbContext.ContentSections.GetListAsync("IdPage = " + page.Id)
            };

                 
                model.Sections = model.Sections.OrderBy(s => s.Order).ToList();

                if (Globals.AppConfig.RestApi)
                {
                    return RestApiResult(model);
                }
                else
                {
                    return View(page.Modul, model);
                }
            }

            Response.StatusCode = 404;
            WebPageViewModel notFoundModel = new WebPageViewModel
            {
                Title = "Stránka nenalezena",
                ShowTitle = true,
                MenuItems = await getMenu(),
            };

            return View("NotFound", notFoundModel);
        }


        private JsonResult RestApiResult(WebPageViewModel model)
        {
            return Json(model);
        }

        private async Task<List<MenuItem>> getMenu(int layoutId = 0)
        {
            List<MenuItem> result = new List<MenuItem>();

            LayoutModel layout;

            if(layoutId == 0)
            {
                layout = await DbContext.Layouts.FirstOrDefaultAsync("IsDefault = 'True'");
            }
            else
            {
                layout = await DbContext.Layouts.SelectSingleByIdAsync(layoutId);
            }

            if (!layout.DisableMenu)
            {
                List<LayoutItemModel> pages = await DbContext.LayoutItems.GetListAsync("Layout = '" + layout.Id + "' AND Parent = 0");
                pages = pages.OrderBy(p => p.Order).ToList();

                foreach (LayoutItemModel page in pages)
                {
                    result.Add(new MenuItem
                    {
                        Text = page.Text,
                        Url = page.Link
                    });
                }
            }

            return result;
        }

        #endregion

        #region Titulní stránka
        /*
        [Route("/")]
        public async Task<IActionResult> HomePage()
        {
            if (Globals.RunInstall) return Redirect("/admin/installation");

            var homepage = await DbContext.Pages.FirstOrDefaultAsync("IsHomepage = 'true'");

            var model = new Data.WebViewModels.HomePageViewModel
            {
                Title = homepage.Title,
                Keywords = homepage.Keywords,
                Description = homepage.Description,
                Robots = homepage.Robots,
                MenuItems = await getMenu(),
                Sections = new List<Section>(),
            };

            List<ContentSectionModel> contentSections = await DbContext.ContentSections.GetListAsync("IdPage = " + homepage.Id);

            foreach (ContentSectionModel contentSection in contentSections)
            {
                var s = Globals.AvalibleSections.FirstOrDefault(s => s.Type == contentSection.Type).Get(contentSection.IdSekce);
                s.IdContentSection = contentSection.Id;

                model.Sections.Add(s);
            }

            return View("HomePage", model);
        }
        */
        #endregion

        #region Zásady GDPR

        [Route("/gdpr")]
        public async Task<IActionResult> GDPR()
        {
            var model = new WebPageViewModel
            {
                Name = "Zásady zpracování osobních údajů",
                Title = "Zásady zpracování osobních údajů",
                Keywords = "klicova slova",
                Description = "popisek",
                MenuItems = await getMenu(),
                
            };

            return View("Gdpr", model);
        }

        #endregion        
    }
}
