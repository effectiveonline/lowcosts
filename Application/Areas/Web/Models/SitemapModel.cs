﻿using System;

namespace EffectiveTools.Web.Models
{
    public class SitemapModel
    {
        public string Url { get; set; }
        public DateTime LastChange { get; set; }
        public double Priority { get; set; }
    }
}
