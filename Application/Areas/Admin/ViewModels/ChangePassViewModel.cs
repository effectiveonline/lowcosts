﻿using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class ChangePassViewModel
    {
        [Required(ErrorMessage = "Zadejte přihlašovací heslo.")]
        [Display(Name = "Nové heslo")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Zadejte potvryení hesla.")]
        [Display(Name = "Potvrzení hesla")]
        [DataType(DataType.Password)]
        public string Confirm { get; set; }

        public string Token { get; set; }
    }
}
