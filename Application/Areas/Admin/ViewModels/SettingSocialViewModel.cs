﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    public class SettingSocialViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Zadejte jméno kotaktní sosoby.")]
        [Display(Name = "Jméno")]
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Bezones { get; set; }
        public string Youtube { get; set; }
    }
}
