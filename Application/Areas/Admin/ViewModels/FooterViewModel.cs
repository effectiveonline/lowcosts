﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class FooterViewModel
    {
        public int IdFooter { get; set; }
        public string Name { get; set; }
    }
}
