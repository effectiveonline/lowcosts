﻿using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Zadejte přihlašovací jméno.")]
        [Display(Name = "Přihlašovací jméno")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Zadejte přihlašovací heslo.")]
        [Display(Name = "Přhlašovací heslo")]
        [DataType(DataType.Password)]
        public string UserPass { get; set; }
    }
}
