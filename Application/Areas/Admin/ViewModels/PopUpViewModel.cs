﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class PopUpViewModel
    {
        public int IdPopUp { get; set; }

        [Required(ErrorMessage = "Zadejte název PopUp okna.")]
        [Display(Name = "Název PopUp okna")]
        public string Name { get; set; }

        [Display(Name = "Titulek PopUp okna")]
        public string Title { get; set; }

        [Display(Name = "Text PopUp okna")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Zadejte odkaz PopUp okna.")]
        [Display(Name = "Odkaz PopUp okna")]
        public string Link { get; set; }

        [Display(Name = "Zobrazovat na všech stránkách")]
        public bool ShowOnAllPages { get; set; }

        [Display(Name = "Zobrazovat na titulní stránce")]
        public bool ShowOnHomePage { get; set; }

        [Display(Name = "Zobrazovat při každém načtení stránky")]
        public bool ShowAlways { get; set; }

        public List<string> SelectedPages { get; set; }
    }
}
