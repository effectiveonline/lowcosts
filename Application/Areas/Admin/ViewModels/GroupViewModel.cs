﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class GroupViewModel
    {
        public int GroupId { get; set; }

        [Required(ErrorMessage = "Zadejte název uživatelské skupiny.")]
        [Display(Name = "Název skupiny")]
        public string Name { get; set; }

        public List<Data.Security> ModulesSecurity { get; set; }
    }
}
