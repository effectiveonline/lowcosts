﻿using Data;
using EffectiveTools.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class TemplateViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Zadejte název šablony.")]
        [Display(Name = "Název šablony")]
        public string Name { get; set; }

        [Display(Name = "Popis šablony")]
        public string Description { get; set; }

        [Display(Name = "Určení šablony")]
        public string Target { get; set; }

        public List<TemplateSectionModel> Sections { get; set; }
    }
}
