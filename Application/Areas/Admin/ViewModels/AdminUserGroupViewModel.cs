﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class AdminUserGroupViewModel
    {
        public int GroupId { get; set; }

        public string Text { get; set; }

        public bool Member { get; set; }
    }
}
