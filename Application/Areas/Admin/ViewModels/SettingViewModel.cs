﻿using Data;
using EffectiveTools.Models;
using System.Collections.Generic;

namespace EffectiveTools.ViewModels
{
    public class SettingViewModel
    {
        public CompanyModel Company { get; set; }
        public List<PersonModel> Persons { get; set; }
        public PersonModel Person { get; set; }
        public AdminConfig Administration { get; set; }
        public AppConfig Application { get; set; }
        public WebConfig Web { get; set; }
        public List<SectionViewModel> Sections { get; set; }
    }
}
