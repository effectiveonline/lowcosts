﻿using EffectiveTools.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EffectiveTools.ViewModels
{
    public class AdminUserViewModel
    {
        private List<AdminUserGroupViewModel> _groups;
        private string[] inGroupsRoles;

        public AdminUserViewModel()
        {
            _groups = new List<AdminUserGroupViewModel>();   
        }

        public int UserId { get; set; }

        #region Osobní údaje

        [Required(ErrorMessage = "Zadejte Vaše jméno.")]
        [Display(Name = "Jméno")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Zadejte Vaše příjmení.")]
        [Display(Name = "Příjmení")]
        public string LastName { get; set; }

        [Display(Name = "E-mail")]
        public string UserEmail { get; set; }

        [Display(Name = "Telefon")]
        public string UserPhone { get; set; }

        #endregion

        #region Přihlašovací údaje

        [Required(ErrorMessage = "Zadejte užvatelské jméno.")]
        [Display(Name = "Uživatelské jméno")]
        public string Login { get; set; }

        [Display(Name = "Uživatelské heslo")]
        [DataType(DataType.Password)]
        public string UserPass { get; set; }

        [Display(Name = "Potvrzení hesla")]
        [DataType(DataType.Password)]
        public string UserPassConf{ get; set; }

        [Display(Name = "Vyžádat změnu hesla")]
        public bool ChangePassword { get; set; }

        [Display(Name = "Zablokovat účet")]
        public bool Locked { get; set; }

        #endregion

        #region Historie u6ivatele

        public List<UserLogModel> HistoryData { get; set; }

        #endregion

        #region Oprávnění a skupiny

        public List<Data.Security> Autorizations { get; set; }
        public List<AdminUserGroupViewModel> Groups { get { return _groups; } }

        public void InicializeAutorizations()
        {
            string[] inGroups = new string[0];
            string[] inUserRoles = new string[0];

            InicializeAutorizations(inGroups, inUserRoles);
        }

        public void InicializeAutorizations(string[] inGroups, string[] inUserRoles)
        {
            _groups = new List<AdminUserGroupViewModel>();

            List<GroupModel> allGroups = DbContext.AdminGroups.GetList();
            List<string> groupsRoles = new List<string>();

            foreach (GroupModel group in allGroups)
            {
                AdminUserGroupViewModel grp = new AdminUserGroupViewModel
                {
                    GroupId = group.Id,
                    Text = group.Name,
                    Member = inGroups.Contains(group.Id.ToString()),
                };

                _groups.Add(grp);

                if (grp.Member)
                {
                    string[] groupRoles = group.Roles.Split(",");

                    foreach (string role in groupRoles)
                    {
                        if (!groupsRoles.Contains(role))
                        {
                            groupsRoles.Add(role);
                        }
                    }
                }
            }

            inGroupsRoles = groupsRoles.ToArray();

            Autorizations  = new List<Data.Security>();

            foreach (Data.Security m in Globals.Securityes)
            {
                Data.Security newM = new Data.Security();
                newM.Text = m.Text;
                newM.Description = m.Description;
                newM.Name = m.Name;
                newM.Roles = new List<Data.SecurityRole>();

                if (inGroupsRoles.Contains(m.Name))
                {
                    newM.Allowed = true;
                    newM.DisableChackbox = true;
                    newM.Checked = true;
                }
                else if(inUserRoles.Contains(m.Name))
                {
                    newM.Allowed = true;
                }

                foreach (Data.SecurityRole r in m.Roles)
                {
                    Data.SecurityRole newR = new Data.SecurityRole();
                    newR.Text = r.Text;
                    newR.Name = r.Name;

                    if (inGroupsRoles.Contains(r.Name))
                    {
                        newR.Allowed = true;
                        newR.DisableChackbox = true;
                    }
                    else if (inUserRoles.Contains(r.Name))
                    {
                        newR.Allowed = true;
                    }

                    newM.Roles.Add(newR);
                }

                Autorizations.Add(newM);
            }
        }

        #endregion
    }
}
