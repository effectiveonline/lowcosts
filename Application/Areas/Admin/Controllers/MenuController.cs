﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EffectiveTools.Areas.Admin.Controllers
{
    [Authorize(Roles = "pages_setting")]
    [Area("Admin")]
    public class MenuController : Controller
    {
        [Route("/admin/pages/settings/menu")]
        [HttpGet]
        public IActionResult Menu()
        {
            Globals.AdminNavigation.Icon = "pe-7s-menu";
            Globals.AdminNavigation.Title = "Menu stránek";
            Globals.AdminNavigation.SubTitle = "Nastavení menu stránek";

            return View(getMenusList());
        }

        #region Získání seznamu všech menu

        [Route("/admin/pages/settings/menu/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getMenusList());
        }

        #endregion

        #region Editace menu

        [Route("/admin/pages/settings/menu/edit/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            List<MenuItemViewModel> model = getMenuItems(id, 0);

            return PartialView(model);
        }

        [Route("/admin/pages/settings/menu/edit/{layoutId}")]
        [HttpPost]
        public IActionResult Edit(List<MenuItemViewModel> model, int layoutId)
        {
            foreach (MenuItemViewModel item in model)
            {
                if(item.Id == 0)
                {
                    ContentModel contentPage = DbContext.Pages.SelectSingleById(item.Page);

                    LayoutItemModel newItem = new LayoutItemModel
                    {
                        Page = item.Page,
                        Text = contentPage.Name,
                        Link = contentPage.Url,
                        Parent = item.Parent,
                        Layout = layoutId,
                        Order = item.Order
                    };

                    DbContext.LayoutItems.Add(newItem);
                }
                else if (item.Order == -1)
                {
                    DbContext.LayoutItems.Delete(item.Id);
                }
                else
                {
                    LayoutItemModel page = DbContext.LayoutItems.SelectSingleById(item.Id);
                    page.Text = item.Name;
                    page.Parent = item.Parent;
                    page.Order = item.Order;
                    page.CustomText = item.CustomText;

                    DbContext.LayoutItems.Update(page);
                }

                if (item.SubItems != null) saveSubItmes(item.SubItems);
            }

            AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Edit, "Úprava menu stránek: ");

            return Ok(true);
        }

        private void saveSubItmes(List<MenuItemViewModel> subitems)
        {
            foreach (MenuItemViewModel item in subitems)
            {
                LayoutItemModel itemData = DbContext.LayoutItems.SelectSingleById(item.Id);

                itemData.Parent = item.Parent;

                if (item.Order == -1)
                {
                    DbContext.LayoutItems.Delete(item.Id);
                }
                else
                {
                    itemData.Order = item.Order;
                }

                DbContext.LayoutItems.Update(itemData);

                if (item.SubItems != null) saveSubItmes(item.SubItems);
            }
        }

        #endregion

        #region Společné moetody

        private List<BasicItemViewModel> getMenusList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<LayoutModel> layouts = DbContext.Layouts.GetList("DisableMenu = 'false'");

            foreach (LayoutModel item in layouts)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/pages/settings/menu/edit/" + item.Id
                });
            }

            return result;
        }

        private List<MenuItemViewModel> getMenuItems(int layoutId, int parentId)
        {
            List<MenuItemViewModel> result = new List<MenuItemViewModel>();

            List<LayoutItemModel> items = DbContext.LayoutItems.GetList("Layout = " +layoutId + "AND Parent = "+parentId);

            foreach (LayoutItemModel item in items)
            {
                result.Add(new MenuItemViewModel
                {
                    Id = item.Id,
                    Layout = layoutId,
                    Page = item.Page,
                    Name = item.Text,
                    Parent = item.Parent,
                    Order = item.Order,
                    CustomText =item.CustomText,
                    SubItems = getMenuItems(layoutId, item.Id)
                }) ;
            }



            return result.OrderBy(i => i.Order).ToList();
        }

        #endregion
    }
}
