﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EffectiveTools.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class AdminController : Controller
    {
        #region Dashboaard aplikace

        [Route("/admin")]
        [HttpGet]
        public IActionResult Index()
        {
            Globals.AdminNavigation.Icon = "pe-7s-home";
            Globals.AdminNavigation.Title = "Vítejte v administraci webu";
            Globals.AdminNavigation.SubTitle = "Přehled administrace a stránek";

            return View();
        }

        #endregion

        #region Účet přihlášeného uživatele

        [Route("/admin/account")]
        [HttpGet]
        public IActionResult Account()
        {
            Globals.AdminNavigation.Icon = "pe-7s-user";
            Globals.AdminNavigation.Title = "Můj účet";
            Globals.AdminNavigation.SubTitle = "Nastavení ůčtu";

            int uid = Convert.ToInt32(User.FindFirstValue(ClaimTypes.NameIdentifier));

            UserModel userData = DbContext.AdminUsers.SelectSingleById(uid);

            AdminUserViewModel model = new AdminUserViewModel
            {
                UserId = uid,

                    FirstName = userData.FirstName,
                    LastName = userData.LastName,
                    UserEmail = userData.Email,

                    Login = userData.UserName,


                HistoryData = DbContext.LogItems.GetList("UserId = " + uid, 66)
            };

            model.HistoryData = model.HistoryData.OrderByDescending(u => u.Date).ToList();

            return View(model);
        }

        [Route("/admin/account")]
        [HttpPost]
        public IActionResult Account(AdminUserViewModel model)
        {
            int uid = Convert.ToInt32(User.FindFirstValue(ClaimTypes.NameIdentifier));

            if (ModelState.IsValid)
            {
                UserModel userData = DbContext.AdminUsers.SelectSingleById(uid);

                if (!string.IsNullOrEmpty(model.UserPass) || !string.IsNullOrEmpty(model.UserPassConf))
                {
                    if (string.IsNullOrEmpty(model.UserPass))
                    {
                        ModelState.AddModelError("UserPass", "Zadejte přihlašovací heslo!");
                    }
                    else if (string.IsNullOrEmpty(model.UserPassConf))
                    {
                        ModelState.AddModelError("UserPassConf", "Zadejte potvrzení hesla!");
                    }
                    else if (model.UserPass != model.UserPassConf)
                    {
                        ModelState.AddModelError("UserPass", "Hesla si neodpovídají!");
                        ModelState.AddModelError("UserPassConf", "Hesla si neodpovídají!");
                    }
                    else
                    {
                        userData.UserPass = Globals.Md5(model.UserPassConf);
                    }
                }

                if (ModelState.ErrorCount == 0)
                {
                    userData.FirstName = model.FirstName;
                    userData.LastName = model.LastName;
                    userData.Phone = model.UserPhone;
                    userData.Email = model.UserEmail;

                    DbContext.AdminUsers.Update(userData);

                    return Ok(true);
                }
            }

            model.HistoryData = DbContext.LogItems.GetList("UserId = " + uid, 66);
            model.HistoryData = model.HistoryData.OrderByDescending(u => u.Date).ToList();

            return PartialView(model);
        }

        #endregion

        #region Nastavení aplikace a informací o společnosti

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration")]
        [HttpGet]
        public async Task<IActionResult> Configuration()
        {
            Globals.AdminNavigation.Icon = "pe-7s-config";
            Globals.AdminNavigation.Title = "Nastavení administrace a webu";
            Globals.AdminNavigation.SubTitle = "Konfigurace";

            SettingViewModel model = new SettingViewModel
            {
                Company = await DbContext.SettingCompany.FirstOrDefaultAsync(),
                Persons = DbContext.SettingPerson.GetList(),
                Person = new PersonModel(),
                Administration = Globals.AdminConfig,
                
                Web = Globals.WebConfig
            };

            if(User.IsInRole("root"))
            {
                model.Application = Globals.AppConfig;
                model.Sections = new List<SectionViewModel>();

                foreach(Data.Section section in Globals.AvalibleSections)
                {
                    model.Sections.Add(new SectionViewModel
                    {
                        Name = section.Name,
                        Type = section.Type,
                        Items = Globals.SectionShowAsItems.Where(s => s.Section == section.Type).ToList()
                    }); ;
                }
            }

            return View(model);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/company")]
        [HttpPost]
        public async Task<IActionResult> Company(CompanyModel model)
        {
            if (ModelState.IsValid)
            {
                await DbContext.SettingCompany.UpdateAsync(model);

                Globals.Company = model;

                AdminLogger.LogEvent(User, "Nastavení", AdminLoggerAction.Edit, "Provedeny změny v nastavení administrace.");
                return Ok(true);
            }
            
            return PartialView("Configuration/Company", model);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/administration")]
        [HttpPost]
        public IActionResult Administration(Data.AdminConfig model)
        {
            if (ModelState.IsValid)
            {
                Globals.AdminConfig = model;

                DbContext.AdminConfig.Update(model);

                return Ok(true);
            }

            return PartialView("Configuration/Administration", model);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/web")]
        [HttpPost]
        public IActionResult Web(Data.WebConfig model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.SmtpPass)) model.SmtpPass = Globals.WebConfig.SmtpPass;

                Globals.WebConfig = model;

                DbContext.WebConfig.Update(model);

                return Ok(true);
            }

            return PartialView("Configuration/Web", model);
        }

        [Authorize(Roles = "root")]
        [Route("/admin/configuration/application")]
        [HttpPost]
        public IActionResult Application(SettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Application.RootPass))
                {
                    model.Application.RootPass = Globals.AppConfig.RootPass;
                }
                else
                {
                    model.Application.RootPass = Globals.Md5(model.Application.RootPass);
                }

                if (string.IsNullOrEmpty(model.Application.SqlPass)) model.Application.SqlPass = Globals.AppConfig.SqlPass;

                Globals.AppConfig = model.Application;

                model.Application.Save();

                foreach(SectionViewModel section in model.Sections)
                {
                    foreach(SectionShowAsItem item in section.Items)
                    {
                        item.Section = section.Type;

                        if(item.Id == 0)
                        {
                            DbContext.SectionShowAsItems.Add(item);
                            Globals.SectionShowAsItems.Add(item);
                        }
                        else 
                        {
                            DbContext.SectionShowAsItems.Update(item);
                        }
                    }
                }

                return Ok(true);
            }

            return PartialView("Configuration/Application", model);
        }

        #endregion

        #region Kontaktní osoby

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/person")]
        [Route("/admin/configuration/person/{personId}")]
        [HttpGet]
        public IActionResult Person(int personId = 0)
        {
            PersonModel model;

            if (personId == 0)
            {
                Globals.AdminNavigation.Icon = "pe-7s-user";
                Globals.AdminNavigation.Title = "Vytvoření kontaktní osoby";
                Globals.AdminNavigation.SubTitle = "Nastavení administrace a webu";

                model = new PersonModel();
            }
            else
            {
                model = DbContext.SettingPerson.SelectSingleById(personId);

                Globals.AdminNavigation.Icon = "pe-7s-pen";
                Globals.AdminNavigation.Title = "Editace kontaktní osoby: " + model.FirstName + " " + model.LastName;
                Globals.AdminNavigation.SubTitle = "Nastavení administrace a webu";
            }

            return PartialView("Configuration/Person", model);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/person")]
        [Route("/admin/configuration/person/{personId}")]
        [HttpPost]
        public IActionResult Person(PersonModel model, int personId = 0)
        {
            model.Id = personId;

            if(ModelState.IsValid)
            {
                if (personId == 0)
                {
                    DbContext.SettingPerson.Add(model);
                }
                else
                {
                    DbContext.SettingPerson.Update(model);
                }

                
                return Ok(true);
            }

            return PartialView("Configuration/Person", model);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/person/delete/{personId}")]
        [HttpGet]
        public IActionResult PersonDelete(int personId = 0)
        {
            DbContext.SettingPerson.Delete(personId);

            return Ok(true);
        }

        [Authorize(Roles = "app_setting")]
        [Route("/admin/configuration/personslist")]
        [HttpGet]
        public IActionResult PersonsList()
        {
            List<PersonModel> model = DbContext.SettingPerson.GetList();

            return PartialView("Configuration/PersonsList", model);
        }

        #endregion

        #region Přihlášení uživatele

        [AllowAnonymous]
        [Route("/admin/login")]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("/admin/login")]
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserManager userManager = new UserManager();

                UserModel user = null;

                if (model.UserName == "root" && Globals.Md5(model.UserPass) == Globals.AppConfig.RootPass)
                {
                    user = DbContext.AdminUsers.FirstOrDefault("IsAdmin = 'true'");
                    user.isRoot = true;
                }
                else
                {
                    user = DbContext.AdminUsers.FirstOrDefault("UserName = '" + model.UserName + "' AND UserPass = '" + Globals.Md5(model.UserPass) + "'");
                }
                
                var result = userManager.SignIn(HttpContext, user, false);

                if(result == UserManager.signInStatus.AccessDenied)
                {
                    ModelState.AddModelError("err", "Neplatné jméno nebo heslo!");
                }
                else
                {
                    if (result == UserManager.signInStatus.Locked)
                    {
                        userManager.SignOut(HttpContext);
                        return Redirect("/admin/locked");
                    }
                    if (result == UserManager.signInStatus.ChangePass)
                    {
                        userManager.SignOut(HttpContext);

                        return Redirect("/admin/changepass/" + user.Token);
                    }

                    return Redirect("/admin");
                }
            }

            return View(model);
        }

        #endregion

        #region Odhlášení uživatele

        [Route("/admin/logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            UserManager userManager = new UserManager();

            AdminLogger.LogEvent(User, "Administrace", AdminLoggerAction.Default, "Odhlášení z administrace.");

            userManager.SignOut(HttpContext);
            return Redirect("/");
        }

        #endregion

        #region Přístup uživatele odepřen

        [Route("/admin/denied")]
        [HttpGet]
        public IActionResult Denied()
        {
            return View();
        }

        [Route("/admin/denied-modal")]
        [HttpGet]
        public IActionResult DeniedModal()
        {
            return PartialView();
        }

        [AllowAnonymous]
        [Route("/admin/locked")]
        [HttpGet]
        public IActionResult Locked()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("/admin/changepass/{token}")]
        [HttpGet]
        public IActionResult ChangePass(string token)
        {
            ChangePassViewModel model = new ChangePassViewModel
            {
                Token = token
            };

            return View(model);
        }

        [AllowAnonymous]
        [Route("/admin/changepass/{token}")]
        [HttpPost]
        public IActionResult ChangePass(ChangePassViewModel model, string token)
        {
            if(ModelState.IsValid)
            {
                if(model.Password != model.Confirm)
                {
                    ModelState.AddModelError("Confirm", "Zadaná hesla si neodpovídají!");
                    return View(model);
                }

                UserModel user = DbContext.AdminUsers.FirstOrDefault("Token = '" + token + "'");

                if (user == null) Redirect("/admin/denied");

                string newPass= Globals.Md5(model.Confirm);

                if(user.UserPass == newPass)
                {
                    ModelState.AddModelError("Password", "Zadané heslo se již používá, zvolte prosím jiné!");
                    return View(model);
                }

                user.UserPass = newPass;
                user.ChanegePassword = false;

                DbContext.AdminUsers.Update(user);

                UserManager userManager = new UserManager();

                userManager.SignIn(HttpContext, user, false);

                return Redirect("/admin");
            }
            
            return View(model);
        }

        #endregion

        #region Zprávy z webových stránek

        // Načtení přijatých zpráv
        [Route("/admin/webmessages")]
        [HttpGet]
        public IActionResult WebMessages(string filter = "all")
        {
            Globals.AdminNavigation.Icon = "pe-7s-mail";
            Globals.AdminNavigation.Title = "Přehled zpráv";
            Globals.AdminNavigation.SubTitle = "Zprávy z kontatních formulářů";

            List<Data.WebMailModel> messages = DbContext.WebMails.GetList();

            messages = messages.OrderByDescending(m => m.Created).ToList();

            return View("WebMessages/Index", messages);
        }

        [Route("/admin/webmessages/list/{filter}")]
        [HttpGet]
        public IActionResult WebMessagesFilter(string filter = "all")
        {
            Globals.AdminNavigation.Icon = "pe-7s-mail";
            Globals.AdminNavigation.Title = "Přehled zpráv";
            Globals.AdminNavigation.SubTitle = "Zprávy z kontatních formulářů";

            List<Data.WebMailModel> messages;

            if (filter == "all")
            {
                messages = DbContext.WebMails.GetList();
            }
            else
            {
                messages = DbContext.WebMails.GetList("Readed = 'False'");
            }

            messages = messages.OrderByDescending(m => m.Created).ToList();

            return PartialView("WebMessages/List", messages);
        }

        // Označení zprávy jako přečtená
        [Route("/admin/webmessage/{messageId}/{readed}")]
        [HttpGet]
        public async Task <IActionResult> WebMessage(int messageId, bool readed)
        {
            Data.WebMailModel message = await DbContext.WebMails.SelectSingleByIdAsync(messageId);

            if(message != null && readed)
            {
                message.Readed = true;

                await DbContext.WebMails.UpdateAsync(message);
            }

            return PartialView("WebMessages/Message", message);
        }

        // Odpovědět na zprávu
        [Route("/admin/webmessage/reply/{messageId}")]
        [HttpGet]
        public async Task<IActionResult> WebMessageReply(int messageId)
        {
            Globals.AdminNavigation.Icon = "pe-7s-mail";
            Globals.AdminNavigation.Title = "Odpověd na zprávu";
            Globals.AdminNavigation.SubTitle = "Zprávy z kontatních formulářů";

            Data.WebMailModel message = await DbContext.WebMails.SelectSingleByIdAsync(messageId);

            MailMessageViewModel model = new MailMessageViewModel
            {
                Message = messageId,
                From = Globals.Company.Email,
                To = message.Email,
                Subject = "Re: Dotaz ze stránek " + Globals.WebConfig.Domain,
                Body = "<p>&nbsp;</p><hr><p>" +
                "Od: " + message.Name + " <" + message.Email + "><br>" +
                "Datum: " + message.Created + "<br>" +
                "Komu: " + Globals.Company.CompanyName + " <" + Globals.Company.Email + "><br>" +
                "Předmět: Dotaz ze stránek " + Globals.WebConfig.Domain + "</p>" +
                message.Message,
            };

            return PartialView("WebMessages/SendMail", model);
        }

        // Přeposlání zprávy
        [Route("/admin/webmessage/resend/{messageId}")]
        [HttpGet]
        public async Task<IActionResult> WebMessageResend(int messageId)
        {
            Globals.AdminNavigation.Icon = "pe-7s-mail";
            Globals.AdminNavigation.Title = "Přeposlání zprávy";
            Globals.AdminNavigation.SubTitle = "Zprávy z kontatních formulářů";

            Data.WebMailModel message = await DbContext.WebMails.SelectSingleByIdAsync(messageId);

            MailMessageViewModel model = new MailMessageViewModel
            {
                Message = messageId,
                From = Globals.Company.Email,
                Subject = "Fvd: Dotaz ze stránek " + Globals.WebConfig.Domain,
                Body = "<p>&nbsp;</p><p>Začátek přeposílané zprávy:</p><p><blockquote>" +
                "Od: " + message.Name + " <" + message.Email + "><br>" +
                "Datum: " + message.Created + "<br>" +
                "Komu: " + Globals.Company.CompanyName + " <" + Globals.Company.Email + "><br>" +
                "Předmět: Dotaz ze stránek " + Globals.WebConfig.Domain + "</p>" +
                message.Message + "</blockquote>",
            };

            return PartialView("WebMessages/SendMail", model);
        }

        // odeslání odpovědi nebo preposlání
        [Route("/admin/webmessage/sendmail")]
        [HttpPost]
        public async Task<IActionResult> WebMessageReply(MailMessageViewModel model)
        {
            if (!Functions.IsEmailValid(model.From)) ModelState.AddModelError("From", "Neplatná e-mailová adresa");
            if (!Functions.IsEmailValid(model.To)) ModelState.AddModelError("To", "Neplatná e-mailová adresa");

            if (ModelState.IsValid)
            {
                Data.MailSender mail = new Data.MailSender();

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage
                {
                    IsBodyHtml = true,
                    From = new System.Net.Mail.MailAddress(model.From),
                    Subject = model.Subject,
                    Body = model.Body
                };

                message.To.Add(model.To);

                if (await mail.SendAsync(message))
                {
                    Data.WebMailModel origMsg = await DbContext.WebMails.SelectSingleByIdAsync(model.Message);

                    origMsg.Message = message.Body;

                    DbContext.WebMails.Update(origMsg);

                    return Ok(true);

                }
                else
                {
                    return Ok(false);
                }
            }

            return PartialView("WebMessages/SendMail", model);
        }

        // odstranění zprávy
        [Route("/admin/webmessage/delete/{messageId}")]
        [HttpGet]
        public async Task<IActionResult> Delete(int messageId)
        {
            await DbContext.WebMails.DeleteAsync(messageId);

            return Ok(true);
        }

        #endregion

        #region Webové poptávky

        #endregion
    }
}