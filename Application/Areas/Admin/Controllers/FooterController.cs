﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EffectiveTools.Areas.Admin.Controllers
{
    [Authorize(Roles = "pages_setting")]
    [Area("Admin")]
    public class FooterController : Controller
    {
        [Route("/admin/pages/settings/footer")]
        [HttpGet]
        public IActionResult Footer()
        {
            Globals.AdminNavigation.Icon = "pe-7s-download";
            Globals.AdminNavigation.Title = "Patička stránek";
            Globals.AdminNavigation.SubTitle = "Nastavení patičky stránek";

            return View(getFootersList());
        }

        #region Získání seznamu patiček

        [Route("/admin/pages/settings/footer/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getFootersList());
        }

        #endregion

        #region Editace patičky

        [Route("/admin/pages/settings/footer/edit/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            LayoutModel layout = DbContext.Layouts.SelectSingleById(id);

            FooterViewModel model = new FooterViewModel
            {
                IdFooter = id,
                Name = layout.Name
            };

            return PartialView(model);
        }

        [Route("/admin/pages/settings/footer/edit/{id}")]
        [HttpPost]
        public IActionResult Edit(FooterViewModel model, int id)
        {
            if (ModelState.IsValid)
            {

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Add, "Úprava patičky stránky: " + model.Name);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Společné moetody

        private List<BasicItemViewModel> getFootersList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<LayoutModel> layouts = DbContext.Layouts.GetList("DisableFooter = 'false'");

            foreach (LayoutModel item in layouts)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/pages/settings/footer/edit/" + item.Id
                });
            }

            return result;
        }

        #endregion
    }
}
