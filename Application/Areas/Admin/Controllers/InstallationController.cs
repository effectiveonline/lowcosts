﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EffectiveTools.Areas.Admin.Controllers
{
    [AllowAnonymous]
    [Area("Admin")]
    public class InstallationController : Controller
    {
        [Route("/admin/installation")]
        [HttpGet]
        public IActionResult Installation()
        {
            if (!Globals.RunInstall) return Redirect("/admin");

            Data.AppConfig model = Globals.AppConfig;

            return View(model);
        }

        [Route("/admin/installation")]
        [HttpPost]
        public IActionResult Installation(Data.AppConfig model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.RootPass)) ModelState.AddModelError("RootPass", "Zadejte heslo root uživatele!");
                if (string.IsNullOrEmpty(model.SqlPass)) ModelState.AddModelError("SqlPass", "Zadejte heslo!");

                if (ModelState.ErrorCount == 0)
                {
                    model.RootPass = Globals.Md5(model.RootPass);

                    Globals.AppConfig = model;

                    Globals.AppConfig.Save();
                    Globals.RunInstall = false;

                    Globals.LoadDbConnectionString();

                    DbContext.Inicialize();
                    Globals.AdminConfig = DbContext.AdminConfig.FirstOrDefault();

                    Globals.Securityes.Add(new Security.Application());
                    Globals.Securityes.Add(new Security.Pages());

                    Interface.Services.Templates.FindTemplates(Globals.Folders.Templates);
                    Interface.Services.Components.FindComponents(Globals.Folders.Components);
                    Interface.Services.Modules.FindPlugins(Globals.Folders.Modules);

                    foreach (Interface.Types.AvailablePlugin plugin in Interface.Services.Modules.AvailablePlugins)
                    {
                        Globals.AdminMenu.Add(plugin.Instance.AdminMenuItem);
                    }

                    return Redirect("/admin");
                }
            }

            return View(model);
        }
    }
}
