﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "pages_setting")]
    [Area("Admin")]
    public class PopUpController : Controller
    {
        [Route("/admin/pages/settings/popup")]
        [HttpGet]
        public IActionResult Popup()
        {
            Globals.AdminNavigation.Icon = "pe-7s-photo-gallery";
            Globals.AdminNavigation.Title = "Pop-Up okno";
            Globals.AdminNavigation.SubTitle = "Nastavení vyskakovacího okna na stránkách";

            return View(getItemsList());
        }

        #region Získání seznamu popup oken

        [Route("/admin/pages/settings/popup/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getItemsList());
        }

        #endregion

        #region Vytvoření Pop-Up okna

        [Route("/admin/pages/settings/popup/create")]
        [HttpGet]
        public IActionResult Create()
        {
            Globals.AdminNavigation.SubTitle = "Vytvoření nového vyskakovacího okna";

            PopUpViewModel model = new PopUpViewModel();

            return PartialView(model);
        }

        [Route("/admin/pages/settings/popup/create")]
        [HttpPost]
        public IActionResult Create(PopUpViewModel model)
        {
            Globals.AdminNavigation.SubTitle = "Vytvoření nového vyskakovacího okna";

            if (ModelState.IsValid)
            {
                PopUpModel newPopup = new PopUpModel
                {
                    Name = model.Name,
                    Text = model.Text,
                    Link = model.Link,
                    Title = model.Title,
                    ShowAlways = model.ShowAlways
                };

                DbContext.PopUps.Add(newPopup);

                if (model.ShowOnAllPages)
                {
                    Globals.WebConfig.PopUp = newPopup.Id;
                    DbContext.WebConfig.Update(Globals.WebConfig);
                }

                if (model.ShowOnHomePage)
                {
                    ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");
                    homepage.PopUp = newPopup.Id;
                    DbContext.Pages.Update(homepage);
                }

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Add, "Vytvoření PopUp okna: " + model.Name);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Úprava Pop-Up okna

        [Route("/admin/pages/settings/popup/edit/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            PopUpModel popup = DbContext.PopUps.SelectSingleById(id);

            PopUpViewModel model = new PopUpViewModel()
            {
                IdPopUp = id,
                Name = popup.Name,
                Title = popup.Title,
                Text = popup.Text,
                Link = popup.Link,
                ShowAlways = popup.ShowAlways,
                SelectedPages = new List<string>()
            };

            List<ContentModel> pages = DbContext.Pages.GetList("PopUp = " + id);

            foreach (ContentModel page in pages)
            {
                model.SelectedPages.Add(page.Name);
            }

            ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");

            if (homepage.PopUp == id) model.ShowOnHomePage = true;

            if (Globals.WebConfig.PopUp == popup.Id) model.ShowOnAllPages = true;

            return PartialView(model);
        }

        [Route("/admin/pages/settings/popup/edit/{id}")]
        [HttpPost]
        public IActionResult Edit(PopUpViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                PopUpModel popup = DbContext.PopUps.SelectSingleById(id);
                ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");

                popup.Name = model.Name;
                popup.Title = model.Title;
                popup.Text = model.Text;
                popup.Link = model.Link;
                popup.ShowAlways = model.ShowAlways;

                if (model.ShowOnAllPages)
                {
                    Globals.WebConfig.PopUp = id;
                    DbContext.WebConfig.Update(Globals.WebConfig);
                }
                else
                {
                    if(Globals.WebConfig.PopUp == id)
                    {
                        Globals.WebConfig.PopUp = 0;
                        DbContext.WebConfig.Update(Globals.WebConfig);
                    }
                }

                if (model.ShowOnHomePage)
                {
                    homepage.PopUp = id;
                }
                else
                {
                    if (homepage.PopUp == id) homepage.PopUp = 0;
                }

                DbContext.Pages.Update(homepage);
                DbContext.PopUps.Update(popup);

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Edit, "Úprava PopUp okna: " + model.Name);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Odstranění PopUp okna

        [Route("/admin/pages/settings/popup/delete/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            PopUpModel popup = DbContext.PopUps.SelectSingleById(id);

            ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");

            if (homepage.PopUp == popup.Id)
            {
                homepage.PopUp = 0;
                DbContext.Pages.Update(homepage);
            }

            if (Globals.WebConfig.PopUp == popup.Id)
            {
                Globals.WebConfig.PopUp = 0;
                DbContext.WebConfig.Update(Globals.WebConfig);
            }

            List<ContentModel> pages = DbContext.Pages.GetList("PopUp = " + popup.Id);

            foreach(ContentModel page in pages)
            {
                page.PopUp = 0;
                DbContext.Pages.Update(page);
            }
            
            DbContext.PopUps.Delete(popup.Id);

            AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Edit, "Odstranění PopUp okna: " + popup.Name);

            return Ok(true);
        }

        #endregion

        #region Společné metody

        private List<BasicItemViewModel> getItemsList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<PopUpModel> popups = DbContext.PopUps.GetList();

            foreach (PopUpModel item in popups)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/pages/settings/popup/edit/" + item.Id
                });
            }

            return result;
        }

        #endregion
    }
}
