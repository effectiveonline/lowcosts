﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using EffectiveTools.Models;
using System.Linq;
using EffectiveTools.ViewModels;
using Data;
using Microsoft.AspNetCore.Authorization;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "pages_setting")]
    [Area("Admin")]
    public class LayoutsController : Controller
    {
        #region Přehled rozložení

        [Route("/admin/layouts")]
        [HttpGet]
        public ActionResult Layouts()
        {
            Globals.AdminNavigation.Icon = "pe-7s-keypad";
            Globals.AdminNavigation.Title = "Přehled rozložení";
            Globals.AdminNavigation.SubTitle = "Rozložení a vzhled webových stránek";

            return View(getItemsList());
        }

        #endregion

        #region Získání seznamu rozložení

        [Route("/admin/layouts/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getItemsList());
        }

        #endregion

        #region Vytvoření rozložení

        [Route("/admin/layouts/create")]
        [HttpGet]
        public ActionResult Create()
        {
            Globals.AdminNavigation.Icon = "pe-7s-pen";
            Globals.AdminNavigation.Title = "Vytvoření rozložení";
            Globals.AdminNavigation.SubTitle = "Rozložení webových stránek";

            return PartialView(new LayoutViewModel());
        }

        [Route("/admin/layouts/create")]
        [HttpPost]
        public ActionResult Create(LayoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                LayoutModel newLayout = new LayoutModel
                {
                    Name = model.Name,
                    Description = model.Description,
                    DisableMenu = model.DisableMenu,
                    DisableFooter = model.DisableFooter,
                    IsDefault = model.IsDefault
                };

                DbContext.Layouts.Add(newLayout);

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Add, "Vytvoření nového rozložení: " + model.Name);

                return Ok(true);
                // znovunacteni seznamu provedeno z JS
            }

            return PartialView(model);
        }

        #endregion

        #region Úprava rozložení

        [Route("admin/layouts/edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Globals.AdminNavigation.Icon = "pe-7s-note";
            Globals.AdminNavigation.Title = "Úprava rozložení";
            Globals.AdminNavigation.SubTitle = "Rozložení a vzhled webových stránek";

            var layout = DbContext.Layouts.SelectSingleById(id);

            LayoutViewModel model = new LayoutViewModel()
            {
                IdLayout = layout.Id,
                Name = layout.Name,
                Description = layout.Description,
                DisableMenu = layout.DisableMenu,
                DisableFooter = layout.DisableFooter,
                IsDefault = layout.IsDefault,
            };

            return PartialView(model);
        }

        [Route("admin/layouts/edit/{id}")]
        [HttpPost]
        public ActionResult Edit(LayoutViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                var layout = DbContext.Layouts.SelectSingleById(id);

                layout.Name = model.Name;
                layout.Description = model.Description;
                layout.DisableMenu = model.DisableMenu;
                layout.DisableFooter = model.DisableFooter;
                layout.IsDefault = model.IsDefault;

                DbContext.Layouts.Update(layout);

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Edit, "Úprava  rozložení: " + model.Name);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Odstranění rozložení

        [Route("/admin/layouts/delete/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            LayoutModel layout = DbContext.Layouts.SelectSingleById(id);

            int used = DbContext.Pages.SelectCount("Layout = " + id);

            if (used != 0) return Content("Rozložení '" + layout.Name + "' se používá, nelze jej odstranit!");

            DbContext.Layouts.Delete(id);

            AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Delete, "Odstranění rozložení: " + layout.Name);

            return Ok(true);
        }

        #endregion

        #region Společné metody

        private List<BasicItemViewModel> getItemsList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<LayoutModel> items = DbContext.Layouts.GetList();

            foreach (LayoutModel item in items)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/layouts/edit/" + item.Id
                });
            }

            return result;
        }

        #endregion
    }
}
