﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using EffectiveTools.Models;
using System.Linq;
using EffectiveTools.ViewModels;
using Data;
using Microsoft.AspNetCore.Authorization;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "pages_setting")]
    [Area("Admin")]
    public class TemplatesController : Controller
    {
        #region Přehled šablon

        [Route("/admin/templates")]
        [HttpGet]
        public ActionResult Templates()
        {
            Globals.AdminNavigation.Icon = "pe-7s-keypad";
            Globals.AdminNavigation.Title = "Šablony obsahu";
            Globals.AdminNavigation.SubTitle = "Přehled šablon obsahu webových stránek";

            return View(getItemsList());
        }

        #endregion

        #region Získání seznamu šablon

        [Route("/admin/templates/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            List<LayoutGroupViewModel> groups = getItemsList();

            if (groups.Count == 1) return PartialView("ItemsList", groups[0].Items);

            return PartialView("GroupsList", groups);
        }

        #endregion

        #region Vytvoření šablony

        [Route("/admin/templates/create")]
        [HttpGet]
        public ActionResult Create()
        {
            Globals.AdminNavigation.Icon = "pe-7s-pen";
            Globals.AdminNavigation.Title = "Vytvoření šablony";
            Globals.AdminNavigation.SubTitle = "Šablony obsahu webových stránek";

            TemplateViewModel model = new TemplateViewModel
            {
                Sections = new List<TemplateSectionModel>()
            };

            return PartialView(model);
        }

        [Route("/admin/templates/create")]
        [HttpPost]
        public ActionResult Create(TemplateViewModel model)
        {
            int exist = DbContext.Templates.SelectCount("Name = '" + model.Name + "'");

            if (exist != 0) ModelState.AddModelError("Name", "Šablona s tímto názvem již existuje! Zadejte jiný název.");

            if (ModelState.IsValid)
            {
                TemplateModel newTemplate = new TemplateModel
                {
                    Name = model.Name,
                    Description = model.Description,
                    Target = model.Target,
                };

                var folder = new Managers.Models.FolderModel
                {
                    Name = newTemplate.Name,
                    Type = "Šablony"
                };

                Managers.DbContext.Folders.Add(folder);

                newTemplate.Folder = folder.Id;

                DbContext.Templates.Add(newTemplate);

                foreach (TemplateSectionModel section in model.Sections)
                {
                    TemplateSectionModel newSection = new TemplateSectionModel
                    {
                        Template = newTemplate.Id,
                        Name = section.Name,
                        Type = section.Type,
                        Order = section.Order,
                        Locked = section.Locked,
                        Required = section.Required
                    };

                    DbContext.TemplateSections.Add(newSection);
                }

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Add, "Vytvoření nové šablony: " + model.Name);

                return Ok(true);
                // znovunacteni seznamu provedeno z JS
            }

            if (model.Sections == null) model.Sections = new List<TemplateSectionModel>();

            return PartialView(model);
        }

        #endregion

        #region Úprava šablony

        [Route("admin/templates/edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Globals.AdminNavigation.Icon = "pe-7s-note";
            Globals.AdminNavigation.Title = "Úprava šablony";
            Globals.AdminNavigation.SubTitle = "Šablony obsahu webových stránek";

            TemplateModel template = DbContext.Templates.SelectSingleById(id);

            TemplateViewModel model = new TemplateViewModel
            {
                Id = template.Id,
                Name = template.Name,
                Description = template.Description,
                Target = template.Target,
                Sections = DbContext.TemplateSections.GetList("Template = " + id)
            };

            model.Sections = model.Sections.OrderBy(s => s.Order).ToList();

            return PartialView(model);
        }

        [Route("admin/templates/edit/{id}")]
        [HttpPost]
        public ActionResult Edit(TemplateViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                var template = DbContext.Templates.SelectSingleById(id);

                template.Name = model.Name;
                template.Description = model.Description;

                DbContext.Templates.Update(template);

                for (int i = 0; i < model.Sections.Count; i++)
                {
                    TemplateSectionModel section = model.Sections[i];

                    section.Template = id;

                    if (section.Locked)
                    {
                        if (section.IdSekce == 0)
                        {
                            //vytvorit obsah sekce
                            section.IdSekce = Globals.AvalibleSections.FirstOrDefault(s => s.Type == section.Type).Create(template.Folder);
                        }
                    }
                    else
                    {
                        if (section.IdSekce != 0)
                        {
                            //odstranit obsah sekce
                            Globals.AvalibleSections.FirstOrDefault(s => s.Type == section.Type).Delete(section.IdSekce);
                            section.IdSekce = 0;
                        }
                    }

                    if (section.Id == 0)
                    {
                        DbContext.TemplateSections.Add(section);
                    }
                    else if (section.Order == -1)
                    {
                        DbContext.TemplateSections.Delete(section.Id);
                        model.Sections.RemoveAt(i);
                    }
                    else
                    {
                        DbContext.TemplateSections.Update(section);
                    }
                }

                AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Edit, "Úprava  šablony: " + model.Name);

                return Ok(true);
            }

            if (model.Sections == null) model.Sections = new List<TemplateSectionModel>();

            model.Sections = model.Sections.OrderBy(s => s.Order).ToList();

            return PartialView(model);
        }

        #endregion

        #region Odstranění šablony

        [Route("/admin/templates/delete/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            TemplateModel template = DbContext.Templates.SelectSingleById(id);

            int used = DbContext.Pages.SelectCount("Template = " + id);

            if (used != 0) return Content("Šablona obsahu '" + template.Name + "' se používá, nelze ji odstranit!");

            DbContext.Templates.Delete(id);
            DbContext.TemplateSections.Delete("Template = " + id);

            AdminLogger.LogEvent(User, "Nastavení stránek", AdminLoggerAction.Delete, "Odstranění šablony: " + template.Name);

            return Ok(true);
        }

        #endregion

        #region Společné metody

        private List<LayoutGroupViewModel> getItemsList()
        {
            List<LayoutGroupViewModel> result = new List<LayoutGroupViewModel>();

            foreach (PageType pageType in Globals.PageTypes)
            {

                LayoutGroupViewModel group = new LayoutGroupViewModel
                {
                    Name = pageType.Text,
                    Items = new List<BasicItemViewModel>()
                };

                List<TemplateModel> items = DbContext.Templates.GetList("Target = '" + pageType.Name + "'");

                foreach (TemplateModel item in items)
                {
                    group.Items.Add(new BasicItemViewModel
                    {
                        Text = item.Name,
                        Url = "/admin/templates/edit/" + item.Id,
                    });
                }

                result.Add(group);

                if(pageType.SubPageType != null)
                {
                    LayoutGroupViewModel subGroup = new LayoutGroupViewModel
                    {
                        Name = pageType.SubPageType.Text,
                        Items = new List<BasicItemViewModel>()
                    };

                    List<TemplateModel> subItems = DbContext.Templates.GetList("Target = '" + pageType.Name + "'");

                    foreach (TemplateModel subItem in subItems)
                    {
                        subGroup.Items.Add(new BasicItemViewModel
                        {
                            Text = subItem.Name,
                            Url = "/admin/templates/edit/" + subItem.Id,
                        });
                    }

                    result.Add(subGroup);
                }
            }

            return result;



            /*
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<TemplateModel> items = DbContext.Templates.GetList();

            foreach (TemplateModel item in items)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/templates/edit/" + item.Id
                });
            }

            return result;
            */
        }

        #endregion

        #region Získání seznamu sekcí

        //Seznam sekcí pro vytváření obsahu při použití šablony.
        //Načítá se při změně selektu při vytvářené obsahu v modulech.
        [Route("/admin/templates/getsections/{templateId}")]
        [HttpGet]
        public IActionResult GetSections(int templateId)
        {
            var templateSections = DbContext.TemplateSections.GetList("Template = " + templateId).OrderBy(s => s.Order).ToList();
            var model = new List<TemplateSectionModel>();

            int order = 0;
            foreach (TemplateSectionModel templateSection in templateSections)
            {
                order++;
                ContentSectionModel section = new ContentSectionModel
                {
                    IdSekce = templateSection.IdSekce,
                    Name = templateSection.Name,
                    Type = templateSection.Type,
                    Order = templateSection.Order,
                    Locked = templateSection.Locked,
                    Required = templateSection.Required
                };

                model.Add(templateSection);
            }

            return PartialView("ContentSections", model);
        }

        // Seznam sekcí pro vytváření šablony
        //Načítá se při změně selectu typ šablony na routě: /admin/templates/create
        [Route("/admin/templates/templatesections/{name}")]
        [HttpGet]
        public IActionResult TemplateSections(string name)
        {
            TemplateViewModel model = new TemplateViewModel
            {
                Sections = Globals.ContentTemplates.FirstOrDefault(s => s.Name == name).RequiredSections
            };

            return PartialView(model);
        }

        #endregion
    }
}