﻿using Data;
using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "app_accounts")]
    [Area("Admin")]
    public class AccountsController : Controller
    {
        public AccountsController()
        {
            Globals.AdminNavigation.Icon = "pe-7s-users";
            Globals.AdminNavigation.Title = "Uživatelské účty";
            Globals.AdminNavigation.SubTitle = "Nastavení ůčtu uživatelů";
        }

        #region Přehlked uživatelů

        [Route("/admin/accounts")]
        [HttpGet]
        public IActionResult Accounts()
        {
            return View(getItemsList());
        }

        #endregion

        #region Získání seznamu uživatelů

        [Route("/admin/accounts/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getItemsList());
        }

        #endregion

        #region Vytvoření uživatele

        [Route("/admin/accounts/create")]
        [HttpGet]
        public IActionResult Create()
        {
            Globals.AdminNavigation.Icon = "pe-7s-users";
            Globals.AdminNavigation.Title = "Vytvoření nového uživatele";
            Globals.AdminNavigation.SubTitle = "Uživatelské účty";

            AdminUserViewModel model = new AdminUserViewModel();
            model.InicializeAutorizations();

            return PartialView(model);
        }

        [Route("/admin/accounts/create")]
        [HttpPost]
        public async Task<IActionResult> Create(AdminUserViewModel model)
        {
            string[] groupsIds = model.Groups.Where(g => g.Member).Select(g => g.GroupId.ToString()).ToArray();

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Login))
                {
                    ModelState.AddModelError("Login", "Zadejte přihlašovací jméno!");
                }
                else
                {
                    int exist = DbContext.AdminUsers.SelectCount("UserName = '" + model.Login + "'");

                    if(exist != 0)
                    {
                        ModelState.AddModelError("Login", "Zadané uživatelské jméno je již použito!");
                    }
                }

                if (string.IsNullOrEmpty(model.UserPass))
                {
                    ModelState.AddModelError("UserPass", "Zadejte heslo!");
                }
                else if(string.IsNullOrEmpty(model.UserPassConf))
                {
                    ModelState.AddModelError("UserPassConf", "Zadejte potvrzení hesla!");
                }
                else if (model.UserPass != model.UserPassConf)
                {
                    ModelState.AddModelError("UserPass", "Hesla si neodpovídají!");
                    ModelState.AddModelError("UserPassConf", "Hesla si neodpovídají!");
                }

                string userGroups = string.Empty;
                if (groupsIds.Count() != 0) userGroups = groupsIds.Aggregate((a, b) => a + "," + b);

                List<string> userRolesList = new List<string>();

                foreach (Data.Security m in model.Autorizations)
                {
                    if (m.Allowed) userRolesList.Add(m.Name);

                    foreach (Data.SecurityRole r in m.Roles)
                    {
                        if (r.Allowed) userRolesList.Add(r.Name);
                    }
                }

                string userRoles = string.Empty;
                if (userRolesList.Count != 0) userRoles = userRolesList.Aggregate((a, b) => a + "," + b);

                if (string.IsNullOrEmpty(userGroups) && string.IsNullOrEmpty(userRoles))
                {
                    if (!Globals.AdminConfig.EmptyUserSecurity)
                    {
                        ModelState.AddModelError("error", "Uživatel nemá nastavena žádná oprávnění!");
                    }
                }

                if (ModelState.ErrorCount == 0)
                {
                    UserModel newUser = new UserModel
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Groups = userGroups,
                        Roles = userRoles,
                        Email = model.UserEmail,
                        Phone = model.UserPhone,
                        UserName = model.Login,
                        UserPass = Globals.Md5(model.UserPassConf),
                        Locked = model.Locked,
                        ChanegePassword = model.ChangePassword,
                    };

                    await DbContext.AdminUsers.AddAsync(newUser);
                    AdminLogger.LogEvent(User, "Uživatelské účty", AdminLoggerAction.Add, "Vytvoření nového uživatele: " + newUser.FirstName + " " + newUser.LastName);

                    return Ok(true);
                }
            }

            model.InicializeAutorizations(groupsIds, new string[0]);
            return PartialView(model);
        }

        // Slouží k aktualizaci switcherů u rozšírených práv uživatele při změně switheru ve vytvoření uživatele na záložce uživatrelské skupiny.
        [Route("/admin/accounts/autorizations/create")]
        [HttpPost]
        public IActionResult Groups(AdminUserViewModel model)
        {
            string[] groupsIds = model.Groups.Where(g => g.Member).Select(g => g.GroupId.ToString()).ToArray();
            model.InicializeAutorizations(groupsIds, new string[0]);

            return PartialView("User/Autorizations", model);
        }

        #endregion

        #region Úprava uživatele

        [Route("/admin/accounts/edit/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            UserModel user = DbContext.AdminUsers.SelectSingleById(id);

            AdminUserViewModel model = new AdminUserViewModel()
            {
                UserId = id,

                FirstName = user.FirstName,
                LastName = user.LastName,
                UserEmail = user.Email,
                UserPhone = user.Phone,

                Login = user.UserName,
                Locked = user.Locked,
                ChangePassword = user.ChanegePassword,

                HistoryData = DbContext.LogItems.GetList("UserId = " + id, 66)
            };

            model.InicializeAutorizations(user.Groups.Split(","), user.Roles.Split(","));
            model.HistoryData = model.HistoryData.OrderByDescending(u => u.Date).ToList();

            return PartialView(model);
        }

        [Route("/admin/accounts/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> Edit(AdminUserViewModel model, int id)
        {
            string[] groupsIds = model.Groups.Where(g => g.Member).Select(g => g.GroupId.ToString()).ToArray();
            UserModel user = await DbContext.AdminUsers.SelectSingleByIdAsync(id);

            if (ModelState.IsValid)
            {
                //aktualizeace prihlasovacich údajů

                if (!string.IsNullOrEmpty(model.UserPass) || !string.IsNullOrEmpty(model.UserPassConf))
                {
                    if (model.UserPass != model.UserPassConf)
                    {
                        model.HistoryData = DbContext.LogItems.GetList("UserId = " + id);
                        model.HistoryData = model.HistoryData.OrderByDescending(u => u.Date).ToList();

                        ModelState.AddModelError("UserPass", "Zadaná hesla si neodpovídají!");
                        ModelState.AddModelError("UserPassConf", "Zadaná hesla si neodpovídají!");
                    }
                    else
                    {
                        user.UserPass = Globals.Md5(model.UserPassConf);
                    }
                    
                }

                string userGroups = string.Empty;
                if(groupsIds.Count() != 0) userGroups = groupsIds.Aggregate((a, b) => a + "," + b);

                string userRoles = string.Empty;
                List<string> userRolesList = new List<string>();

                foreach (Data.Security m in model.Autorizations)
                {
                    if (m.Allowed) userRolesList.Add(m.Name);

                    foreach (Data.SecurityRole r in m.Roles)
                    {
                        if (r.Allowed) userRolesList.Add(r.Name);
                    }
                }

                if (userRolesList.Count != 0) userRoles = userRolesList.Aggregate((a, b) => a + "," + b);

                if(string.IsNullOrEmpty(userGroups) && string.IsNullOrEmpty(userRoles))
                {
                    if (!Globals.AdminConfig.EmptyUserSecurity)
                    {
                        ModelState.AddModelError("error", "Uživatel nemá nastavena žádná oprávnění!");
                    }
                }

                if (ModelState.ErrorCount == 0)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Email = model.UserEmail;
                    user.Phone = model.UserPhone;
                    user.UserName = model.Login;
                    user.ChanegePassword = model.ChangePassword;
                    user.Locked = model.Locked;
                    user.Roles = userRoles;
                    user.Groups = userGroups;

                    DbContext.AdminUsers.Update(user);

                    AdminLogger.LogEvent(User, "Uživatelské účty", AdminLoggerAction.Edit, "Změna uživatele: " + user.FirstName + " " + user.LastName);

                    return Ok(true);
                }
            }

            model.HistoryData = DbContext.LogItems.GetList("UserId = " + id);
            model.HistoryData = model.HistoryData.OrderByDescending(u => u.Date).ToList();

            model.InicializeAutorizations(groupsIds, user.Roles.Split(","));

            return PartialView(model);
        }

        // Slouží k aktualizaci switcherů u rozšírených práv uživatele při změně switheru v záložce uživatrelské skupiny.
        [Route("/admin/accounts/autorizations/{id}")]
        [HttpPost]
        public async Task<IActionResult> Groups(AdminUserViewModel model, int id)
        {
            string[] groupsIds = model.Groups.Where(g => g.Member).Select(g => g.GroupId.ToString()).ToArray();
            UserModel user = await DbContext.AdminUsers.SelectSingleByIdAsync(id);
            model.InicializeAutorizations(groupsIds, user.Roles.Split(","));

            return PartialView("User/Autorizations", model);
        }

        #endregion

        #region Odstranění uživatele

        [Route("/admin/accounts/delete/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            var curentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            UserModel user = DbContext.AdminUsers.SelectSingleById(id);

            if (curentUserId == "0") return Content("Nelze odstranit neexistující účet.");
            if (curentUserId == id.ToString()) return Content("Vlastní účet nelze odstranit.");

            if(user == null) return Content("Nelze odstranit neexistující účet.");
            if(user.IsAdmin) return Content("Nelze odstranit účet hlavního administrátora.");

            DbContext.AdminUsers.Delete(id);

            AdminLogger.LogEvent(User, "Uživatelské účty", AdminLoggerAction.Delete, "Odstranění uživatele: " + user.FirstName + " " + user.LastName);

            return Ok(true);
        }

        #endregion

        #region Společné metody

        private List<BasicItemViewModel> getItemsList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();
            List<UserModel> items = null;

            if (User.IsInRole("root"))
            {
                items = DbContext.AdminUsers.GetList();
            }
            else
            {
                items = DbContext.AdminUsers.GetList("IsAdmin = 'False'");
            }

            foreach (UserModel item in items)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.FirstName + " " + item.LastName,
                    Url = "/admin/accounts/edit/" + item.Id
                });
            }

            return result;
        }

        #endregion
    }
}