﻿using Data;
using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "app_groups")]
    [Area("Admin")]
    public class GroupsController : Controller
    {
        #region Přehled uživatelských skupin

        [Route("/admin/groups")]
        [HttpGet]
        public IActionResult Groups()
        {
            Globals.AdminNavigation.Icon = "pe-7s-users";
            Globals.AdminNavigation.Title = "Uživatelské skupiny";
            Globals.AdminNavigation.SubTitle = "Nastavení opravnění uživatelů";

            return View(getItemsList());
        }

        #endregion

        #region Získání seznamu skupin

        [Route("/admin/groups/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            return PartialView("ItemsList", getItemsList());
        }

        #endregion

        #region Vytvoření uživatelské skupiy

        [Route("/admin/groups/create")]
        [HttpGet]
        public IActionResult Create()
        {
            Globals.AdminNavigation.Icon = "pe-7s-pen";
            Globals.AdminNavigation.Title = "Vytvoření uživatelské skupiny";
            Globals.AdminNavigation.SubTitle = "Nastavení opravnění uživatelů";

            GroupViewModel model = new GroupViewModel
            {
                ModulesSecurity = getEmptySecurityList()
            };

            return PartialView(model);
        }

        [Route("/admin/groups/create")]
        [HttpPost]
        public IActionResult Create(GroupViewModel model)
        {
            if(ModelState.IsValid)
            {
                List<string> roles = new List<string>();

                foreach (Data.Security modul in model.ModulesSecurity)
                {
                    if (modul.Allowed)
                    {
                        roles.Add(modul.Name);

                        foreach (SecurityRole role in modul.Roles)
                        {
                            if (role.Allowed) roles.Add(role.Name);
                        }
                    }
                }

                GroupModel group = new GroupModel();

                group.Name = model.Name;
                group.Roles = string.Empty;

                if (roles.Count != 0) group.Roles = roles.Aggregate((a, b) => a + "," + b);

                DbContext.AdminGroups.Add(group);

                AdminLogger.LogEvent(User, "Uživatelské skupiny", AdminLoggerAction.Add, "Vytvoření uživatelské skupiny: " + model.Name);

                return Ok(true);
                // znovunacteni seznamu provedeno z JS
            }

            return PartialView(model);
        }

        #endregion

        #region Editace uživatelské skupiy

        [Route("/admin/groups/edit/{id}")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            GroupModel group = DbContext.AdminGroups.SelectSingleById(id);

            string[] roles = group.Roles.Split(',');

            GroupViewModel model = new GroupViewModel
            {
                Name = group.Name,
                GroupId = id,
                ModulesSecurity = getEmptySecurityList()
            };

            foreach (Data.Security modul in model.ModulesSecurity)
            {
                modul.Allowed = roles.Contains(modul.Name);

                if (modul.Allowed)
                {
                    foreach (SecurityRole role in modul.Roles)
                    {
                        role.Allowed = roles.Contains(role.Name);
                    }
                }
            }

            return PartialView(model);
        }

        [Route("/admin/groups/edit/{id}")]
        [HttpPost]
        public IActionResult Edit(GroupViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                List<string> roles = new List<string>();

                foreach(Data.Security modul in model.ModulesSecurity)
                {
                    if(modul.Allowed)
                    {
                        roles.Add(modul.Name);

                        foreach(SecurityRole role in modul.Roles)
                        {
                            if (role.Allowed) roles.Add(role.Name);
                        }
                    }
                }

                GroupModel group = DbContext.AdminGroups.SelectSingleById(id);

                group.Name = model.Name;
                group.Roles = string.Empty;

                if (roles.Count != 0) group.Roles = roles.Aggregate((a, b) => a + "," + b);

                DbContext.AdminGroups.Update(group);

                AdminLogger.LogEvent(User, "Uživatelské skupiny", AdminLoggerAction.Edit, "Úprava uživatelské skupiny: " + model.Name);

                return Ok(true);
            }

            return PartialView(model);
        }

        #endregion

        #region Odstranění uživatelské skupiy

        [Route("/admin/groups/delete/{id}")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            GroupModel group = DbContext.AdminGroups.SelectSingleById(id);

            DbContext.AdminGroups.Delete(id);

            AdminLogger.LogEvent(User, "Uživatelské skupiny", AdminLoggerAction.Delete, "Odstranění uživatelské skupiny: " + group.Name);

            return Ok(true);
        }

        #endregion

        #region Společné metody

        private List<BasicItemViewModel> getItemsList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<GroupModel> groups = DbContext.AdminGroups.GetList();

            foreach (GroupModel group in groups)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = group.Name,
                    Url = "/admin/groups/edit/" + group.Id
                });
            }

            return result;
        }

        private List<Data.Security> getEmptySecurityList()
        {
            List<Data.Security> result = new List<Data.Security>();

            foreach (Data.Security modul in Globals.Securityes)
            {
                Data.Security newModul = new Data.Security
                {
                    Name = modul.Name,
                    Text = modul.Text,
                    Description = modul.Description,

                    Roles = new List<SecurityRole>()
                };

                foreach (SecurityRole role in modul.Roles)
                {
                    SecurityRole newRole = new SecurityRole
                    {
                        Name = role.Name,
                        Text = role.Text,
                    };

                    newModul.Roles.Add(newRole);
                }

                result.Add(newModul);
            }

            return result;
        }

        #endregion
    }
}
