﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("AdminLogger")]
    public class UserLogModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Modul { get; set; }
        public int Action { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }

    public enum AdminLoggerAction
    {
        Default = 0,
        Add = 1,
        Edit = 2,
        Delete = 3
    }
}
