﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("AdminGroups")]
    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Roles { get; set; }
    }
}
