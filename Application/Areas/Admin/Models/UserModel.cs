﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("AdminUsers")]
    public class UserModel
    {
        private string _token;
        public bool isRoot;

        public int Id { get; set; }

        public string Groups { get; set; }
        public string Roles { get; set; }
        public bool IsAdmin { get; set; }

        public string UserName { get; set; }
        public string UserPass { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool Locked { get; set; }
        public bool ChanegePassword { get; set; }
        public string Token
        {
            get { return Globals.Md5(UserName + FirstName + LastName + UserPass); }
            set { _token = value; }
        }
    }
}
