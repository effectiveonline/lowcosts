﻿using EffectiveTools.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace EffectiveTools
{
    public class UserManager
    {
        public signInStatus SignIn(HttpContext httpContext, UserModel dbUserData, bool isPersistent = false)
        {
            if (dbUserData == null) return  signInStatus.AccessDenied;

            ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(dbUserData), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            AdminLogger.LogEvent(principal, "Administrace", AdminLoggerAction.Default, "Přihlášení do administrace.");

            if (dbUserData.Locked) return signInStatus.Locked;
            if (dbUserData.ChanegePassword) return signInStatus.ChangePass;

            return  signInStatus.Ok;
        }

        public async void SignOut(HttpContext httpContext)
        {
            await httpContext.SignOutAsync();
        }

        private IEnumerable<Claim> GetUserClaims(UserModel user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName));
            //claims.Add(new Claim(ClaimTypes.Email, user.Email));

            if(user.IsAdmin)
            {
                claims.AddRange(this.GetAdminRoleClaims(user.Id));
            }
            else
            {
                claims.AddRange(this.GetUserRoleClaims(user));
            }

            if(user.isRoot)
            {
                claims.Add(new Claim(ClaimTypes.Role, "root"));
            }

            return claims;
        }

        private IEnumerable<Claim> GetUserRoleClaims(UserModel user)
        {
            string[] groupsRoles = new string[0];
            string[] userRoles = new string[0];

            if (!string.IsNullOrEmpty(user.Groups)) groupsRoles = getGroupRoles(user.Groups.Split(","));
            if (!string.IsNullOrEmpty(user.Roles)) userRoles = user.Roles.Split(",");

            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));

            foreach(string groupRole in groupsRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, groupRole));
            }

            foreach (string userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            return claims;
        }

        private IEnumerable<Claim> GetAdminRoleClaims(int id)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, id.ToString()));

            foreach (Data.Security m in Globals.Securityes)
            {
                claims.Add(new Claim(ClaimTypes.Role, m.Name));

                foreach(Data.SecurityRole r in m.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, r.Name));
                }
            } 

            return claims;
        }


        private string[] getGroupRoles(string[] groupsIds)
        {
            List<string> result = new List<string>();

            foreach(string groupId in groupsIds)
            {
                GroupModel group = DbContext.AdminGroups.SelectSingleById(Convert.ToInt32(groupId));

                string[] roles = group.Roles.Split(",");

                foreach(string role in roles)
                {
                    if (!result.Contains(role)) result.Add(role);
                }
            }

            return result.ToArray();
        }

        public enum signInStatus
        {
            Ok,
            Locked,
            ChangePass,
            AccessDenied
        }
    }
}
