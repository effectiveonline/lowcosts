import { Images } from "./Images.js";
import { Carousel } from "./Carousel.js";

const modalTypes = [
  { id: "images", fce: Images },
  { id: "carousel", fce: Carousel }
];

export function GetScript(id) {
  return modalTypes.find((type) => type.id === id);
}

export function InitSectionsScript(container, pageId) {
  const dataScripts = container.querySelectorAll("div[data-script]");
  const sections = [];

  dataScripts.forEach( (ds) => {
    const id = ds.getAttribute("data-script");

    const s = GetScript(id);

    if (s) {
      const section = s.fce(pageId, ds.closest("div[id]"));
      sections.push(section);
    } 
  });

  return sections;
}
