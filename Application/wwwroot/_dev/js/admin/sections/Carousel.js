import { initScripts } from '../_partials/utils.js';
import { initDragAndDrop } from '../_partials/dragable.js';
import { constants } from '../_partials/constants.js';
import { ImagesManager } from "../modals/ImagesManager.js";
import { ContentManager } from "../content-managers/ContentManager.js";

export function Carousel(pageId, parentContainer) {
  const addBtn = parentContainer.querySelector('button[data-action="addItem"]');
  const folderId = parentContainer.querySelector('form').getAttribute('data-folderid');
  const addImageBtns = parentContainer.querySelectorAll('button[data-action="addImage"]');
  const removeImageBtns = parentContainer.querySelectorAll('.carousel-image .item-delete');

  const dd = initDragAndDrop(parentContainer, {
    listClass: 'dd-list',
    itemClass: 'panel',
    insertListClass: null,
    containerClass: 'carouselitems',
    form: null,
    inputName: null,
    onChange: null
  })

  let contentManager;



  function addItem(parent) {
    let id = parent.closest('form').id.split("-");
    id = id[id.length-1];
    
    const newItem = createItem(id);
  
    parent.appendChild(newItem);
  
    initScripts('', newItem);

    dd.addItem(newItem);
    //dd.sortOrder();
  
    newItem.querySelector('.item-delete').addEventListener('click', e => newItem.remove());
    newItem.querySelector('button[data-action="addImage"]').addEventListener('click', e => addImage(e));
    
  }

  function createItem(id) {
    const tpl = `
    <div class="panel-heading">
    <div class="panel-tools">
        <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
        <a class="item-delete"><i class="fa fa-times"></i></a>
    </div>
    Nová položka
  </div>
  <div class="panel-body" style="display:none;">
    <div class="row">
        <div class="col-md-4">
                <div class="carousel-image">
                    <div class="image-placeholder">
                        <button class="btn btn-default btn-squared no-border" type="button" data-action="addImage"
                            data-url="/admin/managers/images/${folderId}"><span
                                class="pe-7s-plus"></span></button>
                    </div>
                </div>
        </div>
        <div class="col-md-4">
            <div class="form-group mb-3">
                <label>Název položky</label>
                <input name="Items[@].Name" value="Nová položka" class="form-control" />
                <small class="text-muted">Měl by být v rozsahu 8-20 znaků.</small><br />
            </div>
            <div class="form-group mb-3 ">
                <label>Nadpis</label>
                <input name="Items[@].Title" value="" class="form-control" />
                <small class="text-muted">Měl by být v rozsahu 8-20 znaků.</small><br />
            </div>
            <div class="form-group mb-3">
                <label>Odkaz</label>
                <div class="input-group">
                    <input name="Items[@].Link" class="form-control" />
                    <div class="input-group-append">
                        <button class="btn btn-default btn-squared btn-outline-secondary" type="button">Vybrat
                            stránku</button>
                    </div>
                </div>
                <small class="text-muted">V případě vkládání externího odkazu zadejte úplnou adresu včetně
                    https://</small><br />
            </div>
        </div>
  
        <div class="form-group mb-3 col-md-4">
            <label>Vlastní text</label>
            <textarea name="Items[@].Text" class="form-control" rows="11"></textarea>
            <small class="text-muted">Doporučuje se delka popisu 155 znaků.</small><br />
        </div>
    </div>
    <input type="text" name="Items[@].Carousel" value="${id}"/>
    <input type="text" name="Items[@].Order" class="order" />
    <input type="text" name="Items[@].IdImg" class="img"/>
  </div>
    `;
  
    const item = document.createElement('div');
    
    item.classList = 'panel panel-filled new';
    item.setAttribute('draggable', 'true');
    item. innerHTML = tpl;
  
    return item;
  }

  // function onChange() {
  //   console.log('change')
  //   dd.sortOrder();
  // }

  async function addImage(e) { 
      const btn = e.target;
      const imgsManager = ImagesManager(
        btn.getAttribute("data-url"),
        "imagesModal"
      );

      const modalContent = await imgsManager.init();

      contentManager = new ContentManager(modalContent);

      contentManager.setCopyOnClick("img", null, false, null, false);

      const imgInput = e.target.closest('.panel').querySelector('input.img');
  
      modalContent
        .querySelector(".basicContent")
        .addEventListener("contentLoad", (e) => {
          contentManager = new ContentManager(modalContent);
          contentManager.setCopyOnClick("img", null, false, null);
        });
      
  
      modalContent
        .querySelector('button[data-action="save"]')
        .addEventListener("click", (e) => {
          imgsManager.closeModal();
          const images = contentManager.getSelectedItems();

          const img = images[0];
          const el = document.createElement("div");
          el.classList = "panel panel--inner panel-filled mb-0";
          el.innerHTML = `
          <div class="panel-heading">
            <div class="panel-tools">
              <a class="item-delete"><i class="fa fa-times"></i></a>
            </div>
            ${img.getAttribute('alt')}
          </div>
          <div class="panel-body mb-0">
          </div>
          `;

          imgInput.value = img.id;


          el.querySelector('.item-delete').addEventListener('click', e => {
            el.remove();
            imgPlace.querySelector('.image-placeholder').style.display = 'flex';
            imgInput.value = -1;
          })

          const imgPlace = btn.closest('.carousel-image');
          imgPlace.querySelector('.image-placeholder').style.display = 'none';

          const cloneImg = img.cloneNode(true);
          cloneImg.classList = "";

          el.querySelector('.panel-body').appendChild(cloneImg);
          imgPlace.appendChild(el);
          
        });
   }

  addBtn.addEventListener('click', e => {
    addItem(parentContainer.querySelector('.carouselitems')); 
    //dd.sortOrder();
  })

  addImageBtns.forEach(addImg => {

    addImg.addEventListener('click', (e) => addImage(e));
  })

  removeImageBtns.forEach(rmImg => {

    rmImg.addEventListener('click', (e) => {
      const parent =  e.target.closest('.panel:not(.panel--inner)');
      parent.querySelector('.image-placeholder').style.display = 'flex';
      parent.querySelector('input.img').value = -1;
      e.target.closest('.panel--inner').remove();
    });
  })

  return {
    beforeSubmit: dd.sortOrder
  }

}
