import { Modal } from "./Modal.js";
import { reloadContactPerson } from "../modules/settings/ContactPerson.js";


export function CreateContactPersons(url, id) {
  const modal = Modal(url, id);

  let container = null;
  let btnSave;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');

    if (btnSave) {
      btnSave.addEventListener("click", (_) => {
        modal.submit();
      });
    }
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad();
    else reloadContactPerson();
  };

  return {
    init: _init,
  };
}
