import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { initDragAndDrop } from "../_partials/dragable.js";

export function CreateTemplate(url, id) {
  const dragOpt = {
    listClass: "dd-list",
    itemClass: 'panel',
    insertListClass: "avalible-sections",
    form: null,
    inputName: "Sections[@]",
  };

  const modal = Modal(url, id);

  let container = null;
  let sectionsList, btnSave, tmpForSelect, sectionsTab;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');
    tmpForSelect = container.querySelector('#Target');
    sectionsTab = container.querySelector('.individual-layout');

    btnSave.addEventListener("click", (_) => {
      sectionsList.sortOrder();
      modal.submit();
    });

    if(tmpForSelect) tmpForSelect.addEventListener('change', _reloadSections);

    sectionsList = initDragAndDrop(container, dragOpt);
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad(); 
    else {
      document.body.dispatchEvent(
        new CustomEvent("itemsChange", {
          bubbles: true,
          detail: { reload: true, activeItem: -2 },
        })
      );
    }
  };

  //přenačtení sekcí po změně určení šablony
  const _reloadSections = async () => {
      if(tmpForSelect.value === 'Pages') {
        sectionsTab.querySelector('.dd-list').innerHTML = '';
        return;
      };
      
      sectionsTab.innerHTML = await utils.getHtml(`/admin/templates/templatesections/${tmpForSelect.value}`);

      sectionsList.reinitDD();
      utils.initScripts('', sectionsTab);

  }


  return {
    init: _init,
  };
}
