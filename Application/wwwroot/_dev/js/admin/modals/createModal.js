import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { initDragAndDrop } from "../_partials/dragable.js";

export function CreateModal(url, id) {
  const dragOpt = {
    itemClass: 'panel',
    insertListClass: "avalible-sections",
    form: null,
    inputName: "ContentSections[@]",
  };

  const modal = Modal(url, id);

  let container = null;
  let sectionsList, btnSave;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');

    btnSave.addEventListener("click", (_) => {
      sectionsList.sortOrder();
      modal.submit();
    });

    _individualLayoutHandle();

    utils.hardUrlHandle(container);

    sectionsList = initDragAndDrop(container, dragOpt);
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad();
    else {
      document.body.dispatchEvent(
        new CustomEvent("itemsChange", {
          bubbles: true,
          detail: { reload: true, activeItem: -2 },
        })
      );
    }
  };

  //checkbox individuální rozložení
  const _individualLayoutHandle = () => {
    const sections = container.querySelector(".sections");
    const layoutSelect = container.querySelector("#Template");

    if (layoutSelect) {
      layoutSelect.addEventListener("change", async () => {
        sections.innerHTML = await utils.getHtml(
          `/admin/templates/getsections/${layoutSelect.value}`
        );

        utils.initScripts(".sections");

        sectionsList = initDragAndDrop(container, dragOpt);
      });
    }

    const inLayout = container.querySelector("input#CustomSectionsNpage");
    if (inLayout) {
      inLayout.checked
        ? $("#createSectionsList").show(200)
        : $("#createSectionsList").hide(200);
      inLayout.addEventListener("change", () => {
        inLayout.checked
          ? $("#createSectionsList").show(200)
          : $("#createSectionsList").hide(200);
      });
    }
  };


  return {
    init: _init,
  };
}
