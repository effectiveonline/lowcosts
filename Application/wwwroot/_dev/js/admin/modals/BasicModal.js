import { Modal } from "./Modal.js";

export function BasicModal(url, id, successMessage = 'Změny byly uloženy') {
  const modal = Modal(url, id, successMessage);

  let container = null;
  let btnSave;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');

    if (btnSave) {
      btnSave.addEventListener("click", (_) => {
        modal.submit();
      });
    }
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad();
  };

  return {
    init: _init,
  };
}
