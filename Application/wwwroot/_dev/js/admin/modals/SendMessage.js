import { BasicModal } from './BasicModal.js';

export function SendMessage(url, id) {
  const basicModal = BasicModal(url, id, 'Zpráva byla odeslána');

  return {
   init: function() 
   {
     basicModal.init();
   }
  }
}