import * as utils from "../_partials/utils.js";
import { CustomEvents } from "../_partials/customEvents.js";
import { FormsHandle } from "../_partials/_form.js";

import { BasicModal } from "./BasicModal.js";
import { CreateModal as cModal } from "./createModal.js";
import { CreateAccountModal as caModal } from "./CreateAccountModal.js";
import { CreateModal2 } from "./CreateModal2.js";
import { EditEvent } from "./EditEvent.js";
import { CreateTemplate } from "./CreateTemplate.js";
import { CreateContactPersons } from "./CreateContactPersons.js";
import { SendMessage } from "./SendMessage.js";

export function Modal(url, modalId, successMessage = 'Změny byly uloženy') {
  const modalSelector = `.modalBx #${modalId}`;
  const events = CustomEvents();
  const formsHandle = FormsHandle();
  const body = document.body;

  let modalContainer;
  let timer = null;

  /*
  ----  Methods ----
  */
  const _init = async () => {
    modalContainer = _createModal();

    const modalContent = modalContainer.querySelector(".modal-content");
    modalContent.innerHTML = await utils.getHtml(url);

    utils.initScripts(modalSelector);

    $(modalSelector).modal("show");
    body.style.overflow = 'hidden';

    // smazání modalu po zavření
    const btnClose = modalContainer.querySelector(
      `${modalSelector} button.btn.btn-default[data-dismiss="modal"]`
    );

    btnClose.addEventListener(
      "click",
      (e) => {
        e.stopImmediatePropagation();
        _closeModal();
      },
      { capture: true }
    );

    modalContainer.addEventListener(
      "keydown",
      (e) => {
        if (e.key === "Escape") {
          e.stopImmediatePropagation();
          _closeModal();
        }
      },
      { capture: true }
    );

    return modalContainer;
  };

  const _createModal = () => {
    const exist = document.querySelector(`#${modalId}`);

    if(exist) exist.parentNode.parentNode.removeChild(exist.parentNode);

    modalContainer = document.createElement("div");
    modalContainer.classList.add("modalBx");
    modalContainer.innerHTML = `
      <div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
         <div class="modal-dialog modal-lg"> 
              <div class="modal-content"></div>
          </div>
      </div>
    `;

    document.body.append(modalContainer);
    return modalContainer;
  };

  const _onSubmit = (forms = null) => {
    timer = setTimeout(() => {
      body.dispatchEvent(events.showLoader);
    }, 0);

    formsHandle.onSubmit(modalContainer, forms);
  };

  const _onSubmitFinish = (e, responseTarget = null, closeModal = true) => {
    let res = e.detail.result;
    let invalid = res.filter((r) => r.response !== "true");
    const msgTitle = document.querySelector(".view-header .header-title h3")
      .innerText;
    let errMsg = "";

    if (invalid.length > 0) {
      invalid.forEach((f, index) => {
        const targetSelector = responseTarget ? responseTarget : `#${f.id}`;
        const target = document.querySelector(targetSelector);
        target.innerHTML = f.response;
        utils.initScripts(targetSelector);
      });

      clearTimeout(timer);
      body.dispatchEvent(events.hideLoader);

      const error =
        modalContainer.querySelector(`.text-danger.field-validation-error`) ||
        modalContainer.querySelector(".field-validation-error");
      if (error) errMsg = error.innerText;

      toastr["error"](errMsg, msgTitle);

      return false;
    }

    clearTimeout(timer);
    body.dispatchEvent(events.hideLoader);

    if(closeModal) _closeModal();

    toastr["success"](successMessage, msgTitle);

    return true;
  };

  const _closeModal = () => {
    $(modalSelector).modal("hide");
    console.log("modal delete");
    document.querySelector(modalSelector).parentNode.remove();

    if(document.querySelector('.modalBx')) {
      body.style.overflow = 'hidden';
    }
    else body.style.overflow = 'auto';
      
  };

  return {
    init: _init,
    closeModal: _closeModal,
    creatModal: _createModal,
    submit: _onSubmit,
    submitFinish: _onSubmitFinish,
    selector: modalSelector,
  };
}

const modalTypes = [
  { id: "", modal: BasicModal },
  { id: "createModal", modal: CreateModal2 },
  { id: "createEvent", modal: CreateModal2 },
  { id: "createEventGroup", modal: CreateModal2 },
  { id: "eventEdit", modal: EditEvent },
  { id: "accountsCreate", modal: caModal },
  { id: "createGallery", modal: CreateModal2 },
  { id: "createTemplate", modal: CreateTemplate },
  { id: "createPerson", modal: CreateContactPersons },
  { id: "msgReply", modal: SendMessage },
  { id: "msgResend", modal: SendMessage },
];

export function GetModal(id) {
  let match = modalTypes.find((type) => type.id === id);

  if (!match) match = modalTypes[0];

  return match;
}
