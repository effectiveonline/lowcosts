import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";
import { initDragAndDrop } from "../_partials/dragable.js";
import { FormsHandle } from "../_partials/_form.js";
import { ReloadEvents } from "../modules/events/Events.js";

export function CreateModal2(url, id) {
  const dragOpt = {
    itemClass: 'panel',
    insertListClass: "avalible-sections",
    form: null,
    inputName: "ContentSections[@]",
  };

  const modal = Modal(url, id);

  let container = null,
    sectionsList,
    sectionsTab,
    btnSave,
    formsHandle = FormsHandle();

  const _init = async () => {
    container = await modal.init();
    if (container) {
      btnSave = container.querySelector('button[data-action="save"]');
      btnSave.addEventListener("click", _onSubmit);
      utils.hardUrlHandle(container);
      _afterLoad();
    }
  };

  const _afterLoad = (reinit = true) => {
    
    sectionsList = initDragAndDrop(container, dragOpt);
    sectionsTab = container.querySelector('#createSections');
    const sectionsContainer = sectionsTab.querySelector('.individual-layout');

    if(!reinit) return;
    const pageTypeSelect = container.querySelector('select#Type');

    if(pageTypeSelect) {
      pageTypeSelect.addEventListener('change', e => {
        _reloadSections(`/admin/pages/types/${pageTypeSelect.value}`, sectionsTab, true)
      });
    }
    
    const templateSelect = container.querySelector('select#Template');
    
    if(templateSelect) {
      templateSelect.addEventListener('change', e => {
          _reloadSections(`/admin/templates/getsections/${templateSelect.value}`, sectionsContainer, false);
      });
    }
  };

  const _onSubmit = async () => {
    const propertiesForm = container.querySelector("#createProperties");
    const req = await formsHandle.submitForm(propertiesForm);

    const pageId = validateForm(propertiesForm, req.response);

    if (!pageId) return;

    // formuláře pro sekce obsahu
    const sections = container.querySelectorAll(".section-tab");

    for (let i = 0; i < sections.length; i++) {
      const section = sections[i];
      const form = section.querySelector("form");

      form.setAttribute("action", `${form.getAttribute("action")}/${pageId}`);

      const res = await formsHandle.submitForm(form);

      const sectionId = res.response;

      _setId(section, sectionId);
    }

    sectionsList.sortOrder();

    // formulář pro update content sections
    const sectionsForm = document.querySelector("#createSections");

    sectionsForm.setAttribute(
      "action",
      `${sectionsForm.getAttribute("action")}/${pageId}`
    );

    formsHandle.submitForm(sectionsForm);

    modal.closeModal();
    toastr["success"](
      "Vytvořeno",
      container.querySelector(".view-header .header-title h3").innerText
    );

    // přenačtení po submitu podle typu
    const itemToActive = id !== "createEvent" ? -2 : -1;
    
    document.body.dispatchEvent(
      new CustomEvent("itemsChange", {
        bubbles: true,
        detail: { reload: true, activeItem: itemToActive },
      })
    );
  };

  const validateForm = (form, res) => {
    if (parseInt(res)) return res;

    const msgTitle = container.querySelector(".view-header .header-title h3")
      .innerText;

    let errMsg = "";

    form.innerHTML = res;

    utils.initScripts(`#${form.id}`);
    //_individualLayoutHandle();
    utils.hardUrlHandle(container);

    const error =
      container.querySelector(`.text-danger.field-validation-error`) ||
      container.querySelector(".field-validation-error");

    if (error) errMsg = error.innerText;

    toastr["error"](errMsg, msgTitle);

    return false;
  };
  
  //přenesení id vytvořené sekce do rozložení
  const _setId = (section, sectionId) => {
    let tabId = section.id.split("-");
    tabId = tabId[tabId.length - 1];

    container.querySelector(
      `input[name="ContentSections[${tabId}].IdSekce"]`
    ).value = sectionId;
  };

   //přenačtení sekcí po změně určení šablony
   const _reloadSections = async (url, target, reinit) => {
    target.innerHTML = await utils.getHtml(url);

    utils.initScripts('', target);

    _afterLoad(reinit);
}

  return {
    init: _init,
  };
}
