import { Modal } from "./Modal.js";
import * as utils from "../_partials/utils.js";

export function CreateAccountModal(url, id) {
  const modal = Modal(url, id);

  let authTab = null;
  let container = null;
  let btnSave = null;

  const _init = async () => {
    container = await modal.init();
    if (container) {
      container.addEventListener("submitFinish", (e) => _onSubmitFinish(e));

      _afterLoad();
    }
    return container;
  };

  const _afterLoad = () => {
    btnSave = container.querySelector('button[data-action="save"]');

    btnSave.addEventListener("click", (_) => {
      modal.submit();
    });

    authTab = container.querySelector("#tab-crAccount-permissions");
    _tooglePanelCheck();
    _initChangeInputs();
  };

  const _onSubmitFinish = (e) => {
    const res = modal.submitFinish(e, `${modal.selector} .modal-content`);

    if (!res) _afterLoad();
    else {
      document.body.dispatchEvent(
        new CustomEvent("itemsChange", {
          bubbles: true,
          detail: { reload: true, activeItem: -1 },
        })
      );
    }
  };

  const _tooglePanelCheck = () => {
    const panels = authTab.querySelectorAll(".panel");

    panels.forEach((panel) => {
      const panelBody = panel.querySelector(".panel-body");
      const mainCheckBox = panel.querySelector(
        ".panel-heading input.js-switch"
      );

      const someChecked = panelBody.querySelector(
        '.js-switch[checked="checked"]'
      );

      if (someChecked && !mainCheckBox.checked) {
        // mainCheckBox.checked = true;
        mainCheckBox.setAttribute("checked", "checked");
        const switcher = panel.querySelector(".panel-heading .switchery");

        if (switcher) switcher.click();

        setTimeout(() => {
          panelBody.style.display = "block";
        }, 300);
      } else if (!someChecked && !mainCheckBox.checked) {
        panelBody.style.display = "none";
      }
    });
  };

  const _initChangeInputs = () => {
    const groupsTab = container.querySelector("#userGroups");
    //const userId = container.querySelector('form').getAttribute('data-userid');

    const groupInputs = groupsTab.querySelectorAll("input.js-switch");

    for (let i = 0; i < groupInputs.length; i++) {
      const el = groupInputs[i];
      el.addEventListener("change", (_) => _onChange());
    }
  };

  const _onChange = async () => {
    const data = new FormData();

    const inputs = authTab.querySelectorAll("input");

    for (let j = 0; j < inputs.length; j++) {
      const el = inputs[j];

      if (el.getAttribute("type") == "checkbox") {
        if (!el.disabled) {
          data.append(el.name, el.checked);
        }
      } else data.append(el.name, el.value);
    }

    const response = await fetch(`/admin/accounts/autorizations/create`, {
      method: "POST",
      body: data,
    });

    authTab.querySelector(".panel-body").innerHTML = await response.text();
    _tooglePanelCheck();
    _initChangeInputs();
    utils.initScripts("#tab-crAccount-permissions");
  };

  return {
    init: _init,
  };
}
