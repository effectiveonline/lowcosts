import * as utils from "../../_partials/utils.js";
import { WarrningModal } from "../../modals/WarningModal.js";
import { ItemsList } from "../../items/ItemsList.js";
import { LayoutsEdit } from "./LayoutsEdit.js";

export function Layouts() {
  const content = LayoutsEdit();
  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  utils.addGlobalEventListener(
    "click",
    ".nav-link",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    ".luna-nav a",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );
}
