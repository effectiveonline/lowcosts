import { Edit } from "../../_partials/Edit.js";
import { Draggable } from "../../_partials/dragable.js";
import { constants }  from "../../_partials/constants.js";

export function LayoutsEdit() {
  const { container, btnSave, btnDelete, basicUrl, msgTitle } = constants;
  
  let draggable;

  const edit = Edit({
    container: container,
    btnSave: btnSave,
    basicUrl: basicUrl,
    msgTitle: msgTitle,
  });

  const _init = () => {
    draggable = Draggable(container);
  };

  const _loadContent = async (url) => {
    const res = await edit.load(url);
    if (res) _init();
  };

  const _onSubmit = () => {
    draggable.sortOrder();
    edit.submit();
  };

  const _onDelete = async () => {
    const allowDelete =
      btnDelete.getAttribute("data-alowed") === "False" ? false : true;

    const res = await edit.delete(
      allowDelete,
      `${basicUrl}/delete/${edit.getActive()}`
    );

    if (res) {
      if (res === "true") {
        edit.reload();
        toastr["success"]("smazáno", msgTitle);
      } else toastr["error"](res, msgTitle);
    }
  };

  if (btnSave) {
    btnSave.addEventListener("click", (_) => _onSubmit());
    container.addEventListener("submitFinish", (e) => edit.submitFinished(e));
  }

  if (btnDelete) btnDelete.addEventListener("click", (_) => _onDelete());

  return {
    ...edit,
    load: _loadContent,
  };
}
