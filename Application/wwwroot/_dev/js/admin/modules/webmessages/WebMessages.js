import { getHtml, addGlobalEventListener } from '../../_partials/utils.js';
import { constants } from '../../_partials/constants.js';
import { WarrningModal } from '../../modals/WarningModal.js';

export function WebMessages()
{
  const messagesList = document.querySelector('#messagesList');
  let messages = messagesList.querySelectorAll('.message');
  const messageBody = document.querySelector('#messageBody');
  const filters = document.querySelectorAll('.messages-filters a');
  const { msgTitle } = constants;

  let activeMessage = messages[0];
  activeMessage.classList.add('active');

  let activeFilter = filters[0];

  const _onMessageClick = async function(message)
  {
    _setActiveMessage(message);
    activeMessage.classList.remove('c-accent');
    console.log('click');
    messageBody.innerHTML = await getHtml(`${message.getAttribute('data-url')}/true`);
  }

  const _onFilter = async function(e) {
    e.preventDefault();

    activeFilter.classList.remove('active');
    activeFilter = e.target;
    activeFilter.classList.add('active');

    messagesList.innerHTML = await getHtml(activeFilter.getAttribute('data-url'));

    messages = messagesList.querySelectorAll('.message');
    messages.forEach(message =>  message.addEventListener('click', e => _onMessageClick(message) ));

    // zobrazení zprávy bez odoznačení nepřečtené
    _setActiveMessage(messages[0]);
    messageBody.innerHTML = await getHtml(`${activeMessage.getAttribute('data-url')}/false`);
  }

  const _setActiveMessage = function(message)
  {
    if(!message) return;
    
    activeMessage.classList.remove('active');
    
    activeMessage = message;

    activeMessage.classList.add('active');

  }

  const _messageDelete = async function(e)
  {
    e.stopPropagation();

    const warning = WarrningModal();
    const confirm = await warning.show(
      "Opravdu chcete trvale odstranit tuto zprávu?"
    );

    if (!confirm) return;

    const msg = e.target.closest('.message');
    const id = msg
      .getAttribute('data-url')
      .split('/')
      .find((str, i, arr) => i == arr.length-1);
    
    const response =  await getHtml(`/admin/webmessage/delete/${id}`);

    if(response == 'true') {
      toastr["success"]("smazáno", msgTitle);
      msg.remove();
    }
    else toastr["error"](res, settings.msgTitle);
  }

  messages.forEach(message =>  message.addEventListener('click', e => _onMessageClick(message) ));
  filters.forEach(filter =>  filter.addEventListener('click', e => _onFilter(e) ));

  addGlobalEventListener(
    "click",
    ".item-delete .fa.fa-times",
    (e) => {
      _messageDelete(e);
    },
    {
      capture: true
    }
  );
}