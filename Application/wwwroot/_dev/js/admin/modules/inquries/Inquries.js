import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { Edit } from "../../_partials/Edit.js";
import { constants } from "../../_partials/constants.js";
import { DragAndDrop } from "../../_partials/DragAndDrop.js";

export function Inquries() {
  const { container, btnSave, basicUrl, msgTitle } = constants;

  const content = Edit({
    container: container,
    btnSave: btnSave,
    basicUrl: basicUrl,
    msgTitle: msgTitle,
  });

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  btnSave.addEventListener("click", (e) => {
    const htmlInput = container.querySelector("input#Body");

    htmlInput.value = prepareFormTpl(
      container.querySelector("#formBody .dd-list")
    );

    content.submit();
  });

  container.addEventListener("submitFinish", (e) => {
    content.submitFinished(e);
    setDDItems(document.querySelector("#formBody"), ".row");
  });

  container.addEventListener("contentLoad", (e) => {
    setDDItems(document.querySelector("#formBody"), ".row");
  });
}

function setDDItems(container, itemSelector) {
  const items = container.querySelectorAll(itemSelector);

  items.forEach((item) => {
    item.classList.add("dd-item");
  });

  const dd = DragAndDrop(container, {
    onAdd: (item) => {
      createNewRow(item);
    },
  });
  dd.init();
  dd.initInsertList(document.querySelector(".formInputs"));
}

function createNewRow(addedItem) {
  const type = addedItem.getAttribute("data-id");

  addedItem.classList = "row dd-item";
  addedItem.removeAttribute("id");
  addedItem.removeAttribute("data-id");

  addedItem.innerHTML = `<div class="form-group d-flex"><div class="col-12 pl-0"></div></div>`;

  switch (type) {
    case "title":
      addTitle(addedItem);
      break;
    case "textbox":
      addTextInput(addedItem);
      break;
    case "select":
      addSelectInput(addedItem);
      break;
    case "textarea":
      addTextArea(addedItem);
      break;
    case "checkbox":
      addCheckBox(addedItem);
      break;
  }

  addedItem.addEventListener(
    "click",
    (e) => {
      if (
        e.target.classList.contains("switchery") ||
        e.target.parentNode.classList.contains("switchery")
      )
        return;

      showSettings(e.target);
    },
    { capture: true }
  );
}

// create form fields

function addTitle(container) {
  const title = document.createElement("strong");

  title.settings = {
    type: "title",
    name: "Nadpis",
  };

  title.innerText = "Nadpis";

  container.prepend(title);
}

function addTextInput(container) {
  const input = document.createElement("input");

  input.settings = {
    type: "text",
    name: "název",
  };

  input.classList = "form-control";
  input.setAttribute("placeholder", input.settings.name);

  container.querySelector(".form-group .col-12").appendChild(input);
}

function addSelectInput(container) {
  const input = document.createElement("select");

  input.settings = {
    type: "select",
    name: "název",
    options: {
      default: "Vyberte hodnotu",
    },
  };

  input.classList = "form-control";
  input.setAttribute("value", "");

  const defOpt = document.createElement("option");
  defOpt.appendChild(document.createTextNode("Vyberte hodnotu"));
  defOpt.value = "";
  defOpt.setAttribute('name', 'default');

  input.appendChild(defOpt);

  container.querySelector(".form-group .col-12").appendChild(input);
}

function addTextArea(container) {
  const input = document.createElement("textarea");

  input.settings = {
    type: "textarea",
    name: "Textové pole",
  };

  input.classList = "form-control";
  input.setAttribute("placeholder", input.settings.name);

  container.querySelector(".form-group .col-12").appendChild(input);
}

function addCheckBox(container) {
  const input = document.createElement("input");
  const lbl = document.createElement("label");

  lbl.settings = {
    type: "checkbox",
    name: "Název",
  };

  input.setAttribute("type", "checkbox");
  input.classList = "js-switch";

  lbl.innerText = lbl.settings.name;
  lbl.classList = "pl-2";

  container.querySelector(".form-group .col-12").appendChild(input);
  utils.initSwitchers(container.querySelectorAll(".js-switch"));

  container.querySelector(".form-group .col-12").appendChild(lbl);
}

// prepare html for submit
function prepareFormTpl(container) {
  const clone = container.cloneNode(true);

  const draggables = clone.querySelectorAll(".dd-item");

  draggables.forEach((draggable) => {
    draggable.classList.remove("dd-item");
    draggable.removeAttribute("draggable");
  });

  return clone.innerHTML;
}

//form field settings
function showSettings(field) {
  const settingsBox = document.querySelector(".field-settings");
  const fieldsBox = settingsBox.querySelector(".inputs-box");
  const inputs = [];
  const btn = settingsBox.querySelector(".btn-xs");
  const names = {
    name: "Název:",
    default: 'Výchozí hodnota'
  };

  settingsBox.closest('.card').querySelector('.btn.btn-link').click();

  fieldsBox.innerHTML = "";

  const createInputField = function(opt, value, isSelectValue = false) {
    const ipt = document.createElement("input");
    ipt.setAttribute("name", opt);
    ipt.value = value;
    ipt.classList = isSelectValue ? "form-control opt" : "form-control";

    inputs.push(ipt);

    if(!isSelectValue) {
      const lbl = document.createElement("label");
      lbl.innerText = names[opt];
      fieldsBox.appendChild(lbl);

      fieldsBox.appendChild(ipt);
    } else {
      const wrap = document.createElement('div');
      wrap.classList='d-flex';

      const delBtn = document.createElement('span');

      delBtn.innerHTML = '<i class="fa fa-times"></i>';
      delBtn.addEventListener('click', e => {
        delBtn.parentNode.remove();
        ipt.value = 'deleted';
      })

      wrap.appendChild(ipt);
      wrap.appendChild(delBtn);

      fieldsBox.appendChild(wrap);
    }

  
  }

  for (const opt in field.settings) {
    if (opt === "type") continue;

    if (opt === "options") {
      const optionsTitle = document.createElement('label');
      optionsTitle.innerText = 'Položky výběru:';

      fieldsBox.appendChild(optionsTitle);

      for(const o in field.settings.options) {
        createInputField(o, field.settings.options[o], true);
      }

      const addBtn = document.createElement('span');
      addBtn.classList = 'pe-7s-plus';

      addBtn.addEventListener('click', e => {
        const count = fieldsBox.querySelectorAll('.opt').length;

        const el = document.createElement('input');
        el.classList = 'form-control opt';
        el.setAttribute('name',`option_${count}`);
        el.value = 'Zadejte název';

        fieldsBox.insertBefore(el, addBtn);
        inputs.push(el);

        field.settings.options[`option_${count}`] = '';
      });

      fieldsBox.appendChild(addBtn);

      continue;
    }

    createInputField(opt, field.settings[opt]);
  }

  btn.addEventListener(
    "click",
    (e) => {
      e.preventDefault();

      inputs.forEach((input) => {
        if(input.classList.contains('opt'))
          field.settings.options[input.name] = input.value;
        else
          field.settings[input.name] = input.value;

        input.value = "";
      });

      updateInputData(field);
    },
    { once: true }
  );
}

function updateInputData(input) {
  if (input.settings.type === "text" || input.settings.type === "textarea")
    input.setAttribute("placeholder", input.settings.name);
  else if (
    input.settings.type === "title" ||
    input.settings.type === "checkbox"
  )
    input.innerText = input.settings.name;
  else if (input.settings.type === "select") {
    
    for(const opt in input.settings.options) {
      const option = input.querySelector(`option[name="${opt}"]`);
      const val = input.settings.options[opt];

      if(option && val !== 'deleted')
        option.innerText = val;
      else if(option && val === 'deleted'){ 
         option.remove();
         delete input.settings.options[opt];
      }
      else {
        const newOpt = document.createElement("option");
        newOpt.value = val;
        newOpt.innerText = val;
        newOpt.setAttribute('name', opt);
        input.appendChild(newOpt);
      }
    }
  }
}
