import "../../../../../lib/nestable/nestable.js";
import { Sidebar } from "../../_partials/sidebar.js";
import { Basic } from "../basic/Basic.js";
import { ContentManager } from "../../content-managers/ContentManager.js";
import { initDragAndDrop } from "../../_partials/dragable.js";
import { Edit } from "../../_partials/Edit.js";
import { ItemsList } from "../../items/ItemsList.js";
import { SubItemsList } from "../../items/SubItemsList.js";
import { constants } from "../../_partials/constants.js";

export function Menu() {
  const sidebar = new Sidebar();
  const addBtn = document.querySelector("button[data-action='create']");
  const { container, btnSave, basicUrl, msgTitle } = constants;

  const edit = Edit({
    container: container,
    btnSave: btnSave,
    basicUrl: basicUrl,
    msgTitle: msgTitle,
  });

  let list, emptyList, contentManager, dd;

  const itemsList = ItemsList(edit);
  itemsList.init();
  itemsList.setItem(0);

  const afterLoad = () => {
    emptyList = container.querySelector(".dd-item") ? false : true;
    list = container.querySelector(".dd");

    // Sortable.create(list, {
    //   group: {
    //     name: "menuItems",
    //     pull: "clone",
    //     put: true,
    //   },
    //   sort: true,
    //   animation: 150,
    //   handle: ".dd-item",
    //   ghostClass: "dd-placeholder",
    //   forceFallback: true,
    //   fallbackClass: "dragged-cursor",
    //   onAdd: function (evt) {
    //     itemAdded(evt);
    //   },
    // });

    dd = initDragAndDrop(container, {
      containerClass: "dd",
      itemClass: "dd-item",
      listClass: "dd-list",
      insertListClass: null,
      onAdd: itemAdded,
      nested: true,
    });

    //onItemRemove(list.querySelectorAll(".item-delete"));
  };

  const itemAdded = (item) => {
    console.log(emptyList);
    if (emptyList) {
      container.querySelector(".text-danger").remove();
      emptyList = false;
    }

    const itemInner = item.querySelector("a.nav-link");
    const name = itemInner.innerText;
    const id = itemInner.getAttribute("data-id");
    const pageUrl = itemInner.getAttribute("data-url");
    const parent = 0;
    const order = 0;
    const innerList = item.querySelector(".dd-list");

    const closeBtn = document.createElement("a");
    closeBtn.classList = "item-delete";
    closeBtn.innerHTML = '<i class="fa fa-times"></i>';

    closeBtn.addEventListener("click", (_) =>
      closeBtn.closest(".dd-item").remove()
    );

    item.innerHTML = `
      <li data-id="0">
       <input type="hidden" name="[@].Page" value = "${id}">
       <input type="hidden" name="[@].Parent" value = "${parent}" class="parent">
       <input type="hidden" name="[@].Order" value = "${order}" class="order">
       <div class="dd-handle"><span>${name}</span></div>
      </li>
       `;

    item.setAttribute('data-id', '0');
    item.querySelector('li').prepend(closeBtn);
    item.appendChild(innerList);
    item.classList.add("new");
  };

  const _sort = () => {
    const nestedLists = container.querySelectorAll(".dd-list");

    nestedLists.forEach((list) => {
      const items = list.querySelectorAll(".dd-item");
      let itemsCount = list.querySelectorAll(`.dd-item:not(.new):not(.dd-item .dd-item)`).length;
      let order = 1;

      items.forEach((item) => {
        const orderInput = item.querySelector("input.order");
        const parentInput = item.querySelector("input.parent");
        const isNew = item.classList.contains("new") ? true : false;
        
        if (isNew) {
          const inputs = item.querySelectorAll("input");
          
          inputs.forEach((input) => {
            let name = input.getAttribute("name").replace("@", `${itemsCount}`);
            input.setAttribute("name", name);
          });
          
          item.classList.remove("new");
          itemsCount++;
        }

        if (item.parentNode === list) {
          const parentId = _getParent(item);
          parentInput.value = parentId;

          if (item.classList.contains("removed")) orderInput.value = -1;
          else orderInput.value = order;

          console.log(
            `item: ${item.querySelector('li[data-id]').getAttribute(
              "data-id"
            )} | order: ${order} | parent: ${parentId}`
          );
          order++;
        }
      });

      // hasChild(list);
    });
  };

  const _getParent = (item) => {
    const node = item.parentNode;

    if (node.classList.contains("dd-list--main")) {
      return 0;
    } else {
      return node.parentNode.querySelector('li[data-id]').getAttribute("data-id");
    }
  };

  container.addEventListener("contentLoad", () => afterLoad());
  container.addEventListener("submitFinish", (e) => {
    edit.submitFinished(e);
    afterLoad();
  });

  if (addBtn) {
    addBtn.addEventListener("click", (_) => sidebar.openSidebar());
    addBtn.addEventListener(
      "click",
      async (_) => {
        const res = await sidebar.loadContent("/admin/managers/content");
        contentManager = new ContentManager(res);
        //contentManager.setDrag("menuItems");
        console.log(document.querySelector("#accordion"));
        dd.initInsertList(document.querySelector("#accordion"));
      },
      { once: true }
    );
  }

  if (btnSave) {
    btnSave.addEventListener("click", (e) => {
      _sort(list, ".dd-item");

      const form = container.querySelector("#editMenuForm");
      let url = form.getAttribute("action");
      url = `${url}/${edit.getActive()}`;
      form.setAttribute("action", url);

      edit.submit();
    });
  }
}

