import "../../../../../lib/nestable/nestable.js";
import { Sidebar } from "../../_partials/sidebar.js";
import { Basic } from "../basic/Basic.js";

// export function Menu() {
//   const sidebar = new Sidebar();
//   const addBtn = document.querySelector("button[data-action='create']");
//   let draggable = null;

//   Basic();

//   if (addBtn) {
//     addBtn.addEventListener("click", (_) => sidebar.openSidebar());
//     addBtn.addEventListener(
//       "click",
//       (_) => {
//         sidebar.loadContent("/admin/managers/content");
//         setTimeout(() => {
//           initDrag();
//         }, 800);
//       },
//       { once: true }
//     );
//   }
// }

function initDrag() {
  Sortable.create(document.querySelector(".dd-list"), {
    group: {
      name: "shared",
      pull: "clone",
      put: true,
    },
    sort: true,
    animation: 150,
    handle: ".dd-item",
    ghostClass: "dd-placeholder",
    forceFallback: true,
    fallbackClass: "dragged-cursor",
    onAdd: function (evt) {
      itemAdded(evt);
    },
  });

  Sortable.create(document.querySelector(".card-body .nav"), {
    group: {
      name: "shared",
      pull: "clone",
      put: false,
    },
    sort: false,
    animation: 150,
    handle: ".dd-item",
    ghostClass: "dd-placeholder",
    forceFallback: true,
    fallbackClass: "dragged-cursor",
  });
}

function itemAdded(evt) {
  // evt.item.classList.add("new");
  // const closeBtn = document.createElement("a");
  // closeBtn.classList = "item-delete";
  // closeBtn.innerHTML = '<i class="fa fa-times"></i>';
  // evt.item.querySelector(".panel-tools").appendChild(closeBtn);
  // closeBtn.addEventListener("click", (_) =>
  //   closeBtn.closest(".panel").remove()
  // );
  // const type = evt.item.getAttribute("data-id");
  // evt.item.querySelector(".panel-body").innerHTML = `
  // <div class="form-group">
  //   <label>Název</label>
  //   <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
  //   options.inputName
  // }.Name" value="${evt.item.innerText}" class="form-control" />
  //   <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
  //   options.inputName
  // }.Type" value="${type}" />
  //   <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
  //   options.inputName
  // }.Order" value="${evt.newIndex + 1}" class="order" />
  // </div>
  //    `;
}

export function nestedSortable(container) {
  if (!container) return;

  $(container)
    .nestable({
      group: 1,
    })
    .on("change", sort);

  initRemove();
}

function sort() {
  const nestedLists = document.querySelectorAll("#nestable .dd-list");

  nestedLists.forEach((list) => {
    const items = list.querySelectorAll(".dd-item");

    let order = 1;
    items.forEach((item) => {
      const orderInput = item.querySelector("input.order");
      const parentInput = item.querySelector("input.parent");

      if (item.parentNode === list) {
        const parentId = getParent(item);
        parentInput.value = parentId;

        if (item.classList.contains("removed")) orderInput.value = -1;
        else orderInput.value = order;

        console.log(
          `item: ${item.getAttribute(
            "data-id"
          )} | order: ${order} | parent: ${parentId}`
        );
        order++;
      }
    });

    // hasChild(list);
  });
}

function getParent(item) {
  const node = item.parentNode;

  if (node.classList.contains("dd-nested-container")) {
    return 0;
  } else {
    return node.parentNode.getAttribute("data-id");
  }
}

function initRemove() {
  const removeBtns = document.querySelectorAll(".item-delete");

  removeBtns.forEach((rmBtn) => {
    rmBtn.addEventListener("click", (e) => {
      e.preventDefault();
      e.stopPropagation();
      console.log("remove click");
      rmBtn.parentNode.classList.add("removed");
      sort();
    });
  });
}
