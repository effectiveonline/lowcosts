import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { Edit2 } from "../../_partials/Edit.js";
import { FormsHandle } from "../../_partials/_form.js";
import { constants }  from "../../_partials/constants.js";

export function Events() {
  
  const formsHandle = FormsHandle();
  const { container, btnSave, btnDelete, basicUrl, msgTitle, subPageBtn } = constants;

  const content = Edit2(
    {
      container: container,
      btnSave: btnSave,
      basicUrl: basicUrl,
      msgTitle: msgTitle,
      subPageBtn: subPageBtn,
      btnDelete: btnDelete,
    },
    {
      listClass: "dd-list",
      itemClass: 'panel',
      insertListClass: "avalible-sections",
      form: null,
      inputName: "ContentSections[@]",
    }
  );

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  //methods
  const _onSubmit = async () => {
    const propertiesForm = container.querySelector("#editProperties");
    const req = await formsHandle.submitForm(propertiesForm);
    let errMsg = 'Uložení se nezdařilo';

    if (req.response !== "true") {
      propertiesForm.innerHTML = req.response;
      utils.initScripts(`#${propertiesForm.id}`);
      utils.hardUrlHandle(container);

      const error =
        container.querySelector(`.text-danger.field-validation-error`) ||
        container.querySelector(".field-validation-error");

        if (error) errMsg = error.innerText;

      toastr["error"](errMsg, msgTitle);


      return;
    }

    const remainingForms = container.querySelectorAll(
      `form:not(#${propertiesForm.id})`
    );

    content.submit(remainingForms);
  };

  const _deleteEvent = async (e) => {
    const allowDelete =
      e.target.getAttribute("data-alowed") === "False" ? false : true;

    const res = await content.deleteItem(
      allowDelete,
      e.target.getAttribute("data-url")
    );

    if (res) {
      document.body.dispatchEvent(
        new CustomEvent("itemsChange", {
          bubbles: true,
          detail: { reload: true, activeItem: -1 },
        })
      );
    }
  };

  //listeners

  if (btnSave) {
    btnSave.addEventListener("click", (_) => _onSubmit());
    container.addEventListener("submitFinish", (e) =>
      content.submitFinished(e, ".basicContent")
    );
  }

  if (btnDelete) btnDelete.addEventListener("click", (_) => content.delete());

  utils.addGlobalEventListener(
    "click",
    ".nav-link",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    ".luna-nav a",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    "button[data-action='deleteEvent']",
    (e) => _deleteEvent(e)
  );

  utils.addGlobalEventListener(
    "click",
    "button[data-modalid='createModal']",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );
}

export async function ReloadEvents() {
  const tableBox = document.querySelector(".events-list");
  const id = document
    .querySelector("div[data-pageid]")
    .getAttribute("data-pageid");

  tableBox.innerHTML = await utils.getHtml(`/admin/events/eventslist/${id}`);

  utils.initDataTable("#groupEventsTable");
}
