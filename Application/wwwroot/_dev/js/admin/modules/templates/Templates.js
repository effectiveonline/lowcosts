import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { Edit2 } from "../../_partials/Edit.js";
import { constants }  from "../../_partials/constants.js";

export function Templates() {
  const { container, btnSave, btnDelete, basicUrl, msgTitle } = constants;
  
  const content = Edit2(
    {
      container: container,
      btnSave: btnSave,
      basicUrl: basicUrl,
      msgTitle: msgTitle,
      btnDelete: btnDelete,
    },
    {
      listClass: "dd-list",
      itemClass: 'panel',
      insertListClass: "avalible-sections",
      form: "templateEdit",
      inputName: "Sections[@]",
    }
  );

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  if (btnSave) {
    btnSave.addEventListener("click", (_) => content.submit());

    container.addEventListener("submitFinish", (e) =>
      content.submitFinished(e, ".basicContent")
    );
  }

  if (btnDelete) btnDelete.addEventListener("click", (_) => content.delete());

  utils.addGlobalEventListener(
    "click",
    ".nav-link",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    ".luna-nav a",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  utils.addGlobalEventListener(
    "click",
    "button[data-modalid='createModal']",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );
}