import * as utils from "../../_partials/utils.js";
import { ItemsList } from "../../items/ItemsList.js";
import { WarrningModal } from "../../modals/WarningModal.js";
import { Edit } from "../../_partials/Edit.js";
import { constants }  from "../../_partials/constants.js";

export function UserAccount() {
  
  const { container, btnSave, btnDelete, msgTitle } = constants;

  const content = Edit({
    container: container,
    btnSave: btnSave,
    basicUrl: "/admin/accounts",
    msgTitle: msgTitle,
  });

  const itemsList = ItemsList(content);

  itemsList.init();
  itemsList.setItem(0);

  let authTab;

  const afterLoad = () => {
    authTab = container.querySelector("#tabAutorizations");

    tooglePanelCheck();
    initChangeInputs();
  };

  container.addEventListener("submitFinish", (e) => {
    content.submitFinished(e);
    afterLoad();
  });

  container.addEventListener("contentLoad", (e) => afterLoad());

  btnSave.addEventListener("click", (e) => {
    e.preventDefault();

    tooglePanelCheck();
    content.submit();
  });

  if (btnDelete) btnDelete.addEventListener("click", (_) => content.delete());

  const unsaveWarning = async (e) => {
    if (!content.isUnsave() || e.target.hasAttribute("aria-expanded")) return;

    e.stopImmediatePropagation();
    e.preventDefault();

    const warning = WarrningModal();
    const confirm = await warning.show("Máte neuložené změny. Pokračovat?");

    if (!confirm) return;

    content.setUnsave(false);

    e.target.click();
    return;
  };

  utils.addGlobalEventListener("click", ".nav-link", unsaveWarning, {
    capture: true,
  });
  utils.addGlobalEventListener("click", ".luna-nav a", unsaveWarning, {
    capture: true,
  });

  utils.addGlobalEventListener(
    "click",
    "button[data-modalid='createModal']",
    (e) => {
      utils.unsaveWarning(e, content);
    },
    {
      capture: true,
    }
  );

  function tooglePanelCheck() {
    const panels = authTab.querySelectorAll(".panel");

    panels.forEach((panel) => {
      const panelBody = panel.querySelector(".panel-body");
      const mainCheckBox = panel.querySelector(
        ".panel-heading input.js-switch"
      );

      const someChecked = panelBody.querySelector(
        '.js-switch[checked="checked"]'
      );

      if (someChecked && !mainCheckBox.checked) {
        mainCheckBox.setAttribute("checked", "checked");
        const switcher = panel.querySelector(".panel-heading .switchery");
        if (switcher) switcher.click();

        setTimeout(() => {
          panelBody.style.display = "block";
        }, 300);
      } else if (!someChecked && !mainCheckBox.checked) {
        panelBody.style.display = "none";
      }
    });
  }

  function initChangeInputs() {
    const groupsTab = container.querySelector("#userGroups");
    const userId = container.querySelector("form").getAttribute("data-userid");

    const groupInputs = groupsTab.querySelectorAll("input.js-switch");

    for (let i = 0; i < groupInputs.length; i++) {
      const el = groupInputs[i];
      el.addEventListener("change", (_) => onChange(userId));
    }
  }

  async function onChange(userId) {
    const data = new FormData();

    const inputs = authTab.querySelectorAll("input");

    for (let j = 0; j < inputs.length; j++) {
      const el = inputs[j];

      if (el.getAttribute("type") == "checkbox") {
        if (!el.disabled) {
          data.append(el.name, el.checked);
        }
      } else data.append(el.name, el.value);
    }

    const response = await fetch(`/admin/accounts/autorizations/${userId}`, {
      method: "POST",
      body: data,
    });

    authTab.querySelector(".panel-body").innerHTML = await response.text();
    afterLoad();
    utils.initScripts("#tabAutorizations");
  }
}
