import * as utils from "../../_partials/utils.js";
import { constants }  from "../../_partials/constants.js";

export function ContactPerson() {

  const {msgTitle} = constants;

  const _onDelete = async e => {
    const allowDelete =
      e.target.getAttribute("data-alowed") === "False" ? false : true;

      if (!allowDelete) {
        const modal = Modal("/admin/denied-modal", "denied");
        modal.init();
        return false;
      }

    const res = await utils.getHtml(`${e.target.getAttribute('data-url')}`);

    if (res) {
      if (res === "true") {
        reloadContactPerson();
        toastr["success"]("smazáno", msgTitle);
      } else toastr["error"](res, msgTitle);
    }
  }


  utils.addGlobalEventListener(
    "click",
    "button[data-action='deleteContactPerson']",
    (e) => {
      _onDelete(e)
    }
  );
}

export async function reloadContactPerson() {
  const list = document.querySelector('#personsList');

  if(list) {
    list.innerHTML = await utils.getHtml('/admin/configuration/personslist');
  }
}