export class ContentManager {
  constructor(content) {
    this.content = content;
    this.selectedItems = [];
  }

  setDrag(targetGroup) {
    const ddContainers = this.content.querySelectorAll(".dd-container");

    ddContainers.forEach((container) => {
      Sortable.create(container, {
        group: {
          name: targetGroup,
          pull: "clone",
          put: false,
        },
        sort: false,
        animation: 150,
        handle: ".dd-item",
        ghostClass: "dd-placeholder-none",
        forceFallback: true,
        fallbackClass: "dragged-cursor",
      });
    });
  }

  setCopyOnClick(itemSelector, target, directly ,callback, multiselect) {
    const items = this.content.querySelectorAll(itemSelector);
    items.forEach((item) => {
      item.addEventListener("click", (e) => {
        e.target.classList.add("clicked");

        if(multiselect) {
            e.target.classList.toggle("selected");
            e.target.parentNode.parentNode.classList.toggle('choosen');
        } else {
          const chosen = this.content.querySelector('.selected');
          if(chosen) {
            chosen.classList.remove('selected');
            chosen.parentNode.parentNode.classList.remove('choosen');
          }

          e.target.classList.add('selected');
          e.target.parentNode.parentNode.classList.add('choosen');
        }
        


        const clone = this.copyOnClick(e.target);

        if(directly) {
          target.appendChild(clone);
          callback(clone);
        }

        setTimeout(() => e.target.classList.remove("clicked"), 300);

      });
    });
  }

  copyOnClick(source) {
    const clone = source.cloneNode(true);

    clone.classList.remove("clicked");
    clone.classList.remove("selected");
    clone.classList.add("inserted");

    this.selectedItems.push(clone);

    return clone;
  }

  getSelectedItems() {
    return this.content.querySelectorAll(':not(.disabled).selected');
  }
}