export function DragAndDrop(container, options) {
  const opts = {
    rootClass: "dd",
    listClass: "dd-list",
    itemClass: "dd-item",
    dragClass: "dd-dragging",
    placeClass: "dd-placeholder",
    ghostClass: "dd-ghost",
    nested: false,
    sortable: true,
    grid: false,
    onAdd: null,
    onChange: null,
    ...options,
  };

  const mouse = {
    x: 0,
    y: 0
  }

  let  draggables;
  let lists, 
      posX, 
      placeEl, 
      ghostEl, 
      prevSibling, 
      draggingEl, 
      cloneEl, 
      insertList, 
      insertItemParent,
      previousAfterElement,
      redrawing = true;

  const _init = function () {

   draggables = [...container.querySelectorAll(`.${opts.itemClass}`)];
   lists = container.querySelectorAll(`.${opts.listClass}`);

    draggables.forEach((draggable) => {
      draggable.setAttribute("draggable", "true");

      draggable.addEventListener("dragstart", (e) =>_onDragStart(e, draggable));
      draggable.addEventListener("dragend", (e) => _onDragEnd(e, draggable));
    });

    if (opts.sortable) {
      lists.forEach((list) => {
        list.addEventListener("dragover", (e) => _onDragOver(e, list));
        list.addEventListener("dragenter", (e) => _onDragEnter(e, list));
        list.addEventListener("drop", (e) => _onDrop(e, list));
      });
    }
  };

  const _createPlaceholder = function(e, draggable) {
    ghostEl = draggable.cloneNode(true);
    ghostEl.classList.add(opts.ghostClass);
    ghostEl.style.width = draggable.style.width;
    draggable.appendChild(ghostEl);
    e.dataTransfer.setDragImage(ghostEl, 0, 0);


    placeEl = document.createElement("div");
    placeEl.style.width = `${draggable.offsetWidth}px`;
    placeEl.style.height =  `${draggable.offsetHeight}px`;
    placeEl.classList = opts.placeClass;

    draggable.prepend(placeEl);
  }

  const _onDragStart = function (e, draggable) {
    e.stopPropagation();
  
    prevSibling = draggable.previousElementSibling;

    _createPlaceholder(e, draggable);

    draggable.classList.add(opts.dragClass);
  };

  const _onDragEnd = function (e, draggable) {
    e.stopPropagation();

    draggable.classList.remove(opts.dragClass);
    if(placeEl) placeEl.remove();
    if(ghostEl) ghostEl.remove();
    ghostEl = null;
    placeEl = null;

    draggingEl = null;

    if(opts.onChange) opts.onChange();
  };

  const _onDragOver = function (e, list) {
    
    e.preventDefault();
    e.stopPropagation();

    if(!draggingEl) draggingEl = document.querySelector(`.${opts.dragClass}`)

    if(!placeEl) _createPlaceholder(e, draggingEl);

    // const mouseMoveX = mouse.x - e.clientX;
    // const mouseMoveY = mouse.y - e.clientY;

    //  if(mouseMoveX == 0 & mouseMoveY == 0)
    //   return;

    let afterElement = getDragAfterElement(list, e.clientY, e.clientX);

    // if(afterElement == previousAfterElement) {
    //   redrawing = false;
    // } else {
    //   previousAfterElement = afterElement
    //   redrawing = true;
    //   console.log(redrawing)
    //   //setTimeout(()=>{redrawing = false}, 500);
    // }

    // if(!redrawing) return;

    prevSibling = draggingEl.previousElementSibling;

    // nested
    if(opts.nested && !opts.grid) {

      if (posX - e.clientX > 20) {
        const newDepth = draggingEl.parentNode.closest(`.${opts.itemClass}`);
  
        if (newDepth && !draggingEl.nextSibling) {
          afterElement = getDragAfterElement(newDepth.parentNode, e.clientY, e.clientX);
  
          newDepth.parentNode.insertBefore(draggingEl, newDepth.nextSibling);
          posX = e.clientX;
          return;
        }
      } else if (posX - e.clientX < -20) {
        if (prevSibling) {
          const newDepth = prevSibling.querySelector(`.${opts.listClass}`);
          if (newDepth) newDepth.appendChild(draggingEl);
          posX = e.clientX;
          return;
        }
      }
    }

    //place element
    if (list.parentNode == draggingEl) return;

    if (!afterElement) {
      list.appendChild(draggingEl);
      if(cloneEl) cloneEl.style.display = 'block';

      // mouse.x = e.clientX;
      // mouse.y = e.clientY;
      //posX = e.clientX;

    } else {
      if (draggingEl.parentNode.parentNode == afterElement)
        list.appendChild(draggingEl);
      else list.insertBefore(draggingEl, afterElement);

      if(cloneEl) cloneEl.style.display = 'block';
      // mouse.x = e.clientX;
      // mouse.y = e.clientY;
      //posX = e.clientX;

    }
  };

  const _onDrop = function (e, list) {
    e.stopPropagation();
  
    const newItem = draggables.find(el => el == draggingEl);
    
    if(!newItem) {

      const clone = draggingEl.cloneNode(true);

      clone.addEventListener("dragstart", (e) =>_onDragStart(e, clone));
      clone.addEventListener("dragend", (e) => _onDragEnd(e, clone));

      clone.querySelector(`.${opts.ghostClass}`).remove();
      clone.querySelector(`.${opts.placeClass}`).remove();

      clone.classList.remove(opts.dragClass);

      draggingEl.parentNode.replaceChild(clone, draggingEl);

      if(opts.nested) {
        const nestedList = document.createElement('ul');
        nestedList.classList = opts.listClass;

        nestedList.addEventListener("dragover", (e) => _onDragOver(e, nestedList));
        nestedList.addEventListener("dragenter", (e) => _onDragEnter(e, nestedList));
        nestedList.addEventListener("drop", (e) => _onDrop(e, nestedList));

        clone.appendChild(nestedList);  
      }

      if(opts.onAdd) opts.onAdd(clone)
      draggables.push(clone);
    }

    draggingEl = null;
  };

  const _onDragEnter = function (e, list) {  
    e.stopPropagation();
    posX = e.clientX;
  }

  const getDragAfterElement = function (list, y, x) {

    const draggableElements = [
      ...container.querySelectorAll(
        `.${opts.itemClass}:not(.${opts.dragClass})`
      ),
    ];

    return draggableElements
      .filter((el) => el.parentNode == list)
      .reduce(
        (closest, child) => {
          const box = child.getBoundingClientRect();

          if(!opts.grid) {
            //vertical sort
            const offset = {
              x: x - box.left - box.width / 2,
              y: y - box.top - box.height / 2
            }
            if (offset.y < 0 && offset.y > closest.offset.y) {
              return { offset: offset, element: child };
            } else {
              return closest;
            }
          } 
          else {
            //grid sort
            const offset = {
              x: x - box.left - box.width / 2,
              y: y - box.top - box.height
            }

            if (offset.x < 0 && 
                offset.y < 0 &&
                offset.x > closest.offset.x &&
                offset.y > closest.offset.y) {
              return { offset: offset, element: child };
            } else {
              return closest;
            }
          }
          
        },
        { offset: {x: Number.NEGATIVE_INFINITY, y: Number.NEGATIVE_INFINITY} }
      ).element;
  };

  //vkládání nového elementu do listu
  const _initInsertList = function(list) {
    insertList = list;
    const items = insertList.querySelectorAll(`.${opts.itemClass}`);

    items.forEach(item => {
      item.setAttribute("draggable", "true");

      item.addEventListener("dragstart", (e) =>_onAddStart(e, item));
      item.addEventListener("dragend", (e) =>_onAddEnd(e, item));
    })
  }

  const _onAddStart = function(e, draggable) {
    e.stopPropagation();

    insertItemParent = draggable.parentNode;
    
    cloneEl = draggable.cloneNode(true);
    cloneEl.style.display = 'none';

    insertItemParent.insertBefore(cloneEl, draggable);

    draggable.classList.add(opts.dragClass);


  }

  const _onAddEnd = function(e, draggable) {

    _onDragEnd(e, draggable);

    if(cloneEl) {
      draggable.classList.remove(opts.dragClass);
      insertItemParent.replaceChild(draggable, cloneEl);
      cloneEl = null;
    }


  }

  const _addItem = function(item) {
    item.setAttribute("draggable", "true");

    item.addEventListener("dragstart", (e) =>_onDragStart(e, item));
    item.addEventListener("dragend", (e) => _onDragEnd(e, item));

    draggables.push(item);
  }

  return {
    init: _init,
    initInsertList: _initInsertList,
    addItem: _addItem
  };
}

