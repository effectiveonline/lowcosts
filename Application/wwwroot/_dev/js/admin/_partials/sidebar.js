import { getHtml } from "./utils.js";

export class Sidebar {
  constructor() {
    this.sidebarBx = document.querySelector(".sidebar");
    this.closeBtn = this.sidebarBx.querySelector(".sidebar__close");
    this.sidebarContent = this.sidebarBx.querySelector(".sidebar__content");
    this.activeCard = null;

    //obsah nalevo od sidebaru
    this.mainContent = document.querySelector(
      "section.content .container-fluid"
    );

    this.closeBtn.addEventListener("click", (_) => this.closeSidebar());
  }

  closeSidebar() {
    this.sidebarBx.classList.add("sidebar--hide");
    this.mainContent.classList.remove("open");
  }

  openSidebar() {
    this.sidebarBx.classList.remove("sidebar--hide");
    this.mainContent.classList.add("open");
  }

  init() {
    this.cards = this.sidebarBx.querySelectorAll(".btn-link");
    this.cards.forEach((card) =>
      card.addEventListener("click", (e) => {
        if (e.target.closest(".card").classList.contains("active"))
          e.stopPropagation();
        this.fullHeight(e.target);
      })
    );

    this.cards[0].click();
  }

  async loadContent(url) {
    this.sidebarContent.innerHTML = await getHtml(url);

    this.init();

    return this.sidebarContent;
  }

  setContent(content) {
    this.sidebarContent.innerHTML = content;

    this.init();

    return this.sidebarContent;
  }

  getContent() {
    return this.sidebarContent;
  }

  alterContent(newContent) {
    this.sidebarContent.remove();

    this.sidebarBx.querySelector(".sidebar__inner").appendChild(newContent);
  }

  fullHeight(card) {
    if (this.activeCard) this.activeCard.classList.remove("active");

    this.activeCard = card.closest(".card");
    this.activeCard.classList.add("active");
  }
}
