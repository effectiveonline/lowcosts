import { WarrningModal } from "../modals/WarningModal.js";
import { DragAndDrop } from "../_partials/DragAndDrop.js";
import { initScripts } from "./utils.js";

const defOptions = {
  listClass: "dd-list",
  itemClass: "dd-item",
  containerClass: 'individual-layout',
  insertListClass: "avalible-sections",
  form: null,
  inputName: "Sections[@]",
  onChange: null,
};

export function Draggable(container, options = defOptions) {
  const list = container.querySelector(`.${options.listClass}`);
  const avalibleList = container.querySelector(`.${options.itemToAddClass}`);

  let removeBtns;

  const _initList = () => {
    removeBtns = list.querySelectorAll(".item-delete");
    _onRemove();

    return Sortable.create(list, {
      group: {
        name: "shared",
        pull: "clone",
        put: true,
      },
      sort: true,
      animation: 150,
      handle: ".panel-heading",
      ghostClass: "dd-placeholder-none",
      forceFallback: true,
      fallbackClass: "dragged-cursor",
      onAdd: function (evt) {
        options.onChange ? options.onChange() : null;
        _itemAdded(evt);
      },
      onUpdate: function (evt) {
        options.onChange ? options.onChange() : null;
      },
    });
  };

  const _initAvalibleList = () => {
    return Sortable.create(avalibleList, {
      group: {
        name: "shared",
        pull: "clone",
        put: false,
      },
      sort: false,
      animation: 150,
      handle: ".panel-heading",
      ghostClass: "dd-placeholder-none",
      forceFallback: true,
      fallbackClass: "dragged-cursor",
    });
  };

  const _itemAdded = (evt) => {
    evt.item.classList.add("new");

    const closeBtn = document.createElement("a");
    closeBtn.classList = "item-delete";
    closeBtn.innerHTML = '<i class="fa fa-times"></i>';
    evt.item.querySelector(".panel-tools").appendChild(closeBtn);

    closeBtn.addEventListener("click", (_) =>
      closeBtn.closest(".panel").remove()
    );

    const type = evt.item.getAttribute("data-id");

    evt.item.querySelector(".panel-body").innerHTML = `
    <div class="form-group">
      <label>Název</label>
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Name" value="${evt.item.innerText}" class="form-control" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Type" value="${type}" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Order" value="${evt.newIndex + 1}" class="order" />
    </div>
       `;
  };

  const _onRemove = () => {
    onItemRemove(removeBtns, ".panel", options.onChange);
  };

  const _sort = () => sortList(list, ".panel");

  if (list) _initList();
  if (avalibleList) _initAvalibleList();

  return {
    sortOrder: _sort,
  };
}

 function onItemRemove(
  removeBtns,
  itemSelector = ".dd-item",
  callback = null
  ) {
    removeBtns.forEach((rmBtn) => {
      rmBtn.addEventListener("click", async (e) => {
        e.preventDefault();
        e.stopPropagation();
        
        const warning = WarrningModal();
        const confirm = await warning.show(
          "Opravdu chcete odstranit? Dojde ke smazání veškerého obsahu."
          );
          
          if (!confirm) return;
          
          rmBtn.closest(itemSelector).classList.add("removed");
          
          if (callback) callback();
        });
      });
    }
    
 function sortList(list, itemSelector) {
      if (!list) return;

      let order = 1;
      const sections = list.querySelectorAll(itemSelector);
      let itemsCount = list.querySelectorAll(`${itemSelector}:not(.new)`).length;
      
      sections.forEach((item) => {
        const orderInput = item.querySelector("input.order");
        const isNew = item.classList.contains("new") ? true : false;
        
        if (isNew) {
          const inputs = item.querySelectorAll("input");
          
          inputs.forEach((input) => {
            let name = input.getAttribute("name").replace("@", `${itemsCount}`);
            input.setAttribute("name", name);
          });

          const textAreas = item.querySelectorAll("texarea");
          
          textAreas.forEach((textArea) => {
            let name = textArea.getAttribute("name").replace("@", `${itemsCount}`);
            textArea.setAttribute("name", name);
          });
          
          item.classList.remove("new");
          itemsCount++;
        }
        
    if (item.classList.contains("removed")) orderInput.value = -1;
    else {
      orderInput.value = order;
      order++;
    }
  });
}

 export function initDragAndDrop(container, opts = defOptions, initInsertList = true){
  const options = {...defOptions, ...opts}
  const list = container.querySelector(`.${options.containerClass}`);
  const removeBtns = list.querySelectorAll(".item-delete:not(.panel--inner .item-delete)");

  const _sortList = function() {
      if (!list) return;

      let order = 1;
      const sections = list.querySelectorAll(`.${options.itemClass}:not(.panel--inner)`);
      let itemsCount = list.querySelectorAll(`.${options.itemClass}:not(.new):not(.panel--inner)`).length;
      
      sections.forEach((item) => {
        const orderInput = item.querySelector("input.order");
        const isNew = item.classList.contains("new") ? true : false;
        
        if (isNew) {
          const inputs = item.querySelectorAll("input");
          
          inputs.forEach((input) => {
            let name = input.getAttribute("name").replace("@", `${itemsCount}`);
            input.setAttribute("name", name);
          });

          const textAreas = item.querySelectorAll("textarea");
          
          textAreas.forEach((textArea) => {
            let name = textArea.getAttribute("name").replace("@", `${itemsCount}`);
            textArea.setAttribute("name", name);
          });
          
          item.classList.remove("new");
          itemsCount++;
        }
        
    if (item.classList.contains("removed")) orderInput.value = -1;
    else {
      orderInput.value = order;
      order++;
    }
  });
  }

  const _itemAdded = (item) => {
    initScripts('',item);

    item.classList.add("new");
  
    const closeBtn = document.createElement("a");
    closeBtn.classList = "item-delete";
    closeBtn.innerHTML = '<i class="fa fa-times"></i>';
    item.querySelector(".panel-tools").appendChild(closeBtn);
  
    closeBtn.addEventListener("click", (_) =>
      closeBtn.closest(".panel").remove()
    );
  
    const type = item.getAttribute("data-id");
  
    item.querySelector(".panel-body").innerHTML = `
    <div class="form-group">
      <label>Název</label>
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Name" value="${item.innerText}" class="form-control" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Type" value="${type}" />
      <input type="text" ${options.form ? `form=${options.form}` : ""} name="${
      options.inputName
    }.Order" value="0" class="order" />
    </div>
       `;
  };

  const _initInsertList = (list) => {
    dd.initInsertList(list);
  }

  const dd = DragAndDrop(list, {
    itemClass: options.itemClass,
    placeClass: 'dd-placeholder',
    nested: options.nested ? options.nested : false,
    onAdd: options.onAdd ? options.onAdd : _itemAdded,
    onChange: options.onChange ? options.onChange : null,
    grid: options.grid ? options.grid : false
  });


  dd.init();

  if(options.insertListClass && initInsertList)
    dd.initInsertList(container.querySelector(`.${options.insertListClass}`));

  onItemRemove(removeBtns, `.${options.itemClass}`, _sortList)

  return { 
    sortOrder: _sortList,
    initInsertList: _initInsertList,
    reinitDD: dd.init,
    addItem: dd.addItem
    }
}