export const constants = {
  container  : document.querySelector(".basicContent"),
  btnSave    : document.querySelector('button[data-action="save"]'),
  btnDelete  : document.querySelector('button[data-action="delete"]'),
  basicUrl   : document.querySelector(".btn-group").getAttribute("data-url"),
  msgTitle   : document.querySelector(".view-header .header-title h3").innerText,
  subPageBtn : document.querySelector('button[data-action="subpage"]')
}
