export function CustomEvents() {

  const initItems = new Event('itemsChange');
  const showLoader = new Event('showLoader');
  const hideLoader = new Event('hideLoader');

  return {
    initItems,
    showLoader,
    hideLoader
  }
  
}