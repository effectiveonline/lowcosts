import * as luna from "./luna.js";
import { GetModal } from "../modals/Modal.js";
import { CustomEvents } from "./customEvents.js";
import { FormsHandle } from "./_form.js";
import { WarrningModal } from "../modals/WarningModal.js";

const events = CustomEvents();
const body = document.body;
const formsHandle = FormsHandle();
const allowed = document.querySelector('button[data-action="save"]');

export function addGlobalEventListener(
  type,
  selector,
  callback,
  opts = { once: false, capture: false }
) {
  document.addEventListener(
    type,
    (e) => {
      if (e.target.matches(selector)) callback(e);
    },
    opts
  );
}

export function initScripts(selector, container = null) {
  let editable = null;
  if (allowed) editable = allowed.getAttribute("data-alowed");

  container = container ? container : document.querySelector(selector);

  initSwitchers(container.querySelectorAll(".js-switch"));

  const dataTables = container.querySelectorAll(".data-table");
  for (let i = 0; i < dataTables.length; i++) {
    initDataTable(`#${dataTables[i].id}`);
  }

  const multiselect = container.querySelectorAll(".select2");
  for (let i = 0; i < multiselect.length; i++) {
    $(multiselect[i]).select2({
      // placeholder: "Zobrazit dostupné",
      allowClear: true,
    });
  }

  luna.initLunaScripts();
}

export function initSwitchers(switchers) {
  if (switchers) {
    for (let i = 0; i < switchers.length; i++) {
      const switchery = new Switchery(switchers[i], {
        color: "#f6a821",
        size: "small",
      });
      if (
        switchers[i].getAttribute("data-disabled") === "True" ||
        (allowed && allowed.getAttribute("data-alowed") === "False")
      ) {
        switchery.disable();
      }
    }
  }
}

export function initUnsaveWarning(container, callback) {
  const inputs = container.querySelectorAll("input");
  const tArea = container.querySelectorAll("textarea");
  const editable = allowed.getAttribute("data-alowed");

  if (editable && editable === "False") {
    disableInputs(inputs, tArea);
    return false;
  }

  for (let i = 0; i < inputs.length; i++) {
    if (!inputs[i].classList.contains("showSection"))
      inputs[i].addEventListener("change", (_) => callback());
  }

  for (let i = 0; i < tArea.length; i++) {
    tArea[i].addEventListener("change", (_) => callback());
  }
}

export async function unsaveWarning(e, content) {
  if (!content.isUnsave() || e.target.hasAttribute("aria-expanded")) return;

  e.stopImmediatePropagation();
  e.preventDefault();

  const warning = WarrningModal();
  const confirm = await warning.show("Máte neuložené změny. Pokračovat?");

  if (!confirm) return;

  content.setUnsave(false);

  e.target.click();
  return;
}

export function disableInputs(inputs, tArea) {
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].disabled = true;
  }

  for (let i = 0; i < tArea.length; i++) {
    tArea[i].disabled = true;
  }
  if ($(".summernote").length > 0) $(".summernote").summernote("disable");
}

export async function onSubmit(container, responseTarget = null) {
  const timer = setTimeout(() => {
    body.dispatchEvent(events.showLoader);
  }, 100);
  container.addEventListener(
    "submitFinish",
    (e) => {
      onSubmitFinish(e, timer, responseTarget);
    },
    { once: true }
  );
  formsHandle.onSubmit(container);
}

function onSubmitFinish(e, timer, responseTarget) {
  const res = e.detail.result;
  const invalid = res.filter((form) => form.response !== "true");
  let errMsg = "";
  const msgTitle = document.querySelector(".view-header .header-title h3")
    .innerText;

  if (invalid.length > 0) {
    invalid.forEach((f) => {
      const targetSelector = responseTarget ? responseTarget : `#${f.id}`;
      const target = document.querySelector(targetSelector);

      target.innerHTML = f.response;

      initScripts(targetSelector);

      const error = target.querySelector(`.text-danger`);
      if (error) errMsg = error.innerText;
    });

    toastr["error"](errMsg, msgTitle);
  } else {
    const errors = document.querySelectorAll(".text-danger");
    for (let i = 0; i < errors.length; i++) {
      errors[i].parentNode.removeChild(errors[i]);
    }

    toastr["success"]("Změny byly uloženy", msgTitle);
  }

  clearTimeout(timer);
  body.dispatchEvent(events.hideLoader);
}

export async function getHtml(url) {
  try {
    const response = await fetch(url);
    return await response.text();
  } catch (e) {
    toastr["error"]("Něco se pokazilo", "");
  }
}

export async function getPartialHtml(url, selector) {
  const container = document.createElement("div");
  container.innerHTML = await getHtml(url);

  return container.querySelector(selector);
}

export function initModal(e) {
  const btn = e.target;
  let id = btn.getAttribute("data-modalId");
  let url = btn.getAttribute("data-url");
  const allowed = btn.getAttribute("data-alowed");

  if (allowed && allowed === "False") {
    id = "denied";
    url = "/admin/denied-modal";
  }

  if (!url) {
    url = document.querySelector(".btn-group").getAttribute("data-url");
    url += `/${btn.getAttribute("data-action")}`;
  }

  const match = GetModal(id);
  const m = match.modal(url, id);
  m.init();
}

export function initDataTable(selector) {
  const numRecord = 13;

  if ($(selector).length > 0) {
    const table = $(selector).DataTable({
      dom: "<'row'<'col-sm-12'f>>t<'row'<'col-sm-12'p>>",
      lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, "All"],
      ],
      iDisplayLength: numRecord,

      language: {
        search: "Rychlé hledání:",
        zeroRecords: "Nebyl nalezen žádný záznam.",
        info: "Zobrazena strana _PAGE_ z _PAGES_",
        infoEmpty: "Vašemu zadání neodpovídá žádný záznam!",
        infoFiltered: "(Celekm prohledáno záznamů: _MAX_)",
        paginate: {
          previous: "Předchozí",
          next: "Další",
        },
      },
    });

    if (table.data().length <= numRecord)
      document
        .querySelector(selector)
        .parentNode.querySelector(".dataTables_paginate").style.display =
        "none";
  }
}

export function changeRangeValue(rangeInput) {
  rangeInput.parentNode.querySelector(".rangeValue").innerText =
    rangeInput.value;
}

export function hardUrlHandle(container) {
  let nameInput = container.querySelector(".name");

  if (nameInput) {
    nameInput = nameInput.parentNode.parentNode;

    const urlCheckBox = container.querySelector(".js-switch.hard-url");
    const urlBox = container.querySelector(".form-group.hard-url");

    if ($(urlCheckBox).is(":checked")) {
      $(urlBox).show();
      nameInput.classList.add("col-md-6");
    }

    $(urlCheckBox).on("change", function () {
      if (this.checked) {
        nameInput.classList.add("col-md-6");
        nameInput.classList.add("transition");
        setTimeout(() => $(urlBox).show(200), 300);
        $(urlBox).focusin();
      } else {
        setTimeout(() => nameInput.classList.remove("col-md-6"), 200);
        $(urlBox).hide(200);
      }
    });
  }
}
