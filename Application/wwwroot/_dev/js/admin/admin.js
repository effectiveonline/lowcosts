﻿import "/theme/vendor/switchery/switchery.min.js";
import "/lib/sortable/Sortable.js";

import * as utils from "./_partials/utils.js";
import { Pages } from "./modules/pages/Pages.js";
import { HomePage } from "./modules/pages/HomePage.js";
import { Layouts } from "./modules/layouts/Layouts.js";
import { initLunaScripts } from "./_partials/luna.js";
import { Basic } from "./modules/basic/Basic.js";
import { UserAccount } from "./modules/settings/UserAccount.js";
import { Events } from "./modules/events/Events.js";
import { Menu } from "./modules/menu/Menu.js";
import { Photogallery } from "./modules/photogallery/Photogallery.js";
import { Templates } from "./modules/templates/Templates.js";
import { ContactPerson } from "./modules/settings/ContactPerson.js";
import { WebMessages } from "./modules/webmessages/WebMessages.js";
import { Inquries } from "./modules/inquries/Inquries.js";

window.onload = () => {
  const loader = document.querySelector(".preload-container");
  const body = document.querySelector("body");
  const container = document.querySelector("#mainContainer");

  initLunaScripts();
  utils.initScripts("#mainContainer");

  // default setting for toastr
  toastr.options = {
    debug: false,
    newestOnTop: false,
    positionClass: "toast-bottom-right",
    closeButton: true,
    progressBar: true,
  };

  //load js for page

  const getScript = (id) => {
    const types = [
      { id: "pagesEdit", script: Pages },
      { id: "userAccount", script: UserAccount },
      { id: "basicEdit", script: Basic },
      { id: "homePage", script: HomePage },
      { id: "layouts", script: Layouts },
      { id: "eventsEdit", script: Events },
      { id: "menu", script: Menu },
      { id: "photogallery", script: Photogallery },
      { id: "templates", script: Templates },
      { id: "contactPersons", script: ContactPerson },
      { id: "webMessages", script: WebMessages },
      { id: "inquries", script: Inquries },
    ];

    return types.find((type) => type.id == id);
  };

  const useScript = document.querySelector("div[data-script]");
  if (useScript) getScript(useScript.getAttribute("data-script")).script();

  //shared listeners
  body.addEventListener("showLoader", () => (loader.style.display = "flex"));
  body.addEventListener("hideLoader", () => (loader.style.display = "none"));

  utils.addGlobalEventListener("click", "#btnSaveConfigurations", (e) =>
    utils.onSubmit(container)
  );
  utils.addGlobalEventListener("click", "[data-modalId]", (e) => {
    e.preventDefault();
    utils.initModal(e);
  });
  utils.addGlobalEventListener(
    "click",
    ".modal.fade.show",
    (e) => e.stopPropagation(),
    { capture: true }
  );

  //range inputs
  utils.addGlobalEventListener(
    "input",
    "input[type='range']",
    (e) => utils.changeRangeValue(e.target),
    { capture: true }
  );
};
