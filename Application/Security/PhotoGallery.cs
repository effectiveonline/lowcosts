﻿using Data;
using System.Collections.Generic;

namespace EffectiveTools.Security
{
    public class PhotoGallery : Data.Security
    {
        public PhotoGallery()
        {
            Name = "photogallery";
            Text = "Galerie fotografií";
            Description = " ";
            Roles = new List<SecurityRole>();

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_edit",
                Text = "Povolit vytváření a úpravy fotogalerie",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_public",
                Text = "Povolit zveřejňování fotogalerie",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_delete",
                Text = "Povolit mazání fotogalerie",
            });
        }
    }
}
