﻿using Data;
using System.Collections.Generic;

namespace EffectiveTools.Security
{
    public class Pages : Data.Security
    {
        public Pages()
        {
            Name = "pages";
            Text = "Stránky a podstránky";
            Description = " Úpravy nastavení zahrnují komplexní oprávnění pro Menu, Patičky, Pop-Up okna a Rozložení stránek)";
            Roles = new List<SecurityRole>();

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_edit",
                Text = "Povolit vytváření a úpravy obsahu",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_public",
                Text = "Povolit zveřejňování obsahu",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_delete",
                Text = "Povolit mazání obsahu",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_homepage",
                Text = "Povolit úpravy titulní stránky",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_setting",
                Text = "Povolit úpravy v nastavení",
            });
        }
    }
}
