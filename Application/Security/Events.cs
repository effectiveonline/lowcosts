﻿using Data;
using System.Collections.Generic;

namespace EffectiveTools.Security
{
    public class Events : Data.Security
    {
        public Events()
        {
            Name = "events";
            Text = "Události";
            Description = " ";
            Roles = new List<SecurityRole>();

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_edit",
                Text = "Povolit vytváření a úpravy událostí",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_public",
                Text = "Povolit zveřejňování událostí",
            });

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_delete",
                Text = "Povolit mazání událostí",
            });

            /*
            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_setting",
                Text = "Povolit úpravy v nastavení",
            });
            */
        }
    }
}
