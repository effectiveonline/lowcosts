﻿using Data;
using System.Collections.Generic;

namespace EffectiveTools.Security
{
    public class Application : Data.Security
    {
        public Application()
        {
            Name = "app";
            Text = "Aplikace";
            Description = "popis pro panel";
            Roles = new List<SecurityRole>();

            Roles.Add(new SecurityRole
            {
                Name = Name + "_accounts",
                Text = "Povolit správu uživatelských účtů"
            }) ;

            Roles.Add(new SecurityRole
            {
                Name = Name + "_groups",
                Text = "Povolit správu uživatelských skupin"
            });

            Roles.Add(new SecurityRole
            {
                Name = Name + "_setting",
                Text = "Povolit správu nastavení aplikace"
            });
        }
    }
}
