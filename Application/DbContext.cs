﻿using Data;
using EffectiveTools.Models;
using System;

namespace EffectiveTools
{
    public static class DbContext
    {
        public static void Inicialize()
        {
            AdminConfig = new DbSet<AdminConfig>();
            WebConfig = new DbSet<WebConfig>();

            AdminUsers = new DbSet<UserModel>();
            AdminGroups = new DbSet<GroupModel>();
            LogItems = new DbSet<UserLogModel>();

            Layouts = new DbSet<LayoutModel>();
            LayoutItems = new DbSet<LayoutItemModel>();

            Templates = new DbSet<TemplateModel>();
            TemplateSections = new DbSet<TemplateSectionModel>();

            Pages = new DbSet<ContentModel>();

            WebMails = new DbSet<WebMailModel>();
            WebNewsRecipients = new DbSet<WebNewModel>();

            SectionShowAsItems = new DbSet<SectionShowAsItem>();
            ContentSections = new DbSet<ContentSectionModel>();

            SettingCompany = new DbSet<CompanyModel>();
            SettingPerson = new DbSet<PersonModel>();

            PopUps = new DbSet<PopUpModel>();

            if (!AdminUsers.HaveData) AdminUsers.Add(new UserModel
            {
                UserName = "Admin",
                UserPass = Globals.Md5("12345"),
                FirstName = "Effective",
                LastName = "Online",
                IsAdmin = true,
                ChanegePassword = true
            });

            if (!AdminConfig.HaveData) AdminConfig.Add(new AdminConfig());
            if (!WebConfig.HaveData) WebConfig.Add(new WebConfig());
            if (!SettingCompany.HaveData) SettingCompany.Add(new CompanyModel());
            if (!Layouts.HaveData) createDefaultLayout();
            if (!SectionShowAsItems.HaveData) createDefaultSectionShowAs();
        }

        public static DbSet<AdminConfig> AdminConfig { get; set; }
        public static DbSet<WebConfig> WebConfig { get; set; }
        public static DbSet<UserModel> AdminUsers { get; set; }
        public static DbSet<GroupModel> AdminGroups { get; set; }
        public static DbSet<UserLogModel> LogItems { get; set; }

        public static DbSet<SectionShowAsItem> SectionShowAsItems { get; set; }

        public static DbSet<LayoutModel> Layouts { get; set; }
        public static DbSet<LayoutItemModel> LayoutItems { get; set; }

        public static DbSet<TemplateModel> Templates { get; set; }
        public static DbSet<TemplateSectionModel> TemplateSections { get; set; }

        public static DbSet<ContentModel> Pages { get; set; }

        public static DbSet<WebMailModel> WebMails { get; set; }
        public static DbSet<WebNewModel> WebNewsRecipients { get; set; }

        public static DbSet<ContentSectionModel> ContentSections { get; set; }

        public static DbSet<CompanyModel> SettingCompany { get; set; }
        public static DbSet<PersonModel> SettingPerson { get; set; }

        public static DbSet<PopUpModel> PopUps { get; set; }

        public static void CreateHomepage()
        {
            ContentModel homepage = new ContentModel
            {
                Folder = Managers.StaticFilesManager.CreateFolder("Titulní stránka", "Titulní stránka"),
                Modul = "Pages",
                Type = "Page",
                Name = "Titlní stránka",
                Title = "Titlní stránka",
                Description = "Titlní stránka",
                Url = "/",
                IsHomepage = true,
                Priority = 8,
                Created = DateTime.Now,
                Modified = DateTime.Now
            };

            Pages.Add(homepage);
        }

        private static void createDefaultLayout()
        {
            LayoutModel layout = new LayoutModel
            {
                Name = "Hlavní",
                Description = "Výchozí rozložení stránek a podstránek.",
                IsDefault = true,
                DisableMenu = false,
                DisableFooter = false
            };

            Layouts.Add(layout);
        }

        private static void createDefaultSectionShowAs()
        {
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Carousel", Text = "Výchozí" });
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Paralax", Text = "Výchozí" });
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Text", Text = "Výchozí" });
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Events", Text = "Výchozí" });

            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Forms", Text = "Kontaktní formulář", Value = "Pages" });

            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Images", Text = "Dlaždice", Value = "Tiles" });
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Images", Text = "Řádkový výpis", Value = "Lines" });
            SectionShowAsItems.Add(new SectionShowAsItem { Section = "Images", Text = "Carousel", Value = "Carousel" });
        }
    }
}
