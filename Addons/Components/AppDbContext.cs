﻿
namespace Components
{
    public class AppDbContext
    {
        public AppDbContext()
        {
            Carousels = new Data.DbSet<CarouselModel>();
            CarouselsData = new Data.DbSet<CarouselDataModel>();
        }

        public Data.DbSet<CarouselModel> Carousels { get; set; }
        public Data.DbSet<CarouselDataModel> CarouselsData { get; set; }
    }
}