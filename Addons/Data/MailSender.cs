﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class MailSender
    {
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }


        public Task<bool> SendAsync()
        {
            return Task.Run(() => { return Send(); });
        }

        public bool Send()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Globals.WebConfig.SmtpServer);

                mail.IsBodyHtml = true;
                mail.From = new MailAddress(Globals.WebConfig.SmtpEmail, Globals.WebConfig.SmtpName);
                mail.To.Add(Recipient);
                mail.Subject = Subject;
                mail.Body = Message;
                

                SmtpServer.Port = Globals.WebConfig.SmtpPort;
                SmtpServer.Credentials = new NetworkCredential(Globals.WebConfig.SmtpUser, Globals.WebConfig.SmtpPass);
                SmtpServer.EnableSsl = Globals.WebConfig.SmtpSSL;

                SmtpServer.Send(mail);

                return true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                return false;
            }
        }

        public Task<bool> SendAsync(MailMessage _message)
        {
            return Task.Run(() => { return Send(_message); });
        }

        public bool Send(MailMessage message)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient(Globals.WebConfig.SmtpServer);

                SmtpServer.Port = Globals.WebConfig.SmtpPort;
                SmtpServer.Credentials = new NetworkCredential(Globals.WebConfig.SmtpUser, Globals.WebConfig.SmtpPass);
                SmtpServer.EnableSsl = Globals.WebConfig.SmtpSSL;

                SmtpServer.Send(message);

                return true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                return false;
            }
        }
    }
}
