﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("LayoutItems")]
    public class LayoutItemModel
    {
        public int Id { get; set; }
        public int Layout { get; set; }
        public int Page { get; set; }
        public int Parent { get; set; }
        public int Order { get; set; }
        public bool CustomText { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
    }
}