﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [Table("WebMessages")]
    public class WebMailModel
    {
        public int Id { get; set; }
        public int ContentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public bool Readed { get; set; }
    }
}
