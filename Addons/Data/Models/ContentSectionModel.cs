﻿using Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("ContentSections")]

    public class ContentSectionModel
    {
        public string Description;

        public int Id { get; set; }
        public int IdPage { get; set; }
        public int IdSekce { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public bool Locked { get; set; }
        public bool Required { get; set; }
    }
}
