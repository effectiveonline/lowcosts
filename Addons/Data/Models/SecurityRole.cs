﻿namespace Data
{
    public class SecurityRole
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public bool Allowed { get; set; }
        public bool DisableChackbox { get; set; }
    }
}
