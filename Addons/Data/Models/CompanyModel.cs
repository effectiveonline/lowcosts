﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("SettingCompany")]
    public class CompanyModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Zadejte Vaše jméno.")]
        [Display(Name = "Jméno")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Zadejte Vaše příjmení.")]
        [Display(Name = "Příjmení")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Zadejte kontaktní telefon.")]
        [Display(Name = "Kontaktní telefon")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Zadejte kontaktní e-mail.")]
        [Display(Name = "Kontaktní e-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Zadejte Ič společnosti.")]
        [Display(Name = "Ič")]
        public string Ic { get; set; }

        [Required(ErrorMessage = "Zadejte Dič společnosti.")]
        [Display(Name = "Dič")]
        public string Dic { get; set; }

        [Required(ErrorMessage = "Zadejte název společnosti.")]
        [Display(Name = "Název společnosti")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Zadejte ulici.")]
        [Display(Name = "Ulice a č.p.")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Zadejte město.")]
        [Display(Name = "Město")]
        public string City { get; set; }

        [Required(ErrorMessage = "Zadejte PSČ.")]
        [Display(Name = "PSČ")]
        public string PosCode { get; set; }

        [Display(Name = "Název společnosti")]
        public string ShipCompayName { get; set; }

        [Display(Name = "Ulice a č.p.")]
        public string ShipStreet { get; set; }

        [Display(Name = "Město")]
        public string ShipCity { get; set; }

        [Display(Name = "PSČ")]
        public string ShipPosCode { get; set; }

        [Display(Name = "Telefon")]
        public string ShipPhone { get; set; }

        [Display(Name = "Jiná doručovací adresa")]
        public bool RequireShipingAdress { get; set; }

        // Sociální sítě
        [Display(Name = "Facebook")]
        public string Facebook { get; set; }

        [Display(Name = "Twitter")]
        public string Twitter { get; set; }

        [Display(Name = "Instagram")]
        public string Instagram { get; set; }

        [Display(Name = "Bezones")]
        public string Bezones { get; set; }

        [Display(Name = "Youtube")]
        public string Youtube { get; set; }
    }
}
