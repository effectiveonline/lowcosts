﻿using System.Collections.Generic;

namespace Data
{
    public class Security
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public bool Allowed { get; set; }
        public bool DisableChackbox { get; set; }
        public bool Checked { get; set; }
        public List<SecurityRole> Roles { get; set; }
    }
}
