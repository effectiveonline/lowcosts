﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("Templates")]
    public class TemplateModel
    {
        public int Id { get; set; }
        public int Folder { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Target { get; set; }
    }
}
