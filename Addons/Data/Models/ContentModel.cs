﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("Content")]
    public class ContentModel
    {
        public int Id { get; set; }
        public int Parent { get; set; }
        public int Folder { get; set; }
        public int Layout { get; set; }
        public string Type { get; set; }
        public int Template { get; set; }
        public int TemplateItems { get; set; }
        public int PopUp { get; set; }
        public int Owner { get; set; }
        public string Modul { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public bool Robots { get; set; } = true;
        public int Priority { get; set; }
        public bool HardUrl { get; set; }
        public bool ShowName { get; set; } = true;
        public bool IsHomepage { get; set; }
        public DateTime? EnabledFrom { get; set; }
        public DateTime? EnabledTo { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}

