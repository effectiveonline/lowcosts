﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("TemplatesSections")]
    public class TemplateSectionModel
    {
        public int Id { get; set; }
        public int IdSekce { get; set; }
        public int Template { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public bool Locked { get; set; }
        public bool Required { get; set; }
    }
}
