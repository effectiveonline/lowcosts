﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("PopUps")]
    public class PopUpModel
    {
        public int Id { get; set; }
        public int IdImage { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public bool ShowAlways { get; set; }
    }
}
