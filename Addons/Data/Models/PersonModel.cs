﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("SettingPersons")]
    public class PersonModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Zadejte jméno kotaktní sosoby.")]
        [Display(Name = "Jméno")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Zadejte příjmení kotaktní sosoby.")]
        [Display(Name = "Příjmení")]

        public string LastName { get; set; }
        [Required(ErrorMessage = "Zadejte telefon kotaktní sosoby.")]
        [Display(Name = "Telefon")]

        public string Phone { get; set; }
        [Required(ErrorMessage = "Zadejte email kotaktní sosoby.")]
        [Display(Name = "Email")]

        public string Email { get; set; }
        [Display(Name = "Krátký popis")]

        public string Description { get; set; }

        public string Photo { get; set; }

        [Display(Name = "Zobrazit na webu")]
        public bool Enabled { get; set; }
    }
}
