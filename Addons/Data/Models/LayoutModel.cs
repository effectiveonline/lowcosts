﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EffectiveTools.Models
{
    [Table("Layouts")]
    public class LayoutModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DisableMenu { get; set; }
        public bool DisableFooter { get; set; }
        public bool IsDefault { get; set; }
    }
}
