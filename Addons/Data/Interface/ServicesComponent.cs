﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Interface
{
    public class ServicesComponent : IComponentHost   //<--- Notice how it inherits IPluginHost interface!
    {
        public ServicesComponent() { }

        private Types.AvailableComponents colAvailableComponents = new Types.AvailableComponents();

        public Types.AvailableComponents AvailableComponents
        {
            get { return colAvailableComponents; }
            set { colAvailableComponents = value; }
        }

        public void FindComponents()
        {
            FindComponents(AppDomain.CurrentDomain.BaseDirectory);
        }

        public void FindComponents(string Path)
        {
            colAvailableComponents.Clear();

            foreach (string fileOn in Directory.GetFiles(Path))
            {
                FileInfo file = new FileInfo(fileOn);

                if (file.Extension.Equals(".dll"))
                {
                    this.AddComponent(fileOn);
                }
            }
        }
        public void CloseComponents()
        {
            foreach (Types.AvailableComponent componentOn in colAvailableComponents)
            {
                //Close all plugin instances
                //We call the plugins Dispose sub first incase it has to do 
                //Its own cleanup stuff
                componentOn.Instance.Dispose();

                //After we give the plugin a chance to tidy up, get rid of it
                componentOn.Instance = null;
            }

            //Finally, clear our collection of available plugins
            colAvailableComponents.Clear();
        }

        private void AddComponent(string FileName)
        {
            //Create a new assembly from the plugin file we're adding..
            Assembly componentAssembly = Assembly.LoadFrom(FileName);

            //Next we'll loop through all the Types found in the assembly
            foreach (Type componentType in componentAssembly.GetTypes())
            {
                if (componentType.IsPublic) //Only look at public types
                {
                    if (!componentType.IsAbstract)  //Only look at non-abstract types
                    {
                        //Gets a type object of the interface we need the plugins to match
                        Type typeInterface = componentType.GetInterface("Interface.IComponent", true);

                        //Make sure the interface we want to use actually exists
                        if (typeInterface != null)
                        {
                            //Create a new available plugin since the type implements the IPlugin interface
                            Types.AvailableComponent newComponent = new Types.AvailableComponent();

                            //Set the filename where we found it
                            newComponent.AssemblyPath = FileName;

                            //Create a new instance and store the instance in the collection for later use
                            //We could change this later on to not load an instance.. we have 2 options
                            //1- Make one instance, and use it whenever we need it.. it's always there
                            //2- Don't make an instance, and instead make an instance whenever we use it, then close it
                            //For now we'll just make an instance of all the plugins
                            newComponent.Instance = (IComponent)Activator.CreateInstance(componentAssembly.GetType(componentType.ToString()));

                            //Set the Plugin's host to this class which inherited IPluginHost
                            newComponent.Instance.Host = this;

                            //Call the initialization sub of the plugin
                            newComponent.Instance.Initialize();

                            //Add the new plugin to our collection here
                            this.colAvailableComponents.Add(newComponent);

                            //cleanup a bit
                            newComponent = null;
                        }

                        typeInterface = null; //Mr. Clean			
                    }
                }
            }

            componentAssembly = null; //more cleanup
        }

        /// <summary>
        /// Displays a feedback dialog from the plugin
        /// </summary>
        /// <param name="Feedback">String message for feedback</param>
        /// <param name="Plugin">The plugin that called the feedback</param>
        public void Feedback(string Feedback, IComponent Config)
        {
            //This sub makes a new feedback form and fills it out
            //With the appropriate information
            //This method can be called from the actual plugin with its Host Property

            //System.Windows.Forms.Form newForm = null;
            //frmFeedback newFeedbackForm = new frmFeedback();

            //Here we set the frmFeedback's properties that i made custom
            //newFeedbackForm.PluginAuthor = "By: " + Plugin.Author;
            //newFeedbackForm.PluginDesc = Plugin.Description;
            //newFeedbackForm.PluginName = Plugin.Name;
            //newFeedbackForm.PluginVersion = Plugin.Version;
            //newFeedbackForm.Feedback = Feedback;

            //We also made a Form object to hold the frmFeedback instance
            //If we were to declare if not as  frmFeedback object at first,
            //We wouldn't have access to the properties we need on it
            //newForm = newFeedbackForm;
            //newForm.ShowDialog();

            //newFeedbackForm = null;
            //newForm = null;

        }
    }
    namespace Types
    {
        /// <summary>
        /// Collection for AvailablePlugin Type
        /// </summary>
        public class AvailableComponents : System.Collections.CollectionBase
        {
            //A Simple Home-brew class to hold some info about our Available Plugins

            /// <summary>
            /// Add a Plugin to the collection of Available plugins
            /// </summary>
            /// <param name="pluginToAdd">The Plugin to Add</param>
            public void Add(Types.AvailableComponent componentToAdd)
            {
                this.List.Add(componentToAdd);
            }

            /// <summary>
            /// Remove a Plugin to the collection of Available plugins
            /// </summary>
            /// <param name="componentToRemove">The Plugin to Remove</param>
            public void Remove(Types.AvailableComponent componentToRemove)
            {
                this.List.Remove(componentToRemove);
            }

            /// <summary>
            /// Finds a plugin in the available Plugins
            /// </summary>
            /// <param name="pluginNameOrPath">The name or File path of the plugin to find</param>
            /// <returns>Available Plugin, or null if the plugin is not found</returns>
            public Types.AvailableComponent Find(string componentNameOrPath)
            {
                Types.AvailableComponent toReturn = null;

                //Loop through all the plugins
                foreach (Types.AvailableComponent componentOn in this.List)
                {
                    //Find the one with the matching name or filename
                    if ((componentOn.Instance.Name.Equals(componentNameOrPath)) || componentOn.AssemblyPath.Equals(componentNameOrPath))
                    {
                        toReturn = componentOn;
                        break;
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Data Class for Available Plugin.  Holds and instance of the loaded Plugin, as well as the Plugin's Assembly Path
        /// </summary>
        public class AvailableComponent
        {
            //This is the actual AvailablePlugin object.. 
            //Holds an instance of the plugin to access
            //ALso holds assembly path... not really necessary
            private IComponent myInstance = null;
            private string myAssemblyPath = "";

            public IComponent Instance
            {
                get { return myInstance; }
                set { myInstance = value; }
            }
            public string AssemblyPath
            {
                get { return myAssemblyPath; }
                set { myAssemblyPath = value; }
            }
        }
    }
}
