﻿using Data;
using EffectiveTools.Models;
using System;
using System.Collections.Generic;

namespace Interface
{
    #region Modul

    public interface IModul
    {
        IModulHost Host { get; set; }

        Data.AdminMenuItem AdminMenuItem { get; }

        List<SelectBoxItem> FormTypes { get; }

        HelpMenuItem Help { get; }

        Object DashBoardModel { get; }

        string Name { get; }
        string Title { get; }

        public string GetFormBody(string id) { return null; }

        void Initialize();
        void Dispose();
    }

    public interface IModulHost
    {
        void Feedback(string Feedback, IModul Modul);
    }

    #endregion

    #region Component

    public interface IComponent
    {
        IComponentHost Host { get; set; }

        string Name { get; }

        void Initialize();
        void Dispose();
    }

    public interface IComponentHost
    {
        void Feedback(string Feedback, IComponent Config);
    }

    #endregion

    #region Template

    public interface ITemplate
    {
        ITemplateHost Host { get; set; }

        string Name { get; }

        void Initialize();
        void Dispose();
    }

    public interface ITemplateHost
    {
        void Feedback(string Feedback, ITemplate Template);
    }

    #endregion

    public static class Services
    {
        public static ServicesModule Modules = new ServicesModule();
        public static ServicesComponent Components = new ServicesComponent();
        public static ServicesTemplate Templates = new ServicesTemplate();
    }
}
