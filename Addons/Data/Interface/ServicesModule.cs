﻿using System;
using System.IO;
using System.Reflection;

namespace Interface
{
    public class ServicesModule : IModulHost   //<--- Notice how it inherits IPluginHost interface!
    {
        public ServicesModule() { }

        private Types.AvailablePlugins colAvailablePlugins = new Types.AvailablePlugins();

        public Types.AvailablePlugins AvailablePlugins
        {
            get { return colAvailablePlugins; }
            set { colAvailablePlugins = value; }
        }

        public void FindPlugins()
        {
            FindPlugins(AppDomain.CurrentDomain.BaseDirectory);
        }

        public bool Conteins(string nazev)
        {
            bool result = false;

            foreach (Types.AvailablePlugin plugin in AvailablePlugins)
            {
                if (plugin.Instance.Name == nazev) result = true;
            }

            return result;
        }

        public Types.AvailablePlugin GetPlugin(string nazev)
        {
            Types.AvailablePlugin result = null;

            System.Collections.Generic.List<string> a = new System.Collections.Generic.List<string>();

            foreach (Types.AvailablePlugin plugin in AvailablePlugins)
            {
                a.Add(plugin.Instance.Name);

                if (plugin.Instance.Name == nazev) result = plugin;
            }

            return result;
        }

        public void FindPlugins(string Path)
        {
            colAvailablePlugins.Clear();

            foreach (string fileOn in Directory.GetFiles(Path))
            {
                FileInfo file = new FileInfo(fileOn);

                if (file.Extension.Equals(".dll"))
                {
                    this.AddPlugin(fileOn);
                }
            }
        }

        public void ClosePlugins()
        {
            foreach (Types.AvailablePlugin pluginOn in colAvailablePlugins)
            {
                pluginOn.Instance.Dispose();
                pluginOn.Instance = null;
            }

            colAvailablePlugins.Clear();
        }

        private void AddPlugin(string FileName)
        {
            Assembly pluginAssembly = Assembly.LoadFrom(FileName);

            foreach (Type pluginType in pluginAssembly.GetTypes())
            {
                if (pluginType.IsPublic)
                {
                    if (!pluginType.IsAbstract)
                    {
                        Type typeInterface = pluginType.GetInterface("Interface.IModul", true);

                        if (typeInterface != null)
                        {
                            Types.AvailablePlugin newPlugin = new Types.AvailablePlugin();

                            newPlugin.AssemblyPath = FileName;
                            newPlugin.Instance = (IModul)Activator.CreateInstance(pluginAssembly.GetType(pluginType.ToString()));
                            newPlugin.Instance.Host = this;
                            newPlugin.Instance.Initialize();

                            this.colAvailablePlugins.Add(newPlugin);

                            newPlugin = null;
                        }

                        typeInterface = null;
                    }
                }
            }

            pluginAssembly = null;
        }

        public void Feedback(string Feedback, IModul Plugin) { }
    }

    namespace Types
    {
        public class AvailablePlugins : System.Collections.CollectionBase
        {
            public void Add(Types.AvailablePlugin pluginToAdd)
            {
                this.List.Add(pluginToAdd);
            }

            public void Remove(Types.AvailablePlugin pluginToRemove)
            {
                this.List.Remove(pluginToRemove);
            }

            public Types.AvailablePlugin Find(string pluginNameOrPath)
            {
                Types.AvailablePlugin toReturn = null;

                foreach (Types.AvailablePlugin pluginOn in this.List)
                {
                    if ((pluginOn.Instance.Name.Equals(pluginNameOrPath)) || pluginOn.AssemblyPath.Equals(pluginNameOrPath))
                    {
                        toReturn = pluginOn;
                        break;
                    }
                }
                return toReturn;
            }
        }

        public class AvailablePlugin
        {
            private IModul myInstance = null;
            private string myAssemblyPath = "";

            public IModul Instance
            {
                get { return myInstance; }
                set { myInstance = value; }
            }
            public string AssemblyPath
            {
                get { return myAssemblyPath; }
                set { myAssemblyPath = value; }
            }
        }
    }
}
