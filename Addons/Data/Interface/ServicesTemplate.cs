﻿using System;
using System.IO;
using System.Reflection;

namespace Interface
{
    public class ServicesTemplate : ITemplateHost
    {
        public ServicesTemplate() { }

        private Types.AvailableTemplates colAvailableTemplates = new Types.AvailableTemplates();

        public Types.AvailableTemplates AvailableTemplates
        {
            get { return colAvailableTemplates; }
            set { colAvailableTemplates = value; }
        }

        public void FindTemplates()
        {
            FindTemplates(AppDomain.CurrentDomain.BaseDirectory);
        }

        public void FindTemplates(string Path)
        {
            colAvailableTemplates.Clear();

            foreach (string fileOn in Directory.GetFiles(Path))
            {
                FileInfo file = new FileInfo(fileOn);

                if (file.Extension.Equals(".dll"))
                {
                    this.AddTemplate(fileOn);
                }
            }
        }
        public void CloseTemplates()
        {
            foreach (Types.AvailableTemplate templateOn in colAvailableTemplates)
            {
                //Close all plugin instances
                //We call the plugins Dispose sub first incase it has to do 
                //Its own cleanup stuff
                templateOn.Instance.Dispose();

                //After we give the plugin a chance to tidy up, get rid of it
                templateOn.Instance = null;
            }

            //Finally, clear our collection of available plugins
            colAvailableTemplates.Clear();
        }

        private void AddTemplate(string FileName)
        {
            //Create a new assembly from the plugin file we're adding..
            Assembly templateAssembly = Assembly.LoadFrom(FileName);

            //Next we'll loop through all the Types found in the assembly
            foreach (Type templateType in templateAssembly.GetTypes())
            {
                if (templateType.IsPublic)
                {
                    if (!templateType.IsAbstract)
                    {
                        Type typeInterface = templateType.GetInterface("Interface.ITemplate", true);

                        if (typeInterface != null)
                        {
                            Types.AvailableTemplate newTemplate = new Types.AvailableTemplate();
                            newTemplate.AssemblyPath = FileName;


                            newTemplate.Instance = (ITemplate)Activator.CreateInstance(templateAssembly.GetType(templateType.ToString()));
                            newTemplate.Instance.Host = this;
                            newTemplate.Instance.Initialize();
                            newTemplate = null;
                        }

                        typeInterface = null;	
                    }
                }
            }

            templateAssembly = null;
        }

        public void Feedback(string Feedback, ITemplate Template)
        {
            //This sub makes a new feedback form and fills it out
            //With the appropriate information
            //This method can be called from the actual plugin with its Host Property

            //System.Windows.Forms.Form newForm = null;
            //frmFeedback newFeedbackForm = new frmFeedback();

            //Here we set the frmFeedback's properties that i made custom
            //newFeedbackForm.PluginAuthor = "By: " + Plugin.Author;
            //newFeedbackForm.PluginDesc = Plugin.Description;
            //newFeedbackForm.PluginName = Plugin.Name;
            //newFeedbackForm.PluginVersion = Plugin.Version;
            //newFeedbackForm.Feedback = Feedback;

            //We also made a Form object to hold the frmFeedback instance
            //If we were to declare if not as  frmFeedback object at first,
            //We wouldn't have access to the properties we need on it
            //newForm = newFeedbackForm;
            //newForm.ShowDialog();

            //newFeedbackForm = null;
            //newForm = null;

        }
    }

    namespace Types
    {
        public class AvailableTemplates : System.Collections.CollectionBase
        {
            public void Add(Types.AvailableTemplate templateToAdd)
            {
                this.List.Add(templateToAdd);
            }


            public void Remove(Types.AvailableTemplate templateToRemove)
            {
                this.List.Remove(templateToRemove);
            }

            public Types.AvailableTemplate Find(string templateNameOrPath)
            {
                Types.AvailableTemplate toReturn = null;

                foreach (Types.AvailableTemplate templateOn in this.List)
                {
                    if ((templateOn.Instance.Name.Equals(templateNameOrPath)) || templateOn.AssemblyPath.Equals(templateNameOrPath))
                    {
                        toReturn = templateOn;
                        break;
                    }
                }
                return toReturn;
            }
        }

        public class AvailableTemplate
        {
            private ITemplate myInstance = null;
            private string myAssemblyPath = "";

            public ITemplate Instance
            {
                get { return myInstance; }
                set { myInstance = value; }
            }
            public string AssemblyPath
            {
                get { return myAssemblyPath; }
                set { myAssemblyPath = value; }
            }
        }
    }
}
