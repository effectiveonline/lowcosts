﻿using EffectiveTools.Models;
using System.Collections.Generic;

namespace Data.WebViewModels
{
    public class WebPageViewModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public bool Robots { get; set; }

        public bool ShowTitle { get; set; }

        public List<ContentSectionModel> Sections { get; set; }
        public List<MenuItem> MenuItems { get; set; }
        
    }
}
