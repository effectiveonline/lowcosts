﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.WebViewModels
{
    public class MenuItem
    {
        public string Url { get; set; }
        public string Text { get; set; }
    }
}
