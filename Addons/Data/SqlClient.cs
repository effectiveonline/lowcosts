﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Data
{
    public class SqlClient
    {
        public static int AddItem(Object item)
        {
            Type itemType = item.GetType();

            IList<PropertyInfo> props = new List<PropertyInfo>(itemType.GetProperties());
            IList<Attribute> atr = new List<Attribute>(itemType.GetCustomAttributes());

            if (atr.Count == 0) return 0;

            string table = "";
            List<string> columns = new List<string>();
            List<string> values = new List<string>();
            List<Item> items = new List<Item>();

            if (atr[0].GetType() == typeof(TableAttribute))
            {
                TableAttribute x = (TableAttribute)atr[0];
                table = x.Name;
            }

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(item, null);

                if (prop.Name == "Id") continue;

                items.Add(new Item { Name = prop.Name, Value = propValue });

                columns.Add("[" + prop.Name + "]");
                values.Add("@" + prop.Name);
            }

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO " + table + " (" + columns.Aggregate((a, b) => a + ", " + b) + ") VALUES (" + values.Aggregate((a, b) => a + ", " + b) + ")";

            foreach (Item itm in items)
            {
                cmd.Parameters.AddWithValue("@" + itm.Name, itm.Value);
            }

            cmd.ExecuteNonQuery();

            int result = LastInsertedId(table, conn);


            conn.Close();

            item = null;

            return result;
        }
        /*
        public static Object GetSection(Type itemType, int id)
        {
            object item = Activator.CreateInstance(itemType);

            IList<PropertyInfo> props = new List<PropertyInfo>(itemType.GetProperties());
            IList<Attribute> atr = new List<Attribute>(itemType.GetCustomAttributes());

            if (atr.Count == 0) return null;

            string table = "";
            List<string> columns = new List<string>();

            if (atr[0].GetType() == typeof(TableAttribute))
            {
                TableAttribute x = (TableAttribute)atr[0];
                table = x.Name;
            }

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(item, null);

                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT (" + columns.Aggregate((a, b) => a + ", " + b) + ") FROM " + table + " WHERE Id = " + id;

            DataTable res = SqlClient.SelectTable(query);

            if (res.Rows.Count == 0) return null;


            foreach (PropertyInfo prop in props)
            {
                item.GetType().GetProperty(prop.Name).SetValue(item, res.Rows[0][prop.Name], null);
            }

            return item;
        }
        */

        private static int LastInsertedId(string tableName, SqlConnection conn)
        {
            int Id = 0;

            SqlCommand cmd = new SqlCommand("SELECT IDENT_CURRENT('" + tableName + "')", conn);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                Id = Convert.ToInt32(rd.GetValue(0));
            }

            return Id;
        }

        public static void ExecuteQuery(string query)
        {
            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }
        /*
        public static int Insert(string query, string table)
        {
            int result = 0;

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            cmd = new SqlCommand("SELECT IDENT_CURRENT('" + table + "')", conn);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                result = Convert.ToInt32(rd.GetValue(0));
            }
            conn.Close();

            return result;
        }
        */
        private static List<string> GetExistsTables()
        {
            List<string> result = new List<string>();

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SELECT name FROM sys.Tables", conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                result.Add(reader["name"].ToString());
            }
            conn.Close();

            return result;
        }

        public static void CheckTables(Object dbContext)
        {
            List<string> tables = SqlClient.GetExistsTables();
            List<Type> modelsList = getModelsInfo(dbContext);

            foreach (Type model in modelsList)
            {
                string tableName = string.Empty;

                IList<Attribute> atr = new List<Attribute>(model.GetCustomAttributes());

                List<string> columns = new List<string>();

                if (atr[0].GetType() == typeof(TableAttribute))
                {
                    TableAttribute x = (TableAttribute)atr[0];
                    tableName = x.Name;
                }

                if (!tables.Contains(tableName))
                {
                    DataTable dt = new DataTable();

                    foreach (PropertyInfo info in model.GetProperties())
                    {
                        DataColumn column = new DataColumn(info.Name, info.PropertyType);

                        if (info.Name == "Id")
                        {
                            column.AutoIncrement = true;
                            column.AllowDBNull = false;
                        }

                        dt.Columns.Add(column);
                    }

                    string query = CreateTABLE(tableName, dt); // = Data.SqlClient.GetCreateFromDataTableSQL(tableName, dt);

                    ExecuteQuery(query);
                }
            }
        }

        private static List<Type> getModelsInfo(Object dbContext)
        {
            Type dbContextType = dbContext.GetType();
            IList<PropertyInfo> dbContextProps = new List<PropertyInfo>(dbContextType.GetProperties());

            List<Object> dbSetList = new List<Object>();

            foreach (PropertyInfo dbContextProp in dbContextProps)
            {
                Object dbSetObj = dbContextProp.GetValue(dbContext, null);

                dbSetList.Add(dbSetObj);
            }

            List<Type> tableList = new List<Type>();

            foreach (Object i in dbSetList)
            {
                Type t = i.GetType();

                IList<PropertyInfo> tableInfos = new List<PropertyInfo>(t.GetProperties());

                foreach (PropertyInfo tableInfo in tableInfos)
                {
                    //Type tableInfoType = tableInfo.GetType();

                    Object table = tableInfo.GetValue(i, null);

                    tableList.Add((Type)table);
                }
            }

            return tableList;
        }

        public static DataTable SelectTable(string query)
        {
            DataTable result = new DataTable();

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(result);

            conn.Close();
            da.Dispose();

            return result;
        }

        private static string CreateTABLE(string tableName, DataTable table)
        {
            string sqlsc;
            sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Boolean":
                        sqlsc += " bit ";
                        break;
                    case "System.Int32":
                        sqlsc += " int ";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint ";
                        break;
                    case "System.Int16":
                        sqlsc += " smallint";
                        break;
                    case "System.Byte":
                        sqlsc += " tinyint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal ";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime ";
                        break;
                    case "System.DateTime?":
                        sqlsc += " datetime ";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" nvarchar({0}) ", table.Columns[i].MaxLength == -1 ? "max" : table.Columns[i].MaxLength.ToString());
                        break;
                }
                if (table.Columns[i].ColumnName == "Id")
                    sqlsc += "IDENTITY(1,1)  PRIMARY KEY ";

                //if (table.Columns[i].AutoIncrement)
                //    sqlsc += " ";
                
                //if (!table.Columns[i].AllowDBNull)
                //    sqlsc += "  ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + "\n)";
        }

        public static void CheckTable(Type model)
        {
            List<string> tables = SqlClient.GetExistsTables();

            string tableName = string.Empty;

            IList<Attribute> atr = new List<Attribute>(model.GetCustomAttributes());

            List<string> columns = new List<string>();

            if (atr[0].GetType() == typeof(TableAttribute))
            {
                TableAttribute x = (TableAttribute)atr[0];
                tableName = x.Name;
            }

            if (!tables.Contains(tableName))
            {
                DataTable dt = new DataTable();


                foreach (PropertyInfo info in model.GetProperties())
                {
                    DataColumn column = new DataColumn(info.Name, info.PropertyType);

                    if (info.Name == "Id")
                    {
                        column.AutoIncrement = true;
                        column.AllowDBNull = false;
                    }

                    dt.Columns.Add(column);
                }

                string query = CreateTABLE(tableName, dt); // = Data.SqlClient.GetCreateFromDataTableSQL(tableName, dt);

                ExecuteQuery(query);
            }
        }

        private struct Item
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }
    }
}