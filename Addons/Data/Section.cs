﻿using EffectiveTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class Section
    {
        public string Type;
        public string Description;

        public int FolderId { get; set; }

        [Display(Name = "Název")]
        public string Name { get; set; }

        [Display(Name = "Zobrazit jako")]
        public string ShowAs { get; set; }
        public List<SectionShowAsItem> ShowAsItems { get; set; }

        public int IdContentSection { get; set; }

        public virtual int Create(int ParentFolder) { return 0; }

        public virtual void AddItem(ContentSectionModel contentSection) { }

        public virtual Section Get(ContentSectionModel contentSection) { return null; }

        public virtual Section Get(int idSekce) { return null; }

        public virtual Section Get() { return null; }

        public virtual Object GetWebModel(int id) { return null; }

        public virtual bool Delete(int id) { return false; }
    }
}
