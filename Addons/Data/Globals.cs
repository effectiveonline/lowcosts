﻿using Data;
using EffectiveTools.Models;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;

public class Globals
{
    private static string _dbConnectionString;

    public static bool RunInstall { get; set; }
    public static string DbConnectinString { get { return _dbConnectionString; } }
    public static List<Section> AvalibleSections { get; set; }
    public static List<string> AvalibleTemplates { get; set; }
    public static List<Security> Securityes { get; set; }
    public static CompanyModel Company { get; set; }

    public static DataFolders Folders { get; set; }
    public static Navigation AdminNavigation { get; set; }
    public static List<AdminMenuItem> AdminMenu { get; set; }
    public static List<Interface.IModul> InstaledModules { get; set; }

    public static List<PageType> PageTypes { get; set; }

    public static List<ContentTemplate> ContentTemplates { get; set; }

    public static List<SectionShowAsItem> SectionShowAsItems { get; set; }

    // Nastavení aplikace administrace a webu
    public static AppConfig AppConfig { get; set; }
    public static AdminConfig AdminConfig { get; set; }

    public static WebConfig WebConfig { get; set; }

    public static void Inicialize(string wwwPath)
    {
        Folders = new DataFolders(wwwPath);
        AppConfig = loadConfig();
        WebConfig = new WebConfig();
        AvalibleSections = new List<Section>();
        AvalibleTemplates = new List<string>();
        AdminMenu = new List<AdminMenuItem>();
        AdminNavigation = new Navigation();
        Securityes = new List<Security>();
        InstaledModules = new List<Interface.IModul>();

        PageTypes = new List<PageType>();
        ContentTemplates = new List<ContentTemplate>();
        ContentTemplates.Add(new ContentTemplate { Name = "Pages", Text = "Stránky a podstránky" });

        ContentTemplate events = new ContentTemplate
        {
            Name = "Events",
            Text = "Detail události",
            RequiredSections = new List<TemplateSectionModel>()
        };

        events.RequiredSections.Add(new TemplateSectionModel
        {
            Type = "Text",
            Name = "Obsah události",
            Required = true,
        });

        ContentTemplates.Add(events);

        LoadDbConnectionString();
    }

    public static void LoadDbConnectionString()
    {
        _dbConnectionString = "Server = " + AppConfig.SqlServer + "," + AppConfig.SqlPort + "; " +
            "Database = " + AppConfig.SqlDatabaze + "; " +
            "User Id = " + AppConfig.SqlUser + "; " +
            "Password = " + AppConfig.SqlPass + ";";
    }

    private static AppConfig loadConfig()
    {
        AppConfig result = null;

        if (File.Exists(Globals.Folders.Root + "\\appconfig.json"))
        {
            string jsonString = File.ReadAllText(Globals.Folders.Root + "\\appconfig.json");
            result = JsonSerializer.Deserialize<AppConfig>(jsonString);
        }
        else
        {
            result = new AppConfig();
            RunInstall = true;
        }

        return result;
    }

    public static string Md5(string Value)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
        data = x.ComputeHash(data);
        string ret = "";
        for (int i = 0; i < data.Length; i++)
            ret += data[i].ToString("x2").ToLower();
        return ret;
    }

    public static string TextToUrl(string text)
    {
        text = text.ToLower();

        text = text.Replace("/", string.Empty);
        text = text.Replace("\\", string.Empty);
        text = text.Replace("@", string.Empty);
        text = text.Replace("#", string.Empty);
        text = text.Replace("$", string.Empty);
        text = text.Replace("%", string.Empty);
        text = text.Replace("^", string.Empty);
        text = text.Replace("*", string.Empty);
        text = text.Replace("(", string.Empty);
        text = text.Replace(")", string.Empty);
        text = text.Replace("[", string.Empty);
        text = text.Replace("]", string.Empty);
        text = text.Replace("{", string.Empty);
        text = text.Replace("}", string.Empty);
        text = text.Replace("\"", string.Empty);
        text = text.Replace("'", string.Empty);
        text = text.Replace(":", string.Empty);
        text = text.Replace("?", string.Empty);
        text = text.Replace(".", string.Empty);
        text = text.Replace(",", string.Empty);
        text = text.Replace("”", string.Empty);
        text = text.Replace("„", string.Empty);

        text = text.Replace('ě', 'e');
        text = text.Replace('š', 's');
        text = text.Replace('č', 'c');
        text = text.Replace('ř', 'r');
        text = text.Replace('ž', 'z');
        text = text.Replace('ý', 'y');
        text = text.Replace('á', 'a');
        text = text.Replace('í', 'i');
        text = text.Replace('é', 'e');
        text = text.Replace('ú', 'u');
        text = text.Replace('ů', 'u');
        text = text.Replace('ť', 't');
        text = text.Replace('ó', 'o');
        text = text.Replace('ď', 'd');
        text = text.Replace('ň', 'n');

        text = text.Replace(' ', '-');

        return "/" + text;
    }
}

namespace Data
{
    public class DataFolders
    {
        private string _root;
        
        private string _modules;
        private string _components;
        private string _templates;

        private string _upload;
        private string _images;
        private string _files;
        private string _videos;
        private string _webimages;

        public string Root { get { return _root; } }

        public string Modules { get { return _modules; } }
        public string Components { get { return _components; } }
        public string Templates { get { return _templates; } }

        public string Upload { get { return _upload; } }
        public string Files { get { return _files; } }
        public string Videos { get { return _videos; } }
        public string Images { get { return _images; } }
        public string WebImages { get { return _webimages; } }



        public DataFolders(string wwwRootPath)
        {
            string data = wwwRootPath + "\\Data";
            
            _upload = data + "\\Upload";
            _files = data + "\\Files";
            _videos = data + "\\Videos";
            _images = data + "\\Images";

            _webimages = "/Data/Images//";

            _root = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            _modules = _root + "\\Modules";
            _components = _root + "\\Components";
            _templates = _root + "\\Templates";
            _upload = _root + "\\Upload";

            if (!Directory.Exists(Modules)) Directory.CreateDirectory(Modules);
            if (!Directory.Exists(Components)) Directory.CreateDirectory(Components);
            if (!Directory.Exists(Templates)) Directory.CreateDirectory(Templates);
            if (!Directory.Exists(Upload)) Directory.CreateDirectory(Upload);

            if (!Directory.Exists(data)) Directory.CreateDirectory(data);

            if (!Directory.Exists(Files)) Directory.CreateDirectory(Files);
            if (!Directory.Exists(Videos)) Directory.CreateDirectory(Videos);
            if (!Directory.Exists(Images)) Directory.CreateDirectory(Images);
        }
    }

    public struct AdminMenuItem
    {
        public string Text { get; set; }
        public string Link { get; set; }
        public int Order { get; set; }
        public List<AdminMenuItem> Items { get; set; }
    }

    public class Navigation
    {
        public string Area { get; set; }
        public string Group { get; set; }
        public string SubTitle { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
    }

    public class PageType
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public bool AllowTemplates { get; set; }
        public List<TemplateSectionModel> Sections { get; set; }
        public PageType SubPageType { get; set; }
    }

    public class ContentTemplate
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public List<TemplateSectionModel> RequiredSections { get; set; }
    }
}
