﻿using Data;
using EffectiveTools.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class LayoutViewModel
    {
        public int IdLayout { get; set; }

        [Display(Name = "Vypnout menu")]
        public bool DisableMenu { get; set; }

        [Display(Name = "Vypnout patičku")]
        public bool DisableFooter { get; set; }

        [Required(ErrorMessage = "Zadejte název rozložení.")]
        [Display(Name = "Název rozložení")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Zadejte popis rozložení.")]
        [Display(Name = "Popis rozložení")]
        public string Description { get; set; }

        [Display(Name = "Nastavit jako výchozí")]
        public bool IsDefault { get; set; }
        public List<TemplateSectionModel> Sections { get; set; }
    }
}
