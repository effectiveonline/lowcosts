﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class HelpMenuItem
    {
        public int Order { get; set; }
        public string Text { get; set; }
        public string Modul { get; set; }
        public string View { get; set; }
        public string NavigatinTitle { get; set; }
        public List<HelpMenuItem> Items { get; set; }
    }
}
