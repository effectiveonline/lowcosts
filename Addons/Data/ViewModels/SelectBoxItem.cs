﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectiveTools.Models
{
    public class SelectBoxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
