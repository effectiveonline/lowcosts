﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class BasicItemViewModel
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public int Pages { get; set; }
        public string PageType { get; set; }
    }
}
