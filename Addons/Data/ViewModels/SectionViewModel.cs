﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectiveTools.Models
{
    public class SectionViewModel
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public List<SectionShowAsItem> Items { get; set; }
        
    }
}
