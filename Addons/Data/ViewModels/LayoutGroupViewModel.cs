﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class LayoutGroupViewModel
    {
        public string Name { get; set; }
        public List<BasicItemViewModel> Items { get; set; }
    }
}
