﻿using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.Models
{
    public class MailMessageViewModel
    {
        public int Message { get; set; }

        [Required(ErrorMessage = "Zadejte email odesílatele zprávy.")]
        [Display(Name = "Odesílatel")]
        [DataType(DataType.EmailAddress)]
        public string From { get; set; }

        [Required(ErrorMessage = "Zadejte email příjemce zprávy.")]
        [Display(Name = "Příjemce")]
        [DataType(DataType.EmailAddress)]
        public string To { get; set; }

        [Required(ErrorMessage = "Zadejte předmět zprávy.")]
        [Display(Name = "Předmět")]
        public string Subject { get; set; }

        public string Body { get; set; }

        
    }
}
