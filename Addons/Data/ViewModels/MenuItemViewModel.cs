﻿using EffectiveTools.Models;
using System.Collections.Generic;

namespace EffectiveTools.ViewModels
{
    public class MenuItemViewModel
    {
        public int Id { get; set; }
        public int Layout { get; set; }
        public int Page { get; set; }
        public int Parent { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public bool CustomText { get; set; }
        public string Description { get; set; }
        public List<MenuItemViewModel> SubItems { get; set; }
    }
}