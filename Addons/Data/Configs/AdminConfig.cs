﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data
{
    [Table("SettingAdministration")]
    public class AdminConfig
    {
        public int Id { get; set; }

        // Nastaení administrace
        [Display(Name = "Výchozí šablona webu")]
        public string Template { get; set; } = "default";

        [Display(Name = "Povolit oprávnění bez čtení")]
        public bool EmptyUserSecurity { get; set; }

        [Display(Name = "Vypnout logování uživatelských akcí")]
        public bool DisableAdminLoging { get; set; }
    }
}
