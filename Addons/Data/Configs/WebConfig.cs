﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data
{
    [Table("SettingWeb")]
    public class WebConfig
    {
        public int Id { get; set; }

        [Display(Name = "Hlavní doména")]
        public string Domain { get; set; }

        [Display(Name = "Zásady zpracování osobních údajů")]
        public string GDPR { get; set; }

        public int PopUp { get; set; }

        #region Odesílání emailů

        [Display(Name = "Adresa pro příjem a odesílání emailů z webu")]
        public string SmtpEmail { get; set; }

        [Display(Name = "Zobrazované jméno při odesílání emilů z webu")]
        public string SmtpName { get; set; }

        [Display(Name = "Adresa SMTP serveru")]
        public string SmtpServer { get; set; }

        [Display(Name = "Uživatelské jméno")]
        public string SmtpUser { get; set; }

        [Display(Name = "Uživatelské heslo")]
        public string SmtpPass { get; set; }

        [Display(Name = "Port komunikace se serverem")]
        public int SmtpPort { get; set; } = 25;

        [Display(Name = "Server vyžaduje zabezpečené připojení SSL")]
        public bool SmtpSSL { get; set; }

        #endregion
    }
}