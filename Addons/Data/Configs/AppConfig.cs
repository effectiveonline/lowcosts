﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.Json;

namespace Data
{
    public class AppConfig
    {
        #region Root konfigurace

        [Display(Name = "Rest Api")]
        public bool RestApi { get; set; }

        [Display(Name = "Změna hesla")]
        public string RootPass { get; set; }

        #endregion

        #region Konfigurace SQL serveru

        [Required(ErrorMessage = "Zadejte adresu SQL serveru!")]
        [Display(Name = "Adresa SQL serveru")]
        public string SqlServer { get; set; }

        [Required(ErrorMessage = "Zadejte port komunikace s SQL serverem!")]
        [Display(Name = "Port komunikace s SQL serverem")]
        public string SqlPort { get; set; }

        [Required(ErrorMessage = "Zadejte název SQL databáze!")]
        [Display(Name = "Název databáze")]
        public string SqlDatabaze { get; set; }

        [Required(ErrorMessage = "Zadejte uživatelské jméno!")]
        [Display(Name = "Uživatelské jméno")]
        public string SqlUser { get; set; }

        [Display(Name = "Uživatelské heslo")]
        public string SqlPass { get; set; }

        #endregion

        #region Uložení do konfiguračního souboru

        public void Save()
        {
            string jsonString = JsonSerializer.Serialize(this);
            File.WriteAllText(Globals.Folders.Root + @"\appconfig.json", jsonString);
        }

        #endregion
    }
}