﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Data
{
    public class DbSet<T> where T : class
    {
        private string tableName;

        public bool HaveData
        {
            get { return checkData(); }
        }

        public DbSet()
        {
            TypeInfo model = typeof(T).GetTypeInfo();
            IList<Attribute> atr = new List<Attribute>(model.GetCustomAttributes());

            if (atr[0].GetType() == typeof(TableAttribute))
            {
                TableAttribute x = (TableAttribute)atr[0];
                tableName = x.Name;
            }

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + tableName + "'", conn);
            SqlDataReader reader = cmd.ExecuteReader();

            if (!reader.HasRows)
            {
                // tabulka neexistuje, vytvorit

                List<DataColumn> Columns = new List<DataColumn>();

                foreach (PropertyInfo info in model.GetProperties())
                {
                    DataColumn column = new DataColumn(info.Name, getDbType(info));

                    if (info.Name == "Id")
                    {
                        column.AutoIncrement = true;
                        column.AllowDBNull = false;
                    }

                    Columns.Add(column);
                }

                string query = getCreateTableQuery(Columns); // = Data.SqlClient.GetCreateFromDataTableSQL(tableName, dt);

                reader.DisposeAsync();

                cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                cmd.DisposeAsync();
            }

            conn.Close();
            conn.DisposeAsync();
        }

        private Type getDbType(PropertyInfo info)
        {
            var x = info.PropertyType.Name;

            if (x == "Nullable`1") return typeof(DateTime);

            return info.PropertyType;
        }

        public Task<List<T>> GetListAsync(string where = null)
        {
            return Task.Run(() => { return GetList(where); });
        }

        public List<T> GetList(string where = null)
        {
            if (where != null) where = " WHERE " + where;

            List<T> items = new List<T>();

            TypeInfo model = typeof(T).GetTypeInfo();
            IEnumerable<PropertyInfo> props = model.DeclaredProperties;

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT " + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " " + where;

            DataTable res = selectTable(query);

            for (int i = 0; i < res.Rows.Count; i++)
            {
                T item = (T)Activator.CreateInstance(typeof(T));

                foreach (PropertyInfo prop in props)
                {
                    object value = res.Rows[i][prop.Name];

                    if (value.GetType() == typeof(DBNull)) value = null;

                    item.GetType().GetProperty(prop.Name).SetValue(item, value);
                }

                items.Add(item);
            }

            return items;
        }

        public Task<List<T>> GetListAsyncAsync(string where, int rows)
        {
            return Task.Run(() => { return GetList(where, rows); });
        }

        public List<T> GetList(string where, int rowsCount)
        {
            List<T> items = new List<T>();

            TypeInfo model = typeof(T).GetTypeInfo();
            IEnumerable<PropertyInfo> props = model.DeclaredProperties;

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT TOP(" + rowsCount + ") " + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " WHERE " + where + "ORDER BY [Id] DESC";

            DataTable res = selectTable(query);

            for (int i = 0; i < res.Rows.Count; i++)
            {
                T item = (T)Activator.CreateInstance(typeof(T));

                foreach (PropertyInfo prop in props)
                {
                    object value = res.Rows[i][prop.Name];

                    if (value.GetType() == typeof(DBNull)) value = null;

                    item.GetType().GetProperty(prop.Name).SetValue(item, value);
                }

                items.Add(item);
            }

            return items;
        }

        public Task<T> AddAsync(T item)
        {
            return Task.Run(() => { return Add(item); });
        }

        public T Add(T item)
        {
            Type myType = item.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
            IList<Attribute> atr = new List<Attribute>(myType.GetCustomAttributes());

            if (atr.Count == 0) return null;

            List<string> columns = new List<string>();
            List<string> values = new List<string>();
            List<Item> items = new List<Item>();

            foreach (PropertyInfo prop in props)
            {
                Type propT = prop.GetType();

                object propValue = prop.GetValue(item, null);

                //if (prop.PropertyType.Name == "String" && propValue == null) propValue = string.Empty;

                if (propValue == null) propValue = DBNull.Value;

                if (prop.Name == "Id") continue;

                items.Add(new Item { Name = prop.Name, Value = propValue });

                columns.Add("[" + prop.Name + "]");
                values.Add("@" + prop.Name);
            }

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO " + tableName + " (" + columns.Aggregate((a, b) => a + ", " + b) + ") VALUES (" + values.Aggregate((a, b) => a + ", " + b) + ")";

            foreach (Item itm in items)
            {
                cmd.Parameters.AddWithValue("@" + itm.Name, itm.Value);
            }

            cmd.ExecuteNonQuery();

            int id = 0;
            cmd = new SqlCommand("SELECT IDENT_CURRENT('" + tableName + "')", conn);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                id = Convert.ToInt32(rd.GetValue(0));
            }

            conn.Close();

            conn.DisposeAsync();
            cmd.DisposeAsync();

            foreach (PropertyInfo prop in props)
            {
                if (prop.Name == "Id")
                {
                    prop.SetValue(item, id);
                }
            }

            return item;
        }

        public Task<T> UpdateAsync(T item)
        {
            return Task.Run(() => { return Update(item); });
        }

        public T Update(T item)
        {
            List<string> columns = new List<string>();
            List<object> values = new List<object>();

            Type myType = item.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            int id = 0;

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(item, null);

                if (prop.Name == "Id")
                {
                    id = (int)prop.GetValue(item, null);
                    continue;
                }

                columns.Add("[" + prop.Name + "]");
                values.Add("[" + prop.Name + "] = '" + propValue + "'");
            }

            string query = "UPDATE " + tableName + " SET " + values.Aggregate((a, b) => a + ", " + b) + " WHERE Id = " + id;


            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();

            conn.DisposeAsync();
            cmd.DisposeAsync();

            return item;
        }

        public Task<T> SelectSingleByIdAsync(int id)
        {
            return Task.Run(() => { return SelectSingleById(id); });
        }

        public T SelectSingleById(int id)
        {
            TypeInfo model = typeof(T).GetTypeInfo();
            IEnumerable<PropertyInfo> props = model.DeclaredProperties;

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT " + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " WHERE Id = " + id;

            DataTable res = selectTable(query);

            if (res.Rows.Count == 0) return null;

            T item = (T)Activator.CreateInstance(typeof(T));

            foreach (PropertyInfo prop in props)
            {
                object value = res.Rows[0][prop.Name];

                if (value.GetType() == typeof(DBNull)) value = null;

                item.GetType().GetProperty(prop.Name).SetValue(item, value);
            }

            return item;
        }


        public Task<T> FirstOrDefaultAsync()
        {
            return Task.Run(() => { return FirstOrDefault(); });
        }

        public T FirstOrDefault()
        {
            T item = (T)Activator.CreateInstance(typeof(T));

            Type myType = item.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT TOP (1)" + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName;

            DataTable res = selectTable(query);

            if (res.Rows.Count == 0) return null;


            foreach (PropertyInfo prop in props)
            {
                object value = res.Rows[0][prop.Name];

                if (value.GetType() == typeof(DBNull)) value = null;

                item.GetType().GetProperty(prop.Name).SetValue(item, value);
            }

            return item;
        }

        public Task<T> FirstOrDefaultAsync(string where)
        {
            return Task.Run(() => { return FirstOrDefault(where); });
        }

        public T FirstOrDefault(string where)
        {
            T item = (T)Activator.CreateInstance(typeof(T));

            Type myType = item.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT TOP (1)" + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " WHERE " + where;

            DataTable res = selectTable(query);

            if (res.Rows.Count == 0) return null;


            foreach (PropertyInfo prop in props)
            {
                object value = res.Rows[0][prop.Name];

                if (value.GetType() == typeof(DBNull)) value = null;

                item.GetType().GetProperty(prop.Name).SetValue(item, value);
            }

            return item;
        }

        public Task<T> FirstOrDefaultAsync(T item)
        {
            return Task.Run(() => { return FirstOrDefault(item); });
        }

        public T FirstOrDefault(T item)
        {
            List<string> columns = new List<string>();
            List<object> values = new List<object>();

            Type myType = item.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(item, null);

                columns.Add("[" + prop.Name + "]");

                if (prop.Name == "Id") continue;

                if (propValue != null)
                {
                    values.Add(prop.Name + " = '" + propValue + "'");
                }
            }

            string query = "SELECT " + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " WHERE " + values.Aggregate((a, b) => a + " AND " + b);

            DataTable res = selectTable(query);

            if (res.Rows.Count == 0) return null;


            foreach (PropertyInfo prop in props)
            {
                object value = res.Rows[0][prop.Name];

                if (value.GetType() == typeof(DBNull)) value = null;

                item.GetType().GetProperty(prop.Name).SetValue(item, value);
            }

            return item;
        }

        public Task<int> SelectCountAsync(string where)
        {
            return Task.Run(() => { return SelectCountAsync(where); });
        }

        public int SelectCount(string where)
        {
            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            string query = "SELECT COUNT(Id) FROM [" + tableName + "] WHERE " + where;

            SqlCommand cmd = new SqlCommand(query, conn);
            Int32 count = (Int32)cmd.ExecuteScalar();

            conn.Close();

            conn.DisposeAsync();
            cmd.DisposeAsync();

            return count;
        }

        private bool checkData()
        {
            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            string query = "SELECT COUNT(*) FROM [" + tableName + "]";

            SqlCommand cmd = new SqlCommand(query, conn);
            Int32 count = (Int32)cmd.ExecuteScalar();

            conn.Close();

            conn.DisposeAsync();
            cmd.DisposeAsync();

            if (count > 0) return true;

            return false;
        }

        #region Odstranění dat z databáze

        public Task<bool> DeleteAsync(int id)
        {
            return Task.Run(() => { return Delete(id); });
        }

        public bool Delete(int id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
                conn.Open();

                string query = "DELETE FROM [" + tableName + "] WHERE Id = " + id;

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();

                conn.Close();

                conn.DisposeAsync();
                cmd.DisposeAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Task<bool> DeleteAsync(string where)
        {
            return Task.Run(() => { return Delete(where); });
        }

        public bool Delete(string where)
        {
            try
            {
                SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
                conn.Open();

                string query = "DELETE FROM [" + tableName + "] WHERE " + where;

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();

                conn.Close();

                conn.DisposeAsync();
                cmd.DisposeAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        public T SelectLast(string where, string orderBy)
        {
            List<T> items = new List<T>();

            TypeInfo model = typeof(T).GetTypeInfo();
            IEnumerable<PropertyInfo> props = model.DeclaredProperties;

            List<string> columns = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                columns.Add("[" + prop.Name + "]");
            }

            string query = "SELECT TOP(1) " + columns.Aggregate((a, b) => a + ", " + b) + " FROM " + tableName + " WHERE " + where + "ORDER BY [" + orderBy + "] DESC";

            DataTable res = selectTable(query);

            if (res.Rows.Count == 0) return null;

            T item = (T)Activator.CreateInstance(typeof(T));

            foreach (PropertyInfo prop in props)
            {
                object value = res.Rows[0][prop.Name];

                if (value.GetType() == typeof(DBNull)) value = null;

                item.GetType().GetProperty(prop.Name).SetValue(item, value);
            }

            return item;
        }


        private DataTable selectTable(string query)
        {
            DataTable result = new DataTable();

            SqlConnection conn = new SqlConnection(Globals.DbConnectinString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(result);

            conn.Close();
            
            conn.DisposeAsync();
            cmd.DisposeAsync();
            da.Dispose();

            return result;
        }

        private string getCreateTableQuery(List<DataColumn> Columns)
        {
            string sqlsc;
            sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < Columns.Count; i++)
            {
                sqlsc += "\n [" + Columns[i].ColumnName + "] ";
                string columnType = Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Boolean":
                        sqlsc += " bit ";
                        break;
                    case "System.Int32":
                        sqlsc += " int ";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint ";
                        break;
                    case "System.Int16":
                        sqlsc += " smallint";
                        break;
                    case "System.Byte":
                        sqlsc += " tinyint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal ";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime ";
                        break;
                    case "System.DateTime?":
                        sqlsc += " datetime ";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" nvarchar({0}) ", Columns[i].MaxLength == -1 ? "max" : Columns[i].MaxLength.ToString());
                        break;
                }
                if (Columns[i].ColumnName == "Id")
                    sqlsc += "IDENTITY(1,1)  PRIMARY KEY ";

                //if (table.Columns[i].AutoIncrement)
                //    sqlsc += " ";

                sqlsc += ",";
            }

            return sqlsc.Substring(0, sqlsc.Length - 1) + "\n)";
        }

        private struct Item
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }
    }
}
