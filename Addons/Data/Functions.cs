﻿using System.ComponentModel.DataAnnotations;

public static class Functions
{
    public static bool IsEmailValid(string source)
    {
        return new EmailAddressAttribute().IsValid(source);
    }
}