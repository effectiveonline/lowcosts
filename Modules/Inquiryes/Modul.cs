﻿using Data;
using EffectiveTools.ViewModels;
using Inquiryes.Areas.Admin.ViewModels;
using Interface;
using System;
using System.Collections.Generic;

namespace Inquiryes
{
    #region Inicializace modulu

    public class Modul : IModul
    {
        Data.AdminMenuItem menuItem;
        List<EffectiveTools.Models.SelectBoxItem> formTypes;

        IModulHost myHost = null;


        public Modul()
        {
            DbContext.Inicialize();
            StaticData.Inicialize();

            menuItem = new Data.AdminMenuItem
            {
                Text = "Poptávky",
                Order = 2,
                Items = new List<Data.AdminMenuItem>
                {
                    new Data.AdminMenuItem
                    {
                        Text = "Přehled poptávek",
                        Link = "/admin/inquiryes"
                    },
                    new Data.AdminMenuItem
                    {
                        Text = "Nastavení",
                        Link = "/admin/inquiryes/setting"
                    },
                }
            };

            formTypes = new List<EffectiveTools.Models.SelectBoxItem>();

            var inquiryes = DbContext.Inquiryes.GetList();

            foreach(var inquiry in inquiryes)
            {
                formTypes.Add(new EffectiveTools.Models.SelectBoxItem
                {
                    Text = inquiry.Name,
                    Value = inquiry.Id.ToString()
                });
            }

            Globals.Securityes.Add(new Security());
            Globals.SectionShowAsItems.Add(new EffectiveTools.Models.SectionShowAsItem
            {
                Section = "Forms",
                Text = "Poptávkový formulář",
                Value = "Inquiryes"
            });
        }

        public IModulHost Host
        {
            get { return myHost; }
            set { myHost = value; }
        }

        public string Name
        {
            get { return "Inquiryes"; }
        }

        public string Title
        {
            get { return "Poptávky"; }
        }

        public Data.AdminMenuItem AdminMenuItem
        {
            get { return menuItem; }
        }

        public List<EffectiveTools.Models.SelectBoxItem> FormTypes
        {
            get { return formTypes; }
        }

        public HelpMenuItem Help
        {
            get
            {
                return new HelpMenuItem
                {
                    Text = "Poptávky",
                    Modul = Name,
                    Order = 2,
                    View = "Inquiryes",
                    NavigatinTitle = "Poptávky?",
                    Items = new List<HelpMenuItem>
                    {
                        new HelpMenuItem
                        {
                            Text = "Vytvoření formuláře",
                            Modul = Name,
                            View = "create",
                            NavigatinTitle = "Jak vytvořit poptávkový formulář?"
                        }
                    }
                };
            }
        }

        public Object DashBoardModel
        {
            get
            {
                return new DashboardViewModel();
            }
        }

        public string GetFormBody(string id)
        {
            Areas.Admin.Models.InquiryModel inquiry = DbContext.Inquiryes.SelectSingleById(Convert.ToInt32(id));

            return inquiry.Body;
        }

        public void Initialize() { }

        public void Dispose() { }
    }

    #endregion

    public class StaticData
    {
        public static List<FormInputType> InputTypes { get; set; }

        public static void Inicialize()
        {
            InputTypes = new List<FormInputType>();

            InputTypes.Add(new FormInputType
            {
                Type = "title",
                Text = "Nadpis",
                Description = "Vloží editovatelný nadpis"
            });

            InputTypes.Add(new FormInputType
            {
                Type = "textbox",
                Text = "Textové políčko",
                Description = "Jednořádkové textové políčko. (př: jméno)"
            });

            InputTypes.Add(new FormInputType
            {
                Type = "select",
                Text = "Výběr hornoty",
                Description = "Jednořádkový výběr z přesně definovaných hodnot. (př: vyberte město)"
            });

            InputTypes.Add(new FormInputType
            {
                Type = "textarea",
                Text = "Textové pole",
                Description = "Víceřádkové pole textu. (př: poznámka)"
            });

            InputTypes.Add(new FormInputType
            {
                Type = "checkbox",
                Text = "Zaškrtávací políčko",
                Description = "Umožní jedním kliknutím zadat Ano nebo Ne. (př: udání souhlasu)"
            });
        }
    }

    public struct FormInputType
    {
        public string Type { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}