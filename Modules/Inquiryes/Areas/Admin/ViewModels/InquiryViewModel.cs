﻿using Inquiryes.Areas.Admin.Models;
using System.Collections.Generic;

namespace Inquiryes.Areas.Admin.ViewModels
{
    public class InquiryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<InquiryDataModel> Inputs { get; set; }
    }
}
