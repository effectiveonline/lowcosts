﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inquiryes.Areas.Admin.ViewModels
{
    public class DashboardViewModel
    {
        public Inquiryes.Areas.Admin.Models.InquiryDataModel LastInquiry { get; set; }
        public int UnreadedLastInquiryes { get; set; }

        public DashboardViewModel()
        {
            UnreadedLastInquiryes = DbContext.InquiryesData.SelectCount("Readed = 'False'");
            LastInquiry = DbContext.InquiryesData.SelectLast("Readed = 'False'", "Created");
        }
    }
}
