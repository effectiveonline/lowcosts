﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Inquiryes.Areas.Admin.Models;
using Inquiryes.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inquiryes.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "inquiryes_setting")]
    public class SettingController : Controller
    {
        public SettingController()
        {
            Globals.AdminNavigation.Icon = "pe-7s-config";
            Globals.AdminNavigation.Title = "Nastavení poptávek";
            Globals.AdminNavigation.SubTitle = "Konfigurace poptávkového formuláře";
        }

        #region Načtení formulářů

        [Route("/admin/inquiryes/setting")]
        public IActionResult Setting()
        {
            return View(getItemsList());
        }

        #endregion

        #region Naštení inputu při sestavování formuláře

        [Route("/admin/inquiryes/input/{type}")]
        public IActionResult Input(string type)
        {
            return PartialView("Inputs/" + type);
        }

        #endregion

        [Route("/admin/inquiryes/edit/{inquiryId}")]
        public IActionResult Edit(int inquiryId)
        {
            InquiryModel model = DbContext.Inquiryes.SelectSingleById(inquiryId);

            return PartialView(model);
        }

        [Route("/admin/inquiryes/edit/{id}")]
        [HttpPost]
        public IActionResult Edit(InquiryModel model, int id)
        {
            DbContext.Inquiryes.Update(model);

            return PartialView(model);
        }

        #region Společné metody

        //Získání seznamu stránek v levém menu 
        private List<BasicItemViewModel> getItemsList()
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<InquiryModel> items = DbContext.Inquiryes.GetList();

            foreach (InquiryModel item in items)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/inquiryes/edit/" + item.Id,
                });
            }

            return result;
        }

        #endregion
    }
}