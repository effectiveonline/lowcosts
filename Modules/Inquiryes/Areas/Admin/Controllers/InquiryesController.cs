﻿using Inquiryes.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inquiryes.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "inquiryes")]
    public class InquiryesController : Controller
    {
        [Route("/admin/inquiryes")]
        public IActionResult Inquiryes()
        {
            Globals.AdminNavigation.Icon = "pe-7s-global";
            Globals.AdminNavigation.Title = "Poptávky";
            Globals.AdminNavigation.SubTitle = "Přehled a zpráva poptávek";

            List<InquiryDataModel> model = DbContext.InquiryesData.GetList();

            return View(model);
        }

        [Route("/admin/inquiryes/list/{filter}")]
        [HttpGet]
        public IActionResult WebMessagesFilter(string filter = "all")
        {
            Globals.AdminNavigation.Icon = "pe-7s-mail";
            Globals.AdminNavigation.Title = "Přehled zpráv";
            Globals.AdminNavigation.SubTitle = "Zprávy z kontatních formulářů";

            List<InquiryDataModel> model;

            if (filter == "all")
            {
                model = DbContext.InquiryesData.GetList();
            }
            else
            {
                model = DbContext.InquiryesData.GetList("Readed = 'False'");
            }

            model = model.OrderByDescending(m => m.Created).ToList();

            return PartialView("List", model);
        }

        [Route("/admin/inquirye/{messageId}/{readed}")]
        [HttpGet]
        public async Task<IActionResult> WebMessage(int messageId, bool readed)
        {
            InquiryDataModel model = await DbContext.InquiryesData.SelectSingleByIdAsync(messageId);

            if (model != null && readed)
            {
                model.Readed = true;

                await DbContext.InquiryesData.UpdateAsync(model);
            }

            return PartialView("Inquiry", model);
        }


        [Route("/inquiryes/{id}")]
        [HttpPost]
        public async Task<IActionResult> SendInqiry(InquiryDataModel model, int id)
        {
            model.Inquiry = id.ToString();
            model.Created = DateTime.Now;

            await DbContext.InquiryesData.AddAsync(model);
            
            return Ok(true);
        }
    }
}
