﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inquiryes.Areas.Admin.Models
{
    [Table("InquiryesData")]
    public class InquiryDataModel
    {
        public int Id { get; set; }
        public string Inquiry { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Data { get; set; }
        public bool Readed { get; set; }
        public DateTime Created { get; set; }
        
    }
}
