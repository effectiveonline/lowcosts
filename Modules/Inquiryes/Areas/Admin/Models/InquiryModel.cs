﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inquiryes.Areas.Admin.Models
{
    [Table("Inquiryes")]
    public class InquiryModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Název formuláře")]
        public string Name { get; set; }

        public string Body { get; set; }
    }
}
