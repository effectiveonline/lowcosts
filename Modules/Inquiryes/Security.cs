﻿using Data;
using System.Collections.Generic;

namespace Inquiryes
{
    public class Security : Data.Security
    {
        public Security()
        {
            Name = "inquiryes";
            Text = "Poptávky";
            Description = " ";
            Roles = new List<SecurityRole>();

            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_edit",
                Text = "Povolit vyřizování poptávek",
            });
            
            Roles.Add(new SecurityRole
            {
                Name = this.Name + "_setting",
                Text = "Povolit úpravy v nastavení",
            });
        }
    }
}
