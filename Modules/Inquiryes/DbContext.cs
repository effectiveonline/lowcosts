﻿using Data;
using EffectiveTools.Models;
using Inquiryes.Areas.Admin.Models;

namespace Inquiryes
{
    public static class DbContext
    {
        public static DbSet<InquiryModel> Inquiryes { get; set; }
        public static DbSet<InquiryDataModel> InquiryesData { get; set; }

        public static DbSet<LayoutModel> Layouts { get; set; }

        public static void Inicialize()
        {
            Inquiryes = new DbSet<InquiryModel>();
            InquiryesData = new DbSet<InquiryDataModel>();
            Layouts = new DbSet<LayoutModel>();

            if (!Inquiryes.HaveData)
            {
                Inquiryes.Add(new Areas.Admin.Models.InquiryModel
                {
                    Name = "Základní poptávka",
                    Body = DefaultBody()
                });
            }
        }

        private static string DefaultBody()
        {
            string result = "<div class=\"row\">"
             + "<div class=\"col-4 form-group\" data-locked=\"true\">"
             + "<input type=\"text\" name=\"name\" placeholder=\"Jméno a příjmení\" class=\"form-control\" />"
             + "</div>"
             + "<div class=\"col-4 form-group\">"
             + "<input type=\"text\" name=\"phone\" placeholder=\"Telefon\" class=\"form-control\" />"
             + "</div>"
             + "<div class=\"col-4 form-group\">"
             + "<input type=\"text\" name=\"email\" placeholder=\"E-mail\" class=\"form-control\" />"
             + "</div>"
             + "</div>"
             + "</div>";

            return result;
        }
    }
}