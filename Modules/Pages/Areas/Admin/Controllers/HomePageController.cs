﻿using Microsoft.AspNetCore.Mvc;
using EffectiveTools.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using EffectiveTools.ViewModels;
using System.Collections.Generic;
using Data;
using System.Linq;
using System;
using Managers.Models;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "pages_homepage")]
    [Area("Admin")]
    public class HomePageController : Controller
    {
        [Route("/admin/homepage")]
        [HttpGet]
        public async Task<IActionResult> Homepage()
        {
            Globals.AdminNavigation.Icon = "pe-7s-monitor";
            Globals.AdminNavigation.Title = "Titulní stránka";
            Globals.AdminNavigation.SubTitle = "Nastavení titulní stránky webu";

            ContentModel homepage = await DbContext.Pages.FirstOrDefaultAsync("IsHomepage = 'true'");

            var model = new PageViewModel
            {
                Name = homepage.Name,
                Title = homepage.Title,
                Keywords = homepage.Keywords,
                Description = homepage.Description,
                Robots = homepage.Robots,

                ContentSections = await DbContext.ContentSections.GetListAsync("IdPage = " + homepage.Id),
                PopUps = await DbContext.PopUps.GetListAsync(),
                Layouts = await DbContext.Layouts.GetListAsync()
            };

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp Okna" });

            return View(model);
        }

        [Route("/admin/homepage/properities")]
        [HttpPost]
        public ActionResult Properities(PageViewModel model)
        {
            ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");
            if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            if (ModelState.IsValid)
            {
                homepage.Name = model.Name;
                homepage.Title = model.Title;
                homepage.Keywords = model.Keywords;
                homepage.Description = model.Description;
                homepage.Robots = model.Robots;
                homepage.Url = "/";

                homepage.ShowName = model.ShowName;

                homepage.Modified = DateTime.Now;

                DbContext.Pages.Update(homepage);

                //AdminLogger.LogEvent(User, "Titulní strana", AdminLoggerAction.Edit, "Úprava titulní stránky");
            }

            return PartialView("Properities", model);
        }

        [Route("/admin/homepage/sections")]
        [HttpPost]
        public ActionResult Sections(PageViewModel model)
        {
            if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            ContentModel homepage = DbContext.Pages.FirstOrDefault("IsHomepage = 'true'");

            for (int i = 0; i < model.ContentSections.Count; i++)
            {
                if (model.ContentSections[i].Id == 0)
                {
                    model.ContentSections[i].IdPage = homepage.Id;
                    model.ContentSections[i].IdSekce = Globals.AvalibleSections.First(s => s.Type == model.ContentSections[i].Type).Create(homepage.Folder);

                    //vytvoreni
                    DbContext.ContentSections.Add(model.ContentSections[i]);
                }
                else if (model.ContentSections[i].Order == -1)
                {
                    //odstraneni
                    Globals.AvalibleSections.First(s => s.Type == model.ContentSections[i].Type).Delete(model.ContentSections[i].IdSekce);
                    DbContext.ContentSections.Delete(model.ContentSections[i].Id);
                    model.ContentSections.RemoveAt(i);
                }
                else
                {
                    model.ContentSections[i].IdPage = homepage.Id;

                    DbContext.ContentSections.Update(model.ContentSections[i]);
                }
            }

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();

            return PartialView("Sections", model);
        }
    }
}
