﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Managers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EffectiveTools.Areas.Admin.Controllers
{
    [Authorize(Roles = "events")]
    [Area("Admin")]
    public class EventsController : Controller
    {
        public EventsController()
        {
            Globals.AdminNavigation.Icon = "pe-7s-news-paper";
            Globals.AdminNavigation.Title = "Události";
            Globals.AdminNavigation.SubTitle = "Přehled vytvořených skupin událostí";
        }

        [Route("/admin/events")]
        [HttpGet]
        public ActionResult Events()
        {
            return View(getItemsList());
        }

        #region Získání seznamu skupin událostí

        [Route("/admin/events/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            List<LayoutGroupViewModel> groups = getItemsList();

            if (groups.Count == 1) return PartialView("ItemsList", groups[0].Items);

            return PartialView("GroupsList", groups);
        }

        #endregion

        #region Získání seznamu událostí ve skupině

        [Route("/admin/events/eventslist/{idGroup}")]
        [HttpGet]
        public ActionResult EventsList(int idGroup)
        {
            List<EventItemViewModel> model = new List<EventItemViewModel>();

            List<ContentModel> events = DbContext.Pages.GetList("Parent = " + idGroup);

            foreach (ContentModel _event in events)
            {
                model.Add(new EventItemViewModel
                {
                    Id = _event.Id,
                    Name = _event.Name,
                    Created = _event.Created
                });
            }

            return PartialView("Partials/EventsList", model);
        }

        #endregion

        #region Vytvoření

        [Authorize(Roles = "events_edit")]
        [Route("/admin/events/create")]
        [Route("/admin/events/create/{parentId}")]
        [HttpGet]
        public async Task<ActionResult> Create(int parentId = 0)
        {
            Globals.AdminNavigation.Icon = "pe-7s-note";
            Globals.AdminNavigation.Title = "Vytvoření skupiny událostí";
            Globals.AdminNavigation.SubTitle = "Události";

            if(parentId != 0)
            {
                Globals.AdminNavigation.Title = "Vytvoření události";
            }

            EventViewModel model = new EventViewModel
            {
                ParentId = parentId,
                Robots = true,
                ShowName = true,

                Layouts = DbContext.Layouts.GetList(),
                PopUps = await DbContext.PopUps.GetListAsync(),
                Templates = await DbContext.Templates.GetListAsync("Target = 'Events'"),
                ContentSections = new List<ContentSectionModel>()
            };

            if(parentId == 0)
            {
                model.ContentSections.Add(new ContentSectionModel
                {
                    Name = "Události",
                    Type = "Events",
                    Order = 1,
                    Locked = true,
                    Required = true
                });
            }
            else
            {
                ContentModel parentPage = DbContext.Pages.SelectSingleById(parentId);
                model.EventsCount = DbContext.Pages.SelectCount("Parent = " + parentId);

                if (parentPage.TemplateItems == 0)
                {
                    model.ContentSections.Add(new ContentSectionModel
                    {
                        Name = "Obsah události",
                        Type = "Text",
                        Order = 1,
                        Required = true
                    });
                }
                else
                {
                    model.Template = parentPage.TemplateItems;

                    List<TemplateSectionModel> templateSections = DbContext.TemplateSections.GetList("Template = " + parentPage.TemplateItems);

                    foreach (TemplateSectionModel templateSection in templateSections)
                    {
                        model.ContentSections.Add(new ContentSectionModel
                        {
                            IdSekce = templateSection.IdSekce,
                            Name = templateSection.Name,
                            Type = templateSection.Type,
                            Order = templateSection.Order,
                            Locked = templateSection.Locked,
                            Required = templateSection.Required,
                        });
                    }
                }
            }

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView(model);
        }

        [Authorize(Roles = "events_edit")]
        [Route("/admin/events/create")]
        [HttpPost]
        public ActionResult Create(EventViewModel model)
        {
            int exist = DbContext.Pages.SelectCount("Name = '" + model.Name + "' AND Parent = " + 0);

            if (exist != 0) ModelState.AddModelError("Name", "Stránka s tímto názvem již existuje! Zadejte jiný název.");

            if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            if (ModelState.IsValid)
            {
                LayoutModel layout = DbContext.Layouts.FirstOrDefault("IsDefault = 'true'");

                ContentModel newPage = new ContentModel
                {
                    Parent = model.ParentId,
                    Layout = layout.Id,
                    PopUp = model.PopUpId,
                    Modul = "Events",
                    TemplateItems = model.TemplateItems,
                    
                    Name = model.Name,
                    Title = model.Title,
                    Description = model.Description,
                    HardUrl = model.HardUrl,
                    Keywords = model.Keywords,
                    Robots = model.Robots,
                    Priority = model.Priority,

                    //ShowInMenu = model.ShowInMenu,
                    ShowName = model.ShowName,

                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    
                    Owner = Convert.ToInt32(User.FindFirstValue(ClaimTypes.NameIdentifier))
                };

                if (!newPage.HardUrl)
                {
                    newPage.Url = Globals.TextToUrl(newPage.Name);
                }
                else
                {
                    newPage.Url = "/" + newPage.Url;
                }

                //Vytvoření složky pro data stránky
                FolderModel folder = new FolderModel
                {
                    Name = newPage.Name,
                    Type = "Události"
                };

                Managers.DbContext.Folders.Add(folder);

                newPage.Folder = folder.Id;

                //ulozeni do databaze
                DbContext.Pages.Add(newPage);

                //Uložení menu
                if (model.ShowInMenu != null)
                {
                    foreach (int menuId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItem = new LayoutItemModel
                        {
                            Layout = menuId,
                            Page = newPage.Id,
                            Text = newPage.Name,
                            Link = newPage.Url,
                            Order = getMenuOrder(menuId)
                        };

                        DbContext.LayoutItems.Add(menuItem);
                    }
                }

                //AdminLogger.LogEvent(User, "Události", AdminLoggerAction.Add, "Vytvoření skupiny událostí: " + model.Name);

                return Ok(newPage.Id);
            }

            model.Layouts = DbContext.Layouts.GetList();
            model.Templates = DbContext.Templates.GetList("Target = 'Events'");
            model.PopUps = DbContext.PopUps.GetList();

            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();

            return PartialView("Partials/Properities", model);
        }

        #endregion

        #region Editace

        [Route("/admin/events/edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);

            // nastavení navigace
            Globals.AdminNavigation.Icon = "pe-7s-pen";
            Globals.AdminNavigation.Title = "Editace události";
            Globals.AdminNavigation.SubTitle = page.Name;
            
            if (page == null) return PartialView("NotFound");

            EventViewModel model = new EventViewModel()
            {
                PageId = page.Id,
                ParentId = page.Parent,
                Template = page.Template,
                TemplateItems = page.TemplateItems,
                PopUpId = page.PopUp,
                FolderId = page.Folder,

                Name = page.Name,
                Title = page.Title,
                Keywords = page.Keywords,
                Description = page.Description,
                Url = page.Url,
                HardUrl = page.HardUrl,
                Robots = page.Robots,
                ShowName = page.ShowName,
                Priority = page.Priority,

                ShowInMenu = new List<int>(),
                Events = new List<EventItemViewModel>(),
                Layouts = await DbContext.Layouts.GetListAsync(),
                PopUps = await DbContext.PopUps.GetListAsync(),
                ContentSections = DbContext.ContentSections.GetList("IdPage = " + id).OrderBy(s => s.Order).ToList(),
                Templates = await DbContext.Templates.GetListAsync("Target = 'Events'"),
            };

            var inmenuitems = DbContext.LayoutItems.GetList("Page = " + page.Id);

            model.ShowInMenu = (from i in inmenuitems where i.Page == page.Id select i.Layout).ToList();

            model.EventsCount = DbContext.Pages.SelectCount("Parent = " + id);

            List<ContentModel> events = DbContext.Pages.GetList("Parent = " + id).OrderByDescending(e => e.Created).ToList();

            foreach (ContentModel item in events)
            {
                //ContentModel eventData = DbContext.Pages.FirstOrDefault("IdPage = " + item.Id);
                
                EventItemViewModel _event = new EventItemViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Created = item.Created
                }; 

                model.Events.Add(_event);
            }

            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            if(page.Parent == 0) return PartialView(model);

            return PartialView("EditEvent", model);
        }

        [Authorize(Roles = "events_edit")]
        [Route("/admin/events/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> Edit(EventViewModel model, int id)
        {
            int exist = DbContext.Pages.SelectCount("Name = '" + model.Name + "' AND Id != " + id + " AND Parent = " + 0);

            if (exist != 0) ModelState.AddModelError("Page.Name", "Skupina událostí s tímto názvem již existuje! Zadejte jiný název.");

            //if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            if (ModelState.IsValid)
            {
                ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);

                page.PopUp = model.PopUpId;
                page.Name = model.Name;
                page.Title = model.Title;
                page.Keywords = model.Keywords;
                page.Description = model.Description;
                //page.Url = model.Url;
                page.HardUrl = model.HardUrl;
                page.Robots = model.Robots;

                page.ShowName = model.ShowName;
                page.Modified = DateTime.Now;
                page.Priority = model.Priority;

                if (!page.HardUrl)
                {
                    page.Url = Globals.TextToUrl(page.Name);
                }
                else
                {
                    page.Url = page.Url.Replace("/", "");

                    page.Url = "/" + page.Url;
                }

                DbContext.Pages.Update(page);

                if (model.ShowInMenu == null)
                {
                    DbContext.LayoutItems.Delete("Page = " + page.Id);
                }
                else
                {
                    foreach (int layoutId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItm = DbContext.LayoutItems.FirstOrDefault("Layout = " + layoutId + " AND Page = " + page.Id);

                        if (menuItm == null)
                        {
                            DbContext.LayoutItems.Add(new LayoutItemModel
                            {
                                Layout = layoutId,
                                Page = page.Id,
                                Text = page.Name,
                                Link = page.Url
                            });
                        }
                        else
                        {
                            menuItm.Text = page.Name;
                            menuItm.Link = page.Url;

                            DbContext.LayoutItems.Update(menuItm);
                        }
                    }
                }

                //AdminLogger.LogEvent(User, "Události", AdminLoggerAction.Edit, "Úprava skupiny událostí: " + page.Name);

                return Ok(true);
            }

            model.PageId = id;

            model.Layouts = await DbContext.Layouts.GetListAsync();
            model.PopUps = await DbContext.PopUps.GetListAsync();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp Okna" });

            return PartialView("Partials/Properities", model);
        }

        #endregion

        #region Odstranění

        [Authorize(Roles = "events_delete")]
        [Route("/admin/events/delete/{pageId}")]
        [HttpGet]
        public IActionResult Delete(int pageId)
        {
            string errorMsg = null;

            ContentModel page = DbContext.Pages.SelectSingleById(pageId);

            if (page == null) return Ok("Nelze odstranit neexistující skupinu událostí!");

            int subpages = DbContext.Pages.SelectCount("Parent = " + pageId);

            if (subpages != 0) errorMsg += "Skupinu událostí nelze odstanit, obsahuje udáslosti.\r\n";

            if (!string.IsNullOrEmpty(errorMsg)) return Ok(errorMsg);

            //Odstranění statických souborů
            Managers.StaticFilesManager.DeleteFolder(page.Folder);

            //Odstranění sekcí
            List<ContentSectionModel> sections = DbContext.ContentSections.GetList("IdPage = " + pageId);

            foreach (ContentSectionModel section in sections)
            {
                if (!section.Locked)
                {
                    Globals.AvalibleSections.FirstOrDefault(s => s.Type == section.Type).Delete(section.IdSekce);
                }
            }

            DbContext.ContentSections.Delete("IdPage = " + pageId);
            DbContext.LayoutItems.Delete("Page = " + pageId);

            //Odstranění 
            DbContext.Pages.Delete(pageId);

            if(page.Parent == 0)
            {
                //AdminLogger.LogEvent(User, "Události", AdminLoggerAction.Delete, "Odstranění skupiny událostí: " + page.Name);
            }
            else
            {
                //AdminLogger.LogEvent(User, "Události", AdminLoggerAction.Delete, "Odstranění události: " + page.Name);
            }

            return Ok(true);
        }

        #endregion

        #region Uložení sekcí

        [Authorize(Roles = "events_edit")]
        [Route("/admin/events/sections/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Sections(EventViewModel model, int idPage)
        {
            ContentModel page = DbContext.Pages.SelectSingleById(idPage);

            if (page.Parent == 0)
            {
                page.TemplateItems = model.TemplateItems;
            }
            else
            {
                ContentModel parentPage = DbContext.Pages.SelectSingleById(page.Parent);
                page.Template = parentPage.TemplateItems;
            }

            DbContext.Pages.Update(page);

            foreach (ContentSectionModel contentSection in model.ContentSections)
            {
                contentSection.IdPage = page.Id;

                if (contentSection.Id == 0)
                {
                    if (contentSection.IdSekce == 0)
                    {
                        contentSection.IdSekce = Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Create(page.Folder);
                    }

                    contentSection.Locked = contentSection.Locked;

                    //vytvoreni
                    await DbContext.ContentSections.AddAsync(contentSection);
                }
                else if (contentSection.Order == -1)
                {
                    //odstraneni
                    Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Delete(contentSection.IdSekce);
                    await DbContext.ContentSections.DeleteAsync(contentSection.Id);
                }
                else
                {
                    await DbContext.ContentSections.UpdateAsync(contentSection);
                }
            }

            return Ok(true);
        }

        #endregion

        #region Společné metody

        //Získání seznamu stránek v levém menu 
        private List<LayoutGroupViewModel> getItemsList()
        {
            List<LayoutGroupViewModel> result = new List<LayoutGroupViewModel>();

            List<LayoutModel> layouts = DbContext.Layouts.GetList();

            foreach (LayoutModel layout in layouts)
            {
                LayoutGroupViewModel group = new LayoutGroupViewModel
                {
                    Name = layout.Name,
                    Items = new List<BasicItemViewModel>()
                };

                List<ContentModel> items = DbContext.Pages.GetList("Modul = 'Events' AND Parent = 0 AND Layout = '" + layout.Id + "' AND IsHomepage = 'false'");

                foreach (ContentModel item in items)
                {
                    group.Items.Add(new BasicItemViewModel
                    {
                        Text = item.Name,
                        Url = "/admin/events/edit/" + item.Id,
                    });
                }

                result.Add(group);
            }

            return result;
        }


        //Získání pořadí položky v menu
        private int getMenuOrder(int layoutId)
        {
            LayoutItemModel lastItem = DbContext.LayoutItems.SelectLast("Layout = " + layoutId, "Order");

            if (lastItem == null) return 1;

            return lastItem.Order + 1;

        }

        #endregion
    }
}