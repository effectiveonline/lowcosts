﻿using Data;
using Managers.Models;
using Microsoft.AspNetCore.Mvc;
using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace EffectiveTools.Controllers
{
    [Authorize(Roles = "pages")]
    [Area("Admin")]
    public class PagesController : Controller
    {
        public PagesController()
        {
            Globals.AdminNavigation.Icon = "pe-7s-albums";
            Globals.AdminNavigation.Title = "Stránky a podstránky";
            Globals.AdminNavigation.SubTitle = "Přehled vytvořených stránek";
        }

        #region Přehled stránek a podstránek

        [Route("/admin/pages")]
        [HttpGet]
        public ActionResult Pages()
        {
            return View(getItemsList());
        }

        #endregion

        #region Získání seznamu stránek

        [Route("/admin/pages/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            List<LayoutGroupViewModel> groups = getItemsList();

            if(groups.Count == 1) return PartialView("ItemsList", groups[0].Items);

            return PartialView("GroupsList", groups);
        }

        #endregion

        #region Vytvoření stránky

        [Authorize(Roles = "pages_edit")]
        [Route("admin/pages/create")]
        [Route("admin/pages/create/{parentId}")]
        [HttpGet]
        public async Task<IActionResult> Create(int parentId = 0)
        {
            Globals.AdminNavigation.Icon = "pe-7s-note";
            Globals.AdminNavigation.Title = "Vytvoření nové stránky";
            Globals.AdminNavigation.SubTitle = "Stránky a podstránky";

            if (parentId != 0)
            {
                ContentModel parentPage = await DbContext.Pages.SelectSingleByIdAsync(parentId);

                Globals.AdminNavigation.Title = "Vytvoření podstránky pro " + parentPage.Name;
            }

            LayoutModel layout = DbContext.Layouts.FirstOrDefault(new LayoutModel { IsDefault = true });

            PageViewModel model = new PageViewModel
            {
                Robots = true,
                ShowName = true,
                LayoutId = layout.Id,
                ParentId = parentId,
                Type = Globals.PageTypes.FirstOrDefault().Name,

                Layouts = DbContext.Layouts.GetList(),
                PopUps = await DbContext.PopUps.GetListAsync(),
                ContentSections = new List<ContentSectionModel>(),
                ShowInMenu = new List<int>()
            };

            model.Templates = await DbContext.Templates.GetListAsync("Target = '" + model.Type + "'");

            //model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView(model);
        }

        // načtení sekcí do stránky podle změny typu stránky
        [Route("/admin/pages/types/{name}")]
        [HttpGet]
        public ActionResult Types(string name)
        {
            var model = new PageViewModel
            {
                Type = name,
                ContentSections = new List<ContentSectionModel>()
            };

            var pageType = Globals.PageTypes.FirstOrDefault(t => t.Name == name);

            if (pageType.Sections != null)
            {
                foreach (TemplateSectionModel templateSection in pageType.Sections)
                {
                    model.ContentSections.Add(new ContentSectionModel
                    {
                        IdSekce = templateSection.IdSekce,
                        Name = templateSection.Name,
                        Type = templateSection.Type,
                        Order = templateSection.Order,
                        Locked = templateSection.Locked,
                        Required = templateSection.Required
                    });
                }
            }

            //if(pageType.AllowTemplates)
            //{
                model.Templates = DbContext.Templates.GetList("Target = '" + pageType.Name + "'");
                model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });
            //}

            if(pageType.SubPageType != null)
            {
                model.TemplatesSubPage = DbContext.Templates.GetList("Target = '" + pageType.SubPageType.Name + "'");
                model.TemplatesSubPage.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });
            }

            return PartialView("Partials/Sections", model);
        }

        [Authorize(Roles = "pages_edit")]
        [Route("admin/pages/create/{parentId}")]
        [HttpPost]
        public ActionResult Create(PageViewModel model, int parentId = 0)
        {
            int exist = DbContext.Pages.SelectCount("Name = '" + model.Name + "' AND Parent = " + parentId);

            if (exist != 0) ModelState.AddModelError("Name", "Stránka s tímto názvem již existuje! Zadejte jiný název.");

            if (ModelState.IsValid)
            {
                ContentModel newPage = new ContentModel
                {
                    Modul = "Pages",
                    Parent = parentId,
                    Layout = model.LayoutId,
                    PopUp = model.PopUpId,

                    Name = model.Name,
                    Title = model.Title,
                    Description = model.Description,
                    HardUrl = model.HardUrl,
                    Keywords = model.Keywords,
                    Robots = model.Robots,

                    ShowName = model.ShowName,

                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Priority = model.Priority,

                    Owner = Convert.ToInt32(User.FindFirstValue(ClaimTypes.NameIdentifier))
                };

                if (!newPage.HardUrl)
                {
                    if (newPage.Parent == 0)
                    {
                        newPage.Url = Globals.TextToUrl(newPage.Name);
                    }
                    else
                    {
                        var parentPage = DbContext.Pages.SelectSingleById(newPage.Parent);

                        newPage.Url = parentPage.Url + Globals.TextToUrl(newPage.Name);
                    }
                }
                else
                {
                    newPage.Url = newPage.Url.Replace("/", "");
                    newPage.Url = "/" + newPage.Url;
                }

                //Vytvoření složky pro data stránky
                FolderModel folder = new FolderModel
                {
                    Name = newPage.Name,
                    Type = "Stránky a podstránky"
                };

                Managers.DbContext.Folders.Add(folder);

                newPage.Folder = folder.Id;

                //ulozeni do databaze
                DbContext.Pages.Add(newPage);

                //Uložení menu
                if (model.ShowInMenu != null)
                {
                    foreach (int menuId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItem = new LayoutItemModel
                        {
                            Layout = menuId,
                            Page = newPage.Id,
                            Text = newPage.Name,
                            Link = newPage.Url,
                            Order = getMenuOrder(menuId)
                        };

                        DbContext.LayoutItems.Add(menuItem);
                    }
                }

                return Ok(newPage.Id);
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //při neuspechu

            model.ParentId = parentId;
            model.Layouts = DbContext.Layouts.GetList();
            model.PopUps = DbContext.PopUps.GetList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });

            return PartialView("Partials/Properities", model);
        }

        #endregion

        #region Editace stránky

        [Route("admin/pages/edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);
            // nastavení navigace
            Globals.AdminNavigation.SubTitle = "Editace stránky: " + page.Name;

            if (page == null) return PartialView("NotFound");

            PageViewModel model = new PageViewModel()
            {
                PageId = page.Id,
                PopUpId = page.PopUp,
                FolderId = page.Folder,
                LayoutId = page.Layout,
                Name = page.Name,
                Title = page.Title,
                Keywords = page.Keywords,
                Description = page.Description,
                Url = page.Url,
                HardUrl = page.HardUrl,
                Robots = page.Robots,
                ShowName = page.ShowName,
                Priority = page.Priority,

                PopUps = await DbContext.PopUps.GetListAsync(),
                Layouts = await DbContext.Layouts.GetListAsync(),
                Templates = await DbContext.Templates.GetListAsync("Target = 'Pages'"),

                ContentSections = await DbContext.ContentSections.GetListAsync("IdPage = " + id),
                ShowInMenu = new List<int>()
            };

            List<LayoutItemModel> itemsInMenu = DbContext.LayoutItems.GetList("Page = " + id);

            foreach(LayoutItemModel itemInMenu in itemsInMenu)
            {
                model.ShowInMenu.Add(itemInMenu.Layout);
            }

            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });
            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });

            return PartialView(model);
        }

        [Authorize(Roles = "pages_edit")]
        [Route("admin/pages/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> Edit(PageViewModel model, int id)
        {
            if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            if (ModelState.IsValid)
            {
                ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);

                page.Layout = model.LayoutId;
                page.PopUp = model.PopUpId;
                page.Name = model.Name;
                page.Title = model.Title;
                page.Keywords = model.Keywords;
                page.Description = model.Description;
                page.Url = model.Url;
                page.HardUrl = model.HardUrl;
                page.Robots = model.Robots;

                page.ShowName = model.ShowName;
                page.Modified = DateTime.Now;
                page.Priority = model.Priority;

                if (!page.HardUrl)
                {
                    if (page.Parent == 0)
                    {
                        page.Url = Globals.TextToUrl(page.Name);
                    }
                    else
                    {
                        var parentPage = DbContext.Pages.SelectSingleById(page.Parent);

                        page.Url = parentPage.Url + Globals.TextToUrl(page.Name);
                    }
                }
                else
                {
                    page.Url = page.Url.Replace("/", "");
                    page.Url = "/" + page.Url;
                }

                DbContext.Pages.Update(page);

                if (model.ShowInMenu == null)
                {
                    DbContext.LayoutItems.Delete("Page = " + page.Id);
                }
                else
                {
                    foreach (int layoutId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItm = DbContext.LayoutItems.FirstOrDefault("Layout = " + layoutId + " AND Page = " + page.Id);

                        if (menuItm == null)
                        {
                            DbContext.LayoutItems.Add(new LayoutItemModel
                            {
                                Layout = layoutId,
                                Page = page.Id,
                                Text = page.Name,
                                Link = page.Url
                            });
                        }
                        else
                        {
                            if (!menuItm.CustomText)
                            {
                                menuItm.Text = page.Name;
                            }

                            menuItm.Link = page.Url;

                            DbContext.LayoutItems.Update(menuItm);
                        }
                    }
                }

                repairUrl(page.Id, page.Url);

                foreach (ContentSectionModel contentSection in model.ContentSections)
                {
                    if (contentSection.Id == 0)
                    {
                        contentSection.IdPage = page.Id;
                        contentSection.IdSekce = Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Create(page.Folder);

                        //vytvoreni
                        await DbContext.ContentSections.AddAsync(contentSection);
                    }
                    else if (contentSection.Order == -1)
                    {
                        //odstraneni
                        Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Delete(contentSection.IdSekce);
                        await DbContext.ContentSections.DeleteAsync(contentSection.Id);
                    }
                    else
                    {
                        contentSection.IdPage = page.Id;

                        await DbContext.ContentSections.UpdateAsync(contentSection);
                    }
                }

                //AdminLogger.LogEvent(User, "Stránky a podstránky", AdminLoggerAction.Edit, "Úprava stránky: " + page.Name);

                return Ok(true);
            }

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();

            model.Layouts = await DbContext.Layouts.GetListAsync();
            model.Templates = await DbContext.Templates.GetListAsync("Target = 'Pages'");
            model.PopUps = await DbContext.PopUps.GetListAsync();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });

            return PartialView(model);
        }

        #endregion

        #region Načtení podstránek při editaci

        [Authorize(Roles = "pages_edit")]
        [Route("admin/pages/subitems/{id}")]
        [HttpGet]
        public async Task<IActionResult> Subitems(int id)
        {
            List<BasicItemViewModel> result = new List<BasicItemViewModel>();

            List<ContentModel> items = await DbContext.Pages.GetListAsync("Parent = " + id + " AND IsHomepage = 'false'");

            foreach (ContentModel item in items)
            {
                result.Add(new BasicItemViewModel
                {
                    Text = item.Name,
                    Url = "/admin/pages/edit/" + item.Id,
                    Pages = DbContext.Pages.SelectCount("Parent = " + item.Id)
                }); ;
            }

            return PartialView("SubItemsList", result);
        }

        #endregion

        #region Odstranění stránky

        [Authorize(Roles = "pages_delete")]
        [Route("/admin/pages/delete/{pageId}")]
        [HttpGet]
        public ActionResult Delete(int pageId)
        {
            string errorMsg = null;

            ContentModel page = DbContext.Pages.SelectSingleById(pageId);

            if (page == null) return Ok("Nelze odstranit neexistující stránku!");


            int subpages = DbContext.Pages.SelectCount("Parent = " + pageId);

            if (page.IsHomepage) errorMsg += "Stránku nelze odstanit, je nastavena jako títulní strana.\r\n";
            if (subpages != 0) errorMsg += "Stránku nelze odstanit, Obsahuje podstránky.\r\n";

            if (!string.IsNullOrEmpty(errorMsg)) return Ok(errorMsg);

            //Odstranění statických souborů
            Managers.StaticFilesManager.DeleteFolder(page.Folder);

            //Odstranění sekcí
            List<ContentSectionModel> sections = DbContext.ContentSections.GetList("IdPage = " + pageId);

            foreach (ContentSectionModel section in sections)
            {
                if(!section.Locked)
                {
                    Globals.AvalibleSections.FirstOrDefault(s => s.Type == section.Type).Delete(section.IdSekce);
                }
            }

            DbContext.ContentSections.Delete("IdPage = " + pageId);
            DbContext.LayoutItems.Delete("Page = " + pageId);

            //Odstranění stránky
            DbContext.Pages.Delete(pageId);

            //AdminLogger.LogEvent(User, "Stránky a podstránky", AdminLoggerAction.Delete, "Odstranění stránky: " + page.Name);

            return Ok(true);
        }

        #endregion

        #region Uložení sekcí

        [Authorize(Roles = "pages_edit")]
        [Route("/admin/pages/sections/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Sections(PageViewModel model, int idPage)
        {
            ContentModel page = DbContext.Pages.SelectSingleById(idPage);

            page.Type = model.Type;
            page.Template = model.Template;

            DbContext.Pages.Update(page);

            foreach (ContentSectionModel contentSection in model.ContentSections)
            {
                contentSection.IdPage = page.Id;

                if (contentSection.Id == 0)
                {
                    if (contentSection.IdSekce == 0)
                    {
                        contentSection.IdSekce = Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Create(page.Folder);
                    }

                    //vytvoreni
                    await DbContext.ContentSections.AddAsync(contentSection);
                }
                else if (contentSection.Order == -1)
                {
                    //odstraneni
                    Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Delete(contentSection.IdSekce);
                    await DbContext.ContentSections.DeleteAsync(contentSection.Id);
                }
                else
                {
                    await DbContext.ContentSections.UpdateAsync(contentSection);
                }
            }

            return Ok(true);
        }

        #endregion

        #region Společné metody

        //Získání seznamu stránek v levém menu 
        private List<LayoutGroupViewModel> getItemsList()
        {
            List<LayoutGroupViewModel> result = new List<LayoutGroupViewModel>();

            List<LayoutModel> layouts = DbContext.Layouts.GetList();

            foreach (LayoutModel layout in layouts)
            {
                LayoutGroupViewModel group = new LayoutGroupViewModel
                {
                    Name = layout.Name,
                    Items = new List<BasicItemViewModel>()
                };

                List<ContentModel> items = DbContext.Pages.GetList("Modul = 'Pages' AND Parent = 0 AND Layout = '" + layout.Id + "' AND IsHomepage = 'false'");

                foreach (ContentModel item in items)
                {
                    group.Items.Add(new BasicItemViewModel
                    {
                        Text = item.Name,
                        Url = "/admin/pages/edit/" + item.Id,
                        PageType = item.Type,
                        Pages = DbContext.Pages.SelectCount("Parent = " + item.Id)
                    });
                }

                result.Add(group);
            }

            return result;
        }

        //Stromová oprava url adres při změně url nadřazené stránky
        private void repairUrl(int pageId, string url)
        {
            var subpages = DbContext.Pages.GetList("Parent = " + pageId);

            foreach (ContentModel subpage in subpages)
            {
                if (!subpage.HardUrl)
                {
                    subpage.Url = url+ Globals.TextToUrl(subpage.Name);
                    
                    DbContext.Pages.Update(subpage);

                    repairUrl(subpage.Id, subpage.Url);
                }
            }
        }

        //Získání pořadí položky v menu
        private int getMenuOrder(int layoutId)
        {
            LayoutItemModel lastItem = DbContext.LayoutItems.SelectLast("Layout = " + layoutId, "Order");

            if (lastItem == null) return 1;

            return lastItem.Order + 1;
        }

        #endregion
    }
}
