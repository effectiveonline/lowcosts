﻿using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Managers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EffectiveTools.Areas.Admin.Controllers
{
    [Authorize(Roles = "photogallery")]
    [Area("Admin")]
    public class PhotoGalleryController : Controller
    {
        public PhotoGalleryController()
        {
            Globals.AdminNavigation.Icon = "pe-7s-photo-gallery";
            Globals.AdminNavigation.Title = "Galerie fotografií";
            Globals.AdminNavigation.SubTitle = "Přehled vytvořených fotogalerií";
        }

        [Route("/admin/photogallery")]
        [HttpGet]
        public ActionResult PhotoGallery()
        {
            return View(getItemsList());
        }

        #region Získání seznamu fotogalerií

        [Route("/admin/photogallery/itemslist")]
        [HttpGet]
        public IActionResult ItemsList()
        {
            List<LayoutGroupViewModel> groups = getItemsList();

            if (groups.Count == 1) return PartialView("ItemsList", groups[0].Items);

            return PartialView("GroupsList", groups);
        }

        #endregion

        #region Vytvoření fotogalerie

        [Authorize(Roles = "photogallery_edit")]
        [Route("/admin/photogallery/create")]
        [HttpGet]
        public async Task<ActionResult> Create(int parentId = 0)
        {
            Globals.AdminNavigation.Icon = "pe-7s-note";
            Globals.AdminNavigation.Title = "Vytvoření nové galerie fotografií";
            Globals.AdminNavigation.SubTitle = "Galerie fotografií";

            PageViewModel model = new PageViewModel
            {
                ParentId = parentId,
                Robots = true,
                ShowName = true,

                Layouts = DbContext.Layouts.GetList(),
                PopUps = await DbContext.PopUps.GetListAsync(),
                Templates = await DbContext.Templates.GetListAsync("Target = 'PhotoGallery'"),
                ContentSections = new List<ContentSectionModel>()
            };

            List<TemplateSectionModel> templateSections = new List<TemplateSectionModel>();

            if (model.Templates.Count == 0)
            {
                model.Template = 0;

                model.ContentSections.Add(new ContentSectionModel
                {
                    Name = "Obrázky",
                    Type = "Images",
                    Order = 1,
                    Required = true
                });
            }
            else
            {
                foreach (TemplateSectionModel templateSection in templateSections)
                {
                    model.ContentSections.Add(new ContentSectionModel
                    {
                        IdSekce = templateSection.IdSekce,
                        Name = templateSection.Name,
                        Type = templateSection.Type,
                        Order = templateSection.Order,
                        Locked = templateSection.Locked,
                    });
                }

                model.Template = model.Templates.FirstOrDefault().Id;
                templateSections = DbContext.TemplateSections.GetList("Template = " + model.Template);
            }

            model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();
            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView(model);
        }

        [Authorize(Roles = "photogallery_edit")]
        [Route("/admin/photogallery/create")]
        [HttpPost]
        public ActionResult Create(PageViewModel model)
        {
            int exist = DbContext.Pages.SelectCount("Name = '" + model.Name + "' AND Parent = " + 0);

            if (exist != 0) ModelState.AddModelError("Name", "Stránka s tímto názvem již existuje! Zadejte jiný název.");

            if (ModelState.IsValid)
            {
               
                ContentModel newPage = new ContentModel
                {
                    PopUp = model.PopUpId,
                    Modul = "PhotoGallery",
                    
                    Name = model.Name,
                    Title = model.Title,
                    Description = model.Description,
                    HardUrl = model.HardUrl,
                    Keywords = model.Keywords,
                    Robots = model.Robots,
                    Priority = model.Priority,

                    ShowName = model.ShowName,

                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    
                    Owner = Convert.ToInt32(User.FindFirstValue(ClaimTypes.NameIdentifier))
                };

                if (!newPage.HardUrl)
                {
                    newPage.Url = Globals.TextToUrl(newPage.Name);
                }
                else
                {
                    newPage.Url = "/" + newPage.Url;
                }

                //Vytvoření složky pro data stránky
                FolderModel folder = new FolderModel
                {
                    Name = newPage.Name,
                    Type = "Foto galerie"
                };

                Managers.DbContext.Folders.Add(folder);

                newPage.Folder = folder.Id;

                //ulozeni do databaze
                DbContext.Pages.Add(newPage);

                //vlozeni do menu
                if (model.ShowInMenu != null)
                {
                    foreach (int menuId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItem = new LayoutItemModel
                        {
                            Layout = menuId,
                            Page = newPage.Id,
                            Text = newPage.Name,
                            Link = newPage.Url,
                            Order = getMenuOrder(menuId)
                        };

                        DbContext.LayoutItems.Add(menuItem);
                    }
                }

                //AdminLogger.LogEvent(User, "Galerie fotografií", AdminLoggerAction.Add, "Vytvoření galerie: " + model.Name);

                return Ok(newPage.Id);
            }

            model.Layouts = DbContext.Layouts.GetList();
            model.Templates = DbContext.Templates.GetList("Target = 'PhotoGallery'");
            model.PopUps = DbContext.PopUps.GetList();

            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView("Partials/Properities", model);
        }

        #endregion

        #region Editace galerie

        [Route("/admin/photogallery/edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);

            if (page == null) return PartialView("NotFound");

            PageViewModel model = new PageViewModel()
            {
                PageId = page.Id,
                PopUpId = page.PopUp,
                FolderId = page.Folder,
                Name = page.Name,
                Title = page.Title,
                Keywords = page.Keywords,
                Description = page.Description,
                Url = page.Url,
                HardUrl = page.HardUrl,
                Robots = page.Robots,
                ShowName = page.ShowName,
                Priority = page.Priority,

                ShowInMenu = new List<int>(),
                Layouts = DbContext.Layouts.GetList(),
                PopUps = await DbContext.PopUps.GetListAsync(),
                Templates = await DbContext.Templates.GetListAsync("Target = 'PhotoGallery'"),
                ContentSections = DbContext.ContentSections.GetList("IdPage = " + id).OrderBy(s => s.Order).ToList()
            };

            var inmenuitems = DbContext.LayoutItems.GetList("Page = " + page.Id);

            model.ShowInMenu = (from i in inmenuitems where i.Page == page.Id select i.Layout).ToList();

            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView(model);
        }

        [Authorize(Roles = "photogallery_edit")]
        [Route("/admin/photogallery/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> Edit(PageViewModel model, int id)
        {
            int exist = DbContext.Pages.SelectCount("Name = '" + model.Name + "' AND Id != " + id + " AND Parent = " + 0);

            if (exist != 0) ModelState.AddModelError("Page.Name", "Galerie s tímto názvem již existuje! Zadejte jiný název.");

            if (model.ContentSections == null) model.ContentSections = new List<ContentSectionModel>();

            if (ModelState.IsValid)
            {
                ContentModel page = await DbContext.Pages.SelectSingleByIdAsync(id);

                page.PopUp = model.PopUpId;
                page.Name = model.Name;
                page.Title = model.Title;
                page.Keywords = model.Keywords;
                page.Description = model.Description;
                //page.Url = model.Url;
                page.HardUrl = model.HardUrl;
                page.Robots = model.Robots;

                page.ShowName = model.ShowName;
                page.Modified = DateTime.Now;
                page.Priority = model.Priority;

                if (!page.HardUrl)
                {
                    page.Url = Globals.TextToUrl(page.Name);
                }
                else
                {
                    page.Url = "/" + model.Url;
                }

                DbContext.Pages.Update(page);

                if (model.ShowInMenu == null)
                {
                    DbContext.LayoutItems.Delete("Page = " + page.Id);
                }
                else
                {
                    foreach (int layoutId in model.ShowInMenu)
                    {
                        LayoutItemModel menuItm = DbContext.LayoutItems.FirstOrDefault("Layout = " + layoutId + " AND Page = " + page.Id);

                        if (menuItm == null)
                        {
                            DbContext.LayoutItems.Add(new LayoutItemModel
                            {
                                Layout = layoutId,
                                Page = page.Id,
                                Text = page.Name,
                                Link = page.Url
                            });
                        }
                        else
                        {
                            menuItm.Text = page.Name;
                            menuItm.Link = page.Url;

                            DbContext.LayoutItems.Update(menuItm);
                        }
                    }
                }


                //AdminLogger.LogEvent(User, "Galerie", AdminLoggerAction.Edit, "Úprava galerie: " + page.Name);

                return Ok(true);
            }

            model.PageId = id;

            //model.ContentSections = model.ContentSections.OrderBy(s => s.Order).ToList();

            model.Layouts = DbContext.Layouts.GetList();
            model.PopUps = await DbContext.PopUps.GetListAsync();
            model.Templates = await DbContext.Templates.GetListAsync("Target = 'PhotoGallery'");

            model.PopUps.Add(new PopUpModel { Id = 0, Name = "Bez PopUp Okna" });
            model.Templates.Add(new TemplateModel { Id = 0, Name = "Nepoužívat" });

            return PartialView(model);
        }

        #endregion

        #region Odstranění galerie

        [Authorize(Roles = "photogallery_delete")]
        [Route("/admin/photogallery/delete/{pageId}")]
        [HttpGet]
        public IActionResult Delete(int pageId)
        {
            string errorMsg = null;

            ContentModel page = DbContext.Pages.SelectSingleById(pageId);

            if (page == null) return Ok("Nelze odstranit neexistující galerii!");

            int subpages = DbContext.Pages.SelectCount("Parent = " + pageId);

            //if (subpages != 0) errorMsg += "Galerii nelze odstanit, obsahuje udáslosti.\r\n";

            if (!string.IsNullOrEmpty(errorMsg)) return Ok(errorMsg);

            //Odstranění statických souborů
            Managers.StaticFilesManager.DeleteFolder(page.Folder);

            //Odstranění sekcí
            List<ContentSectionModel> sections = DbContext.ContentSections.GetList("IdPage = " + pageId);

            foreach (ContentSectionModel section in sections)
            {
                if (!section.Locked)
                {
                    Globals.AvalibleSections.FirstOrDefault(s => s.Type == section.Type).Delete(section.IdSekce);
                }
            }

            DbContext.ContentSections.Delete("IdPage = " + pageId);

            //Odstranění skupiny událostí
            DbContext.Pages.Delete(pageId);

            //AdminLogger.LogEvent(User, "Galerie fotografií", AdminLoggerAction.Delete, "Odstranění galerie: " + page.Name);

            return Ok(true);
        }

        #endregion

        #region Uložení sekcí

        [Authorize(Roles = "events_edit")]
        [Route("/admin/photogallery/sections/{idPage}")]
        [HttpPost]
        public async Task<IActionResult> Sections(EventViewModel model, int idPage)
        {
            ContentModel page = DbContext.Pages.SelectSingleById(idPage);

            page.Layout = model.LayoutId;

            DbContext.Pages.Update(page);

            if (page.Parent == 0)
            {
                //page.TemplateItems = model.TemplateItems;
            }

            foreach (ContentSectionModel contentSection in model.ContentSections)
            {
                contentSection.IdPage = page.Id;

                if (contentSection.Id == 0)
                {
                    if (contentSection.IdSekce == 0)
                    {
                        contentSection.IdSekce = Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Create(page.Folder);
                    }

                    //vytvoreni
                    await DbContext.ContentSections.AddAsync(contentSection);
                }
                else if (contentSection.Order == -1)
                {
                    //odstraneni
                    Globals.AvalibleSections.First(s => s.Type == contentSection.Type).Delete(contentSection.IdSekce);
                    await DbContext.ContentSections.DeleteAsync(contentSection.Id);
                }
                else
                {
                    await DbContext.ContentSections.UpdateAsync(contentSection);
                }
            }

            return Ok(true);
        }

        #endregion

        #region Společné metody

        //Získání seznamu stránek v levém menu 
        private List<LayoutGroupViewModel> getItemsList()
        {
            List<LayoutGroupViewModel> result = new List<LayoutGroupViewModel>();

            List<LayoutModel> layouts = DbContext.Layouts.GetList();

            foreach (LayoutModel layout in layouts)
            {
                LayoutGroupViewModel group = new LayoutGroupViewModel
                {
                    Name = layout.Name,
                    Items = new List<BasicItemViewModel>()
                };

                List<ContentModel> items = DbContext.Pages.GetList("Modul = 'PhotoGallery' AND Parent = 0 AND Layout = '" + layout.Id + "' AND IsHomepage = 'false'");

                foreach (ContentModel item in items)
                {
                    group.Items.Add(new BasicItemViewModel
                    {
                        Text = item.Name,
                        Url = "/admin/photogallery/edit/" + item.Id,
                        Pages = DbContext.Pages.SelectCount("Parent = " + item.Id)
                    });
                }

                result.Add(group);
            }

            return result;
        }

        //Získání pořadí položky v menu
        private int getMenuOrder(int layoutId)
        {
            LayoutItemModel lastItem = DbContext.LayoutItems.SelectLast("Layout = " + layoutId, "Order");

            if (lastItem == null) return 1;

            return lastItem.Order + 1;

        }

        #endregion
    }
}
