﻿using EffectiveTools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class PageSectionsViewModel
    {
        public int PageId { get; set; }
        public List<ContentSectionModel> Sections { get; set; }
    }
}
