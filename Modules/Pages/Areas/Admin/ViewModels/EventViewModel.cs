﻿using EffectiveTools.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class EventViewModel : PageViewModel
    {
        [Display(Name = "Šablona detailu události")]
        public int TemplateItems { get; set; }

        public List<EventItemViewModel> Events { get; set; }
        public int EventsCount { get; set; }
    }
}
