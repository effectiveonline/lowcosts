﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EffectiveTools.ViewModels
{
    public class DashboardViewModel
    {
        public int UnreadedMessages { get; set; }
        public Data.WebMailModel LastMessage { get; set; }

        public DashboardViewModel()
        {
            UnreadedMessages = DbContext.WebMails.SelectCount("Readed = 'False'");
            LastMessage = DbContext.WebMails.SelectLast("Readed = 'False'", "Created");
        }
    }
}
