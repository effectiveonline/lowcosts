﻿using Data;
using EffectiveTools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EffectiveTools.ViewModels
{
    public class PageViewModel
    {
        public int PageId { get; set; }
        public int ParentId { get; set; }

        [Display(Name = "PopUp okno stránky")]
        public int PopUpId { get; set; }

        public int FolderId { get; set; }

        [Display(Name = "Rozložení stránek")]
        public int LayoutId { get; set; }

        [Display(Name = "Šablona obsahu stránky")]
        public int Template { get; set; }

        [Display(Name = "Šablona obsahu podstránek")]
        public int TemplateSubPage { get; set; }

        //[Required(ErrorMessage = "Zadejte typ stránky.")]
        [Display(Name = "Typ stránky")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Zadejte název stránky.")]
        [Display(Name = "Název a nadpis stránky")]
        public string Name { get; set; }

        [Display(Name = "Pevná URL adresa stránky")]
        public string Url { get; set; }

        [Required(ErrorMessage = "Zadejte titulek stránky.")]
        [Display(Name = "Titulek stránky")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Zadejte Klíčová slova.")]
        [Display(Name = "Klíčová slova")]
        public string Keywords { get; set; }

        [Required(ErrorMessage = "Zadejte popisek stránky.")]
        [Display(Name = "Popisek stránky")]
        public string Description { get; set; }

        [Display(Name = "Povolit přístup robotům")]
        public bool Robots { get; set; }

        [Display(Name = "Pevná URL adresa stránky")]
        public bool HardUrl { get; set; }

        [Display(Name = "Zobrazit nadpis")]
        public bool ShowName { get; set; }

        [Display(Name = "Priorita stránky")]
        public int Priority { get; set; } = 5;

        public DateTime Created { get; set; }

        public List<LayoutModel> Layouts { get; set; }

        [Display(Name = "Zobrazit v menu")]
        public List<int> ShowInMenu { get; set; }

        public List<PopUpModel> PopUps { get; set; }

        public List<ContentSectionModel> ContentSections { get; set; }

        public List<TemplateModel> Templates { get; set; }

        public List<TemplateModel> TemplatesSubPage { get; set; }
    }
}
