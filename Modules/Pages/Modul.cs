﻿using Data;
using EffectiveTools.Models;
using EffectiveTools.ViewModels;
using Interface;
using System;
using System.Collections.Generic;

namespace EffectiveTools
{
    public class Modul : IModul
    {
        IModulHost myHost = null;

        public Modul()
        {
            DbContext.Inicialize();

            Globals.PageTypes.Add(new PageType
            {
                Name = "Page",
                Text = "Základní stránka",
                AllowTemplates = true
            });

            Globals.PageTypes.Add(new PageType
            {
                Name = "PhotoGallery",
                Text = "Fotogalerie",
                Sections = new List<TemplateSectionModel>
                {
                    new TemplateSectionModel
                    {
                        Name = "Obrázky",
                        Required = true,
                        Order = 1,
                        Type = "Images"
                    }
                }
            });

            Globals.PageTypes.Add(new PageType
            {
                Name = "Events",
                Text = "Události",
                Sections = new List<TemplateSectionModel>
                {
                    new TemplateSectionModel
                    {
                        Name = "Události",
                        Required = true,
                        Locked = true,
                        Order = 1,
                        Type = "Events"
                    }
                },
                SubPageType = new PageType
                {
                    Name = "Event",
                    Text = "Detail události",
                    Sections  = new List<TemplateSectionModel>
                    {
                        new TemplateSectionModel
                        {
                            Name = "Obsah události",
                            Type = "Text",
                            Required = true,
                            Order = 1,
                        }
                    }
                }
            });
        }

        public IModulHost Host
        {
            get { return myHost; }
            set { myHost = value; }
        }

        public string Name
        {
            get { return "Pages"; }
        }

        public string Title
        {
            get { return "Stránky a podstránky"; }
        }

        public Data.AdminMenuItem AdminMenuItem
        {
            get
            {
                return new Data.AdminMenuItem
                {
                    Text = "Webové stránky",
                    Order = 1,
                    Items = new List<Data.AdminMenuItem>
                    {
                        new Data.AdminMenuItem
                        {
                            Text = "Titulní strana",
                            Link = "/admin/homepage"
                        },
                        new Data.AdminMenuItem
                        {
                            Text = "Stránky a podstránky",
                            Link = "/admin/pages"
                        },
                        new Data.AdminMenuItem
                        {
                            Text = "Události",
                            Link = "/admin/events"
                        },
                        new Data.AdminMenuItem
                        {
                            Text = "Fotogalerie",
                            Link = "/admin/photogallery"
                        },
                        new Data.AdminMenuItem
                        {
                            Text = "Nastavení",
                            Link = "websetting",
                            Items = new List<Data.AdminMenuItem>
                            {
                                new Data.AdminMenuItem
                                {
                                    Text = "Menu",
                                    Link = "/admin/pages/settings/menu"
                                },
                                new Data.AdminMenuItem
                                {
                                    Text = "Patička",
                                    Link = "/admin/pages/settings/footer"
                                },
                                new Data.AdminMenuItem
                                {
                                    Text = "Pop-Up okno",
                                    Link = "/admin/pages/settings/popup"
                                },
                                new AdminMenuItem
                                {
                                    Text = "Rozložení",
                                    Link = "/admin/layouts"
                                },
                                new AdminMenuItem
                                {
                                    Text = "Šablony obsahu",
                                    Link = "/admin/templates"
                                }
                            }
                        }
                    }
                };


            }
        }

        public HelpMenuItem Help
        {
            get
            {
                return new HelpMenuItem
                {
                    Text = "Stránky a podstránky",
                    Modul = Name,
                    View = "index",
                    Order = 1,
                    Items = new List<HelpMenuItem>
                    {
                        new HelpMenuItem
                        {
                            Text = "Vytvoření stránky",
                            Modul = Name,
                            View = "create",
                            NavigatinTitle = "Jak vytvořit novou stránku a postránku?"
                        },
                        new HelpMenuItem
                        {
                            Text = "Úprava stránky",
                            Modul = Name,
                            View = "edit",
                            NavigatinTitle = "Jak upravit stránku a postránku?"
                        },
                        new HelpMenuItem
                        {
                            Text = "Pevná URL adresa",
                            Modul = Name,
                            View = "hardurl",
                            NavigatinTitle = "Jak sprývně zadat URL adresu stránky?"
                        },
                        new HelpMenuItem
                        {
                            Text = "Sekce obsahu",
                            Modul = Name,
                            View = "Sections",
                            NavigatinTitle = "Sekce obsahu stránky?",
                            Items = new List<HelpMenuItem>
                            {
                                new HelpMenuItem
                                {
                                    Text = "Carousel",
                                    Modul = "Sections",
                                    View = "carousel",
                                    NavigatinTitle = "Carousel",
                                },
                                new HelpMenuItem
                                {
                                    Text = "Výpis událostí",
                                    Modul = "Sections",
                                    View = "events",
                                    NavigatinTitle = "Výpis událostí",
                                },
                                new HelpMenuItem
                                {
                                    Text = "Formuláře",
                                    Modul = "Sections",
                                    View = "forms",
                                    NavigatinTitle = "Formuláře",
                                },
                                new HelpMenuItem
                                {
                                    Text = "Obrázky",
                                    Modul = "Sections",
                                    View = "images",
                                    NavigatinTitle = "Obrázky",
                                },
                                new HelpMenuItem
                                {
                                    Text = "Paralax",
                                    Modul = "Sections",
                                    View = "paralax",
                                    NavigatinTitle = "Paralax",
                                },
                                new HelpMenuItem
                                {
                                    Text = "Vlastní text",
                                    Modul = "Sections",
                                    View = "text",
                                    NavigatinTitle = "Vlastní text",
                                },
                            }
                        }
                        
                    }
                };
            }
        }
        public List<SelectBoxItem> FormTypes
        {
           get
            {
                return new List<SelectBoxItem>
                {
                    new SelectBoxItem
                    {
                        Text = "Kontaktní zpráva",
                        Value = "Message"
                    },
                    new SelectBoxItem
                    {
                        Text = "Zavolejte mi zpět",
                        Value = "CallMyBack"
                    },
                    new SelectBoxItem
                    {
                        Text = "Registrace pro příjem novinek",
                        Value = "RegNews"
                    }
                };
            }
        }

        public Object DashBoardModel
        {
            get
            {
                return new EffectiveTools.ViewModels.DashboardViewModel();
            }
        }

        public void Initialize() { }

        public void Dispose() { }
    }
}

/*
  <ItemGroup>
    <Folder Include="Controllers\" />
    <Folder Include="Models\" />
    <Folder Include="Views\" />
  </ItemGroup>
*/