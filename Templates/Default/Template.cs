﻿using Interface;

namespace Default
{
    public class Template : ITemplate
    {
        ITemplateHost myHost = null;

        public Template()
        {
            Globals.AvalibleTemplates.Add("default");
        }

        public string Name
        {
            get { return "default"; }
        }

        public ITemplateHost Host
        {
            get { return myHost; }
            set { myHost = value; }
        }

        public void Initialize() { }

        public void Dispose() { }
    }
}
