#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "37dc9d05c7e5d95f6dbaab77b63bc77746838029"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Web_Views_Web_Sections_Forms), @"mvc.1.0.view", @"/Areas/Web/Views/Web/Sections/Forms.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Data.WebViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.WebModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"37dc9d05c7e5d95f6dbaab77b63bc77746838029", @"/Areas/Web/Views/Web/Sections/Forms.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"13d852cde1aae953f43957cb31162c641d2f03c1", @"/Areas/Web/Views/_ViewImports.cshtml")]
    public class Areas_Web_Views_Web_Sections_Forms : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<FormsWebModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"container\">\r\n    <main role=\"main\" class=\"pb-3\">\r\n");
#nullable restore
#line 5 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml"
         if (Model.ShowAs == "Pages")
        {
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml"
       Write(await Html.PartialAsync("sections/Forms/" + Model.FormType, Model));

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml"
                                                                               
        }
        else
        {
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml"
       Write(await Html.PartialAsync("sections/Forms/Dinamic", Model));

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Web\Sections\Forms.cshtml"
                                                                     
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </main>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FormsWebModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
