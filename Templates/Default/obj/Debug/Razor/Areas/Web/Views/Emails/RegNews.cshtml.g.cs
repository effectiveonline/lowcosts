#pragma checksum "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1dcd203cdb2bb7ff61bc15f7c41ad5aa7557d9e6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Web_Views_Emails_RegNews), @"mvc.1.0.view", @"/Areas/Web/Views/Emails/RegNews.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Data.WebViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.WebModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\_ViewImports.cshtml"
using Areas.Sections.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1dcd203cdb2bb7ff61bc15f7c41ad5aa7557d9e6", @"/Areas/Web/Views/Emails/RegNews.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"13d852cde1aae953f43957cb31162c641d2f03c1", @"/Areas/Web/Views/_ViewImports.cshtml")]
    public class Areas_Web_Views_Emails_RegNews : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Data.WebNewModel>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("<html lang=\"cs\">\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1dcd203cdb2bb7ff61bc15f7c41ad5aa7557d9e63889", async() => {
                WriteLiteral("\r\n    <h1>Přihlášení k odběru novinek na ");
#nullable restore
#line 7 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
                                  Write(Globals.WebConfig.Domain);

#line default
#line hidden
#nullable disable
                WriteLiteral("</h1>\r\n    <p>Na základě Vašeho požadavku jsme Váš e-mail ");
#nullable restore
#line 8 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
                                              Write(Model.Email);

#line default
#line hidden
#nullable disable
                WriteLiteral(" přidali do seznamu příjemců novinek.</p>\r\n    <p>Přihlášení můžete kdykoliv odhlásit kliknutím na níže uvedený odkaz.</p>\r\n    <p><a");
                BeginWriteAttribute("href", " href=\"", 345, "\"", 406, 3);
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
WriteAttributeValue("", 352, Globals.WebConfig.Domain, 352, 27, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 379, "/uregisternews/", 379, 15, true);
#nullable restore
#line 10 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
WriteAttributeValue("", 394, Model.Token, 394, 12, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">Zrušit odběr novinek.</a></p>\r\n    <p>S pozdravem:</p>\r\n    <p>\r\n        ");
#nullable restore
#line 13 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
   Write(Globals.Company.CompanyName);

#line default
#line hidden
#nullable disable
                WriteLiteral("<br />\r\n        ");
#nullable restore
#line 14 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
   Write(Globals.Company.Street);

#line default
#line hidden
#nullable disable
                WriteLiteral("<br />\r\n        ");
#nullable restore
#line 15 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
   Write(Globals.Company.City);

#line default
#line hidden
#nullable disable
                WriteLiteral(", ");
#nullable restore
#line 15 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
                          Write(Globals.Company.PosCode);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </p>\r\n    <p>\r\n        ");
#nullable restore
#line 18 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
   Write(Globals.Company.Phone);

#line default
#line hidden
#nullable disable
                WriteLiteral("<br />\r\n        ");
#nullable restore
#line 19 "D:\Visual Studio\Effective Online\EffectiveTools\Templates\Default\Areas\Web\Views\Emails\RegNews.cshtml"
   Write(Globals.WebConfig.SmtpEmail);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </p>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Data.WebNewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
